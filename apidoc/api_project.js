define({
  "name": "IPCA Patient Recruitment App API",
  "version": "1.0.0",
  "description": "API Documentation",
  "title": "IPCA Patient Recruitment App API",
  "url": "https://uat-ipca.techizertech.in",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-04-09T13:40:34.820Z",
    "url": "https://apidocjs.com",
    "version": "0.27.1"
  }
});
