<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Prescription extends User_Controller
{
	private $module = 'prescription';
    private $model_name = 'mdl_prescription';
    private $controller = 'prescription';
    private $settings = [
        'permissions'=> ['add','download'],
        'pagination_index' => 3
    ];
    private $scripts = ['prescription.js','division.js','sales_performance.js'];

   
	function __construct() {
         parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }

     function uploadcsv(){
        $this->session->is_Ajax_and_admin_logged_in();
        /*upload csv file */

        if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
        }

        if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
        }

        $file = $_FILES['csvfile']['tmp_name'];
        $handle = fopen($file, "r");
        $cnt = 0; $newrows = 0;

        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

            if(count($data) !== 4) { continue; }

            if(! $cnt){
                $cnt++; continue;
            }
            
            $doctor_name = trim($data[0]);
            $doctor_mobile = trim($data[1]);
            $speciality = trim($data[2]);
            $users_mobile = trim($data[3]);
           
            if(empty($doctor_name) || empty($doctor_mobile) || empty($users_mobile) || empty($speciality)){
                continue;
            }

            if( 
                ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $doctor_name)
                || ! preg_match('/^[1-9][0-9]{9}$/', $doctor_mobile) 
                || ! preg_match('/^[1-9][0-9]{9}$/', $users_mobile)
                || ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $speciality) 
            ){
                continue;
            }
            
            $DrInfo = $this->model->get_records(['doctor_mobile'=> $doctor_mobile], 'doctor', ['doctor_id'], '', 1);
            if(count($DrInfo)) {
                continue;
            }

            $MRInfo = $this->model->get_records(['users_mobile'=> $users_mobile], 'manpower', ['users_id'], '', 1);
            if(!count($MRInfo)) {
                continue;
            }
            
            $users_id = $MRInfo[0]->users_id;

            #check for speciality
            $SpecialityInfo = $this->model->get_records(['speciality'=> $speciality], 'speciality', ['speciality_id'], '', 1);
            if(!count($SpecialityInfo)) {
                continue;
            }
            
            $speciality_id = $SpecialityInfo[0]->speciality_id;            

            $insert['users_id'] = $users_id;
            $insert['doctor_name'] = $doctor_name;
            $insert['doctor_mobile'] = $doctor_mobile;
            $insert['speciality_id'] = $speciality_id;
          

            $this->model->_insert($insert);

            $newrows++;
        }

        fclose($handle);

        echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
    }

    function get_doctor(){
        $this->session->is_Ajax_and_logged_in();
        $limit = $this->dropdownlength;
        $page = 0;
        $page = ($page <= 0) ? 0 : $page;

        $s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
        $id = (isset($_POST['id'])) ? (int) $this->input->post('id') : 0;
        $userId = $this->session->get_field_from_session('user_id','user');
        $filters['users_id'] = $userId;
        //  $filters = array();
        $new = array(); $json['results'] = array();

        $_options = $this->model->get_options($s_term, 'doctor_name', $filters, $page * $limit, $limit,[],'doctor');
        
        $_opt_count = count($this->model->get_options($s_term, 'doctor_name', $filters,$page * $limit, $limit,[],'doctor'));
  
        foreach($_options as $option){
         
            $new['id'] = $option->doctor_id;
            $new['text'] = trim($option->doctor_name);
            $new['sub_code'] = $option->sbu_code;
            $new['speciality_id'] = $option->speciality_id;
            array_push($json['results'], $new);
        }

        $more = ($_opt_count > count($_options)) ? TRUE : FALSE;
        $json['pagination']['more'] = $more;

        echo json_encode($json);
    }

    function get_doctor_data(){
        $doctor_id = $this->input->post('doctor_id');
        $dcotorinfo = $this->model->get_records(['doctor_id' => $doctor_id], 'doctor',['doctor_id','sbu_code']);
        // echo $this->db->last_query();exit;
       // $doc_name = array_column($dcotorRecords, 'doctor_name');
        echo json_encode($dcotorinfo);
    }

    function getspecialiy(){
        $speciality_id = $this->input->post('speciality_id');
        $specialityinfo = $this->model->get_records(['speciality_id' => $speciality_id], 'speciality',['speciality_id','speciality']);
        // echo $this->db->last_query();exit;
       // $doc_name = array_column($dcotorRecords, 'doctor_name');
        echo json_encode($specialityinfo);  
    }
}
