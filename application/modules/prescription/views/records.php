<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['presc_id']; ?>
    <tr>
    <td>
        <!-- <input type="checkbox" name="ids[]" value="<?php echo $id ?>" id="check_<?= $id ?>" class="form-check-input form-check-{color}" /> -->
        <!-- <label for="check_<?= $id ?>"></label> -->
    </td>
    
    <td><?php echo $record['doctor_name'] ?></td>
    <td><?php echo $record['sbu_code'] ?></td>
    <td><?php echo $record['speciality'] ?></td>
    <td><?php echo $record['rxn_count'] ?></td>
    <td>
   
        <?php if(!empty($record['rxn_path']) ) {
              $temp_img = explode(",", $record['rxn_path']);
              foreach ($temp_img as $key => $rxn_path) { 
                if($rxn_path) { ?>
             <a target="_blank" href="<?php echo base_url().$rxn_path; ?>" style="" data-fancybox="images" data-fancybox >
                <img src="<?php echo base_url().$rxn_path; ?>" alt="" style="width:30px;height:30px;border-radius: 0%;">
            </a>
        <?php } }}?>
    </td>
    <td><?php echo $record['insert_dt'] ?></td>
   
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>">
        <div class="row">
            <?php echo $this->ajax_pagination->create_links(); ?>    
        </div>
    </td>
</tr>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<link href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" rel="stylesheet">

<script src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
      
        $('[data-fancybox="images"]').fancybox({
  buttons : [
    'download',
    'thumbs',
    'close'
  ]
});
    </script>