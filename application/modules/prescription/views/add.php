<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>


<style type="text/css">
#doctor_id {
    display: none;
}

.list-gpfrm-list a {

    text-decoration: none !important;

}

.list-gpfrm li {

    cursor: pointer;

    padding: 4px 0px;

}

.list-gpfrm {

    list-style-type: none;

    background: #d4e8d7;

}

.list-gpfrm li:hover {

    color: white;

    background-color: #3d3d3d;

}

ul#doctor_id {
    position: absolute;
    z-index: 99999;
    width: 100%;
}
</style>

<div class="form-group row">
    <label for="title" class="col-sm-12 col-form-label">Doctor Name <span class="text-danger">*</span></label>
    <div class="col-sm-12">

        <div class="autocomplete" style="width:100%">
            <input id="doctor_name" class="form-control" type="text" name="doctor_name" placeholder="Doctor Name"
                autocomplete="off">
        </div>
    </div>

</div>

 <div class="form-group row">
    <!-- <label for="" class="col-sm-12 col-form-label"></label> -->
    <div class="col-sm-12">
        
            <ul class="list-gpfrm" id="doctor_id"></ul>
            <!-- <select class="form-control list-gpfrm" id="doctor_id" name="doctor_id">

            </select> -->
       
    </div>
</div> 

<div class="form-group row">
    <label for="" class="col-sm-12 col-form-label">SBU Code <span class="text-danger">*</span></label>
    <div class="col-sm-12">
        <div class="">
            <input type="text" class="form-control" id="sbu_code" name="sbu_code" placeholder="SBU Code">
        </div>
    </div>
</div>
<div class="form-group row">
    <label for="speciality" class="col-sm-12 col-form-label">Speciality <span class="text-danger">*</span></label>
    <div class="col-sm-12">
        <div class="">
            <select class="form-control form-control-sm js-example-basic-single" id="speciality_id" name="speciality_id"
                data-placeholder="Select Speciality">
                <option value="">Select Speciality</option>
            </select>
        </div>
    </div>
</div>
<div class="form-group row">
    <label for="rxn_count" class="col-sm-12 col-form-label">Total No. Of Prescription<span
            class="text-danger">*</span></label>
    <div class="col-sm-12">
        <div class="">
            <input type="text" class="form-control" id="rxn_count" name="rxn_count" placeholder="RXN">
        </div>
    </div>
</div>
<div class="form-group row">
    <label for="rxn" class="col-sm-12 col-form-label">Image Upload<span class="text-danger">*</span> (Max Upload 3
        Images)</label>
    <div class="col-sm-12" id="">
        <input type="file" id="rxn" name="rxn[]" accept="image/png, image/jpeg" multiple width="100%"
            placeholder="Choose file...">

        <p>Support format: png, jpeg, jpg</p>
        <input type="hidden" name="field_rxn" id="field_rxn" value="" />
    </div>

</div>




<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
    Cancel
</a>
<?php echo form_close(); ?>

<script>

</script>