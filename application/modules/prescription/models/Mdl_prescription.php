<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_prescription extends MY_Model {

    private $p_key = 'presc_id';
    private $table = 'prescription';
    private $tb_alias = 'pre';
    private $fillable = ['doctor_name', 'sbu_code','speciality_id','rxn_count','upload_path'];
    private $column_list = ['Doctor Name','SBU Code','Speciality','Total No. Of Prescription','Display Image','DateTime'];
    private $csv_columns =[];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'Doctor Name',
            ],
            [
                'field_name'=>'d|sbu_code',
                'field_label'=> 'Doctor SBU Code',
            ],
            [
                'field_name'=>'s|speciality',
                'field_label'=> 'Speciality',
            ]
    

        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $user_id =(int) $this->security->xss_clean($this->session->get_field_from_session('user_id', 'user'));

        $role = $this->security->xss_clean($this->session->get_field_from_session('role', 'user'));

        $q = $this->db->select('pre.presc_id,pre.doctor_id,pre.rxn_count,pre.rxn_path,pre.insert_dt,d.doctor_name,d.sbu_code,s.speciality')
        ->from('prescription pre')
        ->join('doctor d','d.doctor_id = pre.doctor_id')
        // ->join('ho_connect_fields cfield', 'cfield.ho_conn_id = conn.ho_conn_id');
        // ->join('city c', 'm.users_city_id = c.city_id')
        // ->join('area a', 'a.area_id = c.area_id')
        // ->join('region r', 'r.region_id = a.region_id')
        // ->join('zone z', 'z.zone_id = r.zone_id')
        ->join('speciality s', 's.speciality_id = d.speciality_id')
        ;

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(pre.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(pre.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }
       
        if(! $count) {
            $q->order_by('pre.update_dt desc, pre.presc_id desc');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
    //   echo '<pre>';
    //   print_r($this->db->last_query());die;
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
                [
					'field' => 'doctor_name',
					'label' => 'Doctor Name',
                    'rules' => 'trim|required|valid_name|max_length[150]|xss_clean'
                ],
                [
					'field' => 'sbu_code',
					'label' => 'Title',
					'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'speciality_id',
                    'label' => 'Doctor speciality',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
					'field' => 'rxn_count',
					'label' => 'RXN',
					'rules' => 'trim|numeric|required|xss_clean'
                ],

            ];
        }

        if($type == 'modify') {
            return [
                
                [
					'field' => 'document_type_id',
					'label' => 'Document Type',
					'rules' => 'trim|required|xss_clean'
                ]
                ,
                [
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|max_length[150]|xss_clean'
                ],
                [
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|xss_clean'
                ],
                [
					'field' => 'status',
					'label' => 'status',
					'rules' => 'trim|required|xss_clean'
                ],
            ];
        }
    }

    function save(){

        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->load->helper('upload_media');
        $this->form_validation->set_rules($this->validate('save'));

        if(!$this->form_validation->run()) {
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');
            
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }
        #insert doctor
        $doctorInsert['doctor_name'] = $doctor_name = $this->input->post('doctor_name');
        $doctorInsert['sbu_code'] =  $sbu_code = $this->input->post('sbu_code');
        $doctorInsert['speciality_id'] =  $speciality_id = $this->input->post('speciality_id');
        $doctorInsert['users_id'] = $users_id =  $this->session->get_field_from_session('user_id','user');
       
        if(!isset($_FILES['rxn']['name'][0]) || empty($_FILES['rxn']['name'][0])) {
 
            $fild_errors['status'] = FALSE;
			$fild_errors['errors'] = ['field_rxn' => '<label class="error">Files Required.</label>'];
			return $fild_errors;
		}

        if(count($_FILES['rxn']['name']) < 0 || count($_FILES['rxn']['name']) > 3) {
			$upload_errors['status'] = FALSE;
			$upload_errors['errors'] = ['rxn[]' => '<label class="required">Rxn Files Required. Maximum 3 Images Allowed.</label>'];
			return $upload_errors;
		}

       

        $doctorInfo = $this->model->get_records(['sbu_code'=>$sbu_code],'doctor');

        if(count($doctorInfo)){
           $doctor_id = $doctorInfo[0]->doctor_id;
        }else{
            $doctor_id = $this->_insert($doctorInsert,'doctor');
        }


        $upload_rxn_data = $errors = array();
        $uploadRxnPath = $s3uploadRxnPath = "uploads/rxn/$doctor_id/";

        if(count($_FILES["rxn"]['name']) > 0) {
            $file_rxn_status = $this->upload_file_response("rxn", $uploadRxnPath, $s3uploadRxnPath, ['jpg','png','jpeg','pdf','JPG','PNG','JPEG']);
            
            
            if($file_rxn_status['status']){
                $file_rxn_status = is_array($file_rxn_status['data']) ? $file_rxn_status['data'] : (array)$file_rxn_status['data'];
                for($i=0;$i < count($file_rxn_status); ++$i){
                    array_push($upload_rxn_data, $file_rxn_status[$i]) ;
                }
            } else {
                array_push($errors, $file_rxn_status['error']);
            }
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
        
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $prescriptionInsert = array();
        $prescriptionInsert['users_id'] = $users_id;
        $prescriptionInsert['doctor_id'] = $doctor_id;
        $prescriptionInsert['rxn_count'] =  $rxn_count = $this->input->post('rxn_count');
		$prescriptionInsert['rxn_path'] = !empty($upload_rxn_data) ? implode(',', $upload_rxn_data) : NULL;
        
        $presc_id = $this->_insert($prescriptionInsert, 'prescription');

        if(! $presc_id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
    }
    
    function modify(){
        // print_r($_FILES);exit;
        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->load->helper('upload_media');

        $is_Available = $this->check_for_posted_record($this->p_key, $this->table);
        if(! $is_Available['status']){ return $is_Available; }
        
        $this->form_validation->set_rules($this->validate('modify'));

        if(! $this->form_validation->run()){
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');

            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }   

        $document_type_id = $this->input->post('document_type_id');
        $fields_perm = $this->input->post('permission');

        if(!isset($_POST['permission']) || empty($_POST['permission'])) {
            $fild_errors['status'] = FALSE;
            $fild_errors['errors'] = ['field_permission' => '<label class="error">Please select Send Notification Fields .</label>'];
            return $fild_errors;
        }
        $p_key = $this->p_key;
        $id  = (int) $this->input->post($p_key);
        if(isset($_FILES['upload_path']['size']) && !empty($_FILES['upload_path']['size'])) {
        #get document type
		$documentTypeInfo = $this->get_records(['document_type_id' => $document_type_id], 'document_type', ['document_type']);

		$document_type = $documentTypeInfo[0]->document_type;

        $path = $_FILES['upload_path']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

        $file_icon = '';
		$duration = '0';
		if(strtolower($document_type) == 'audio') {
			if(!in_array($ext,['wav','aif','mp3','mid','ogg','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload audio files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/audio.png";
		} else if(strtolower($document_type) == 'video') {
			if(!in_array($ext,['mp4','mov','wmv','avi','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload video files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/video.png";
		} else if(strtolower($document_type) == 'ppt') {
			if(!in_array($ext,['ppt','pptx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload ppt files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'pdf') {
			if(!in_array($ext,['pdf'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/pdf_file.png";
		}else if(strtolower($document_type) == 'spredsheet' || strtolower($document_type) == 'excel') {
			if(!in_array($ext,['xls','xlsx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'document' || strtolower($document_type) == 'doc') {
			if(!in_array($ext,['doc','docx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload doc files Only.</label>'];
				return $upload_errors;
			}
		} else {
			if(!in_array($ext,['png', 'jpeg', 'jpg','PNG','JPEG','JPG'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload image files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/images_file.png";
		}

        $upload_filename = $this->UploadFile($document_type_id);
       
        $inserted_ids = [];
		if(!empty($upload_filename)) {
			
			$upload_result['document_type_id']	=	$document_type_id;
			$upload_result['upload_path'] = $upload_filename;
			$upload_result['display_image'] = $file_icon;

			$id = $this->_update([$p_key => $id],$upload_result,'ho_connect');

          }
        }
            
        $update_result['title']	=	$this->input->post('title');
		$update_result['description']	=	$this->input->post('description');
        $update_result['status'] = $this->input->post('status');

         
         $status = (int) $this->_update([$p_key => $id], $update_result);

         

         $this->_delete('ho_conn_id',['ho_conn_id' => $id], 'ho_connect_fields');

         for($ic=0;$ic<count($_POST['permission']);$ic++){ 
              
           

            $fpermission = $_POST['permission'][$ic];

            $ho_connect_fields['ho_conn_id'] =  $id;
            $ho_connect_fields['field_permission'] =  $fpermission;
            $this->_insert($ho_connect_fields, 'ho_connect_fields');

        }
        if(! $status){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
    }

    function remove(){
        
        if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
            $ids = $this->input->post('ids');
            $response = $this->_delete($this->p_key, $ids, $this->table);

            $msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
            return ['msg'=> $msg];
        }

        return ['msg'=> 'No Records Selected'];
    }

    
    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
            $rxn_path_url = '';
			$temp_rxn_path = explode(",", $rows['rxn_path']);
	        foreach ($temp_rxn_path as $key => $rxn_path) { 
				if(!empty($rxn_path)){
					$rxn_path_url .= base_url().$rxn_path.',';
				}	
			}
            //  $records['MR Name'] = $rows['users_name'];
             $records['Doctor Name'] = $rows['doctor_name'];
             $records['Doctor SBU Code'] = $rows['sbu_code'];
             $records['Speciality'] = $rows['speciality'];
             $records['Total No. Of Prescription'] = $rows['rxn_count'];
             $records['Rxn Images Paths'] = $rxn_path_url;
            $records['DateTime'] = $rows['insert_dt'];
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }

    function UploadFile($document_type_id) {
		$upload_data = $errors = array();
        $uploadPath = "uploads/ho_connect/$document_type_id/files";
        $s3uploadPath = '';

        if(!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, TRUE);
        }

       $file_status = $this->upload_file_response("upload_path", $uploadPath, '', ['png', 'jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi','PNG','JPEG','JPG']);

        if(isset($file_status['status'])){
            $file_status = is_array($file_status['data']) ? $file_status['data'] : (array)$file_status['data'];
            for($i=0;$i < count($file_status); ++$i){
                array_push($upload_data, $file_status[$i]) ;
            }
        } else {
            array_push($errors, $file_status['error']);
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
    
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $filename = '';
        if(count($upload_data)) {
            foreach ($upload_data as $key => $value) {
               $filename = $value;
            }
            
        } 
        return $filename;
	}

    function upload_file_response($file_name, $uploadPath, $s3uploadPath, $allowed_types = ['png','jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi','PNG','JPG','JPEG']){

		$file_response = upload_media($file_name, $uploadPath, $allowed_types, 100000000000, 0, $s3uploadPath);
//   print_r($file_response);exit;
		$upload_file_count = !empty($_FILES[$file_name]['name']) ? 1:0;

		if($upload_file_count > 1){
			$temp = array();
			$temp['status'] = TRUE;
            
			for($i=0; $i < $upload_file_count; ++$i){
				if(!empty($file_response[$i]['errors'])){
					$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
					$temp['status'] = FALSE;
				} else {
					#echo 'asfsaf';die;
					$temp['data'][] = $file_response[$i]['file_name'];
				}
			}
			return $temp;
		}else{

			if(!empty($file_response['errors'])){
				$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
				$temp['status'] = FALSE;

				return $temp;
			}

			if($file_response) {

				$file_name = [];
				foreach ($file_response as $key => $value) {
					if(!empty($value['file_name'])){
						$temp['status'] = TRUE;
						array_push($file_name, $value['file_name']);
						
					}
				}
				$temp['data'] = $file_name;
				return $temp;
			}

			
		}
	}

    
}