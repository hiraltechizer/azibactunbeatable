<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_patient extends MY_Model {

    private $p_key = 'patient_id';
    private $table = 'patient';
    private $tb_alias = 'p';
    private $fillable = [];
    private $column_list = ['MR Name', 'Patient Code', 'Patient Name', 'Patient Type', 'Patient Mobile', 'Patient Age', 'Address', 'City','Pincode','Doctor Name','DateTime'];
    private $csv_columns =[];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
             [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
            [
                'field_name'=>'p|patient_code',
                'field_label'=> 'Patient Code',
            ],
            [
                'field_name'=>'p|patient_name',
                'field_label'=> 'Patient Name',
            ],
            [
                'field_name'=>'p|patient_type',
                'field_label'=> 'Patient Type',
            ],
            [
                'field_name'=>'p|patient_mobile_no',
                'field_label'=> 'Patient Mobile',
            ],
            [
                'field_name'=>'p|patient_age',
                'field_label'=> 'Patient Age',
            ],
            [
                'field_name'=>'p|address',
                'field_label'=> 'Address',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'City Name',
            ],
            [
                'field_name'=>'p|pincode',
                'field_label'=> 'Pincode',
            ],
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'Doctor Name',
            ]
        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $q = $this->db->select('p.patient_code,p.patient_name,p.patient_type,p.patient_mobile_no,p.patient_age,p.address,p.city_id,p.pincode,p.doctor_id,p.users_id,p.psa_test,p.coupon_code,c.city_name,p.patient_id,m.users_name,d.doctor_name,p.insert_dt')
        ->from('patient p')
        ->join('manpower m', 'm.users_id = p.users_id','LEFT')
        ->join('doctor d', 'd.doctor_id = p.doctor_id','LEFT')
        ->join('cities c', 'c.city_id = p.city_id','LEFT')
        ->join('division dv', 'dv.division_id = m.users_division_id','LEFT');

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(p.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(p.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        

        if(! $count) {
            $q->order_by('p.patient_id DESC');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
      // echo '<pre>';
        //print_r($this->db->last_query());die;
        return $collection;
    }   

    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
            $records['MR Name']         = $rows['users_name'];
            $records['Patient Code']    = $rows['patient_code'];
            $records['Patient Name']    = $rows['patient_name'];
            $records['Patient Type']    = $rows['patient_type'];
            $records['Patient Mobile']  = $rows['patient_mobile_no'];
            $records['Patient Age']     = $rows['patient_age'];
            $records['Address']         = $rows['address'];
            $records['City']            = $rows['city_name'];
            $records['Pincode']         = $rows['pincode'];
            $records['Doctor Name']     = $rows['doctor_name'];
            $records['DateTime']        = $rows['insert_dt'];

             
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }

    function getCycleInfo($brand_id,$id) {

        $q = $this->db->select('ps.repurchase_date,ps.revisit_date,ps.psa_test,ps.coupon_code,ps.psa_report,ps.`comment`,c.cycle_no,c.`status`,c.insert_dt,c.brand_id,b.brand_name')
        ->from('cycle c')
        ->join('patient_sub ps', 'ps.patient_sub_id = c.patient_sub_id', 'LEFT')
        ->join('brands b', 'b.brand_id = ps.brand_id', 'LEFT')
        ->where('c.brand_id', $brand_id)
        ->where('c.patient_id', $id);
        ;

        $q->order_by('c.cycle_id DESC');
        
        $collection = $q->get()->result_array();
        return $collection;
    }

    function getUniquebrandrecords($pId) {
        $q = $this->db->select('ps.brand_id,b.brand_name')
        ->from('patient_sub ps')
        ->join('brands b', 'b.brand_id = ps.brand_id')
        ->where('ps.patient_id', $pId);
        ;

        $q->group_by('b.brand_id');
        $q->order_by('ps.brand_id');

        $collection = $q->get()->result_array();
      //   echo '<pre>';
       // print_r($this->db->last_query());die;
        return $collection;
    }
}