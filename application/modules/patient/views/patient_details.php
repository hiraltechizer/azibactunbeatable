<div class="col-md-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Patients Info</h4>
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Patient details</a>
                    </li>
                    <?php if(count($Rsbrand)) { ?>
                        <?php foreach ($Rsbrand as $key => $value) { ?>
                        <li class="nav-item">
                            <a class="nav-link" id="bbrand<?php echo $value['brand_id'] ?>" data-toggle="tab" href="#brand<?php echo $value['brand_id'] ?>" role="tab" aria-controls="brand<?php echo $value['brand_id'] ?>" aria-selected="false"><?php echo $value['brand_name'] ?></a>
                        </li>
                        <?php } ?>
                    <?php } ?>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                      <div class="media">
                        <div class="media-body">
                           <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>Code</th>
                                            <td><?php echo $info[0]['patient_code']; ?></td>
                                            <th>Name</th>
                                            <td><?php echo $info[0]['patient_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Type</th>
                                            <td><?php echo $info[0]['patient_type']; ?></td>
                                            <th>Mobile No</th>
                                            <td><?php echo $info[0]['patient_mobile_no']; ?></td>
                                        </tr>
                                        <tr>
                                             <th>Age</th>
                                            <td><?php echo $info[0]['patient_age']; ?></td>
                                            <th>Doctor Name</th>
                                            <td><?php echo $info[0]['doctor_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>MR Name</th>
                                            <td><?php echo $info[0]['users_name']; ?></td>
                                            <th>Address</th>
                                            <td><?php echo $info[0]['address']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>City</th>
                                            <td><?php echo $info[0]['city_name']; ?></td>
                                            <th>Pincode</th>
                                            <td><?php echo $info[0]['pincode']; ?></td>
                                        </tr>
                                    </table>
                                  </div>
                        </div>
                      </div>
                    </div>
                    <?php if(count($cycleInfo)) { ?>
                      <?php foreach ($cycleInfo as $key => $value) { ?>
                        <div class="tab-pane fade" id="brand<?php echo $value['brand_id']; ?>" role="tabpanel" aria-labelledby="bbrand<?php echo $value['brand_id']; ?>">
                            <div class="media">
                                <div class="media-body">
                                    <table class="table table-responsive">
                                        <thead>
                                          <tr>
                                            <th>Cycle No</th>
                                            <th>Status</th>
                                            <th>Repurchase Date</th>
                                            <th>Revisit Date</th>
                                            <th>PSA Test?</th>
                                            <th>Coupon Code</th>
                                            <th>PSA Report?</th>
                                            <th>Comment</th>
                                            <th>DateTime</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($value['cycleInfo'])) { ?>

                                                <?php foreach ($value['cycleInfo'] as $k => $val) { ?>
                                                    <tr>
                                                        <td>Cycle <?php echo $val['cycle_no']; ?></td>
                                                        <td><?php echo $val['status']; ?></td>
                                                        <td><?php echo date('Y-m-d',strtotime($val['repurchase_date'])); ?></td>
                                                        <td><?php echo date('Y-m-d',strtotime($val['revisit_date'])); ?></td>
                                                        <td><?php echo $val['psa_test']; ?></td>
                                                        <td><?php echo $val['coupon_code']; ?></td>
                                                        <td><?php echo $val['psa_report']; ?></td>
                                                        <td><?php echo $val['comment']; ?></td>
                                                        <td><?php echo $val['insert_dt']; ?></td>
                                                    </tr>
                                                <?php } ?>

                                            <?php } else { ?>
                                              <tr>
                                                  <td class="text-center" colspan="8">Information not available.</td>
                                              </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                          </div>
                      </div>
                   <?php } ?> 
                   <?php } ?>
                  </div>
            <a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light" style="margin-top: 10px;">Cancel</a>
        </div>
    </div>
</div>