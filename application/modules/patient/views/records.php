<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['patient_id']; ?>
<tr>
    <td>
       <label for="check_<?= $id ?>"></label>
    </td>
    
    <td><?php echo $record['users_name'] ?></td>   
    <td><?php echo $record['patient_code'] ?></td>
    <td><?php echo $record['patient_name'] ?></td>
    <td><?php echo $record['patient_type'] ?></td>
    <td><?php echo $record['patient_mobile_no'] ?></td>
    <td><?php echo $record['patient_age'] ?></td>
    <td><?php echo $record['address'] ?></td>
    <td><?php echo $record['city_name'] ?></td>
    <td><?php echo $record['pincode'] ?></td>
    <td><?php echo $record['doctor_name'] ?></td>
    <td><?php echo $record['insert_dt'] ?></td>
    
    <td>
        <div class="btn-group" role="group" aria-label="Video Disclaimer">
            <a class="blue-color" href="<?php echo base_url("$controller/view_patients/record/$id?c=$timestamp") ?>" title="View Details">
                <i class="far fa-eye btn-icon-prepend"></i>
            </a>
        </div>
    </td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>">
        <div class="row">
            <?php echo $this->ajax_pagination->create_links(); ?>    
        </div>
    </td>
</tr>