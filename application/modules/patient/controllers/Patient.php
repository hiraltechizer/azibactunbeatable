<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Patient extends Admin_Controller
{
	private $module = 'patient';
    private $model_name = 'mdl_patient';
    private $controller = 'patient';
    private $settings = [
        'permissions'=> ['download'],
        'pagination_index' => 3
    ];
    private $scripts = [];

   
	function __construct() {
         parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }

    function view_patients() {

        if( ! $this->session->is_admin_logged_in() )
            redirect('admin/login','refresh');

        $uri_string = $this->uri->uri_string();

        if(! strpos($uri_string, 'record')) {
            show_404();
        }

        $revised_url = strstr($uri_string, 'record');

        list($k, $v) = explode('/', $revised_url);
        $array[$k] = $v;

        // $array = $this->uri->uri_to_assoc(); 
        /* print_r($array); 
        die();
     */
        $tb_alias = $this->model->get_alias();

        $alias = (!empty($tb_alias)) ? $tb_alias : '';
        $table = $this->model->get_table();
        $p_key = $this->model->get_pkey();

        if(!isset($array['record'])) {
            show_404();
        }

        $temp = [];
        foreach($array as $i=>$value) {
            if($i == 'record') {
                $i = $p_key;
                $this->data[$i] = $id = (int) $array['record'];
            }
            $tempKey = (empty($alias)) ? "$table.$i" : "$alias.$i";
            $temp[$tempKey] = $value;
        }

        $this->data['info'] = $this->model->get_collection(FALSE, $temp);

        if(! count($this->data['info']) ){ show_404(); }

        #get brand info
        $BrandInfo = $this->model->getUniquebrandrecords($id);

        $BrandWiseCycle = [];
        $temp_array = [];
        if(!empty($BrandInfo)) {

            foreach ($BrandInfo as $key => $value) {
                
                $brand_id = $value['brand_id'];
                #get brand wise cycle and respurchase info
                $RsCycleInfo = $this->model->getCycleInfo($brand_id,$id);

                if(count($RsCycleInfo)) {
                    $temp_array['brand_id']     =   $brand_id;
                    $temp_array['brand_name']   =   $value['brand_name'];
                    $temp_array['cycleInfo']    =   $RsCycleInfo;

                    array_push($BrandWiseCycle, $temp_array);    
                } else {
                    unset($BrandInfo[$key]);
                }  
            }
        }

        $this->data['cycleInfo']    =   $BrandWiseCycle;
        $this->data['Rsbrand']      =   $BrandInfo;

        $this->data['section_title'] = 'View Patient Detail';
        $this->set_view($this->data, $this->controller.'/patient_details', '_admin');
    }

  
}
