<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sms extends Admin_Controller
{
	private $module = 'sms';
    private $model_name = 'mdl_sms';
    private $controller = 'sms';
    private $settings = [
        'permissions'=> ['download'],
         'paginate_index' => 3
    ];
    #private $scripts = ['admin/couponcode.js'];

	function __construct() {

        //$this->settings['filters'] = ['show_filters'=> FALSE, 'date_filters'=> FALSE];
        parent::__construct( 
            $this->module, 
            $this->controller, 
            $this->model_name,
            $this->settings
        );

        $this->set_defaults();
    }
}
