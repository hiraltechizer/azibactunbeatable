<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_sms extends MY_Model {

	private $p_key = 'id';
	private $table = 'sms_log';
	private $tb_alias = 'sl';
    private $fillable = ['coupon_code','booklet_code'];
    private $column_list = ['Message For', 'Mobile', 'Message', 'Response', 'Date'];
    private $csv_columns =  ['Message For', 'Mobile', 'Message', 'Response', 'Date'];

	function __construct() {
		
        parent::__construct($this->table, $this->p_key);
    }
    
    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_filters() {
        return [
            [
                'field_name'=>'msg_for',
                'field_label'=> 'Message For',
            ],
            [
                'field_name'=>'mobile',
                'field_label'=> 'Mobile no',
            ],
            [
                'field_name'=>'message',
                'field_label'=> 'Message',
            ],

        ];
    }

    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }


	function get_collection( $count = FALSE, $sfilters = [], $rfilters = [], $limit = 0, $offset = 0, ...$params ) {
        
        $field_filters = $this->get_filters_from($rfilters);

    	$q = $this->db->select('*')
        ->from('sms_log');
		
				
		if(sizeof($sfilters)) { 
			foreach ($sfilters as $key=>$value) { $q->where("$key", $value); }
		}

		 if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->table.'.insertdatetime) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date'  && $value) {
                    $this->db->where('DATE('.$this->table.'.insertdatetime) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        	$q->where('is_deleted','0');

		if(! $count) {
			$q->order_by('insertdatetime desc, id desc');
		}


		if(!empty($limit)) { $q->limit($limit, $offset); }        
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
		//echo $this->db->last_query();die;
		return $collection;
    }

    function get_sms_balance(){
        $this->db->select("ifnull(sum(ceil(length(message)/160)),0) netconsump",false);
        $this->db->from("sms_log"); //define record from which table
        $query = $this->db->get();
        $r = $query->row();
        $netconsump = $r->netconsump;
        
        $this->db->select("ifnull(sum(balance),0 ) netbal",false);
        $this->db->from("sms_balance bal"); //define record from which table
        $query = $this->db->get();
        $r = $query->row();
        $netbal = $r->netbal;
        return $totbal =  $netbal - $netconsump;

    }

     function total_sms() {
        $q = $this->db->select('SUM(balance) AS balance')
            ->from('sms_balance')
            ->get()->result();

        return $q;
    }

    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
            $records['Message For'] = $rows['msg_for'];
            $records['Mobile'] = $rows['mobile'];
            $records['Message'] = $rows['message'];
            $records['Response'] = $rows['output'];
            $records['Date'] = $rows['insertdatetime'];
          
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }
}