<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="coupon_id" value="<?php echo $info[0]['coupon_id']; ?>" />

<div class="form-group">
    <div class="form-line">
        <select name="booklet_code" class="form-control" data-placeholder="Select Booklet Code" id="booklet_code">
            <option value="<?php echo $info[0]['booklet_code']; ?>" selected="selected"><?php echo $info[0]['bookletCode']; ?></option>
        </select>
    </div>
</div>
<label class="form-label">Coupon Code<span class="required">*</span></label>
<div class="form-group">
    <div class="form-line">
        <input type="text" name="coupon_code" class="form-control" maxlength="50" value="<?php echo $info[0]['coupon_code']; ?>" />
    </div>
</div>
<button type="submit" class="btn bg-<?= $settings['theme'] ?> m-t-15 waves-effect">
    <i class="material-icons">save</i>
    <span>Save</span>
</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-danger m-t-15 waves-effect">
    <i class="material-icons">reply_all</i>	
    <span>Cancel</span>
</a>
<?php echo form_close(); ?>