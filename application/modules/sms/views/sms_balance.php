




            <div class="col-md-4 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">TOTAL SMS</h4>
                  <div class="media">
                    <i class="fa fa-globe icon-sm text-info d-flex align-self-start mr-3"></i>
                    <div class="media-body">
                      <p class="card-text"><?= number_format($total_balance[0]->balance); ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">SMS REMAINING</h4>
                  <div class="media">
                    <i class="fa fa-globe icon-sm text-info d-flex align-self-center mr-3"></i>
                    <div class="media-body">
                      <p class="card-text"><?= number_format($sms_balance); ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">SMS USAGE</h4>
                  <div class="media">
                    <i class="fa fa-globe icon-sm text-info d-flex align-self-end mr-3"></i>
                    <div class="media-body">
                      <p class="card-text"><?= number_format($total_balance[0]->balance - $sms_balance) ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>


