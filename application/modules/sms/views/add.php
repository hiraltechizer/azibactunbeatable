<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<label class="form-label">Booklet Code<span class="required">*</span></label>
<div class="form-group">
    <div class="form-line">
        <select name="booklet_code" class="form-control" data-placeholder="Select Booklet Code" id="booklet_code">
            <option></option>
        </select>
    </div>
</div>
<label class="form-label">Coupon Code<span class="required">*</span></label>
<div class="form-group">
    <div class="form-line">
        <input type="text" name="coupon_code" class="form-control" maxlength="50" />
    </div>
</div>
<button type="submit" class="btn bg-<?= $settings['theme'] ?> m-t-15 waves-effect">
    <i class="material-icons">save</i>
    <span>Save</span>
</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-danger m-t-15 waves-effect">
    <i class="material-icons">reply_all</i>	
    <span>Cancel</span>
</a>
<?php echo form_close(); ?>