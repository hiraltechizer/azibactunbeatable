<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Zone extends Admin_Controller
{
	private $module = 'zone';
    private $model_name = 'mdl_zone';
    private $controller = 'geography/zone';
    private $settings = [
		'permissions'=> ['add', 'edit', 'download', 'upload'],
		'paginate_index' => 4
    ];
    private $scripts = ['load-geography.js'];


	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];
        parent::__construct( 
            $this->module, 
            $this->controller, 
            $this->model_name,
            $this->settings,
            $this->scripts
        );

        $this->set_defaults();
    }
    
	function options(){
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
		$id = (isset($_POST['id'])) ? (int) $this->input->post('id') : 0;
		$new = array(); $json['results'] = array();

		$filters['division_id'] = $id;
		
		$_options = $this->model->get_options($s_term, 'zone_name', $filters, $page * $limit, $limit);
		$_opt_count = count($this->model->get_options($s_term, 'zone_name', $filters));

		foreach($_options as $option){
			$new['id'] = $option->zone_id;
			$new['text'] = $option->zone_name;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

	function uploadcsv(){
		$this->session->is_Ajax_and_admin_logged_in();
		/*upload csv file */

		if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
		}

		if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
		}

		$file = $_FILES['csvfile']['tmp_name'];
		$handle = fopen($file, "r");
		$cnt = 0; $newrows = 0;

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

			if(count($data) !== 3) { continue; }

			if(! $cnt){
                $cnt++; continue;
            }
            $territory_code = trim($data[0]);
            $zone_name = trim($data[1]);
			$division_name = trim($data[2]);
            
            if(empty($territory_code) || empty($zone_name) || empty($division_name) ) {
				continue;
            }

            if( ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $zone_name)
            	|| ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $division_name)
				|| ! preg_match('/^[a-zA-Z0-9]+$/', $territory_code) 
            	 ){
                continue;
            }

			$territoryRecords = $this->model->get_records(['territory_code' => $territory_code], 'zone');
           
			if(count($territoryRecords)) {
				continue;
			}

            $divRecords = $this->model->get_records(['division_name' => $division_name], 'division');
           
			if(!count($divRecords)) {
				continue;
			}
 
			$division_id = $divRecords[0]->division_id;

            $z_records = $this->model->get_records([ 'zone_name'=> $zone_name, 'division_id' => $division_id ], 'zone', ['zone_id', 'zone_name'], 'zone_id', 1);

            if(count($z_records)){
                continue;
            }

            $insert['zone_name'] = $zone_name;
            $insert['division_id'] = $division_id;
			$insert['territory_code'] = $territory_code;
            $this->model->_insert($insert);

            $newrows++;
		}

		fclose($handle);

		echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
	}
}
