<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Main_city extends Admin_Controller
{
    private $module = 'main_city';
    private $model_name = 'mdl_main_city';
    private $controller = 'geography/main_city';
    private $settings = [
        'permissions'=> ['add', 'edit', 'download', 'upload'],
        'paginate_index' => 4
    ];
    private $scripts = ['division.js'];

    function __construct() {
        $this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];
        parent::__construct( 
            $this->module, 
            $this->controller, 
            $this->model_name,
            $this->settings,
            $this->scripts
        );

        $this->set_defaults();
    }


function options(){
        $this->session->is_Ajax_and_logged_in();
        $filters = array();
        $limit = $this->dropdownlength;
        $page = intval($_POST['page']) - 1;
        $page = ($page <= 0) ? 0 : $page;

        $s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
        $id = (isset($_POST['id'])) ? (int) $this->input->post('id') : 0;

        //if($id){ $filters['area_id'] = $id; }

        $new = array(); $json['results'] = array();

        $_options = $this->model->get_options($s_term, 'mcity_name', $filters, $page * $limit, $limit);
        $_opt_count = count($this->model->get_options($s_term, 'mcity_name', $filters));

        foreach($_options as $option){
            $new['id'] = $option->m_city_id;
            $new['text'] = $option->mcity_name;

            array_push($json['results'], $new);
        }
        
        $more = ($_opt_count > count($_options)) ? TRUE : FALSE;
        $json['pagination']['more'] = $more;

        echo json_encode($json);
    }

    function uploadcsv(){
        $this->session->is_Ajax_and_admin_logged_in();
        /*upload csv file */

        if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
        }

        if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
        }

        $file = $_FILES['csvfile']['tmp_name'];
        $handle = fopen($file, "r");
        $cnt = 0; $newrows = 0;

        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

            if(count($data) !== 1) { continue; }

            if(! $cnt){
                $cnt++; continue;
            }

            $city_name = trim($data[0]);
           
            if( empty($city_name) ) {
                continue;
            }

            if( 
                !preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $city_name) ){
                continue;
            }

            $main_city = $this->model->get_records(['mcity_name'=> $city_name], 'main_city');
            if(count($main_city)) {
                continue;
            }

            $insert['mcity_name'] = $city_name;

            $this->model->_insert($insert);
            $newrows++;
        }

        fclose($handle);

        echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
    }
}
