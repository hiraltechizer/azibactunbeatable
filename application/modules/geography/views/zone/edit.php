<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="zone_id" value="<?php echo $info[0]['zone_id']; ?>" />
<div class="form-group row">
    <label for="division_id" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="division_id" name="division_id" data-placeholder="Select Division">
            <option value="<?php echo $info[0]['division_id']; ?>" selected><?php echo $info[0]['division_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
  <label for="zone_name" class="col-sm-3 col-form-label">Zone Name <span class="text-danger">*</span></label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="zone_name" name="zone_name" placeholder="Zone" value="<?php echo $info[0]['zone_name']; ?>">
  </div>
</div>
<div class="form-group row">
  <label for="territory_code" class="col-sm-3 col-form-label"> Territory Code <span class="text-danger">*</span></label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="territory_code" name="territory_code" placeholder="Territory Code" value="<?php echo $info[0]['territory_code']; ?>" >
  </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>