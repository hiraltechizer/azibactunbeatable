<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
    <label for="division_id" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="division_id" name="division_id" data-placeholder="Select Division">
            <option value="">Select Division</option>
        </select>
    </div>
</div>
<div class="form-group row">
  <label for="zone_name" class="col-sm-3 col-form-label">Zone Name <span class="text-danger">*</span></label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="zone_name" name="zone_name" placeholder="Zone Name" maxlength="100">
  </div>
</div>

<div class="form-group row">
  <label for="territory_code" class="col-sm-3 col-form-label"> Territory Code <span class="text-danger">*</span></label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="territory_code" name="territory_code" placeholder="Territory Code" >
  </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>