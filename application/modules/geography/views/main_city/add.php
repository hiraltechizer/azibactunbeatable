<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>

<!-- <div class="form-group row">
    <label for="area_id" class="col-sm-3 col-form-label">City Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm" id="city_id" name="city_id" data-placeholder="Select Sub City">
            <option value="">Select Sub City</option>
        </select>
    </div>
</div> -->

<div class="form-group row">
    <label for="mcity_name" class="col-sm-3 col-form-label">Main City Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="mcity_name" name="mcity_name" placeholder="Main City Name" maxlength="100">
    </div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>