<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_quarter_target extends MY_Model {

    private $p_key = 'quw_target_id';
    private $table = 'quarter_wise_target';
    private $tb_alias = 'quw';
    private $fillable = ['quarter_id','city_id', 'slab1','slab2'];
    private $column_list = ['Quarter','Territory Code', 'HQ','Slab 1','Slab 2','DateTime'];
    private $csv_columns =['Quarter [Quarter 1,..]','Territory Code', 'HQ', 'Slab 1','Slab 2'];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
            [],
             [
                'field_name'=>'c|territory_code',
                'field_label'=> 'Territory Code',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'City Name',
            ],
            // [
            //     'field_name'=>'quw|quarter_target',
            //     'field_label'=> 'Quarter Targete',
            // ],
           

        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $user_id =(int) $this->security->xss_clean($this->session->get_field_from_session('user_id', 'user'));

        $role = $this->security->xss_clean($this->session->get_field_from_session('role', 'user'));

        $q = $this->db->select('quw.*,c.city_name,c.territory_code,q.quarter_name')
        ->from('quarter_wise_target quw')
        ->join('city c', 'c.city_id = quw.city_id')
        ->join('quarter q', 'q.quarter_id = quw.quarter_id')
        // ->join('area a', 'a.area_id = c.area_id')
        // ->join('region r', 'r.region_id = a.region_id')
        // ->join('zone z', 'z.zone_id = r.zone_id')
        // ->join('speciality s', 's.speciality_id = d.speciality_id')
        ;

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        

        if(! $count) {
            $q->order_by('quw.quw_target_id DESC');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
      // echo '<pre>';
        //print_r($this->db->last_query());die;
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
                [
                    'field' => 'quarter_id',
                    'label' => 'Quarter',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'city_id',
                    'label' => 'city',
                    'rules' => 'trim|required|unique_record[add.table.quarter_wise_target.quarter_id.'.$this->input->post('quarter_id') .'.city_id.' . $this->input->post('city_id') . ']|xss_clean'
                ],
                [
                    'field' => 'territory_code',
                    'label' => 'Territory Code',
                    'rules' => 'trim|required|xss_clean'
                ],
                // [
                //     'field' => 'quarter_target',
                //     'label' => 'Quarter Target',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
                [
                    'field' => 'slab1',
                    'label' => 'Slab 1',
                    'rules' => 'trim|numeric|required|xss_clean'
                ],
                [
                    'field' => 'slab2',
                    'label' => 'Slab 2',
                    'rules' => 'trim|numeric|required|xss_clean'
                ],
                // [
                //     'field' => 'emerald',
                //     'label' => 'Emerald',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
                // [
                //     'field' => 'pearl',
                //     'label' => 'Pearl',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
                // [
                //     'field' => 'saphire',
                //     'label' => 'Saphire',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
            ];
        }

        if($type == 'modify') {
            return [
                [
                    'field' => 'quarter_id',
                    'label' => 'Quarter',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'city_id',
                    'label' => 'city',
                    'rules' => 'trim|required|unique_record[edit.table.quarter_wise_target.quarter_id.'. $this->input->post('quarter_id') .'.city_id.' . (int) $this->input->post('city_id') . '.quw_target_id.'. $this->input->post('quw_target_id') .']|xss_clean'
                ],
                [
                    'field' => 'territory_code',
                    'label' => 'Territory Code',
                    'rules' => 'trim|required|xss_clean'
                ],
                // [
                //     'field' => 'quarter_target',
                //     'label' => 'Quarter Target',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
                [
                    'field' => 'slab1',
                    'label' => 'Slab 1',
                    'rules' => 'trim|numeric|required|xss_clean'
                ],
                [
                    'field' => 'slab2',
                    'label' => 'Slab 2',
                    'rules' => 'trim|numeric|required|xss_clean'
                ],
                // [
                //     'field' => 'emerald',
                //     'label' => 'Emerald',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
                // [
                //     'field' => 'pearl',
                //     'label' => 'Pearl',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
                // [
                //     'field' => 'saphire',
                //     'label' => 'Saphire',
                //     'rules' => 'trim|numeric|required|xss_clean'
                // ],
            ];
        }
    }

    function save(){

        /*Load the form validation Library*/
        // echo "<pre>";print_r($_POST);exit;
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validate('save'));
        
        if(!$this->form_validation->run()) {
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');
            
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }
        
        $data = $this->process_data($this->fillable, $_POST);
        $id = $this->_insert($data);
        
        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
    }
    
    function modify(){
        /*Load the form validation Library*/
        $this->load->library('form_validation');

        $is_Available = $this->check_for_posted_record($this->p_key, $this->table);
        if(! $is_Available['status']){ return $is_Available; }
        
        $this->form_validation->set_rules($this->validate('modify'));

        if(! $this->form_validation->run()){
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');

            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }       
        
        $data = $this->process_data($this->fillable, $_POST);

        $p_key = $this->p_key;
        $id  = (int) $this->input->post($p_key);

        $status = (int) $this->_update([$p_key => $id], $data);

        if(! $status){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
    }

    function remove(){
        
        if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
            $ids = $this->input->post('ids');
            $response = $this->_delete($this->p_key, $ids, $this->table);

            $msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
            return ['msg'=> $msg];
        }

        return ['msg'=> 'No Records Selected'];
    }

    
    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
             $records['Quarter'] = $rows['quarter_name'];
             $records['Territory Code'] = $rows['territory_code'];
             $records['HQ'] = $rows['city_name'];
            //  $records['Q2 Tgt'] = $rows['quarter_target'];
             $records['Slab 1'] = $rows['slab1'];
             $records['Slab 2'] = $rows['slab2'];
             
            $records['DateTime'] = $rows['insert_dt'];
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }
}