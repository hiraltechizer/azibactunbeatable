<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>

<div class="form-group row">
    <label for="city_id" class="col-sm-3 col-form-label">Quarter Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
    
        <select class="form-control form-control-sm js-example-basic-single" id="quarter_id" name="quarter_id" data-placeholder="Select Quarter">
            <option value="">Select Quarter</option>

            <?php if(count($quarter_list)): ?>
             <?php foreach ($quarter_list as  $quarter) { ?>
                <option value="<?php echo $quarter->quarter_id; ?>"><?php echo $quarter->quarter_name; ?></option>
                <?php } ?>
            <?php endif; ?>

        </select>
    </div>
</div>
<div class="form-group row">
    <label for="city_id" class="col-sm-3 col-form-label">City Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="city_id" name="city_id" data-placeholder="Select City">
            <option value="">Select City</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="territory_code" class="col-sm-3 col-form-label">Territory Code <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control " id="territory_code" name="territory_code" data-placeholder="Territory Code" >
           
        </select>
    </div>
</div>

<!-- <div class="form-group row">
    <label for="quarter_target" class="col-sm-3 col-form-label">Quarter Target <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="quarter_target" name="quarter_target" placeholder="Quarter Target" >
    </div>
</div> -->


<div class="form-group row">
    <label for="slab1" class="col-sm-3 col-form-label">Slab 1 <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="slab1" name="slab1" placeholder="slab1" >
    </div>
</div>
<div class="form-group row">
    <label for="slab2" class="col-sm-3 col-form-label">Slab 2 <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="slab2" name="slab2" placeholder="slab2">
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>