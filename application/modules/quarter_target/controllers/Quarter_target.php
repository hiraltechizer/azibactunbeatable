<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Quarter_target extends Admin_Controller
{
	private $module = 'Quarter_target';
    private $model_name = 'mdl_quarter_target';
    private $controller = 'quarter_target';
    private $settings = [
        'permissions'=> ['add','upload','download','edit'],
        'pagination_index' => 3
    ];
    private $scripts = ['division.js'];

   
	function __construct() {
         parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }

     function uploadcsv(){
        $this->session->is_Ajax_and_admin_logged_in();
        /*upload csv file */

        if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
        }

        if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
        }

        $file = $_FILES['csvfile']['tmp_name'];
        $handle = fopen($file, "r");
        $cnt = 0; $newrows = 0;

        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

            if(count($data) !== 5) { continue; }

            if(! $cnt){
                $cnt++; continue;
            }
            
            $quarter = trim($data[0]);
            $city_code = trim($data[1]);
            $city = trim($data[2]);
            // $quarter_target = trim($data[3]);
            $slab1 = trim($data[3]);
            $slab2 = trim($data[4]);
            // $emerald = trim($data[6]);
            // $saphire = trim($data[7]);
            // $diamond = trim($data[8]);
            if(empty($quarter) || empty($city_code) || empty($city) || empty($slab1) || empty($slab2) ){
                continue;
            }
            
            // if( 
                //     ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $doctor_name)
                //     || ! preg_match('/^[1-9][0-9]{9}$/', $doctor_mobile) 
                //     || ! preg_match('/^[1-9][0-9]{9}$/', $users_mobile)
            //     || ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $speciality) 
            // ){
            //     continue;
            // }
            
            $quarterInfo = $this->model->get_records(['quarter_name'=> $quarter], 'quarter', ['quarter_id'], '', 1);
            if(!count($quarterInfo)) {
                continue;
            }
            $quarter_id = $quarterInfo[0]->quarter_id;

            $cityInfo = $this->model->get_records(['city_name' => $city,'territory_code'=> $city_code], 'city', ['city_id'], '', 1);
            if(!count($cityInfo)) {
                continue;
            }
            
            $city_id = $cityInfo[0]->city_id;
            
            #check for speciality
            $cityquarterInfo = $this->model->get_records(['city_id'=> $city_id,'quarter_id' => $quarter_id ], 'quarter_wise_target', ['quw_target_id'], '', 1);
            if(count($cityquarterInfo)) {
                continue;
            }          
            
            
            $insert['quarter_id'] = $quarter_id;
            $insert['city_id'] = $city_id;
            // $insert['quarter_target'] = $quarter_target;
            $insert['slab1'] = $slab1;
            $insert['slab2'] = $slab2;
            // $insert['emerald'] = $emerald;
            // $insert['pearl'] = $pearl;
            // $insert['saphire'] = $saphire;
            

            $this->model->_insert($insert);

            $newrows++;
        }

        fclose($handle);

        echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
    }

    function getcityBycode(){
        $city_id = $this->input->post('city_id');
        $cityCode = $this->model->get_records(['city_id' => $city_id],'city',['territory_code']);
        echo json_encode($cityCode);
    }
}
