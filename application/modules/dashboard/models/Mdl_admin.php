<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_admin extends MY_Model {

	private $p_key = '';
	private $table = '';

	function __construct() {
		parent::__construct($this->table, $this->p_key);
    }


	function get_brand_wise_patient_count($f_filters = [], $keywords =''){
	  
		$m = date('m', strtotime(date('Y-m-d', time())));
		$y = date('Y', strtotime(date('Y-m-d', time())));
		$zonefiler ='';	
		$regionfilter = '';
		if($f_filters){
			$month = $f_filters['month'];
			$year = $f_filters['year'];
		}else{	
			$month = date('m', strtotime(date('Y-m-d', time())));
			$year = date('Y', strtotime(date('Y-m-d', time())));
		}
		if(isset($f_filters['zone_id'])) { 
			$zone_id = $f_filters['zone_id'];
			$zonefiler .= " AND z.zone_id = $zone_id";
		}
		if(isset($f_filters['region_id'])) { 
			$region_id = $f_filters['region_id'];
			$regionfilter .= " AND r.region_id = $region_id";
		}

		$q = $this->db->select("b.brand_id,b.brand_name,(SELECT COUNT(DISTINCT(ps.patient_id))
		FROM patient_sub ps
		JOIN doctor d ON d.doctor_id = ps.doctor_id 
		JOIN manpower m ON m.users_id = d.users_id
		JOIN city c ON c.city_id = m.users_city_id
		JOIN area a ON a.area_id = c.area_id
		JOIN region r ON r.region_id = a.region_id
		JOIN zone z ON z.zone_id = r.zone_id 
		where ps.brand_id = b.brand_id AND MONTH(ps.insert_dt) = $month AND YEAR(ps.insert_dt) = $year  $zonefiler $regionfilter) AS patient_count")
	    ->from('brands b');
	
			// if(sizeof($f_filters)) { 
			// 	foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
			// }
			$q->order_by('patient_count','desc');
			if(!empty($limit)) { $q->limit($limit, $offset); }
			$collection1 = $q->get();
			// echo $this->db->last_query();exit;
			$collection = $collection1->result_array();
			return $collection;

	}

	function get_speciality_wise_doctor_count($f_filters = [], $keywords =''){
	  
		$m = date('m', strtotime(date('Y-m-d', time())));
		$y = date('Y', strtotime(date('Y-m-d', time())));
		if($f_filters){
			$month = $f_filters['month'];
			$year = $f_filters['year'];
		}else{	
			$month = date('m', strtotime(date('Y-m-d', time())));
			$year = date('Y', strtotime(date('Y-m-d', time())));
		}
		$q = $this->db->select("sp.speciality_id,sp.speciality,

		(SELECT COUNT(DISTINCT(d.doctor_id))
		FROM doctor d 
		where d.speciality_id = sp.speciality_id AND MONTH(d.insert_dt) = $month AND YEAR(d.insert_dt) = $year) AS doctor_count")
	    ->from('speciality sp');
	
			// if(sizeof($f_filters)) { 
			// 	foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
			// }
			$q->order_by('doctor_count','desc');
			if(!empty($limit)) { $q->limit($limit, $offset); }
			$collection1 = $q->get();
			// echo $this->db->last_query();exit;
			$collection = $collection1->result_array();
			return $collection;

	}
	function get_age_wise_patient_count($f_filters = [], $keywords ='',$age_start,$age_end){
		// print_r($f_filters);exit;
	   $y = date('Y', strtotime(date('Y-m-d', time())));
	   $m = date('m', strtotime(date('Y-m-d', time())));
	

	
	   if($f_filters){
		  if(isset($f_filters['year'])){ 
				   $year = $f_filters['year'];
				}
			if(isset($f_filters['month'])){ 
				  $month = $f_filters['month'];
				}
		
		  }else{
			  $year = date('Y', strtotime(date('Y-m-d', time())));
			  $month = date('m', strtotime(date('Y-m-d', time())));
		 }
	
		 $q = $this->db->select("COUNT(DISTINCT(p.patient_id)) AS patient_cout")
		 ->from('patient p');
		
		 $q->where("MONTH(p.insert_dt) = $month AND YEAR(p.insert_dt) = $year AND p.patient_age >= $age_start
			 AND p.patient_age <= $age_end");
		
		 $q->order_by('patient_cout','desc');
		
		 if(!empty($limit)) { $q->limit($limit, $offset); }
			 $collection1 = $q->get();
			 // echo $this->db->last_query();exit;
			 $collection = $collection1->result_array();
			 return $collection;
	}

	function get_month_wise_npt_pot_count($f_filters = [], $keywords =''){
		//print_r($f_filters['month']);exit;
	 $m = date('m', strtotime(date('Y-m-d', time())));
	 $y = date('Y', strtotime(date('Y-m-d', time())));
	 $zonefiler ='';	
	 $regionfilter = '';
	 if($f_filters){
		$month = $f_filters['month'];
		$year = $f_filters['year'];
	 }else{	
		$month = date('m', strtotime(date('Y-m-d', time())));
		$year = date('Y', strtotime(date('Y-m-d', time())));
	 }
		if(isset($f_filters['zone_id'])) { 
			$zone_id = $f_filters['zone_id'];
			$zonefiler .= " AND z.zone_id = $zone_id";
			}
		if(isset($f_filters['region_id'])) { 
			$region_id = $f_filters['region_id'];
			$regionfilter .= " AND r.region_id = $region_id";
		}

		$query = "SELECT max(temp.pot) as POT_Patient_Count,
			 MAX(temp.npt) as NPT_Patient_Count
			 FROM
			 (
			 SELECT 
			 COUNT(DISTINCT(p.patient_id)) AS npt, 0 as pot
			 FROM patient p
			 JOIN manpower m ON m.users_id = p.users_id
			 JOIN city c ON c.city_id = m.users_city_id
			 JOIN area a ON a.area_id = c.area_id
			 JOIN region r ON r.region_id = a.region_id
			 JOIN zone z ON z.zone_id = r.zone_id
			 WHERE MONTH(p.insert_dt) = $month AND YEAR(p.insert_dt) = $year  $zonefiler $regionfilter
	        
			 UNION
			 SELECT  0 as npt, COUNT(DISTINCT(p.patient_id)) AS pot 
			 FROM patient p
			 JOIN manpower m ON m.users_id = p.users_id
			 JOIN city c ON c.city_id = m.users_city_id
			 JOIN area a ON a.area_id = c.area_id
			 JOIN region r ON r.region_id = a.region_id
			 JOIN zone z ON z.zone_id = r.zone_id
			 WHERE MONTH(p.insert_dt) < $month AND YEAR(p.insert_dt) = $year $zonefiler $regionfilter
			 )
			 temp
			 ";
			 $query .= " WHERE 1=1 ";

	 $collection = $this->db->query($query)->result_array();
	//  echo $this->db->last_query();exit;
	 // print_r($this->db->get_compiled_select());exit;
	 return $collection;
   }
	
}