<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_user extends MY_Model {

	private $p_key = '';
	private $table = '';

	function __construct() {
		parent::__construct($this->table, $this->p_key);
		$this->load->helper('upload_media');
    }


	function get_ho_conect_records($role){
		$q = $this->db->select("conn.title,conn.description,conn.display_image,conn.insert_dt")
				->from('`ho_connect` `conn`')
				->join('`ho_connect_fields` `cfield`', '`cfield`.`ho_conn_id` = `conn`.`ho_conn_id`');
		$q->where('cfield.field_permission',$role);
		$q->where('conn.status','send');
		$q->order_by('`conn`.`ho_conn_id` desc');
		$q->limit(1);
		$collection1 = $q->get();
			//  echo $this->db->last_query();exit;
			 $collection = $collection1->result_array();
			 return $collection;
	}

	function get_doctor_records($condtion){
		$q = $this->db->select("d.doctor_id")
				->from('doctor d')
				->join('manpower m', 'm.users_id = d.users_id')
				->join('manpower asm', 'asm.users_id = m.users_parent_id')
				->join('manpower rsm', 'rsm.users_id = asm.users_parent_id')
				->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id');
		$q->where($condtion);
		$collection1 = $q->get();
			//  echo $this->db->last_query();exit;
			 $collection = $collection1->result_array();
			 return $collection;
	}
}