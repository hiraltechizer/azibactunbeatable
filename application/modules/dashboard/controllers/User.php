<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class User extends Front_Controller
{
	private $module = 'user';
	private $controller = 'dashboard/user';
	private $model_name = 'mdl_user';
	
	function __construct() {
		$this->load->model('doctor/mdl_doctor', 'mdl_doctor');
		$this->load->model('patient/mdl_patient', 'mdl_patient');
		$this->load->model('master/mdl_speciality', 'mdl_speciality');
		parent::__construct($this->module, $this->controller, $this->model_name);
	}

	function index(){
		if( ! $this->session->user_logged_in() ){
			redirect('user','refresh');
        }
        $this->load->helper('text');
        $user_id = $this->session->get_field_from_session('user_id', 'user');
        $role = $this->session->get_field_from_session('role', 'user');
        if($role == 'HO') {
			$this->data['doctor_count'] = count($this->model->get_records([], 'doctor'));
			$this->data['BoCount'] = count($this->model->get_records(['users_type' => 'MR'], 'manpower'));
			$this->data['specialityCount'] = count($this->model->get_records([], 'speciality'));
		}elseif($role == 'MR'){
		   $this->data['doctor_count'] = count($this->model->get_records(['users_id' => $user_id], 'doctor'));
		   $this->data['BoCount'] = ""; 
		   $this->data['specialityCount'] = "";
		   $this->data['ho_conect_notification'] = $this->model->get_ho_conect_records($role);
		}elseif($role == 'ASM'){
			$this->data['doctor_count'] = count($this->model->get_doctor_records("asm.users_id = $user_id"));
			$this->data['BoCount'] = ""; 
			$this->data['specialityCount'] = "";
			$this->data['ho_conect_notification'] = $this->model->get_ho_conect_records($role);
		}elseif($role == 'RSM'){
			$this->data['doctor_count'] = count($this->model->get_doctor_records("rsm.users_id = $user_id"));
			$this->data['BoCount'] = ""; 
			$this->data['specialityCount'] = "";
			$this->data['ho_conect_notification'] = $this->model->get_ho_conect_records($role);
		}elseif($role == 'ZSM'){
			$this->data['doctor_count'] = count($this->model->get_doctor_records("zsm.users_id = $user_id"));
			$this->data['BoCount'] = ""; 
			$this->data['specialityCount'] = "";
			$this->data['ho_conect_notification'] = $this->model->get_ho_conect_records($role);
		}else{
			$this->data['doctor_count'] ="";
			$this->data['BoCount'] = "";
			$this->data['specialityCount'] = "";
		}
        
		$this->data['role'] = $role;
		$this->data['js'] = ['form-submit.js'];
		$this->data['plugins'] = ['countTo','select2','material-datetime'];
        $this->data['mainmenu'] = 'dashboard';

    	$this->set_view($this->data, $this->controller . '/dashboard',  '_user');
    }
    
    function logout() {
    	$userId = $this->session->get_field_from_session('user_id', 'user_');
    	
    	#update login attampt
		$ret = $this->model->_update(['users_id' => $userId], ['attempt' => '0'], 'manpower');

        $session_key = 'user_' . config_item('session_data_key');
		$sessionData = array('user_id'=>'',	'user_name'=>'', 'role'=>'');
		
		$this->session->unset_userdata($session_key, $sessionData);
		redirect('/user','refresh');
    }


	function filters_info() {

		$this->role = $this->session->get_field_from_session('role','user');
		if(empty( $this->role)) {
			$this->role = $this->session->get_field_from_session('role');
		}

		$this->data['role'] = $this->role;
		$type = $this->input->post('type');
		$id = $this->input->post('id');
		$value = $this->input->post('value');
		$month_id   = $this->input->post('month');
		$year_id   = $this->input->post('year');
		$zone_id   = $this->input->post('zone_id');
		$region_id   = $this->input->post('region_id');

		$records = array();
		$sfilters = array();
		$data_arr = array();
  
		if($id!="") {
			$countfilterarray = "";
			$from_day = "";
			$to_day = "";
			$countfilterarray1 = "";
			$brand_slug = "";

			$urole = strtolower($this->session->get_field_from_session('role','user'));
			if($urole == 'sho'){
				$user_id = '';
			}else{
				$user_id = $this->session->get_field_from_session('user_id', 'user');
			}

		switch ($id) {      

			case 'brand_wise_patient_count':
  
				if($month_id!=''){
					$sfilters['month']= $month_id;
				 }
				 if($year_id!=''){
					 $sfilters['year']= $year_id;
				}
				if($zone_id!=''){
					$sfilters['zone_id']= $zone_id;
				 }
				if($region_id!=''){
					 $sfilters['region_id']= $region_id;
				}
// print_r($sfilters);exit;
				$keywords =  "";
				$brand_wise_patient = $this->model->get_brand_wise_patient_count($sfilters,$keywords);
				$doc_patient = [];
				$brand_name = array_column($brand_wise_patient, 'brand_name');
				$count = array_map(function($data) {
					 		return round((float) $data, 2);
						} ,array_column($brand_wise_patient, 'patient_count'));	
				$records = ["brand_name" => $brand_name, 'patient_count' => $count];
			break;

			case 'speciality_wise_patient_count':
  
				if($month_id!=''){
					$sfilters['month']= $month_id;
				 }
				 if($year_id!=''){
					 $sfilters['year']= $year_id;
				}


				$keywords =  "";
				$spci_wise_doctor = $this->model->get_speciality_wise_doctor_count($sfilters,$keywords);
				$doc_patient = [];
				$speciality_name = array_column($spci_wise_doctor, 'speciality');
				$count = array_map(function($data) {
					 		return round((float) $data, 2);
						} ,array_column($spci_wise_doctor, 'doctor_count'));	
				$records = ["speciality_name" => $speciality_name, 'doctor_count' => $count];
			break;

			case 'age_wise_patient_count':
  
				if($month_id!=''){
					$sfilters['month']= $month_id;
				 }
				 if($year_id!=''){
					 $sfilters['year']= $year_id;
				}
				$keywords =  "";
				$age_slab = $this->model->get_records([], 'age_slab');

				foreach ($age_slab as $ages){
									
					$age_wise_patientCount = $this->model->get_age_wise_patient_count($sfilters, $keywords ='',$ages->age_start,$ages->age_end);
					
					$age_group_p[] = array(
						'name'=> $ages->age_start.' - '.$ages->age_end,
						'y'=> (float) $age_wise_patientCount[0]['patient_cout'],
					);
				}
				$records = $age_group_p;
			break;
			case 'status_wise_patient_count':
				if($month_id!=''){
					$sfilters['month']= $month_id;
				 }
				 if($year_id!=''){
					 $sfilters['year']= $year_id;
				}
				if($zone_id!=''){
					$sfilters['zone_id']= $zone_id;
				 }
				if($region_id!=''){
					 $sfilters['region_id']= $region_id;
				}
				$keywords =  "";
				$call_wise_call_status = $this->model->get_month_wise_npt_pot_count($sfilters,$keywords);
				$call_wise_call_status = $call_wise_call_status[0];
				//echo "<pre>" ;print_r($call_wise_call_status);exit;
				$call_keys = array_keys($call_wise_call_status);
				$call_values = array_values($call_wise_call_status);
				$call_values = array_map('intval', $call_values);	
				$records = ["patient_status" => $call_keys, 'patient_count' => $call_values];	
  
					default:
					# code...
					break;
				}

			if(!empty($records)) {
				$data_arr['success'] = "0";
				$data_arr['message'] = "Data Found";  
				$data_arr['result'] = $records;
			} else {
				$data_arr['success'] = "1";
				$data_arr['message'] = "No Relevant Data Found";
			}
		} else {
			$data_arr['success'] = "1";
			$data_arr['message'] = "Please select the {$type}";
		}
		echo json_encode($data_arr);
		exit;
	}

	function popupmodel(){
		if( ! $this->session->user_logged_in() ){
			redirect('user','refresh');
        }
		
	 $data['title'] = "Remozen and world cup ";
	 $data['controller'] =  $this->module;
	 $this->load->view("verify_modules.php",$data);

	}
}
