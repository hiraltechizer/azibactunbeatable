<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
</script>

<div class="row">
    <div class="col-md-4 grid-margin">
    <a href="<?php echo base_url('doctor/lists');?>" >
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Total Doctors</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-md-flex">
                            <h2 class="mb-0"><?php echo $doctor_count; ?></h2>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <i class="fas fa-user-md blue-color icon-lg"></i>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-md-4 grid-margin">
    <a href="<?php echo base_url('manpower/mr/lists');?>" >
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Total BO</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-md-flex">
                            <h2 class="mb-0"><?php echo $BoCount; ?></h2>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <i class="fas fa-wheelchair text-success icon-lg"></i>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-md-4 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Total Speciality</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-md-flex">
                            <h2 class="mb-0"><?php echo $specialityCount; ?></h2>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <i class="fas fa-comment-alt icon-lg blue-color"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<!-- <script>
   var brand_wise_patient = <?php ///echo $brand_wise_patientCout; 
                            ?>;
</script>  -->