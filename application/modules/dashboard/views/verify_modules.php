<!DOCTYPE html>
<html lang="en">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="<?= $this->security->get_csrf_hash(); ?>">
  <title>Azibact Unbeatable</title>

  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/iconfonts/font-awesome/css/all.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/css/vendor.bundle.addons.css">
  <!-- plugins:js -->
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/baguetteBox.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/fluid-gallery.css">


  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
  <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>


  <?php if (isset($plugins) && in_array('chosen', $plugins)) : ?>
    <!-- SELECT2 plugins CSS loaded -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/chosen/chosen.css" />
  <?php endif; ?>
  <?php if (isset($plugins) && in_array('fancybox', $plugins)) : ?>
    <!-- FANCYBOX plugins CSS loaded -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/fancybox/dist/jquery.fancybox.css" /><?php endif; ?>

  <link type="text/css" rel="stylesheet" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap-material-datetimepicker.css">
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png" />
  <!-- endinject -->
  <script type="text/javascript">
    var baseUrl = "<?php echo base_url() ?>";
  </script>
</head>
<body class="body-background">

<div class="container">

<div class="row pt-5 row-small">


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-right">
        
         
         <h5 class="modal-title" id="exampleModalLongTitle"></h5>
        <button type="button" class="close" >
          <a href="<?php echo base_url("dashboard/$controller") ?>" class="">
         x</a> 
        </button>
      </div>
      <div class="modal-body text-center">
          <img class="img-fluid" src="<?php echo base_url('assets/images/popup2.jpg'); ?>" />
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        
      </div>
    </div>
  </div>
</div>

</div>

</div>











        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->


  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>assets/vendors/js/vendor.bundle.addons.js"></script>
  <script src="<?php echo base_url() ?>assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert2.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/misc.js"></script>
  <script src="<?php echo base_url() ?>assets/js/settings.js"></script>
  <script src="<?php echo base_url() ?>assets/js/todolist.js"></script>
  <script src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
  <script type="text/javascript">
    var controller = "<?php echo $controller ?>";
  </script>
  <?php if (isset($plugins) && in_array('select2', $plugins)) : ?>
    <script src="<?php echo base_url() ?>assets/js/select2.js"></script>
  <?php endif; ?>



<script type="text/javascript">
    $(window).on('load', function() {
        $('#myModal').modal(

            {
                backdrop: 'static',
        keyboard: false,
        show: true // added property here
            }
        );
    });
</script>

</html>
