<?php if ($role == 'HO') { ?>
<div class="row">
    <div class="col-md-4 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Total Doctors</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-md-flex">
                            <h2 class="mb-0"><?php echo $doctor_count; ?></h2>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <i class="fas fa-user-md blue-color icon-lg"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Total BO</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-md-flex">
                            <h2 class="mb-0"><?php echo $BoCount; ?></h2>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <i class="fas fa-wheelchair text-success icon-lg"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Total Speciality</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-md-flex">
                            <h2 class="mb-0"><?php echo $specialityCount; ?></h2>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <i class="fas fa-comment-alt icon-lg blue-color"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>



<div class="row">
<?php if (($role == 'MR') || ($role == 'ASM') || ($role == 'RSM') || ($role == 'ZSM')) : ?>
    <div class="<?php if($role == 'MR'){ ?>col-md-4 <?php } else { ?> col-md-3 <?php } ?> grid-margin mar-b">
        <a href="<?php echo base_url("sales_performance/lists?t=$timestamp");?>">
            <div class="card user-dash-card">
                <div class="card-body user-box-sales-performance">
                    <img src="<?php echo base_url(); ?>web/images/icon1.png" alt="" class="dash-box-icon">
                    <h4 class="card-title mb-0">Sales Performance</h4>
                    <div class="d-flex justify-content-between align-items-center"></div>
                </div>
            </div>
        </a>
    </div>
    <?php endif; ?>
    <?php if (($role == 'MR') || ($role == 'ASM') || ($role == 'RSM') || ($role == 'ZSM')) : ?>
    <div class="<?php if($role == 'MR'){ ?>col-md-4 <?php } else { ?> col-md-3 <?php } ?> grid-margin mar-b">
    <a href="<?php if ($role == 'MR'){ 
           echo base_url("prescription/lists?t=$timestamp");
           }else{
            echo base_url("reports/live_report");}?>">
        <div class="card user-dash-card">
            <div class="card-body user-box-sales-performance">
                <img src="<?php echo base_url(); ?>web/images/icon2.png" alt="" class="dash-box-icon">
                <h4 class="card-title mb-0"> Upload Prescription</h4>
                <div class="d-flex justify-content-between align-items-center"></div>
            </div>
        </div>
        </a>
    </div>
    <?php endif; ?>

    <?php if (($role == 'MR') || ($role == 'ASM') || ($role == 'RSM') || ($role == 'ZSM')) : ?>
    <div class="<?php if($role == 'MR'){ ?>col-md-4 <?php } else { ?> col-md-3 <?php } ?> grid-margin mar-b">
    <a href="<?php echo base_url("ho_connect/lists?t=$timestamp");?>">
        <div class="card user-dash-card">
            <div class="card-body user-box-sales-performance">
                <img src="<?php echo base_url(); ?>web/images/icon3.png" alt="" class="dash-box-icon">
                <h4 class="card-title mb-0">HO Communication</h4>
                <div class="d-flex justify-content-between align-items-center"></div>
            </div>
        </div>
   </a>
    </div>
    <?php endif; ?>

    <?php if ($role == 'MR') : ?>
    <!-- <div class="col-md-3 grid-margin mar-b">
    <a href="<?php echo base_url("winwheel/add");?>">
        <div class="card user-dash-card">
            <div class="card-body user-box-sales-performance">
                <img src="<?php echo base_url(); ?>web/images/icon4.png" alt="" class="dash-box-icon">
                <h4 class="card-title mb-0">Spin O Win</h4>
                <div class="d-flex justify-content-between align-items-center"></div>
            </div>
        </div>
    </a>
    </div> -->
    <?php endif; ?>

    <?php if (($role == 'ASM') || ($role == 'RSM') || ($role == 'ZSM')) : ?>
    <div class="col-md-3 grid-margin mar-b">
    <a href="<?php echo base_url("reports/live_report");?>">
        <div class="card user-dash-card">
            <div class="card-body user-box-sales-performance">
                <img src="<?php echo base_url(); ?>web/images/icon4.png" alt="" class="dash-box-icon">
                <h4 class="card-title mb-0">Reports</h4>
                <div class="d-flex justify-content-between align-items-center"></div>
            </div>
        </div>
        </a>
    </div>

    <?php endif; ?>




    <?php if ($role == "MR" || $role == 'ASM' || $role == 'RSM' || $role == 'ZSM') { ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="" style="">

        <?php
                foreach ($ho_conect_notification as $notification) { ?>
        <div class="card box-body" style="padding:25px">
            <div class="row">
                <?php if ($notification) : ?>

                <div class="col-md-12">
                    <img src="<?php echo base_url() . $notification['display_image']; ?>" style="width:45px">
                    <div class="time-txt">
                        <?php echo time_elapsed_string($notification['insert_dt']); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="dash-panel-ttl">
                        <?php echo $notification['title']; ?>
                    </div>

                    <div class="dash-panel-text">
                        <?php echo word_limiter($notification['description'], 40); ?>
                        </p>

                    </div>
                </div>

                <?php endif ?>
                <div class="col-md-12 read-more-pos">
                    <div class="text-right" style="padding-top:15px">
                        <a class=" read-more-text" href="<?php echo base_url('ho_connect/lists') ?>"> Read More </a>
                    </div>
                </div>

            </div>

        </div>
        <?php  } ?>

    </div>
    <?php } ?>
</div>
<?php

        function time_elapsed_string($datetime, $full = false)
        {
            $now = new DateTime;
            $ago = new DateTime($datetime);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
            );
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                    unset($string[$k]);
                }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            return $string ? implode(', ', $string) . ' ago' : 'just now';
        }
        ?>