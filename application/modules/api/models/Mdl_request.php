<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_request extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	function UserAuthenticate($username, $password)
	{
		#check username and password form manpower data
        $where_in_type = array('MR','RSM');
		$this->db->select('users_id, users_name, users_type, users_mobile,users_emp_id');
		$this->db->from('manpower');
		$this->db->where("users_username", $username);
		$this->db->where("users_password", $password);
		$this->db->where_in("users_type", $where_in_type);
		$this->db->where("is_deleted", '0');
		$query = $this->db->get();

		$collection = $query->result();
		return $collection;
	}

	function get_patient_unique_code(){
        
        $unique = TRUE;
        while($unique){
            $patient_code = 'PT'. str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
            $record = $this->get_records(['patient_code'=> $patient_code], 'patient', ['patient_id'], '', 1);

            $unique = count($record) ? TRUE : FALSE;
            
        }
        
        return $patient_code; 
    }

    function initiate_cycle($patient_id, $patient_sub_id, $brand_id){

        if(! $patient_sub_id){
            return;
        }

        #check for active cycle
        $active_cycle = $this->get_records(['patient_id'=> $patient_id, 'brand_id' => $brand_id], 'cycle', ['cycle_id'], 'insert_dt desc', 1);
            
        if(!count($active_cycle)) {

            $patient_cycles = $this->get_records(['patient_id'=> $patient_id, 'brand_id' => $brand_id], 'cycle', ['cycle_no'], 'cycle_id desc', 1);

            $cycle_no = (count($patient_cycles)) ? ($patient_cycles[0]->cycle_no + 1) : 1;

            $cycle_data = [];
            $cycle_data['patient_id'] = $patient_id;
            $cycle_data['patient_sub_id'] = $patient_sub_id;
            $cycle_data['brand_id'] = $brand_id;
            $cycle_data['strength_id'] = NULL;
            $cycle_data['cycle_no'] = $cycle_no;
            $cycle_data['status'] = 'ongoing';

            $inserts = $this->_insert($cycle_data, 'cycle');

            // return TRUE;
        } else {

            $cycle_id = $active_cycle[0]->cycle_id;

            $ret =  $this->_update(['cycle_id'=> $cycle_id], ['status'=> 'completed'], 'cycle');

            if($ret) {

                $patient_cycles = $this->get_records(['patient_id'=> $patient_id, 'brand_id' => $brand_id], 'cycle', ['cycle_no'], 'cycle_id desc', 1);

                $cycle_no = (count($patient_cycles)) ? ($patient_cycles[0]->cycle_no + 1) : 1;

                $cycle_data = [];
                $cycle_data['patient_id'] = $patient_id;
                $cycle_data['patient_sub_id'] = $patient_sub_id;
                $cycle_data['brand_id'] = $brand_id;
                $cycle_data['strength_id'] = NULL;
                $cycle_data['cycle_no'] = $cycle_no;
                $cycle_data['status'] = 'ongoing';

                $inserts = $this->_insert($cycle_data, 'cycle'); 

                return TRUE;   
            }
            

        }
       
    }

    function getAllBrandInfo($brand_id,$patient_id,$cycle_no) {

        #check username and password form manpower data
        $this->db->select('ps.brand_id,ps.strength_id,b.brand_name,DATE_FORMAT(ps.repurchase_date,"%d/%m/%Y") as repurchase_date,DATE_FORMAT(ps.revisit_date,"%d/%m/%Y") as revisit_date');
        $this->db->from('patient_sub ps');
        $this->db->join('brands b','b.brand_id = ps.brand_id');
        $this->db->join('cycle c','c.patient_sub_id = ps.patient_sub_id');
        $this->db->where_in("ps.brand_id", $brand_id);
        $this->db->where("ps.patient_id", $patient_id);
        $this->db->where("c.cycle_no", $cycle_no);
        $this->db->group_by("ps.brand_id");

        $query = $this->db->get();

        $collection = $query->result();

        return $collection;

    } 

    function getActiveKnowledgeword($document_type_id) {
         $this->db->select('*');
        $this->db->from('knowledge_world kw');
         $this->db->join('quarter q','q.quarter_id = kw.quarter_id');
        $this->db->where("kw.document_type_id", $document_type_id);
        $this->db->where("q.status",'Active');
       

        $query = $this->db->get();

        $collection = $query->result();

        return $collection;
    }

    function get_doctor_patient_count($doctor_id){
        $this->db->select('d.doctor_id,d.doctor_name,count(DISTINCT(ps.patient_id)) AS patient_count');
        $this->db->from('doctor d');
        $this->db->join('patient_sub ps','d.doctor_id = ps.doctor_id');
        $this->db->where("d.doctor_id", $doctor_id);
        $query = $this->db->get();
        $collection = $query->result();
        return $collection; 
    }
}	
