SELECT
	*
FROM (SELECT
		d.doctor_name, d.doctor_mobile, d.doctor_email,
		-- d.doctor_id, 
		d.doctor_garnet_code,d.doctor_id,d.doctor_user_id,
		mr.users_name as mr_name,
		asm.users_name as asm_name,
		rsm.users_name as rsm_name,
		zsm.users_name as zsm_name,
		
		-- p.passenger_id, 
		p.passenger_name, 
		p.gender, p.age, IF(p.is_doctor = 1, 'Doctor', 'Passenger') AS passenger_type,
		
		travel_date, 

		-- td.from_location, td.to_location, 
		a.airport_name AS travel_from, v.venue_name AS travel_to,
		
		td.flight_preferred_time, 
		td.journey_mode
FROM travel_details td 
JOIN passenger p ON p.travel_id = td.travel_id
LEFT JOIN doctor d ON td.doctor_id = d.doctor_id
LEFT JOIN airports a ON a.id = td.from_location
LEFT JOIN venues v ON v.id = td.to_location
JOIN manpower mr on mr.users_id = d.doctor_user_id
JOIN manpower asm on asm.users_id = mr.users_parent_id
JOIN manpower rsm on rsm.users_id = asm.users_parent_id
JOIN manpower zsm on zsm.users_id = rsm.users_parent_id
WHERE 1=1
	-- AND d.doctor_id = 76
	AND td.journey_mode = 'conference'
	
UNION ALL

SELECT
		d.doctor_name, d.doctor_mobile, d.doctor_email,
		-- d.doctor_id, 
		d.doctor_garnet_code,d.doctor_id,d.doctor_user_id,
		mr.users_name as mr_name,
		asm.users_name as asm_name,
		rsm.users_name as rsm_name,
		zsm.users_name as zsm_name,
		
		-- p.passenger_id, 
		p.passenger_name, 
		p.gender, p.age, IF(p.is_doctor = 1, 'Doctor', 'Passenger') AS passenger_type,
		
		-- td.travel_id, d.doctor_id, state_id,
		travel_date, 
			
		-- td.from_location, td.to_location, 
		v.venue_name AS travel_from,
		a.airport_name AS travel_to, 
		td.flight_preferred_time, 
		td.journey_mode
FROM travel_details td 
JOIN passenger p ON p.travel_id = td.travel_id
LEFT JOIN doctor d ON td.doctor_id = d.doctor_id
LEFT JOIN airports a ON a.id = td.to_location
LEFT JOIN venues v ON v.id = td.from_location
JOIN manpower mr on mr.users_id = d.doctor_user_id
JOIN manpower asm on asm.users_id = mr.users_parent_id
JOIN manpower rsm on rsm.users_id = asm.users_parent_id
JOIN manpower zsm on zsm.users_id = rsm.users_parent_id

WHERE 1=1
	-- AND d.doctor_id = 76
	AND td.journey_mode = 'return') temp 
ORDER BY doctor_garnet_code, journey_mode  甮