<?php
class Login extends Api_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->model('api/mdl_request');
	}

	function login() {

		#MR Logging in to the system
		#Get Username , Password and Device ID for Validating
		#If records are present, return the token to the APP
		#Else return the error message to the APP

		/**
		 * @api {post} /api/login/login Login
		 * @apiName login
		 * @apiGroup Login
		 * @apiVersion 1.0.0
		 * @apiParam {String} username MR Username
		 * @apiParam {String} password Password.
		 * @apiParam {String}  device_id Device ID.
		 * @apiParam {String}  device_type Device Type.
		 * @apiParam {String}  os Operating System.
		 * @apiParam {String}  device_name Device Name.
		 * @apiParam {String}  app_version APP version.
		 *
		 * @apiSuccess {Number} code HTTP Status Code.
		 * @apiSuccess {String} message  Associated Message.
		 * @apiSuccess {Object} data  MR Record Object With Token
		 * @apiSuccess {Object} error  Error if Any.
		 *
		 *@apiExample sample input:
		 *	{"username":"MR001","password":"123123","device_id":"123","device_type":"android","os":"10.0.0","device_name":"mobile","app_version":"1.0"}
		 *
		 * @apiSuccessExample Success-Response:
			*     HTTP/1.1 200 OK
*     	{
"message": "User Information Retrive Successfully!!!",
"error": "",
"code": 200,
"status": "Success",
"data": {
    "token": "0bd08aea178983854052e1dc28a1d36a938fb639",
    "UserInfo": {
        "users_id": "12",
        "users_name": "MR001",
        "users_type": "MR",
        "users_mobile": "1222222222",
        "users_emp_id": "MR001"
    },
    "request_id": 1602070586.832002
}
}
			*/
 		
 		#collect required information
		$username = trim(isset($this->input_data['username'])?$this->input_data['username']:'');
		$password = trim(isset($this->input_data['password'])?$this->input_data['password']:'');
		$device_id = trim(isset($this->input_data['device_id'])?$this->input_data['device_id']:'');
		$device_type = trim(isset($this->input_data['device_type'])?$this->input_data['device_type']:'');
		$os = trim(isset($this->input_data['os'])?$this->input_data['os']:'');
		$device_name = trim(isset($this->input_data['device_name'])?$this->input_data['device_name']:'');
		$app_version = trim(isset($this->input_data['app_version'])?$this->input_data['app_version']:'');

		#get logged in user info
		$UserInfo = $this->mdl_request->UserAuthenticate($username, $password);

		//echo '<pre>';
		//print_r($UserInfo);die;
		
		#check username eits or not		
		if(empty($username)){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Username is Mandatory";
			$this->error = array('message'=>'Username is Mandatory!');
			$this->sendResponse();
			return;
		}

		#check password exits or not
		if(empty($password)){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Password is Mandatory";
			$this->error = array('message'=>'Password is Mandatory!');
			$this->sendResponse();
			return;
		}

		#check device id exits or not
		if(empty($device_id)){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Device Id is Mandatory";
			$this->error = array('message'=>'Device Id is Mandatory!');
			$this->sendResponse();
			return;
		}

		#check device type exits or not
		if(empty($device_type) || !in_array(strtolower($device_type),['android','ios'])){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Device type is Mandatory";
			$this->error = array('message'=>'Device type is Mandatory!');
			$this->sendResponse();
			return;
		}

		#Check os name exits or not
		if(empty($os)){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "OS Name is Mandatory";
			$this->error = array('message'=>'OS Name is Mandatory!');
			$this->sendResponse();
			return;
		}

		#device name is exits or not
		if(empty($device_name)){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Device Name is Mandatory";
			$this->error = array('message'=>'Device Name is Mandatory!');
			$this->sendResponse();
			return;
		}

		#app version info exits or not
		if(empty($app_version)){
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "App Version info is Mandatory";
			$this->error = array('message'=>'App Version info is Mandatory!');
			$this->sendResponse();
			return;
		}

		#check userinfo avilable or not
		if(empty($UserInfo)) {
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Invalid Username or Password!!";
			$this->error = array('message'=>'Invalid Username or Password!!');
			$this->sendResponse();
			return;
		} else {

			#set users id and users type info
			$users_type	=	$UserInfo[0]->users_type;
			$users_id	=	$UserInfo[0]->users_id;

			#generate token
			$token = sha1(md5(microtime() . "" . $users_id));

			#prepare access token insert information
			$data = array(
				'users_id' 		=> $users_id,
				'access_token' 	=> $token,
				'device_id' 	=> $device_id,
				'device_type' 	=> $device_type,
				'os' 			=> $os,
				'device_name' 	=> $device_name,
				'app_version' 	=> $app_version
			);

			#Generate the Access Token	and make user login
			$access_token = $this->mdl_request->_insert($data,'access_token'); 


			
			#assign required info in response array
			$user_response				= array();
			$user_response['token']		= $token;
			$user_response['UserInfo']	= $UserInfo[0];
			
			#check userinfo exits or not
			if(!empty($UserInfo)) {
				$this->response['code'] = 200;
				$this->response['status'] = 'Success';
				$this->response['message'] = "User Information Retrive Successfully!!!";
				$this->response['data'] = $user_response;
				$this->error = [];
				$this->sendResponse();
				return;
			} else {
				$this->response['code'] = 400;
				$this->response['status'] = 'error';
				$this->response['message'] = "User Information not available";
				$this->error = array('message'=>'User Information not available!!');
				$this->sendResponse();
				return;
			}

		}
	}

	function match_version(){

	    $get_ios_version_code = $this->mdl_request->get_records([],'version_ctr',[]);

        $user_response['version_data']	=	$get_ios_version_code;
        if(!empty($get_ios_version_code)){
            /*$this->response['code'] = 200;
            $this->response['message'] = "Successful";
            $this->error = array('message'=>'Successful');
            $this->sendResponse();
            return;*/
            $this->response['code'] = 200;
			$this->response['status'] = 'Success';
			$this->response['message'] = "User Information Retrive Successfully!!!";
			$this->response['data'] = $user_response;
			$this->error = [];
			$this->sendResponse();
			return;
        }
        else{
            $this->response['code'] = 401;
            $this->response['message'] = "Failure";
            $this->error = array('message'=>'Failure');
            $this->sendResponse();
            return;
        }
	    
	    
	}
}
