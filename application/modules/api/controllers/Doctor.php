<?php
class Doctor extends Api_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('api/mdl_request');
        $this->load->helper('tiny_url');
        $this->load->helper('send_sms');
	}
   
	function add_doctor() {
		/**
		 * @api {post} /api/doctor/add_doctor Add Doctor
		 * @apiName Add Doctor
		 * @apiGroup Doctor
		 * @apiVersion 1.0.0
		 * @apiParam {String} Token 
		 * @apiParam {String} doctor_name Doctor Name
         * @apiParam {Numeric} doctor_mobile Doctor Mobile Number
         * @apiParam {Varchar} city_id Doctor City
         * @apiParam {Varchar} state_id Doctor State
        * @apiParam {Numeric} speciality_id Doctor Speciality
         * @apiParam {Varchar} clinic_address Doctor Clinic Address
		 *
		 * @apiSuccess {Number} code HTTP Status Code.
		 * @apiSuccess {String} message  Associated Message.
		 * @apiSuccess {Object} data  Phlebo Record Object With Token
		 * @apiSuccess {Object} error  Error if Any.
		 *
		 *@apiExample sample input:
		 *	{"doctor_name":"Test Doctor1","doctor_mobile":"1234545678","city_id":"1","state_id":"21","speciality_id":"1","clinic_address":"Test Address"}
		 *
		 * @apiSuccessExample Success-Response:
			*     HTTP/1.1 200 OK
			* 
			{
    "message": "Doctor has been added Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "request_id": 1603179033.360525
    }
}
		*/
		#collect required information
        $doctor_name    = trim(isset($this->input_data['doctor_name'])?$this->input_data['doctor_name']:'');
        $doctor_mobile  = trim(isset($this->input_data['doctor_mobile'])?$this->input_data['doctor_mobile']:'');
        $state_id        = trim(isset($this->input_data['state_id'])?$this->input_data['state_id']:'');
        $city_id        = trim(isset($this->input_data['city_id'])?$this->input_data['city_id']:'');
        $speciality_id = trim(isset($this->input_data['speciality_id'])?$this->input_data['speciality_id']:'');
        $clinic_address = trim(isset($this->input_data['clinic_address'])?$this->input_data['clinic_address']:'');
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        $division_id = $UsersInfo[0]->users_division_id;

        #Check Logged in User Type for MR
        if($users_type != 'MR') {
        	$this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Invalid Authentication";
            $this->error = array('message'=>'Invalid Authentication');
            $this->sendResponse();
            return;
        }

        #Check doctor name value 
        if(empty($doctor_name)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Doctor Name";
            $this->error = array('message'=>'Please Enter Doctor Name');
            $this->sendResponse();
            return;
        }

        #validate doctor name
        if(!preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $doctor_name)) {
             $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valide Doctor Name";
            $this->error = array('message'=>'Please Enter Valide Doctor Name');
            $this->sendResponse();
            return;            
        }

        #Check doctor mobile number
        if(empty($doctor_mobile)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Doctor Mobile Number";
            $this->error = array('message'=>'Please Enter Doctor Mobile Number');
            $this->sendResponse();
            return;
        }

        #validate doctor mobile
       if(! preg_match('/^[1-9][0-9]{9}$/', $doctor_mobile)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valid Doctor Mobile Number";
            $this->error = array('message'=>'Please Enter Valid Doctor Mobile Number');
            $this->sendResponse();
            return;
        }

        #Check for speciality
        if(empty($speciality_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Select Speciality";
            $this->error = array('message'=>'Please Select Speciality');
            $this->sendResponse();
            return;
        }

        #Check for city
        if(empty($city_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Select Language";
            $this->error = array('message'=>'Please Select Language');
            $this->sendResponse();
            return;
        }

        #Check for Clinic address
        /*if(empty($clinic_address)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Clinic Address";
            $this->error = array('message'=>'Please Enter Clinic Address');
            $this->sendResponse();
            return;
        }*/

        #check mobile number exits or not
        $DoctorInfo = $this->mdl_request->get_records(['doctor_mobile' => $doctor_mobile,'is_deleted' => '0'], 'doctor', ['doctor_id']);

        #If Doctor record found then show error
        if(count($DoctorInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Doctor Mobile Number Already Exist.";
            $this->error = array('message'=>'Doctor Mobile Number Already Exist.');
            $this->sendResponse();
            return;   
        }

        #check speciality exits or not
        $SpecialityInfo = $this->mdl_request->get_records(['speciality_id'=> $speciality_id, 'is_deleted' => '0'], 'speciality', ['speciality_id']);

        #If Speciality record not found then show error
        if(!count($SpecialityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected Speciality Information not available.";
            $this->error = array('message'=>'Selected Speciality Information not available.');
            $this->sendResponse();
            return;   
        }

         #check speciality exits or not
        $StateInfo = $this->mdl_request->get_records(['state_id'=> $state_id], 'state', ['state_name']);

        #If Speciality record not found then show error
        if(!count($StateInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected State Information not available.";
            $this->error = array('message'=>'Selected State Information not available.');
            $this->sendResponse();
            return;   
        } 

         #check speciality exits or not
        $CityInfo = $this->mdl_request->get_records(['city_id'=> $city_id, 'state_id' => $state_id], 'cities', ['city_id']);

        #If Speciality record not found then show error
        if(!count($CityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected City Information not available.";
            $this->error = array('message'=>'Selected City Information not available.');
            $this->sendResponse();
            return;   
        }      

        #prepare doctor data
        $insert_data['doctor_name']     =   $doctor_name;
        $insert_data['doctor_mobile']   =   $doctor_mobile;
        $insert_data['state_id']        =   $state_id;
        $insert_data['city_id']         =   $city_id;
        $insert_data['speciality_id']   =   $speciality_id;
        $insert_data['clinic_address']  =   $clinic_address;
        $insert_data['users_id']        =   $users_id;
       
        #Insert Doctor Data
        $doctor_id = $this->mdl_request->_insert($insert_data, 'doctor');

        $users_response = [];
        
        #check userinfo exits or not
		if(!empty($doctor_id)) {
			$this->response['code'] = 200;
			$this->response['status'] = 'Success';
			$this->response['message'] = "Doctor has been added Successfully!!!";
			$this->response['data'] = $users_response;
			$this->error = [];
			$this->sendResponse();
			return;
		} else {
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Internal Server Error";
			$this->error = array('message'=>'Internal Server Error!!');
			$this->sendResponse();
			return;
		}
	}

    function edit_doctor() {
        /**
         * @api {post} /api/doctor/edit_doctor Edit Doctor
         * @apiName Edit Doctor
         * @apiGroup Doctor
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         * @apiParam {Numeric} doctor_id Doctor Id
         * @apiParam {String} doctor_name Doctor Name
         * @apiParam {Numeric} doctor_mobile Doctor Mobile Number
         * @apiParam {Varchar} city_id Doctor City
         * @apiParam {Varchar} state_id Doctor State
        * @apiParam {Numeric} speciality_id Doctor Speciality
         * @apiParam {Varchar} clinic_address Doctor Clinic Address
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  Phlebo Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {"doctor_id":"1","doctor_name":"Test Doctor1","doctor_mobile":"1234545678","city_id":"1","state_id":"21","speciality_id":"1","clinic_address":"Test Address"}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
            {
    "message": "Doctor has been added Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "request_id": 1603179033.360525
    }
}
        */
        #collect required information
        $doctor_id    = trim(isset($this->input_data['doctor_id'])?$this->input_data['doctor_id']:'');        
        $doctor_name    = trim(isset($this->input_data['doctor_name'])?$this->input_data['doctor_name']:'');
        $doctor_mobile  = trim(isset($this->input_data['doctor_mobile'])?$this->input_data['doctor_mobile']:'');
        $state_id        = trim(isset($this->input_data['state_id'])?$this->input_data['state_id']:'');
        $city_id        = trim(isset($this->input_data['city_id'])?$this->input_data['city_id']:'');
        $speciality_id = trim(isset($this->input_data['speciality_id'])?$this->input_data['speciality_id']:'');
        $clinic_address = trim(isset($this->input_data['clinic_address'])?$this->input_data['clinic_address']:'');
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        $division_id = $UsersInfo[0]->users_division_id;

        #Check Logged in User Type for MR
        if($users_type != 'MR') {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Invalid Authentication";
            $this->error = array('message'=>'Invalid Authentication');
            $this->sendResponse();
            return;
        }

        #Check doctor name value 
        if(empty($doctor_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Doctor Id";
            $this->error = array('message'=>'Please Enter Doctor Id');
            $this->sendResponse();
            return;
        }

        #Check doctor name value 
        if(empty($doctor_name)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Doctor Name";
            $this->error = array('message'=>'Please Enter Doctor Name');
            $this->sendResponse();
            return;
        }

        #validate doctor name
        if(!preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $doctor_name)) {
             $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valide Doctor Name";
            $this->error = array('message'=>'Please Enter Valide Doctor Name');
            $this->sendResponse();
            return;            
        }

        #Check doctor mobile number
        if(empty($doctor_mobile)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Doctor Mobile Number";
            $this->error = array('message'=>'Please Enter Doctor Mobile Number');
            $this->sendResponse();
            return;
        }

        #validate doctor mobile
       if(! preg_match('/^[1-9][0-9]{9}$/', $doctor_mobile)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valid Doctor Mobile Number";
            $this->error = array('message'=>'Please Enter Valid Doctor Mobile Number');
            $this->sendResponse();
            return;
        }

        #Check for speciality
        if(empty($speciality_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Select Speciality";
            $this->error = array('message'=>'Please Select Speciality');
            $this->sendResponse();
            return;
        }

        #Check for city
        if(empty($city_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Select Language";
            $this->error = array('message'=>'Please Select Language');
            $this->sendResponse();
            return;
        }

        #Check for Clinic address
        /*if(empty($clinic_address)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Clinic Address";
            $this->error = array('message'=>'Please Enter Clinic Address');
            $this->sendResponse();
            return;
        }*/

        #check mobile number exits or not
        $DoctorInfo = $this->mdl_request->get_records(['doctor_id' => $doctor_id,'is_deleted' => '0'], 'doctor', ['doctor_id']);

        #If Doctor record found then show error
        if(!count($DoctorInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Doctor Information not available.";
            $this->error = array('message'=>'Doctor Information not available.');
            $this->sendResponse();
            return;   
        }

        #check mobile number exits or not
        $RsDocInfo = $this->mdl_request->get_records(['doctor_id !=' => $doctor_id,'doctor_mobile' => $doctor_mobile,'is_deleted' => '0'], 'doctor', ['doctor_id']);

        #If Doctor record found then show error
        if(count($RsDocInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] =  "Doctor Mobile Number Already Exist.";
            $this->error = array('message'=> "Doctor Mobile Number Already Exist.");
            $this->sendResponse();
            return;   
        }        


        #check speciality exits or not
        $SpecialityInfo = $this->mdl_request->get_records(['speciality_id'=> $speciality_id, 'is_deleted' => '0'], 'speciality', ['speciality_id']);

        #If Speciality record not found then show error
        if(!count($SpecialityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected Speciality Information not available.";
            $this->error = array('message'=>'Selected Speciality Information not available.');
            $this->sendResponse();
            return;   
        }

         #check speciality exits or not
        $StateInfo = $this->mdl_request->get_records(['state_id'=> $state_id], 'state', ['state_name']);

        #If Speciality record not found then show error
        if(!count($StateInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected State Information not available.";
            $this->error = array('message'=>'Selected State Information not available.');
            $this->sendResponse();
            return;   
        } 

         #check speciality exits or not
        $CityInfo = $this->mdl_request->get_records(['city_id'=> $city_id, 'state_id' => $state_id], 'cities', ['city_id']);

        #If Speciality record not found then show error
        if(!count($CityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected City Information not available.";
            $this->error = array('message'=>'Selected City Information not available.');
            $this->sendResponse();
            return;   
        }      

        #prepare doctor data
        $update_data['doctor_name']     =   $doctor_name;
        $update_data['doctor_mobile']   =   $doctor_mobile;
        $update_data['state_id']        =   $state_id;
        $update_data['city_id']         =   $city_id;
        $update_data['speciality_id']   =   $speciality_id;
        $update_data['clinic_address']  =   $clinic_address;
        $update_data['users_id']        =   $users_id;
       
        #Insert Doctor Data
        $ret = $this->mdl_request->_update(['doctor_id' => $doctor_id], $update_data, 'doctor');

        $users_response = [];
        
        #check userinfo exits or not
        if(!empty($ret)) {
            $this->response['code'] = 200;
            $this->response['status'] = 'Success';
            $this->response['message'] = "Doctor data has been updated Successfully!!!";
            $this->response['data'] = $users_response;
            $this->error = [];
            $this->sendResponse();
            return;
        } else {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Internal Server Error";
            $this->error = array('message'=>'Internal Server Error!!');
            $this->sendResponse();
            return;
        }
    }

    function doctor_list() {
        /**
         * @api {post} /api/doctor/doctor_list Doctor List
         * @apiName Doctor List
         * @apiGroup Doctor
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  Phlebo Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
          {
    "message": "Doctors information has been retrive Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "docdata": [
            {
                "doctor_id": "1",
                "doctor_name": "Test Doctor1",
                "doctor_mobile": "1234545678",
                "city_id": "1",
                "city_name": "Kolhapur",
                "speciality_id": "1",
                "speciality": "Speciality 1",
                "clinic_address": "Test Address"
            }
        ],
        "request_id": 1617285133.644452
    }
}
        */
        #collect required information
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        #check sc code 
        $DoctorInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'doctor',[],'update_dt DESC,doctor_id DESC');

        $doctor_records = [];
        $temp_data  =   [];
        if(count($DoctorInfo)) {

            foreach ($DoctorInfo as $key => $value) {
                
                $city_name = '';
                $city_id  = '';
                if(!empty($value->city_id)) {
                    $CityInfo = $this->mdl_request->get_records(['city_id'=> $value->city_id], 'cities', ['city_name']);

                    if(!count($CityInfo)) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Doctor city information no available";
                        $this->error = array('message'=>'Doctor city information no available');
                        $this->sendResponse();
                        return;
                    }
                    $city_name  =   $CityInfo[0]->city_name; 
                    $city_id    =   $value->city_id;
                }

                $state_name = '';
                $state_id  = '';
                if(!empty($value->state_id)) {
                    $StateInfo = $this->mdl_request->get_records(['state_id'=> $value->state_id], 'state', ['state_name']);

                    if(!count($StateInfo)) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Doctor state information no available";
                        $this->error = array('message'=>'Doctor state information no available');
                        $this->sendResponse();
                        return;
                    }
                    $state_name  =   $StateInfo[0]->state_name; 
                    $state_id    =   $value->state_id;
                }
                

                $SpecialityInfo = $this->mdl_request->get_records(['speciality_id'=> $value->speciality_id], 'speciality', ['speciality']);

                if(!count($SpecialityInfo)) {
                    $this->response['code'] = 400;
                    $this->response['status'] = 'error';
                    $this->response['message'] = "Doctor speciality information no available";
                    $this->error = array('message'=>'Doctor speciality information no available');
                    $this->sendResponse();
                    return;
                }
                $speciality  =   $SpecialityInfo[0]->speciality;

                $docPatient_count = $this->mdl_request->get_doctor_patient_count($value->doctor_id);

                $patient_count  =   $docPatient_count[0]->patient_count;

                $temp_data['doctor_id'] =   $value->doctor_id;
                $temp_data['doctor_name']   =   $value->doctor_name;
                $temp_data['doctor_mobile'] =   !empty($value->doctor_mobile)?$value->doctor_mobile:'';
                $temp_data['city_id'] =   $city_id;
                $temp_data['city_name'] =   $city_name;
                 $temp_data['state_id'] =   $state_id;
                $temp_data['state_name'] =   $state_name;
                $temp_data['speciality_id'] =   $value->speciality_id;
                $temp_data['speciality'] =   $speciality;
                $temp_data['clinic_address'] =   !empty($value->clinic_address)?$value->clinic_address:'';
                $temp_data['patient_count'] =   $patient_count;

                array_push($doctor_records, $temp_data);
            }

        }


        $users_response = [];
        $users_response['docdata'] = $doctor_records;

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = "Doctors information has been retrive Successfully!!!";
        $this->response['data'] = $users_response;
        $this->error = [];
        $this->sendResponse();
        return;
    }    


    function knowledge_world() {
        /**
         * @api {post} /api/doctor/knowledge_world Knowledge World List
         * @apiName Knowledge World List
         * @apiGroup Doctor
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  Phlebo Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
         {
    "message": "Knowledge Word information has been retrive Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "kwinfo": [
            {
                "type": "PDF",
                "data": [
                    {
                        "title": "Testing",
                        "link": "http://localhost/ipca_sahyog_app/uploads/knowledge_world/1/1/files/f2841f6239a8ded58e7f42b1bdf65933.pdf",
                        "description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                        "display_image": "http://localhost/ipca_sahyog_app/assets/images/pdf_file.png",
                        "duration": "0",
                        "insert_date": "2021-04-06"
                    }
                ]
            },
            {
                "type": "Audio",
                "data": [
                    {
                        "title": "Testing Audio",
                        "link": "http://localhost/ipca_sahyog_app/uploads/knowledge_world/1/5/files/8e6d9ec44417eb0236637698114e0b40.mp3",
                        "description": "Testtttttttttttttttttt",
                        "display_image": "http://localhost/ipca_sahyog_app/assets/images/audio.png",
                        "duration": "00:00:27",
                        "insert_date": "2021-04-03"
                    }
                ]
            },
            {
                "type": "Video",
                "data": [
                    {
                        "title": "Test Video",
                        "link": "http://localhost/ipca_sahyog_app/uploads/knowledge_world/1/6/files/ae9559997dcad788022dc7ec1797f2c6.avi",
                        "description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                        "display_image": "http://localhost/ipca_sahyog_app/assets/images/video.png",
                        "duration": "00:00:30",
                        "insert_date": "2021-04-03"
                    },
                    {
                        "title": "Testing video2",
                        "link": "http://localhost/ipca_sahyog_app/uploads/knowledge_world/1/6/files/a121681461cc6f03fd217488e031814d.mov",
                        "description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                        "display_image": "http://localhost/ipca_sahyog_app/assets/images/video.png",
                        "duration": "00:02:06",
                        "insert_date": "2021-04-03"
                    }
                ]
            },
            {
                "type": "Images",
                "data": [
                    {
                        "title": "Testing",
                        "link": "http://localhost/ipca_sahyog_app/uploads/knowledge_world/1/7/files/6873be668bd9100980762746f42c0afb.jpg",
                        "description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                        "display_image": "http://localhost/ipca_sahyog_app/assets/images/images_file.png",
                        "duration": "0",
                        "insert_date": "2021-04-06"
                    }
                ]
            }
        ],
        "request_id": 1617806119.576527
    }
}
        */
        #collect required information
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        #check sc code 
        $DocumentType = $this->mdl_request->get_records(['is_deleted' => '0'], 'document_type');

        $rsdata  = [];
        $kw_data    =   [];
        $users_response = [];
        foreach ($DocumentType as $key => $value) {
            
            #get all the document from table
            //$rsRecord = $this->mdl_request->get_records(['document_type_id' => $value->document_type_id, 'is_deleted' => '0'], 'knowledge_world');

            $rsRecord   =   $this->mdl_request->getActiveKnowledgeword($value->document_type_id);

            $doctype_array  =   [];
            if(!empty($rsRecord)) {
                
                foreach ($rsRecord as $k => $v) {
                    $temparray['knowledge_world_id'] =   $v->knowledge_world_id;
                    $temparray['title'] =   $v->title;
                    $temparray['link']  =   base_url().$v->upload_path;
                    $temparray['description']  =   $v->description;
                    $temparray['display_image']  =   base_url().$v->display_image;
                    $temparray['duration']  =  $v->duration;
                    $temparray['insert_date']  =  date('Y-m-d',strtotime($v->insert_dt));

                    array_push($doctype_array, $temparray);
                }
            }
           // if(!empty($doctype_array)) {
                $kw_data['type']    =   $value->document_type;
                $kw_data['data']    = $doctype_array;  

                array_push($rsdata, $kw_data);  
            //}
        }

       
        $users_response = [];
        $users_response['kwinfo'] = $rsdata;  
        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = "Knowledge Word information has been retrive Successfully!!!";
        $this->response['data'] = $users_response;
        $this->error = [];
        $this->sendResponse();
        return;
    }    
    
    function send_knowledge_world(){
 /**
         * @api {post} /api/doctor/send_knowledge_world Knowledge World List
         * @apiName Knowledge World List
         * @apiGroup Doctor
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         
         * @apiparam {Number}   knowledge_world_id knowledge world id
         * @apiParam {Number}   id[1,2,3]  Patient id
 
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  Phlebo Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
         
        */
        #collect required information
        $users_id = $this->id;
        $patient_ids = isset($this->input_data['patient_id'])? $this->input_data['patient_id']: '';
        $knowledge_world_id = trim(isset($this->input_data['knowledge_world_id'])? $this->input_data['knowledge_world_id']:'');
        //echo "<pre>"; print_r($knowledge_world_id);exit;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        #check patient id 
        if(empty($patient_ids)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Not Selected Patients.. Please Select Patients";
            $this->error = array('message'=>'Not Selected Patients.. Please Select Patients');
            $this->sendResponse();
            return;
        } 
        if(empty($knowledge_world_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Not Selected Knowleage World.. Please Select Knowleage World";
            $this->error = array('message'=>'Not Selected Knowleage World.. Please Select Knowleage World');
            $this->sendResponse();
            return;
        }

        $knowledge_worldInfo = $this->mdl_request->get_records(['knowledge_world_id'=> $knowledge_world_id], 'knowledge_world');

        if(!count($knowledge_worldInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Knowledge World information no available";
            $this->error = array('message'=>'Knowledge World information no available');
            $this->sendResponse();
            return;
        } 

        $link = base_url().$knowledge_worldInfo[0]->upload_path;

       // $patient_id_arrs = explode (",", $patient_ids); 

        foreach ($patient_ids as $patient_id) {
          
            $PatientInfo = $this->mdl_request->get_records(['patient_id'=> $patient_id], 'patient',['patient_name','patient_mobile_no']);
             #define patient data
             if($PatientInfo){
              
                $patient_name = $PatientInfo[0]->patient_name;
                $patient_mobile_no = $PatientInfo[0]->patient_mobile_no;
                
                $insert_request['users_id']     =   $users_id;
                $insert_request['patient_id']     =   $patient_id;
                $insert_request['patient_mobile']     =   $patient_mobile_no;
                $insert_request['knowledge_world_id']   =   $knowledge_world_id;
                #Insert Patient Request knowledge
             
                $kw_request_id = $this->mdl_request->_insert($insert_request, 'knowledge_world_request');

                #send sms for patient
            if(!empty($patient_mobile_no)) {
                
		        $tiny_url = !empty(tiny_url($link)) ? tiny_url($link) : NULL;
		        $link_url = !empty($tiny_url) ? $tiny_url : $link;
               
                 $patient_knowledge_sms =  "Dear $patient_name, Your knowledge world file $link_url . here click";
                $knowledge_sms = send_sms($patient_mobile_no,$patient_knowledge_sms,'KNOWLEDGEWORD'); 

                if($knowledge_sms){

                    $update_request['sms_send'] =  '1';
                    $update_request['sms_body'] =  $patient_knowledge_sms;

                    $this->mdl_request->_update(['kw_request_id' => $kw_request_id], $update_request, 'knowledge_world_request');
                }  
             }
             

           }
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = "Knowledge Word information sms send has been Successfully!!!";
        $this->error = [];
        $this->sendResponse();
        return;
    }
}
?>
