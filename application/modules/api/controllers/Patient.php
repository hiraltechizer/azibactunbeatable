<?php
class Patient extends Api_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('api/mdl_request');
        $this->load->helper('send_sms');
	}
   
	function add_patient() {
		/**
		 * @api {post} /api/patient/add_patient Add Patient
		 * @apiName Add Patient
		 * @apiGroup Patient
		 * @apiVersion 1.0.0
		 * @apiParam {String} Token 
		 * @apiParam {String} patient_name Patient Name
         * @apiParam {String} patient_type Patient Mobile Number
         * @apiParam {Numeric} patient_mobile_no Patient Mobile No
         * @apiParam {Numeric} patient_age Patient Age
         * @apiParam {Varchar} address Patient Address
         * @apiParam {Numeric} city_id Patient City
         * @apiParam {Numeric} pincode Patient pincode
         * @apiParam {array} brand_data Brand Data
         * @apiParam {string} psa_test PSA Test 
         * @apiParam {string} coupon_code Coupon Code
		 *
		 * @apiSuccess {Number} code HTTP Status Code.
		 * @apiSuccess {String} message  Associated Message.
		 * @apiSuccess {Object} data  
		 * @apiSuccess {Object} error  Error if Any.
		 *
		 *@apiExample sample input:
		 *	{"patient_name":"Test Patient","patient_type":"New","patient_mobile_no":"9803930111","patient_age":"26","address":"Test Addresss","city_id":"1","doctor_id":"1","pincode":"123456","brand_data":[{"id":"1","repurchase_date":"2021-03-14","revisit_date":"2021-03-15"},{"id":"2","repurchase_date":"2021-03-15","revisit_date":"2021-03-16"}],"psa_test":"Yes","coupon_code":"COP001"}
		 *
		 * @apiSuccessExample Success-Response:
			*     HTTP/1.1 200 OK
			* 
			{
    "message": "Patient has been added Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "request_id": 1617110864.460834
    }
}
		*/
		#collect required information
        $patient_name       = trim(isset($this->input_data['patient_name'])?$this->input_data['patient_name']:'');
        $patient_type       = trim(isset($this->input_data['patient_type'])?$this->input_data['patient_type']:'');
        $patient_mobile_no  = trim(isset($this->input_data['patient_mobile_no'])?$this->input_data['patient_mobile_no']:'');
        $patient_age        = trim(isset($this->input_data['patient_age'])?$this->input_data['patient_age']:'');
        $address            = trim(isset($this->input_data['address'])?$this->input_data['address']:'');
        $city_id            = trim(isset($this->input_data['city_id'])?$this->input_data['city_id']:'');
        $language_id        = trim(isset($this->input_data['language_id'])?$this->input_data['language_id']:1);
        $pincode            = trim(isset($this->input_data['pincode'])?$this->input_data['pincode']:'');
        $doctor_id          = trim(isset($this->input_data['doctor_id'])?$this->input_data['doctor_id']:'');
        $psa_test           = trim(isset($this->input_data['psa_test'])?$this->input_data['psa_test']:'No');
        //$psa_report          = trim(isset($this->input_data['psa_test'])?$this->input_data['psa_report']:'No');
        $coupon_code        = trim(isset($this->input_data['coupon_code'])?$this->input_data['coupon_code']:'');
        $brand_data         = isset($this->input_data['brand_data'])?$this->input_data['brand_data']:[];
        $users_id           = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id','users_city_id','users_parent_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;
        $division_id = $UsersInfo[0]->users_division_id;
        $users_name     =   $UsersInfo[0]->users_name;
        $users_mobile   =   $UsersInfo[0]->users_mobile;
        $users_city_id  =   $UsersInfo[0]->users_city_id;
        $users_parent_id    =   $UsersInfo[0]->users_parent_id;

        #Check Logged in User Type for MR
        if($users_type != 'MR') {
        	$this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Invalid Authentication";
            $this->error = array('message'=>'Invalid Authentication');
            $this->sendResponse();
            return;
        }

        #Check patient_name value 
        if(empty($patient_name)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Patient Name";
            $this->error = array('message'=>'Please Enter Patient Name');
            $this->sendResponse();
            return;
        }

        #validate doctor name
        if(!preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $patient_name)) {
             $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valide Patient Name";
            $this->error = array('message'=>'Please Enter Valide Patient Name');
            $this->sendResponse();
            return;            
        }

        #Check patient type
        if(empty($patient_type)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Patient Type";
            $this->error = array('message'=>'Please Enter Patient Type');
            $this->sendResponse();
            return;
        }

        #check patient type in array
        if(!in_array($patient_type, ['New','Old'])) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valid Patient Type";
            $this->error = array('message'=>'Please Enter Valid Patient Type');
            $this->sendResponse();
            return;
        }

        #Check patient mobile number
        if(empty($patient_mobile_no)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Patient Mobile Number";
            $this->error = array('message'=>'Please Enter Patient Mobile Number');
            $this->sendResponse();
            return;
        }

        #validate patient mobile
       if(! preg_match('/^[1-9][0-9]{9}$/', $patient_mobile_no)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valid Patient Mobile Number";
            $this->error = array('message'=>'Please Enter Valid Patient Mobile Number');
            $this->sendResponse();
            return;
        }

        #Check for patient age
        if(empty($patient_age)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Patient Age.";
            $this->error = array('message'=>'Please Enter Patient Age.');
            $this->sendResponse();
            return;
        }

        if(empty($brand_data)){
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Brand Data is Mandatory";
            $this->error = array('message'=>'Brand Data is Mandatory!');
            $this->sendResponse();
            return;
        }

        #check mobile number exits or not
        $PatientInfo = $this->mdl_request->get_records(['patient_mobile_no' => $patient_mobile_no,'is_deleted' => '0'], 'patient', ['patient_id']);

        #If Patient record found then show error
        if(count($PatientInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Patient Mobile Number Already Exist.";
            $this->error = array('message'=>'Patient Mobile Number Already Exist.');
            $this->sendResponse();
            return;   
        }

         #check doctor exits or not
        $DoctorInfo = $this->mdl_request->get_records(['doctor_id'=> $doctor_id], 'doctor', ['doctor_id','doctor_name']);

        #If Speciality record not found then show error
        if(!count($DoctorInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected Doctor Information not available.";
            $this->error = array('message'=>'Selected Doctor Information not available.');
            $this->sendResponse();
            return;   
        }

         #check city exits or not
        $CityInfo = $this->mdl_request->get_records(['city_id'=> $city_id], 'cities', ['city_id']);

        #If Speciality record not found then show error
        if(!count($CityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected City Information not available.";
            $this->error = array('message'=>'Selected City Information not available.');
            $this->sendResponse();
            return;   
        } 

        foreach ($brand_data as $key => $value) {

            if(empty($value['id'])) {
                $this->response['code'] = 400;
                $this->response['status'] = 'error';
                $this->response['message'] = "Please select brand.";
                $this->error = array('message'=>'Please select brand.');
                $this->sendResponse();
                return;   
            }

                    /*if(empty($value['strength_id'])) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Please select strength.";
                        $this->error = array('message'=>'Please select strength.');
                        $this->sendResponse();
                        return;   
                    }*/

            if(empty($value['repurchase_date'])) {
                $this->response['code'] = 400;
                $this->response['status'] = 'error';
                $this->response['message'] = "Please select Repurchase Date.";
                $this->error = array('message'=>'Please select Repurchase Date.');
                $this->sendResponse();
                return;   
            }

            $current_date = date('Y-m-d');
            if($value['repurchase_date'] < $current_date) {


                $this->response['code'] = 400;
                $this->response['status'] = 'error';
                $this->response['message'] = "Past repurchase date is not allow.";
                $this->error = array('message'=>'Past repurchase date is not allow.');
                $this->sendResponse();
                return;
            }

            if(empty($value['revisit_date'])) {
                $this->response['code'] = 400;
                $this->response['status'] = 'error';
                $this->response['message'] = "Please select Revisit Date.";
                $this->error = array('message'=>'Please select Revisit Date.');
                $this->sendResponse();
                return;   
            }

            if($value['revisit_date'] < $current_date) {


                $this->response['code'] = 400;
                $this->response['status'] = 'error';
                $this->response['message'] = "Past revisit date is not allow.";
                $this->error = array('message'=>'Past revisit date is not allow.');
                $this->sendResponse();
                return;
            }

        }  

        #users cityinfo
        $usersCityInfo  = $this->mdl_request->get_records(['city_id' => $users_city_id], 'city', ['city_id','city_name']);

        if(empty($usersCityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Users city information not available.";
            $this->error = array('message'=>'Users city information not available.');
            $this->sendResponse();
            return;   
        }   

        #prepare doctor data
        $insert_data['patient_code']        =   $this->mdl_request->get_patient_unique_code();
        $insert_data['patient_name']        =   $patient_name;
        $insert_data['patient_type']        =   $patient_type;
        $insert_data['patient_mobile_no']   =   $patient_mobile_no;
        $insert_data['patient_age']         =   $patient_age;
        $insert_data['address']             =   $address;
        $insert_data['city_id']             =   $city_id;
        $insert_data['pincode']             =   $pincode;
        $insert_data['doctor_id']           =   $doctor_id;
        $insert_data['users_id']            =   $users_id;
        $insert_data['psa_test']            =   $psa_test;
        $insert_data['coupon_code']         =   $coupon_code;
        $insert_data['language_id']         =   $language_id;
       
        #Insert Doctor Data
        $patient_id = $this->mdl_request->_insert($insert_data, 'patient');

        

        #users cityname
        $users_city_name    =   $usersCityInfo[0]->city_name;

        if($patient_id) {

            #insert brand brand data
            if(count($brand_data)) {

                #insert data
                foreach ($brand_data as $key => $value) {

                   
                    $brand_id   =   (int) $value['id'];
                   // $strength_id   =  (int) $value['strength_id'];

                    #check already info available or not
                    //$patient_subInfo = $this->mdl_request->get_records(['patient_id' => $patient_id, 'brand_id' => $brand_id, 'strength_id' => $strength_id], 'patient_sub',['patient_sub_id'], '', 1);

                    $patient_subInfo = $this->mdl_request->get_records(['patient_id' => $patient_id, 'brand_id' => $brand_id], 'patient_sub',['patient_sub_id'], '', 1);

                    if(!count($patient_subInfo)) {
                        $in_data['patient_id']      =   $patient_id;
                        $in_data['doctor_id']       =   $doctor_id;
                        $in_data['brand_id']        =   $brand_id;
                        $in_data['strength_id']     =   NULL;
                        $in_data['repurchase_date'] =   $value['repurchase_date'];
                        $in_data['revisit_date']    =   $value['revisit_date'];
                        $in_data['psa_test']        =   $psa_test;
                        $in_data['coupon_code']     =   $coupon_code;

                        $patient_sub_id = $this->mdl_request->_insert($in_data, 'patient_sub');

                        $patient_cycle = $this->mdl_request->initiate_cycle($patient_id, $patient_sub_id, $brand_id);    
                    }                    
                }
            }

            #insert test coupon info
            $insert_test_dt = [];
            $insert_test_dt['patient_id']   =   $patient_id;
            $insert_test_dt['psa_test']     =   $psa_test;
            $insert_test_dt['psa_report']     =   'No';
            $insert_test_dt['coupon_code']  =   $coupon_code;

            $patient_test_id = $this->mdl_request->_insert($insert_test_dt, 'patient_test');

            $doctor_name = $DoctorInfo[0]->doctor_name;
            #send sms to patient
            //$patient_sms_text = "Dear $patient_name, you have successfully enrolled under Sahyog Campaign and eligible to avail the benefit of therapy adherence.";

            $patient_sms_text = "Dear $patient_name, you have successfully enrolled under Sahyog and eligible to avail the benefit of therapy adherence.
Regards,
Ipca Uro";

            $psms_ret =  send_sms($patient_mobile_no,$patient_sms_text, 'ENROLL-SMS-SEND-PATIENT');

            #send sms to MR
            if(!empty($users_mobile)) {
               // $mr_sms_text    =   "Dear $users_name, you have successfully enrolled $patient_name under Sahyog Campaign.";
               $mr_sms_text    =   "Dear $users_name, you have successfully enrolled Patient $patient_name from Dr. $doctor_name under Sahyog.
Regards,
Ipca Uro";

                $tmsms_ret =  send_sms($users_mobile,$mr_sms_text, 'ENROLL-SMS-SEND-TM');     
            }
           
            #send sms to ASM
            $asmInfo = $this->mdl_request->get_records(['users_id' => $users_parent_id],'manpower',['users_name','users_mobile','users_parent_id']);
            $asm_name   =   $asmInfo[0]->users_name;
            $asm_mobile     =   $asmInfo[0]->users_mobile;
            $asm_parent_id  =   $asmInfo[0]->users_parent_id;
            $patient_count = '1';
            #send sms to ASM
//             if(!empty( $asm_mobile)) {
//                 //$asm_sms_text    =   "Dear $asm_name, Let’s congratulate $users_name for enrolling 1 patient from $users_city_name under Sahyog Campaign.";

//                 $asm_sms_text    =   "Dear $asm_name, Let's congratulate TM $users_name for enrolling $patient_count patient from Dr. $doctor_name under Sahyog.
// Regards,
// Ipca Uro";

//                 $asmsms_ret =  send_sms($asm_mobile,$asm_sms_text, 'ENROLL-SMS-SEND-ASM');     
//             }

            #send sms to RSM
            $rsmInfo = $this->mdl_request->get_records(['users_id' => $asm_parent_id],'manpower',['users_name','users_mobile','users_parent_id']);
            $rsm_name   =   $rsmInfo[0]->users_name;
            $rsm_mobile     =   $rsmInfo[0]->users_mobile;
            $rsm_parent_id  =   $rsmInfo[0]->users_parent_id;

            #send sms to RSM
            if(!empty($rsm_mobile)) {
                //$rsm_sms_text    =   "Dear $rsm_name, Let’s congratulate $users_name for enrolling 1 patient from $users_city_name under Sahyog Campaign.";
                
                $rsm_sms_text    =   "Dear $rsm_name, Let's congratulate TM $users_name for enrolling $patient_count patient from Dr. $doctor_name under Sahyog.
Regards,
Ipca Uro";

            $rsmsms_ret =  send_sms($rsm_mobile,$rsm_sms_text, 'ENROLL-SMS-SEND-RSM');    
            }
            

            #send sms to ZSM
            $zsmInfo = $this->mdl_request->get_records(['users_id' => $rsm_parent_id],'manpower',['users_name','users_mobile','users_parent_id']);
            $zsm_name   =   $zsmInfo[0]->users_name;
            $zsm_mobile     =   $zsmInfo[0]->users_mobile;
            $zsm_parent_id  =   $zsmInfo[0]->users_parent_id;

            #send sms to ZSM
            if(!empty($zsm_mobile)) {
               // $zsm_sms_text    =   "Dear $zsm_name, Let’s congratulate $users_name for enrolling 1 patient from $users_city_name under Sahyog Campaign.";

                $zsm_sms_text    =   "Dear $zsm_name, Let's congratulate TM $users_name for enrolling $patient_count patient from Dr. $doctor_name under Sahyog.
Regards,
Ipca Uro";

            $zsmsms_ret =  send_sms($zsm_mobile,$zsm_sms_text, 'ENROLL-SMS-SEND-ZSM');    
            }
            
        }

        $users_response = [];
        
        #check userinfo exits or not
		if(!empty($patient_id)) {
			$this->response['code'] = 200;
			$this->response['status'] = 'Success';
			$this->response['message'] = "Patient has been added Successfully!!!";
			$this->response['data'] = $users_response;
			$this->error = [];
			$this->sendResponse();
			return;
		} else {
			$this->response['code'] = 400;
			$this->response['status'] = 'error';
			$this->response['message'] = "Internal Server Error";
			$this->error = array('message'=>'Internal Server Error!!');
			$this->sendResponse();
			return;
		}
	}

    function patient_list() {
        /**
         * @api {post} /api/patient/patient_list patient List
         * @apiName Patient List
         * @apiGroup Patient
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  Patient Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
         {
    "message": "Patient information has been retrive Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "patient_data": [
            {
                "patient_id": "1",
                "patient_name": "Test Patient",
                "patient_type": "New",
                "patient_age": "26",
                "patient_mobile_no": "3243456556",
                "city_name": "Kolhapur"
            }
        ],
        "request_id": 1617111189.612841
    }
}
        */
        #collect required information
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        #check sc code 
        $PatientInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'patient',[],'patient_id DESC');

        $patient_records = [];
        $patientInfo = [];
        if(count($PatientInfo)) {

            foreach ($PatientInfo as $key => $value) {
                #get cityname
                $CityInfo = $this->mdl_request->get_records(['city_id' => $value->city_id], 'cities', ['city_name']);

                if(empty($CityInfo)) {
                    $this->response['code'] = 400;
                    $this->response['status'] = 'error';
                    $this->response['message'] = "Selected Patient City Information not available.";
                    $this->error = array('message'=>'Selected Patient City Information not available.');
                    $this->sendResponse();
                    return;
                }

                $DoctorInfo = $this->mdl_request->get_records(['doctor_id' => $value->doctor_id], 'doctor', ['doctor_id','doctor_name']);

                $patient_records['patient_id']      =   $value->patient_id;
                $patient_records['patient_name']    =   $value->patient_name;
                $patient_records['patient_type']    =   $value->patient_type;
                $patient_records['patient_age']     =   $value->patient_age;
                $patient_records['patient_mobile_no']   =   $value->patient_mobile_no;
                $patient_records['city_name']       =   $CityInfo[0]->city_name;
                $patient_records['doctor_id']       =   $DoctorInfo[0]->doctor_id;
                $patient_records['doctor_name']       =   $DoctorInfo[0]->doctor_name;


                array_push($patientInfo, $patient_records);
            }
        }     
        
        $users_response = [];
        $users_response['patient_data'] = $patientInfo;

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = "Patient information has been retrive Successfully!!!";
        $this->response['data'] = $users_response;
        $this->error = [];
        $this->sendResponse();
        return;
    }  

    function add_cycle() {
        /**
         * @api {post} /api/patient/add_cycle Add Cycle
         * @apiName Add Cycle
         * @apiGroup Patient
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         * @apiParam {array} brand_data Brand Data
         * @apiParam {string} psa_test PSA Test 
         * @apiParam {string} coupon_code Coupon Code
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {"patient_id":"2","brand_data":[{"id":"1","repurchase_date":"2021-03-27","revisit_date":"2021-03-15"}],"psa_test":"Yes","coupon_code":"COP001","psa_report":"Yes","comment":"Test Comment"}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
           {
    "message": "Patient Cycle has been added Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "request_id": 1617625178.73221
    }
}
        */
        $patient_id         = trim(isset($this->input_data['patient_id'])?$this->input_data['patient_id']:'');
        $brand_data         = isset($this->input_data['brand_data'])?$this->input_data['brand_data']:[];
        $psa_test           = trim(isset($this->input_data['psa_test'])?$this->input_data['psa_test']:'No');
        $coupon_code        = trim(isset($this->input_data['coupon_code'])?$this->input_data['coupon_code']:'');
        $psa_report         = trim(isset($this->input_data['psa_report'])?$this->input_data['psa_report']:'No');
        $comment            = trim(isset($this->input_data['comment'])?$this->input_data['comment']:'');
        $users_id           = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);


        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;
        $division_id = $UsersInfo[0]->users_division_id;

        #Check Logged in User Type for MR
        if($users_type != 'MR') {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Invalid Authentication";
            $this->error = array('message'=>'Invalid Authentication');
            $this->sendResponse();
            return;
        }

        #check patient id
        if(empty($patient_id)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please select patient id";
            $this->error = array('message'=>'Please select patient id');
            $this->sendResponse();
            return;   
        }

        if(empty($brand_data)) {

            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please select brand data";
            $this->error = array('message'=>'Please select brand data');
            $this->sendResponse();
            return; 
        }

        #check patient Data
        $patientInfo = $this->mdl_request->get_records(['patient_id' => $patient_id], 'patient', ['patient_name','doctor_id'], 'patient_id DESC', 1);

        if(empty($patientInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Selected patient information not available";
            $this->error = array('message'=>'Selected patient information not available');
            $this->sendResponse();
            return;
        }

        $psubIds = [];
        if(count($brand_data)) {

            #insert data
            foreach ($brand_data as $key => $value) {
                 if(empty($value['id'])) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Please select brand.";
                        $this->error = array('message'=>'Please select brand.');
                        $this->sendResponse();
                        return;   
                    }

                    /*if(empty($value['strength_id'])) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Please select strength.";
                        $this->error = array('message'=>'Please select strength.');
                        $this->sendResponse();
                        return;   
                    }*/

                    if(empty($value['repurchase_date'])) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Please select Repurchase Date.";
                        $this->error = array('message'=>'Please select Repurchase Date.');
                        $this->sendResponse();
                        return;   
                    }

                    if(empty($value['revisit_date'])) {
                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Please select Revisit Date.";
                        $this->error = array('message'=>'Please select Revisit Date.');
                        $this->sendResponse();
                        return;   
                    }

                $brand_id   =   (int) $value['id'];

                $RsInfo = $this->mdl_request->get_records(['brand_id' => $brand_id,'patient_id'=> $patient_id], 'patient_sub', ['repurchase_date'], 'patient_sub_id DESC', 1);

                if(count($RsInfo)) {

                    $repurchase_date    =   $RsInfo[0]->repurchase_date;

                    $r_purchasedate = date("Y-m-d",strtotime($repurchase_date));

                    if($r_purchasedate == $value['repurchase_date']) {


                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Repurchase date can not be same. Please select anouther repurchase date";
                        $this->error = array('message'=>'Repurchase date can not be same. Please select anouther repurchase date');
                        $this->sendResponse();
                        return;
                    } else if($value['repurchase_date'] < $r_purchasedate) {


                        $this->response['code'] = 400;
                        $this->response['status'] = 'error';
                        $this->response['message'] = "Repurchase date can not be less then previous repurchase date. Please select anouther repurchase date";
                        $this->error = array('message'=>'Repurchase date can not be less then previous repurchase date. Please select anouther repurchase date');
                        $this->sendResponse();
                        return;
                    }
                }

                $doctor_id  =   $patientInfo[0]->doctor_id;

                $in_data['patient_id']      =   $patient_id;
                $in_data['doctor_id']       =   $doctor_id;
                $in_data['brand_id']        =   $brand_id;
                $in_data['strength_id']     =   NULL;
                $in_data['repurchase_date'] =   $value['repurchase_date'];
                $in_data['revisit_date']    =   $value['revisit_date'];
                $in_data['psa_test']        =   $psa_test;
                $in_data['coupon_code']     =   $coupon_code;
                $in_data['psa_report']      =   $psa_report;
                $in_data['comment']         =   $comment;

                $patient_sub_id = $this->mdl_request->_insert($in_data, 'patient_sub');

                $patient_cycle = $this->mdl_request->initiate_cycle($patient_id, $patient_sub_id, $brand_id); 

                array_push($psubIds, $patient_sub_id);  
            }

            $insert_test_dt = [];
            $insert_test_dt['patient_id']      =   $patient_id;
            $insert_test_dt['psa_test']     =   $psa_test;
            $insert_test_dt['coupon_code']  =   $coupon_code;
            $insert_test_dt['psa_report']     =   $psa_report;
            $insert_test_dt['comment']  =   $comment;

            $patient_test_id = $this->mdl_request->_insert($insert_test_dt, 'patient_test');
        }
        $users_response = [];
        
        //print_r($psubIds);die;
        #check userinfo exits or not
        if(!empty($psubIds)) {
            $this->response['code'] = 200;
            $this->response['status'] = 'Success';
            $this->response['message'] = "Patient Cycle has been added Successfully!!!";
            $this->response['data'] = $users_response;
            $this->error = [];
            $this->sendResponse();
            return;
        } else {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Internal Server Error";
            $this->error = array('message'=>'Internal Server Error!!');
            $this->sendResponse();
            return;
        }
    }  

    function get_patient_journey(){

         /**
         * @api {post} /api/patient/get_patient_journey Patient Jouney
         * @apiName Patient Jouney
         * @apiGroup Patient
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  Patient Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {"patient_id":1}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
       {
    "message": "Patient information has been retrive Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "patient_data": {
            "patient_id": "1",
            "patient_name": "Test Patient",
            "patient_type": "New",
            "patient_age": "26",
            "patient_mobile_no": "9427160299",
            "address": "Test Addresss",
            "pincode": "123456",
            "doctor_name": "Doctor12",
            "city_name": "Kolhapur",
            "cycle_data": [
                {
                    "cycle_no": "1",
                    "brand_data": [
                        {
                            "brand_id": "1",
                            "strength_id": null,
                            "repurchase_date": "14/03/2021",
                            "revisit_date": "15/03/2021"
                        },
                        {
                            "brand_id": "2",
                            "strength_id": null,
                            "repurchase_date": "15/03/2021",
                            "revisit_date": "16/03/2021"
                        }
                    ],
                    "psa_test": "Yes",
                    "psa_report": "No",
                    "coupon_code": "COP001",
                    "comment": null
                }
            ]
        },
        "request_id": 1617973965.587933
    }
}
        */
        #collect required information
        $patient_id  = trim(isset($this->input_data['patient_id'])?$this->input_data['patient_id']:'');
        $users_id = $this->id;

        if(empty($patient_id)) {
             $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please select patient id";
            $this->error = array('message'=>'Please select patient id');
            $this->sendResponse();
            return; 
        }

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type = $UsersInfo[0]->users_type;

        #check sc code 
        $PatientInfo = $this->mdl_request->get_records(['patient_id' => $patient_id,'users_id' => $users_id, 'is_deleted' => '0'], 'patient');

       
        $doctor_name = '';
        $city_name = '';
        $patient_name   =   '';
        $patient_type   =   '';
        $patient_mobile_no  =   '';
        $patient_age        =   '';
        $address    =   '';
        $pincode = '';
        if(count($PatientInfo)) {

            $patient_name   =   $PatientInfo[0]->patient_name;
            $patient_type   =   $PatientInfo[0]->patient_type;
            $patient_mobile_no  =   $PatientInfo[0]->patient_mobile_no;
            $patient_age        =   $PatientInfo[0]->patient_age;
            $address            =   $PatientInfo[0]->address;
            $pincode            =   $PatientInfo[0]->pincode;
            $city_id = $PatientInfo[0]->city_id;
            $doctor_id = $PatientInfo[0]->doctor_id;

            #cityInfo
            $cityInfo = $this->mdl_request->get_records(['city_id' => $city_id], 'cities', ['city_name']);

            if(count($cityInfo)) {
                $city_name = $cityInfo[0]->city_name;
            }

            #doctorInfo
            $doctorInfo = $this->mdl_request->get_records(['doctor_id' => $doctor_id], 'doctor', ['doctor_name']);

            if(count($doctorInfo)) {
                $doctor_name = $doctorInfo[0]->doctor_name;
            }            
        }

        #get distinct cycle no for patient
        $CycleNOInfo = $this->mdl_request->get_records(['patient_id' => $patient_id], 'cycle', ['DISTINCT(cycle_no) as cycle_no']);

        $patient_records = [];
        $patientInfo = [];


        if(count($CycleNOInfo)) {
            foreach ($CycleNOInfo as $key => $val) {
                
                $temp_data['cycle_no']  =   $cycle_no =   $val->cycle_no;

                #get brand info by cycle no
                $rsbrandIdinfo = $this->mdl_request->get_records(['cycle_no' => $cycle_no, 'patient_id' => $patient_id], 'cycle', ['GROUP_CONCAT(brand_id) as brand_id']);


                $brand_ids = [];
                if(count($rsbrandIdinfo)) {
                    $brand_ids  =   explode(",", $rsbrandIdinfo[0]->brand_id);
                }

                #get brand Information
                $RsRecord = [];
                if(!empty($brand_ids)) {
                    
                    $RsRecord = $this->mdl_request->getAllBrandInfo($brand_ids, $patient_id, $cycle_no);


                   // print_r($this->db->last_query());   die;          
                    //$RsRecord = $this->mdl_request->get_records(['brand_id' => $brand_ids,'patient_id' => $patient_id],'patient_sub', ['brand_id', 'strength_id','DATE_FORMAT(repurchase_date,"%d/%m/%Y") as repurchase_date','DATE_FORMAT(revisit_date,"%d/%m/%Y") as revisit_date']); 
                }
                
                $temp_data['brand_data']  = $RsRecord;
                
                #get latest test info
                $testInfo = $this->mdl_request->get_records(['patient_id' => $patient_id], 'patient_test', ['psa_test','psa_report','coupon_code','comment'],'patient_test_id DESC', 1);

                $psa_test   = 'No';
                $psa_report =   'No';
                $coupon_code    =   '';
                $comment   = '';
                if(count($testInfo)) {

                    $psa_test   =   $testInfo[0]->psa_test;
                    $psa_report =   $testInfo[0]->psa_report;
                    $coupon_code    =   $testInfo[0]->coupon_code;
                    $comment        =   $testInfo[0]->comment;
                }

                $temp_data['psa_test']  = $psa_test;
                $temp_data['psa_report']  = $psa_report;
                $temp_data['coupon_code']  = $coupon_code;
                $temp_data['comment']  = $comment;

                array_push($patientInfo, $temp_data);               
            }   
        }
 
            
                #get Cycle info
               # $CycleInfo = $this->mdl_request->get_records(['patient_id' => $patient_id, ], 'cycle');

                #$doctorInfo = $this->mdl_request->get_records(['doctor_id' => $value->doctor_id], 'doctor', ['doctor_name']);

        $patient_records['patient_id']          =   $patient_id;
        $patient_records['patient_name']        =   $patient_name;
        $patient_records['patient_type']        =   $patient_type;
        $patient_records['patient_age']         =   $patient_age;
        $patient_records['patient_mobile_no']   =   $patient_mobile_no;
        $patient_records['address']             =   $address;
        $patient_records['pincode']             =   $pincode;
        $patient_records['doctor_name']         =   $doctor_name;
        $patient_records['city_name']           =   $city_name;
        $patient_records['cycle_data']          =   $patientInfo;
 
        
        $users_response = [];
        $users_response['patient_data'] = $patient_records;

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = "Patient information has been retrive Successfully!!!";
        $this->response['data'] = $users_response;
        $this->error = [];
        $this->sendResponse();
        return;


    }

}
?>
