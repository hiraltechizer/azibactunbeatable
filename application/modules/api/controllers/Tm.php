<?php
class Tm extends Api_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('api/mdl_request');
	}
    function my_profile() {
        /**
         * @api {post} /api/tm/my_profile My Profile
         * @apiName My Profile
         * @apiGroup Tm
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
         * @apiSuccess {Object} data  TM Record Object With Token
         * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
         {
    "message": "TM information has been retrive Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "tm_info": {
            "tm_name": "mr02",
            "tm_id": "610",
            "region": "region02",
            "hq": "city02",
            "mobile_number": "9800000030",
            "reporting_manager": "mr02"
        },
        "request_id": 1617178889.005911
    }
}
        */
        #collect required information
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id','users_parent_id','users_city_id','users_area_id','users_region_id','users_division_id','users_zone_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   

        #define users type
        $users_type     = $UsersInfo[0]->users_type;
        $users_name     = $UsersInfo[0]->users_name;  
        $users_emp_id   = $UsersInfo[0]->users_emp_id; 
        $users_mobile   = $UsersInfo[0]->users_mobile; 
        $users_parent_id =  $UsersInfo[0]->users_parent_id;
        $city_id        = $UsersInfo[0]->users_city_id;
        $area_id        = $UsersInfo[0]->users_area_id;
        $region_id      = $UsersInfo[0]->users_region_id;
        $zone_id        = $UsersInfo[0]->users_zone_id;
        $division_id    = $UsersInfo[0]->users_division_id;

        if($users_type != 'MR') {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Invalid Authentication";
            $this->error = array('message'=>'Invalid Authentication');
            $this->sendResponse();
            return;
        }

        #check sc code 
        $cityInfo =$this->mdl_request->get_records(['city_id' => $city_id], 'city', ['city_name'], 'city_id DESC', 1);

        if(!count($cityInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "TM selected city information not available";
            $this->error = array('message'=>'TM selected city information not available');
            $this->sendResponse();
            return;
        }

        $city_name = $cityInfo[0]->city_name;

        $regionInfo =$this->mdl_request->get_records(['region_id' => $region_id], 'region', ['region_name'], 'region_id DESC', 1);

        if(!count($regionInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "TM selected region information not available";
            $this->error = array('message'=>'TM selected region information not available');
            $this->sendResponse();
            return;
        }

        $region_name = $regionInfo[0]->region_name;

        #get users parent info
        $tmManagerInfo =$this->mdl_request->get_records(['users_id' => $users_parent_id], 'manpower', ['users_name','users_parent_id'], 'users_id DESC', 1);

        if(!count($tmManagerInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "TM reporting manager information not available";
            $this->error = array('message'=>'TM reporting manager information not available');
            $this->sendResponse();
            return;
        }

        $rsmId = $tmManagerInfo[0]->users_parent_id;  


         $rsmInfo =$this->mdl_request->get_records(['users_id' => $rsmId], 'manpower', ['users_name','users_parent_id'], 'users_id DESC', 1);

        if(!count($rsmInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "ASM reporting manager information not available";
            $this->error = array('message'=>'ASM reporting manager information not available');
            $this->sendResponse();
            return;
        }
        $manager_name = $rsmInfo[0]->users_name;        

        $tm_info    =   ['tm_name'  =>  $users_name,
                        'tm_id'     =>  $users_emp_id,
                        'region'    => $region_name,
                        'hq'        =>  $city_name,
                        'mobile_number' => $users_mobile,
                        'reporting_manager' => $manager_name
                        ];

        $users_response = [];
        $users_response['tm_info'] = $tm_info;

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = "TM information has been retrive Successfully!!!";
        $this->response['data'] = $users_response;
        $this->error = [];
        $this->sendResponse();
        return;
    }  

   function update_profile() {
        /**
         * @api {post} /api/tm/update_profile Updte profile
         * @apiName Update Profile
         * @apiGroup Tm
         * @apiVersion 1.0.0
         * @apiParam {String} Token 
         * @apiParam {Numeric} users_mobile Tm Mobile Number
         *
         * @apiSuccess {Number} code HTTP Status Code.
         * @apiSuccess {String} message  Associated Message.
        * @apiSuccess {Object} error  Error if Any.
         *
         *@apiExample sample input:
         *  {"users_mobile":"1234545678"}
         *
         * @apiSuccessExample Success-Response:
            *     HTTP/1.1 200 OK
            * 
           {
    "message": "TM mobile number has been updated Successfully!!!",
    "error": "",
    "code": 200,
    "status": "Success",
    "data": {
        "request_id": 1617179640.450565
    }
}
        */
        #collect required information
        $users_mobile  = trim(isset($this->input_data['users_mobile'])?$this->input_data['users_mobile']:'');
        $users_id = $this->id;

        #Check users info exits or not base on token       
        $UsersInfo = $this->mdl_request->get_records(['users_id' => $users_id, 'is_deleted' => '0'], 'manpower', ['users_id', 'users_type', 'users_name', 'users_mobile', 'users_emp_id', 'users_division_id']);

        #If users info not exits then show error        
        if(!count($UsersInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Usernames information no available";
            $this->error = array('message'=>'Usernames information no available');
            $this->sendResponse();
            return;
        }   
        
        #define users type
        $users_type = $UsersInfo[0]->users_type;
        
        #Check Logged in User Type for MR
        if($users_type != 'MR') {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Invalid Authentication";
            $this->error = array('message'=>'Invalid Authentication');
            $this->sendResponse();
            return;
        }

        #Check doctor mobile number
        if(empty($users_mobile)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter TM Mobile Number";
            $this->error = array('message'=>'Please Enter TM Mobile Number');
            $this->sendResponse();
            return;
        }

        #validate doctor mobile
       if(! preg_match('/^[1-9][0-9]{9}$/', $users_mobile)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Please Enter Valid TM Mobile Number";
            $this->error = array('message'=>'Please Enter Valid TM Mobile Number');
            $this->sendResponse();
            return;
        }

        #check mobile number exits or not
        $UserInfo = $this->mdl_request->get_records(['users_mobile' => $users_mobile,'is_deleted' => '0', 'users_id !=' => $users_id], 'manpower', ['users_id']);

        #If Doctor record found then show error
        if(count($UserInfo)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Mobile Number Already Exist.";
            $this->error = array('message'=>'Mobile Number Already Exist.');
            $this->sendResponse();
            return;   
        }

        #prepare doctor data
        $update_data['users_mobile']   =   $users_mobile;
        
        #Insert Doctor Data
        $ret = $this->mdl_request->_update(['users_id' => $users_id], $update_data, 'manpower');

        $users_response = [];
        
        #check userinfo exits or not
        if(!empty($ret)) {
            $this->response['code'] = 200;
            $this->response['status'] = 'Success';
            $this->response['message'] = "Tm mobile number has been updated Successfully!!!";
            $this->response['data'] = $users_response;
            $this->error = [];
            $this->sendResponse();
            return;
        } else {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = "Internal Server Error";
            $this->error = array('message'=>'Internal Server Error!!');
            $this->sendResponse();
            return;
        }
    }   
}
?>
