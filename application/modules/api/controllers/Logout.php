<?php class Logout extends Api_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->model('api/mdl_request');
	}

	function logout()
	{

		// Get User Id from APP
		// If User Token id is valid, update the token_status as inactive to the APP
		// Else return the error message to the APP

		/**
		 * @api {post} /api/logout/logout Logout
		 * @apiName logout
		 * @apiGroup Logout
		 *
		 * @apiHeader {String} Token User unique Access-Token
		 *
		 *
		 * @apiSuccessExample Success-Response:
		 *     HTTP/1.1 200 OK
		 *     {
    "message": "Logout Successful",
    "error": "Logout Successful",
    "code": 200,
    "data": {
        "request_id": 1603185929.548529
    }
}
		 */

		$access_token = $this->accesstoken;
		$data['token_status'] = 'inactive';
		$ret = $this->mdl_request->_update(['access_token' => $access_token], $data, 'access_token');
		
		if ($ret) {
			$this->response['code'] = 200;
			$this->response['message'] = "Logout Successful";
			$this->error = array('message' => 'Logout Successful');
			$this->sendResponse();
			return;
		} else {
			$this->response['code'] = 401;
			$this->response['message'] = "Logout Failure. Please Try Again";
			$this->error = array('message' => 'Logout Failure. Please Try Again');
			$this->sendResponse();
			return;
		}
	
	}
}