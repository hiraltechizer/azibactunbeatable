
<?php
class Applicationview extends Api_Controller {

	function __construct() {
		
		parent::__construct();
		$this->load->model('api/mdl_request');
	}


    function app_activation(){
   
         $appInfo = $this->mdl_request->get_records([], 'app_view', ['is_view']);
		 $app_response['isView']	= $appInfo[0]->is_view;
		if ($appInfo) {
			$this->response['code'] = 200;
			$this->response['message'] = "Ios application is view 1";
			$this->error = array('message' => 'Ios application is view ');
			$this->response['data'] = $app_response;
			$this->sendResponse();
			return;
		} else {
			$this->response['code'] = 400;
			$this->response['message'] = "Failure. Please Try Again";
			$this->error = array('message' => 'Failure. Please Try Again');
			$this->sendResponse();
			return;
		}
	
	}

}  