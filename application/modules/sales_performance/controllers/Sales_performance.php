<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sales_performance extends User_Controller
{
	private $module = 'sales_performance';
    private $model_name = 'mdl_sales_performance';
    private $controller = 'sales_performance';
    private $settings = [
        'permissions'=> [],
        'pagination_index' => 3,
        'filters' => ['column_filters'=> FALSE, 'date_filters'=> FALSE]

    ];
    private $scripts = [];

   
	function __construct() {
         parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }

     

    function get_doctor(){
        $userId = $this->session->get_field_from_session('user_id','user');
        $dcotorRecords = $this->model->get_records(['users_id' => $userId], 'doctor',['doctor_name']);
        $doc_name = array_column($dcotorRecords, 'doctor_name');
        echo json_encode($doc_name);
    }
}
