<?php $i = 1;
if (sizeof($collection)) : ?>
      <div id="tbody" class="">

            <?php foreach ($collection as $record) {


                  $id = $record['qwmw_target_id']; ?>
                  <div class="col-lg-12 col-sm-12 col-xs-12">

                        <div class="tbl-ttl"> Updated on :

                              <?php if (isset($record['sale_arc_date'])) {
                                    echo date("F j, Y", strtotime($record['sale_arc_date']));
                              } else {
                                    echo "";
                              } ?>
                        </div>
                        <div class="tbl-scrol">

                              <div class="rTable ">

                                    <div class="rTableRow">
                                          <div class="rTableHead"><?php echo $record['city_name'];
                                                                  echo " HQ - " . $record['users_name']; ?></div>

                                          <div class="rTableHead">Slab 1</div>


                                          <div class="rTableHead"></div>


                                          <div class="rTableHead">Slab 2</div>

                                    </div>
                                    <div class="rTableRow">
                                          <!-- <div class="rTableCell">Target PMPT Mr Value (In Lakhs)</div> -->
                                          <div class="rTableCell">Target PMPT quarter 1 (in lakhs)</div>
                                          <!-- <?php
                                                if ($record['slab1'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['slab1']; ?></div>
                                          <?php  } ?>
                                          <?php
                                          if ($record['slab2'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['slab2']; ?></div>
                                          <?php  } ?> -->
                                          <div class="rTableCell"></div>
                                          <div class="rTableCell"><?php echo $record['quarter_target']; ?></div>
                                          <div class="rTableCell"></div>
                                    </div>
                                    <div class="rTableRow">
                                          <div class="rTableCell" style="">Expected PMPT To Win Prize (In Lakhs)</div>


                                          <!-- <div class="rTableCell"><?php echo $record['month_status'] == 0  ? $record['slab1'] : $record['exp_slab1']; ?></div> -->

                                          <div class="rTableCell"><?php if ($record['month_status'] == 0 && $record['up_slab1']) {
                                                                        echo  $record['up_slab1'];
                                                                  } elseif ($record['month_status'] == 1 && $record['up_slab1']) {
                                                                        echo  $record['up_slab1'];
                                                                  } else {
                                                                        echo  $record['slab1'];
                                                                  }   ?></div>

                                          <div class="rTableCell"></div>

                                          <!-- <div class="rTableCell"><?php echo $record['month_status'] == 0 ? $record['exp_slab2'] : $record['exp_slab2']; ?></div> -->

                                          <div class="rTableCell"><?php if ($record['month_status'] == 0 && $record['up_slab2']) {
                                                                        echo  $record['up_slab2'];
                                                                  } elseif ($record['month_status'] == 1 && $record['up_slab2']) {
                                                                        echo  $record['up_slab2'];
                                                                  } else {
                                                                        echo  $record['slab2'];
                                                                  }   ?></div>


                                    </div>
                                    <div class="rTableRow" class='text-center '>
                                          <div class="rTableCell">PMPT Till Date (In Lakhs)</div>

                                          <div class="rTableCell"></div>

                                          <div class="rTableCell">
                                                <?php if ($record['sale']) {
                                                      echo $record['month_status'] == 0 ? $record['sale'] : 0.00;
                                                } else {
                                                      echo "0.00";
                                                } ?>
                                          </div>
                                          <div class="rTableCell"></div>

                                    </div>
                                    <div class="rTableRow">
                                          <div class="rTableCell">Balance PMPT to win the price</div>


                                          <div class="rTableCell" style="">
                                                <!-- <?php echo number_format(($record['sale'] - $record['slab1']), 2, '.', ''); ?> -->
                                                <?php if (!empty($record['exp_slab1'])) {
                                                      echo $record['exp_slab1'];
                                                } else {
                                                      echo $record['slab1'];
                                                } ?>
                                          </div>


                                          <div class="rTableCell"></div>

                                          <div class="rTableCell" style="">
                                                <!-- <?php echo number_format($record['sale'] - $record['slab2'], 2, '.', ''); ?> -->
                                                <?php if (!empty($record['exp_slab2'])) {
                                                      echo $record['exp_slab2'];
                                                } else {
                                                      echo $record['slab2'];
                                                } ?>
                                          </div>




                                    </div>

                                    <!-- <div class="rTableRow rTableFoot">
                                          <div class="rTableCell">Q-2 Target (In Lakhs)</div>
                                          <div class="rTableCell"></div>
                                          <div class="rTableCell"><?php echo $record['quarter_target']; ?></div>
                                          <div class="rTableCell">
                                                <div>
                                                </div>

                                          </div>
                                    </div> -->
                              </div>
                        </div>
                  </div>
            <?php $i++;
            } ?>
            <div class="clearfix"></div>
            <div class="col-md-12 text-center pagination-wrap">
                  <?php if ($role !== 'MR') { ?>
                        <?php echo $this->ajax_pagination->create_links(); ?>
                  <?php } ?>

            </div>
      </div>
<?php else : ?>
      <div class="col-md-12 text-center">
            <i>No Record Found</i>
            <div>
            <?php endif; ?>