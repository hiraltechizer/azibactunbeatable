<?php $i = 1;
if (sizeof($collection)) : ?>
      <div id="tbody" class="row">
            <?php foreach ($collection as $record) {
                  $id = $record['quw_target_id']; ?>
                  <div class="col-lg-12 col-sm-12 col-xs-12">

                             <div class="tbl-ttl"> Updated on : 
                                   
                             <?php if($record['sale_arc_date']){ echo date("F j, Y", strtotime($record['sale_arc_date']));}else{echo"";} ?> </div>
                        <div class="rTable tbl-scrol">
                         
                              <div class="rTableRow">
                                    <div class="rTableHead"><?php echo $record['city_name'];
                                                            echo " HQ"; ?></div>
                                    <div class="rTableHead">Pearl</div>
                                    <div class="rTableHead">Ruby</div>
                                    <div class="rTableHead">Emerald</div>
                                    <div class="rTableHead">Saphire</div>
                                    <div class="rTableHead">Diamond</div>
                              </div>
                              <div class="rTableRow">
                                    <div class="rTableCell">Aimed HQ Value (In Lakhs)</div>
                                    <div class="rTableCell"><?php echo $record['pearl']; ?></div>
                                    <div class="rTableCell"><?php echo $record['ruby']; ?></div>
                                    <div class="rTableCell"><?php echo $record['emerald']; ?></div>
                                    <div class="rTableCell"><?php echo $record['saphire']; ?></div>
                                    <div class="rTableCell"><?php echo $record['diamond']; ?></div>
                              </div>
                              <div class="rTableRow">
                                    <div class="rTableCell">Sales Till Date (In Lakhs)</div>
                                    <div class="rTableCell"></div>
                                    <div class="rTableCell"></div>
                                    <div class="rTableCell"><?php if($record['sale_arc']){ echo $record['sale_arc'];}else{echo"0.00";} ?></div>
                                    <div class="rTableCell"></div>
                                    <div class="rTableCell"></div>
                              </div>

                              <div class="rTableRow">
                                    <div class="rTableCell">Gap To Win Prize (In Lakhs)</div>
                                    <div class="rTableCell">
                                       <?php echo number_format($record['pearl'] - $record['sale_arc'],2, '.', '');?>
                                    </div>
                                    <div class="rTableCell">
                                    <?php echo number_format($record['ruby'] - $record['sale_arc'],2, '.', '');?>
            
                                    </div>
                                    <div class="rTableCell">
                                    <?php echo number_format($record['emerald'] - $record['sale_arc'],2, '.', '');?>
                                         
                                    </div>
                                    <div class="rTableCell">
                                    <?php echo number_format($record['saphire'] - $record['sale_arc'],2, '.', '');?>
                                          
                                    </div>

                                    <div class="rTableCell">
                                    <?php echo number_format($record['diamond'] - $record['sale_arc'],2, '.', '');?>
                                          
                                    </div>
                              </div>

                              <div class="rTableRow rTableFoot">
                                    <div class="rTableCell">Q-2 Target (In Lakhs)</div>
                                    <div class="rTableCell"></div>
                                    <div class="rTableCell"></div>
                                    <div class="rTableCell">
                                    <div><?php echo $record['quarter_target']; ?></div>
                                       
                                    </div>
                              </div>
                        </div>
                  </div>
            <?php $i++;
            } ?>
            <div class="clearfix"></div>
            <div class="col-md-12 text-center">
                  <?php if($role !== 'MR'){ ?>
                  <?php echo $this->ajax_pagination->create_links(); ?>
                  <?php } ?>
                 
            </div>
      </div>
<?php else : ?>
      <div class="col-md-12 text-center">
            <i>No Record Found</i>
            <div>
            <?php endif; ?>