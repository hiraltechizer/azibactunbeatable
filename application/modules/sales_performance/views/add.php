<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>

<style>
* {
  box-sizing: border-box;
}

body {
  font: 16px Arial;  
}

/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

input {
  border: 1px solid transparent;
  /* background-color: #f1f1f1; */
  padding: 10px;
  font-size: 16px;
}

input[type=text] {
  /* background-color: #f1f1f1; */
  width: 100%;
}

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
</style>

<!-- <input type="hidden" class="form-control" id="users_id" name="users_id" value = "<?php echo $userId ?>" > -->
<div class="form-group row">
    <label for="title" class="col-sm-3 col-form-label">Doctor Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        
      <div class="autocomplete" style="width:100%" >
    <input id="doctor_id" class="form-control" type="text" name="doctor_name" placeholder="Doctor Name">
  </div>
    </div>
</div>
<div class="form-group row">
	<label for="sbu_code" class="col-sm-3 col-form-label">SBU Code <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    <div class="" >
    	<input type="text" class="form-control" id="sbu_code" name="sbu_code" placeholder="SBU Code">
  	</div>
    </div>
</div>
<div class="form-group row">
	<label for="speciality" class="col-sm-3 col-form-label">Speciality <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    <div class="" >
    <select class="form-control form-control-sm js-example-basic-single" id="speciality_id" name="speciality_id" data-placeholder="Select Speciality">
            <option value="">Select Speciality</option>
        </select>
  	</div>
    </div>
</div>
<div class="form-group row">
	<label for="rxn_count" class="col-sm-3 col-form-label">RXN <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    <div class="" >
    	<input type="text" class="form-control" id="rxn_count" name="rxn_count"   placeholder="RXN">
  	</div>
    </div>
</div>
<div class="form-group row">
    <label for="rxn" class="col-sm-3 col-form-label">Content File <span class="text-danger">*</span></label>
    <div class="col-sm-9" id="">
    <input type="file"  id="validatedCustomFile1" name="rxn[]" accept="image/png, image/jpeg" multiple required width="100%" placeholder="Choose file...">
        <p>Support format: png, jpeg, jpg</p>
    </div>
    <input type="hidden" name="field_rxn" value=""/>
</div>




<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>

<script>

</script>