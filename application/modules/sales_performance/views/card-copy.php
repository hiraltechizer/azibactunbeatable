
<?php $i = 1; if(sizeof($collection)) : ?>
<div id="tbody" class="clearfix">
    <?php foreach ($collection as $record) { $id = $record['quw_target_id']; ?>
    <div class="col-lg-4 col-sm-4 col-xs-12">
       
    <table class="table table-striped  table-bordered">
    <p> Updated on : <?php echo date("F j, Y",strtotime($record['sale_arc_date']));?> </p>
  <tr>
    <th><?php echo $record['city_name']; echo " HQ"; ?></th>
    <th>Pearl</th>
    <th>Ruby</th>
    <th>Emerald</th>
    <th>Saphire</th>
    <th>Diamond</th>
  </tr>
  <tr>
    <td>Aimed HQ Val (L.)</td>
    <td class='text-center'><?php echo $record['pearl']; ?></td>
    <td class='text-center'><?php echo $record['ruby']; ?></td>
    <td class='text-center'><?php echo $record['emerald']; ?></td>
    <td class='text-center'><?php echo $record['saphire']; ?></td>
    <td class='text-center'><?php echo $record['diamond']; ?></td>
  </tr>
  <tr>
  <td>Sales Till Date</td>
    <td class='text-center'> 
    <?php $flg = 0;
       if($record['pearl'] >= $record['quarter_target']) {
       if($flg == 0){ echo $record['sale_arc']; $flg = 1;}else{echo "";}} ?>
    </td>
    <td class='text-center'> 
    <?php if($record['ruby'] >= $record['quarter_target']) { 
          if($flg == 0){ echo $record['sale_arc']; $flg = 1;}else{echo "";}} ?>
    </td>
    <td class='text-center'> 
    <?php if($record['emerald'] >= $record['quarter_target']) { 
          if($flg == 0){ echo $record['sale_arc']; $flg = 1;}else{echo "";}} ?>
    </td>
    <td class='text-center'> 
    <?php if($record['saphire'] >= $record['quarter_target']) {
          if($flg == 0){ echo $record['sale_arc']; $flg = 1;}else{echo "";}} ?>
    </td>
    <td class='text-center'> 
    <?php if($record['diamond'] >= $record['quarter_target']) { 
          if($flg == 0){ echo $record['sale_arc'];  $flg = 1;}else{echo "";}} ?>
    </td>
  </tr>
  
  <tr>
    <td>Gap to win prize</td>
    <td class='text-center'> 
    <?php $sale_falg = 0;?>
    <?php if($record['pearl'] >= $record['quarter_target']  ) {
          if($sale_falg == 0){ echo $record['pearl'] - $record['sale_arc']; $sale_falg = 1;
          }else{echo "";}} 
    ?>
    </td>
    <td class='text-center'> 
    <?php if($record['ruby'] >= $record['quarter_target']) {
          if($sale_falg == 0){ echo $record['ruby'] - $record['sale_arc']; $sale_falg = 1;
          }else{echo "";}} 
    ?>
    </td>
    <td class='text-center'> 
    <?php if($record['emerald'] >= $record['quarter_target']) {
          if($sale_falg == 0){ echo $record['emerald'] - $record['sale_arc']; $sale_falg = 1;
          }else{echo "";}}
    ?>
    </td>
    <td class='text-center'> 
    <?php if($record['saphire'] >= $record['quarter_target']) {
          if($sale_falg == 0){ echo $record['saphire'] - $record['sale_arc']; $sale_falg = 1;
          }else{echo "";}} 
      ?>
</td>

    <td class='text-center'> 
    <?php if($record['diamond'] >= $record['quarter_target']) {
      if($sale_falg == 0){ echo $record['diamond'] - $record['sale_arc']; $sale_falg = 1;
       }else{echo "";}} 
    ?>
  </td>
  </tr>
  
  <tr>
    <td>Q2 Tgt</td>
    <td colspan = '5' class='text-center'><?php echo $record['quarter_target']; ?></td>
  </tr>

</table>

    </div>
    <?php $i++;  } ?>
    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <?php echo $this->ajax_pagination->create_links(); ?>
    </div>
</div> 
<?php else: ?>
<div class="col-md-12 text-center"><i>No Record Found</i><div>
<?php endif; ?>


