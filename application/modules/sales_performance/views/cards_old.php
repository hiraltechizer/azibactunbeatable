<?php $i = 1;
if (sizeof($collection)) : ?>
      <div id="tbody" class="">
            <?php foreach ($collection as $record) {
                  $id = $record['quw_target_id']; ?>
                  <div class="col-lg-12 col-sm-12 col-xs-12">

                        <div class="tbl-ttl"> Updated on :

                              <?php if ($record['sale_arc_date']) {
                                    echo date("F j, Y", strtotime($record['sale_arc_date']));
                              } else {
                                    echo "";
                              } ?>
                        </div>
                        <div class="tbl-scrol">

                              <div class="rTable ">

                                    <div class="rTableRow">
                                          <div class="rTableHead"><?php echo $record['city_name'];
                                                                  echo " HQ"; ?></div>
                                          <?php
                                          if ($record['pearl'] >= $record['quarter_target']) { ?>
                                                <div class="rTableHead">Pearl</div>
                                          <?php  } ?>

                                          <?php
                                          if ($record['ruby'] >= $record['quarter_target']) { ?>
                                                <div class="rTableHead">Ruby</div>
                                          <?php  } ?>

                                          <?php
                                          if ($record['emerald'] >= $record['quarter_target']) { ?>
                                                <div class="rTableHead">Emerald</div>
                                          <?php  } ?>

                                          <?php
                                          if ($record['saphire'] >= $record['quarter_target']) { ?>
                                                <div class="rTableHead">Saphire</div>
                                          <?php  } ?>

                                          <?php
                                          if ($record['diamond'] >= $record['quarter_target']) { ?>
                                                <div class="rTableHead">Diamond</div>
                                          <?php  } ?>
                                    </div>
                                    <div class="rTableRow">
                                          <div class="rTableCell">Aimed HQ Value (In Lakhs)</div>
                                          <?php
                                          if ($record['pearl'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['pearl']; ?></div>
                                          <?php  } ?>
                                          <?php
                                          if ($record['ruby'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['ruby']; ?></div>
                                          <?php  } ?>
                                          <?php
                                          if ($record['emerald'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['emerald']; ?></div>
                                          <?php  } ?>
                                          <?php
                                          if ($record['saphire'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['saphire']; ?></div>
                                          <?php  } ?>
                                          <?php
                                          if ($record['diamond'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell"><?php echo $record['diamond']; ?></div>
                                          <?php  } ?>
                                    </div>
                                    <div class="rTableRow" class='text-center ' style="border:2px solid #000">
                                          <div class="rTableCell" style="border-bottom:none">Sales Till Date (In Lakhs)</div>
                                          <div class="rTableCell" style="border-bottom:none"></div>
                                          <div class="rTableCell" style="border-bottom:none">
                                                <?php if ($record['sale_arc']) {
                                                      echo $record['sale_arc'];
                                                } else {
                                                      echo "0.00";
                                                } ?>
                                          </div>
                                    </div>

                                    <div class="rTableRow">
                                          <div class="rTableCell" style="border-top: 1px solid #cbcbcb;">Gap To Win Prize (In Lakhs)</div>

                                          <?php
                                          if ($record['pearl'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell" style="border-top: 1px solid #cbcbcb;">
                                                      <?php echo number_format($record['pearl'] - $record['sale_arc'], 2, '.', ''); ?>
                                                </div>
                                          <?php } ?>

                                          <?php
                                          if ($record['ruby'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell" style="border-top: 1px solid #cbcbcb;">
                                                      <?php echo number_format($record['ruby'] - $record['sale_arc'], 2, '.', ''); ?>
                                                </div>
                                          <?php } ?>

                                          <?php
                                          if ($record['emerald'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell" style="border-top: 1px solid #cbcbcb;">
                                                      <?php echo number_format($record['emerald'] - $record['sale_arc'], 2, '.', ''); ?>
                                                </div>
                                          <?php } ?>

                                          <?php
                                          if ($record['saphire'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell" style="border-top: 1px solid #cbcbcb;">
                                                      <?php echo number_format($record['saphire'] - $record['sale_arc'], 2, '.', ''); ?>
                                                </div>
                                          <?php } ?>

                                          <?php
                                          if ($record['diamond'] >= $record['quarter_target']) { ?>
                                                <div class="rTableCell" style="border-top: 1px solid #cbcbcb;">
                                                      <?php echo number_format($record['diamond'] - $record['sale_arc'], 2, '.', ''); ?>
                                                </div>
                                          <?php } ?>
                                    </div>

                                    <div class="rTableRow rTableFoot">
                                          <div class="rTableCell">Q-2 Target (In Lakhs)</div>
                                          <div class="rTableCell"></div>
                                          <div class="rTableCell"><?php echo $record['quarter_target']; ?></div>
                                          <div class="rTableCell">
                                                <div>
                                                </div>

                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
            <?php $i++;
            } ?>
            <div class="clearfix"></div>
            <div class="col-md-12 text-center pagination-wrap">
                  <?php if ($role !== 'MR') { ?>
                        <?php echo $this->ajax_pagination->create_links(); ?>
                  <?php } ?>

            </div>
      </div>
<?php else : ?>
      <div class="col-md-12 text-center">
            <i>No Record Found</i>
            <div>
            <?php endif; ?>