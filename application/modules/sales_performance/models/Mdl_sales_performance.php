<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_sales_performance extends MY_Model {

    private $p_key = 'presc_id';
    private $table = 'prescription';
    private $tb_alias = 'pre';
    private $fillable = [];
    private $column_list = [];
    private $csv_columns =[];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
            
    

        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $user_id =(int) $this->security->xss_clean($this->session->get_field_from_session('user_id', 'user'));

        $role = $this->security->xss_clean($this->session->get_field_from_session('role', 'user'));

        // $q = $this->db->select('c.city_name,qua.quw_target_id,qua.quarter_id,qua.city_id,qua.quarter_target,
        // qua.diamond,qua.ruby,qua.emerald,qua.pearl,qua.saphire,
        // (SELECT sale.sale
        // FROM sale_archive sale 
        // WHERE sale.city_id = qua.city_id AND qua.quarter_id = sale.quarter_id
        // ) AS sale_arc,
        // (SELECT sale.update_dt
        // FROM sale_archive sale 
        // WHERE sale.city_id = qua.city_id AND qua.quarter_id = sale.quarter_id
        // ) AS sale_arc_date')
        // ->from('quarter_wise_target qua')
        // ->join("quarter q","q.quarter_id = qua.quarter_id AND q.status = 'Active'")
        // // ->join('ho_connect_fields cfield', 'cfield.ho_conn_id = conn.ho_conn_id');
        // ->join('city c', 'c.city_id = qua.city_id')
        // ->join('area a', 'a.area_id = c.area_id')
        // ->join('region r', 'r.region_id = a.region_id')
        // ->join('zone z', 'z.zone_id = r.zone_id')
        // ->join('speciality s', 's.speciality_id = d.speciality_id')
        ;

        // $q = $this->db->select('qwmwt.qwmw_target_id,qwmwt.quarter_target,qwt.quarter_id,qwt.slab1,qwt.slab2,m.users_name,sa.sale,c.city_name')
        //     ->from('quarter_wise_mr_wise_target qwmwt')
        //     ->join('quarter_wise_target qwt','qwt.quarter_id = qwmwt.quarter_id')
        //     ->join('manpower m','m.users_id = qwmwt.mr_id')
        //     ->join('city c','c.city_id = m.users_city_id')
        //     ->join('sale_archive sa','sa.mr_id = qwmwt.mr_id','LEFT')
        // ;
        
        $q = $this->db->select('qwt.slab1,qwt.slab2,qwmwt.quarter_target,qwmwt.qwmw_target_id,m.users_name,sa.sale,ep.exp_slab1,ep.exp_slab2,ep.month_status,c.city_name,c.city_id,umws.up_slab1,umws.up_slab2')
            ->from('quarter_wise_target qwt')
            ->join('quarter_wise_mr_wise_target qwmwt','qwmwt.quarter_id = qwt.quarter_id')
            ->join('manpower m','m.users_id = qwmwt.mr_id')
            ->join('(SELECT sale_archive.*
            FROM sale_archive,
             (
            SELECT mr_id, MAX(DATE) AS sale_date
            FROM sale_archive
            GROUP BY mr_id) max_sale
            WHERE sale_archive.mr_id=max_sale.mr_id AND sale_archive.date=max_sale.sale_date ) sa','sa.mr_id = qwmwt.mr_id','LEFT')
            ->join('(SELECT exp_pmpt.*
            FROM exp_pmpt,
             (
            SELECT mr_id, MAX(sale_arch_id) AS salearch_id
            FROM exp_pmpt
            GROUP BY mr_id) max_pmpt
            WHERE exp_pmpt.mr_id=max_pmpt.mr_id AND exp_pmpt.sale_arch_id=max_pmpt.salearch_id) ep','ep.mr_id = qwmwt.mr_id','LEFT')
            ->join('city c','c.city_id = m.users_city_id')
            ->join('(SELECT updated_mr_wise_slab.*
FROM updated_mr_wise_slab, (
SELECT mr_id, MAX(sale_archive_id) AS salearch_id
FROM updated_mr_wise_slab
GROUP BY mr_id) max_updated_mr_wise_slab
WHERE updated_mr_wise_slab.mr_id=max_updated_mr_wise_slab.mr_id AND updated_mr_wise_slab.sale_archive_id=max_updated_mr_wise_slab.salearch_id) umws', 'umws.city_id = c.city_id', 'LEFT')
            // ->join('city c', 'c.city_id = m.users_city_id')
            // ->join('updated_mr_wise_slab umws', 'umws.city_id = c.city_id')

        ;

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        
                        $q->where("$key", $value); 
                    }
                //    $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }
       
        if(! $count) {
            // $q->order_by('qua.quw_target_id DESC');
            $q->group_by('qwmwt.qwmw_target_id','DESC');
            // $q->order_by('sa.update_dt DESC');
        }
        // $q->limit(1);


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
        // echo "<pre>";print_r($collection);exit;
        // echo $this->db->last_query();die;
        // $city_id 
      
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
                [
					'field' => 'doctor_name',
					'label' => 'Doctor Name',
                    'rules' => 'trim|required|valid_name|max_length[150]|xss_clean'
                ],
                [
					'field' => 'sbu_code',
					'label' => 'Title',
					'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'speciality_id',
                    'label' => 'Doctor speciality',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
					'field' => 'rxn_count',
					'label' => 'RXN',
					'rules' => 'trim|required|xss_clean'
                ],

            ];
        }

        if($type == 'modify') {
            return [
                
                [
					'field' => 'document_type_id',
					'label' => 'Document Type',
					'rules' => 'trim|required|xss_clean'
                ]
                ,
                [
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|max_length[150]|xss_clean'
                ],
                [
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|xss_clean'
                ],
                [
					'field' => 'status',
					'label' => 'status',
					'rules' => 'trim|required|xss_clean'
                ],
            ];
        }
    }

    function save(){
        //print_r($_FILES['rxn']['size']);exit;
        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->load->helper('upload_media');
        $this->form_validation->set_rules($this->validate('save'));

        if(!$this->form_validation->run()) {
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');
            
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }
        #insert doctor
        $doctorInsert['doctor_name'] = $doctor_name = $this->input->post('doctor_name');
        $doctorInsert['sbu_code'] =  $sbu_code = $this->input->post('sbu_code');
        $doctorInsert['speciality_id'] =  $speciality_id = $this->input->post('speciality_id');
       
        if(count($_FILES['rxn']['name']) < 0 || count($_FILES['rxn']['name']) > 7) {
			$upload_errors['status'] = FALSE;
			$upload_errors['errors'] = ['rxn[]' => '<label class="required">Rxn Files Required. Maximum 6 Images Allowed.</label>'];
			return $upload_errors;
		}

        if(!isset($_FILES['rxn']['size']) || empty($_FILES['rxn']['size'])) {
            $fild_errors['status'] = FALSE;
			$fild_errors['errors'] = ['field_rxn' => '<label class="error">Files Required.</label>'];
			return $fild_errors;
		}

        $doctorInfo = $this->model->get_records(['sbu_code'=>$sbu_code],'doctor');

        if(count($doctorInfo)){
           $doctor_id = $doctorInfo[0]->doctor_id;
        }else{
            $doctor_id = $this->_insert($doctorInsert,'doctor');
        }


        $upload_rxn_data = $errors = array();
        $uploadRxnPath = $s3uploadRxnPath = "uploads/rxn/$doctor_id/";

        if(count($_FILES["rxn"]['name']) > 0) {
            $file_rxn_status = $this->upload_file_response("rxn", $uploadRxnPath, $s3uploadRxnPath, ['jpg','png','jpeg','pdf','JPG','PNG','JPEG']);
            
            
            if($file_rxn_status['status']){
                $file_rxn_status = is_array($file_rxn_status['data']) ? $file_rxn_status['data'] : (array)$file_rxn_status['data'];
                for($i=0;$i < count($file_rxn_status); ++$i){
                    array_push($upload_rxn_data, $file_rxn_status[$i]) ;
                }
            } else {
                array_push($errors, $file_rxn_status['error']);
            }
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
        
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $prescriptionInsert = array();
        $prescriptionInsert['doctor_id'] = $doctor_id;
        $prescriptionInsert['rxn_count'] =  $rxn_count = $this->input->post('rxn_count');
		$prescriptionInsert['rxn_path'] = !empty($upload_rxn_data) ? implode(',', $upload_rxn_data) : NULL;
        
        $presc_id = $this->_insert($prescriptionInsert, 'prescription');

        if(! $presc_id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
    }
    
    function modify(){
        // print_r($_FILES);exit;
        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->load->helper('upload_media');

        $is_Available = $this->check_for_posted_record($this->p_key, $this->table);
        if(! $is_Available['status']){ return $is_Available; }
        
        $this->form_validation->set_rules($this->validate('modify'));

        if(! $this->form_validation->run()){
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');

            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }   

        $document_type_id = $this->input->post('document_type_id');
        $fields_perm = $this->input->post('permission');

        if(!isset($_POST['permission']) || empty($_POST['permission'])) {
            $fild_errors['status'] = FALSE;
            $fild_errors['errors'] = ['field_permission' => '<label class="error">Please select Send Notification Fields .</label>'];
            return $fild_errors;
        }
        $p_key = $this->p_key;
        $id  = (int) $this->input->post($p_key);
        if(isset($_FILES['upload_path']['size']) && !empty($_FILES['upload_path']['size'])) {
        #get document type
		$documentTypeInfo = $this->get_records(['document_type_id' => $document_type_id], 'document_type', ['document_type']);

		$document_type = $documentTypeInfo[0]->document_type;

        $path = $_FILES['upload_path']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

        $file_icon = '';
		$duration = '0';
		if(strtolower($document_type) == 'audio') {
			if(!in_array($ext,['wav','aif','mp3','mid','ogg','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload audio files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/audio.png";
		} else if(strtolower($document_type) == 'video') {
			if(!in_array($ext,['mp4','mov','wmv','avi','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload video files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/video.png";
		} else if(strtolower($document_type) == 'ppt') {
			if(!in_array($ext,['ppt','pptx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload ppt files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'pdf') {
			if(!in_array($ext,['pdf'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/pdf_file.png";
		}else if(strtolower($document_type) == 'spredsheet' || strtolower($document_type) == 'excel') {
			if(!in_array($ext,['xls','xlsx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'document' || strtolower($document_type) == 'doc') {
			if(!in_array($ext,['doc','docx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload doc files Only.</label>'];
				return $upload_errors;
			}
		} else {
			if(!in_array($ext,['png', 'jpeg', 'jpg','PNG','JPEG','JPG'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload image files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/images_file.png";
		}

        $upload_filename = $this->UploadFile($document_type_id);
       
        $inserted_ids = [];
		if(!empty($upload_filename)) {
			
			$upload_result['document_type_id']	=	$document_type_id;
			$upload_result['upload_path'] = $upload_filename;
			$upload_result['display_image'] = $file_icon;

			$id = $this->_update([$p_key => $id],$upload_result,'ho_connect');

          }
        }
            
        $update_result['title']	=	$this->input->post('title');
		$update_result['description']	=	$this->input->post('description');
        $update_result['status'] = $this->input->post('status');

         
         $status = (int) $this->_update([$p_key => $id], $update_result);

         

         $this->_delete('ho_conn_id',['ho_conn_id' => $id], 'ho_connect_fields');

         for($ic=0;$ic<count($_POST['permission']);$ic++){ 
              
           

            $fpermission = $_POST['permission'][$ic];

            $ho_connect_fields['ho_conn_id'] =  $id;
            $ho_connect_fields['field_permission'] =  $fpermission;
            $this->_insert($ho_connect_fields, 'ho_connect_fields');

        }
        if(! $status){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
    }

    function remove(){
        
        if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
            $ids = $this->input->post('ids');
            $response = $this->_delete($this->p_key, $ids, $this->table);

            $msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
            return ['msg'=> $msg];
        }

        return ['msg'=> 'No Records Selected'];
    }

    
    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
             $records['MR Name'] = $rows['users_name'];
             $records['Doctor Name'] = $rows['doctor_name'];
             $records['Doctor Mobile'] = $rows['doctor_mobile'];
             $records['Speciality'] = $rows['speciality'];
             $records['HQ'] = $rows['city_name'];
             $records['Area'] = $rows['area_name'];
             $records['Region'] = $rows['region_name'];
             $records['Zone'] = $rows['zone_name'];
            $records['DateTime'] = $rows['insert_dt'];
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }

    function UploadFile($document_type_id) {
		$upload_data = $errors = array();
        $uploadPath = "uploads/ho_connect/$document_type_id/files";
        $s3uploadPath = '';

        if(!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, TRUE);
        }

       $file_status = $this->upload_file_response("upload_path", $uploadPath, '', ['png', 'jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi','PNG','JPEG','JPG']);

        if(isset($file_status['status'])){
            $file_status = is_array($file_status['data']) ? $file_status['data'] : (array)$file_status['data'];
            for($i=0;$i < count($file_status); ++$i){
                array_push($upload_data, $file_status[$i]) ;
            }
        } else {
            array_push($errors, $file_status['error']);
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
    
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $filename = '';
        if(count($upload_data)) {
            foreach ($upload_data as $key => $value) {
               $filename = $value;
            }
            
        } 
        return $filename;
	}

    function upload_file_response($file_name, $uploadPath, $s3uploadPath, $allowed_types = ['png','jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi','PNG','JPG','JPEG']){

		$file_response = upload_media($file_name, $uploadPath, $allowed_types, 100000000000, 0, $s3uploadPath);
//   print_r($file_response);exit;
		$upload_file_count = !empty($_FILES[$file_name]['name']) ? 1:0;

		if($upload_file_count > 1){
			$temp = array();
			$temp['status'] = TRUE;
            
			for($i=0; $i < $upload_file_count; ++$i){
				if(!empty($file_response[$i]['errors'])){
					$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
					$temp['status'] = FALSE;
				} else {
					#echo 'asfsaf';die;
					$temp['data'][] = $file_response[$i]['file_name'];
				}
			}
			return $temp;
		}else{

			if(!empty($file_response['errors'])){
				$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
				$temp['status'] = FALSE;

				return $temp;
			}

			if($file_response) {

				$file_name = [];
				foreach ($file_response as $key => $value) {
					if(!empty($value['file_name'])){
						$temp['status'] = TRUE;
						array_push($file_name, $value['file_name']);
						
					}
				}
				$temp['data'] = $file_name;
				return $temp;
			}

			
		}
	}


    function get_role_wise_city($users_id,$role){
    //  echo "test";exit;
		$q = $this->db->select("GROUP_CONCAT(DISTINCT (c.city_id)) as city_id")
				->from('manpower m')
				->join('region r' ,'r.region_id = m.users_region_id')
                ->join('area a' ,'a.region_id = r.region_id')
                ->join('city c' ,'c.area_id = a.area_id')
               
                ;
		$q->where('m.users_id',$users_id);
        // $q->group_by('m.users_city_id ');
		$q->order_by('m.users_id desc');
		$collection1 = $q->get();
	   $collection = $collection1->result_array();
    //    echo $this->db->last_query();exit;
		return $collection;
    }
    
    function get_role_wise_region($users_id,$role){
    
		$q = $this->db->select("m.users_region_id")
				->from('manpower m')
				// ->join('region r' ,'r.region_id = m.users_region_id')
                // ->join('area a' ,'a.region_id = r.region_id')
                // ->join('city c' ,'c.area_id = a.area_id')
               
                ;
		$q->where('m.users_id',$users_id);
    	$collection1 = $q->get();
	   $collection = $collection1->result_array();
    	return $collection;
    }
    function get_role_wise_zone($users_id,$role){
    
		$q = $this->db->select("m.users_zone_id")
				->from('manpower m')
				// ->join('region r' ,'r.region_id = m.users_region_id')
                // ->join('area a' ,'a.region_id = r.region_id')
                // ->join('city c' ,'c.area_id = a.area_id')
               
                ;
		$q->where('m.users_id',$users_id);
    	$collection1 = $q->get();
	   $collection = $collection1->result_array();
    //    echo $this->db->last_query();exit;

    	return $collection;
    }
    function get_role_wise_region_wise_mr($region_id){
    
		$q = $this->db->select("GROUP_CONCAT(m.users_id) As UsersId")
				->from('manpower m')
				// ->join('region r' ,'r.region_id = m.users_region_id')
                // ->join('area a' ,'a.region_id = r.region_id')
                // ->join('city c' ,'c.area_id = a.area_id')
               
                ;
		$q->where('m.users_region_id = '.$region_id .' AND m.users_type = "MR"');
    	$collection1 = $q->get();
	   $collection = $collection1->result_array();
    	return $collection;
    }

    function get_role_wise_zone_wise_mr($zone_id){
    
		$q = $this->db->select("GROUP_CONCAT(m.users_id) As UsersId")
				->from('manpower m')
				// ->join('region r' ,'r.region_id = m.users_region_id')
                // ->join('area a' ,'a.region_id = r.region_id')
                // ->join('city c' ,'c.area_id = a.area_id')
               
                ;
		$q->where('m.users_zone_id = '.$zone_id .' AND m.users_type = "MR"');
    	$collection1 = $q->get();
	   $collection = $collection1->result_array();
    	return $collection;
    }


    function get_zsm_wise_city($users_id,$role){
     
		$q = $this->db->select("GROUP_CONCAT(DISTINCT(c.city_id)) as city_id")
				->from('manpower m')
				->join('manpower asm' ,'asm.users_id = m.users_parent_id')
                ->join('manpower rsm' ,'rsm.users_id = asm.users_parent_id')
                ->join('manpower zsm' ,'zsm.users_id = rsm.users_parent_id')
                ->join('city c' ,'c.city_id = m.users_city_id')
                ;
		$q->where('zsm.users_id',$users_id);
        // $q->group_by('m.users_city_id ');
		// $q->order_by('m.users_id desc');
		$collection1 = $q->get();
	   $collection = $collection1->result_array();
    //    echo $this->db->last_query();exit;
		return $collection;
    }
    
}