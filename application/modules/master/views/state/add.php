<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
	<label for="state_name" class="col-sm-3 col-form-label">State Name <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="state_name" name="state_name" placeholder="State" maxlength="50">
  	</div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>