<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="state_id" value="<?php echo $info[0]['state_id']; ?>" />
<div class="form-group row">
	<label for="state_name" class="col-sm-3 col-form-label">State Name <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="state_name" name="state_name" placeholder="State" value="<?php echo $info[0]['state_name']; ?>">
  	</div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>