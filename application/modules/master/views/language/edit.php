<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="language_id" value="<?php echo $info[0]['language_id']; ?>" />
<div class="form-group row">
	<label for="language_name" class="col-sm-3 col-form-label">Language Name <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="language_name" name="language_name" placeholder="Language" value="<?php echo $info[0]['language_name']; ?>">
  	</div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>