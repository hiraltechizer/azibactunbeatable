<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
	<label for="document_type" class="col-sm-3 col-form-label">Document Type <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="document_type" name="document_type" placeholder="Document Type" maxlength="50">
  	</div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>