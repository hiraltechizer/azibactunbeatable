<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
    <label for="quarter_name" class="col-sm-3 col-form-label">Quarter Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="quarter_name" name="quarter_name" placeholder="Quarter Name" maxlength="50">
    </div>
</div>

<div class="form-group row">
    <label for="quarter" class="col-sm-3 col-form-label">Quarter Month<span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="js-example-basic-multiple w-100" multiple="multiple" name="month_id[]" id="quarter">
            <?php foreach ($quarter_array as $key => $value) { ?>
                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>