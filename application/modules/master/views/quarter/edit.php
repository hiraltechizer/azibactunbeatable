<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="quarter_id" value="<?php echo $info[0]['quarter_id']; ?>" />

<div class="form-group row">
    <label for="quarter_name" class="col-sm-3 col-form-label">Quarter Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="quarter_name" name="quarter_name" placeholder="Quarter Name" maxlength="50" value="<?php echo $info[0]['quarter_name']; ?>" >
    </div>
</div>
<div class="form-group row">
    <label for="month_id" class="col-sm-3 col-form-label">Quarter Month<span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="js-example-basic-multiple w-100" multiple="multiple" name="month_id[]" id="quarter">
             <?php $i = 0; ?>
            <?php foreach ($quarter_array as $key => $value) { ?>
                <option value="<?php echo $key; ?>" <?php if(in_array($key, $selected_quarter)) { ?> selected="selected" <?php } ?> ><?php echo $value; ?></option>
                <?php $i++; ?>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="status" class="col-sm-3 col-form-label">Status <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="js-example-basic-multiple w-100" name="status" id="status">
            <option value="Active" <?php if($info[0]['status'] == 'Active') {?> selected="selected" <?php } ?> >Active</option>
            <option value="Disable" <?php if($info[0]['status'] == 'Disable') {?> selected="selected" <?php } ?>>Disable</option>
        </select>
    </div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>