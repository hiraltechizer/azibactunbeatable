<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="speciality_id" value="<?php echo $info[0]['speciality_id']; ?>" />
<div class="form-group row">
	<label for="speciality" class="col-sm-3 col-form-label">Speciality <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="speciality" name="speciality" placeholder="Speciality" value="<?php echo $info[0]['speciality']; ?>">
  	</div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>