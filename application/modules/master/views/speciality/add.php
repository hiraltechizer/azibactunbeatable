<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
	<label for="speciality" class="col-sm-3 col-form-label">Speciality <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="speciality" name="speciality" placeholder="Speciality" maxlength="50">
  	</div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>