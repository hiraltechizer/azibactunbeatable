<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="brand_id" value="<?php echo $info[0]['brand_id']; ?>" />
<div class="form-group row">
    <label for="division_id" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="division_id" name="division_id" data-placeholder="Select Division">
            <option value="<?php echo $info[0]['division_id']; ?>" selected><?php echo $info[0]['division_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
	<label for="brand_name" class="col-sm-3 col-form-label">Brand Name <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="brand_name" name="brand_name" placeholder="Brand Name" maxlength="50" value="<?php echo $info[0]['brand_name']; ?>">
  	</div>
</div>
<div class="form-group row">
    <label for="strength_id" class="col-sm-3 col-form-label">Strength <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-multiple" id="strength_id" name="strength_id[]" data-placeholder="Select Strength" multiple="multiple">
            <?php $i = 0; ?>
            <?php foreach ($strength_array as $key => $value) { ?>
                <option value="<?php echo $value['strength_id']; ?>" <?php if(in_array($value['strength_id'], $selected_strength)) { ?> selected="selected" <?php } ?> ><?php echo  $value['strength']; ?></option>
                <?php $i++; ?>
            <?php } ?>
        </select>
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>