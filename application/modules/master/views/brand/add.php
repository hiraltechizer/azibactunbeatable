<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
    <label for="division_id" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="division_id" name="division_id" data-placeholder="Select Division">
            <option value="">Select Division</option>
        </select>
    </div>
</div>
<div class="form-group row">
	<label for="brand_name" class="col-sm-3 col-form-label">Brand Name <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="brand_name" name="brand_name" placeholder="Brand Name" maxlength="50">
  	</div>
</div>
<div class="form-group row">
    <label for="strength_id" class="col-sm-3 col-form-label">Strength <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-multiple" id="strength_id" name="strength_id[]" data-placeholder="Select Strength" multiple="multiple">
            <option value="">Select Strength</option>
        </select>
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>