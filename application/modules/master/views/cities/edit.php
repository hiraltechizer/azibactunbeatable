<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="city_id" value="<?php echo $info[0]['city_id']; ?>" />
<div class="form-group row">
	<label for="state_id" class="col-sm-3 col-form-label">State <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="state_id" name="state_id" data-placeholder="Select State">
            <option value="<?php echo $info[0]['state_id']; ?>" selected><?php echo $info[0]['state_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
	<label for="city_name" class="col-sm-3 col-form-label">City Name <span class="text-danger">*</span></label>
  	<div class="col-sm-9">
    	<input type="text" class="form-control" id="city_name" name="city_name" placeholder="City Name" maxlength="50" value="<?php echo $info[0]['city_name']; ?>">
  	</div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>

<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>