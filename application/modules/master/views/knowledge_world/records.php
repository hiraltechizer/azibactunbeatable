<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['knowledge_world_id']; ?>
<tr>
    <td>
        <input type="checkbox" name="ids[]" value="<?php echo $id ?>" id="check_<?= $id ?>" class="form-check-input form-check-{color}" />
        <label for="check_<?= $id ?>"></label>
    </td>
     <?php 
        $fileUrl = '';
        if(file_exists('./'.$record['display_image'])) {
            $fileUrl = base_url().$record['display_image'];
        } 

        $OfileUrl = '';
        if(file_exists('./'.$record['upload_path'])) {
            $OfileUrl = base_url().$record['upload_path'];
        }         
    ?>
    <td><?php echo $record['quarter_name'] ?></td>
    <td><?php echo $record['document_type'] ?></td>
    <td><?php echo $record['title'] ?></td>
    <td><?php echo $record['description'] ?></td>
    <td>
        <?php if(!empty($fileUrl)) { ?>
             <a target="_blank" href="<?php echo $OfileUrl; ?>" style="cursor: pointer;;">
                <img src="<?php echo $fileUrl; ?>" alt="" style="width: 100px;height: 100px;">
            </a>
        <?php } ?>
    </td>
    <td><?php echo $record['insert_dt'] ?></td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>">
        <div class="row">
            <?php echo $this->ajax_pagination->create_links(); ?>    
        </div>
    </td>
</tr>