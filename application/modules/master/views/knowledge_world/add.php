<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
    <label for="quarter_id" class="col-sm-3 col-form-label">Quarter <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="quarter_id" name="quarter_id" data-placeholder="Select Quarter">
            <option value="">Select Quarter</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="document_type_id" class="col-sm-3 col-form-label">File Type <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="document_type_id" name="document_type_id" data-placeholder="Select File Type">
            <option value="">Select File Type</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="title" class="col-sm-3 col-form-label">Title <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" maxlength="150">
    </div>
</div>
<div class="form-group row">
    <label for="description" class="col-sm-3 col-form-label">Description </label>
    <div class="col-sm-9">
        <textarea name="description" id="description" class="form-control" rows="8"></textarea>
    </div>
</div>
<!--
<div class="form-group row">
    <label for="display_image" class="col-sm-3 col-form-label">Display Image </label>
    <?php $default_file = base_url().'assets/images/default-file.png'; ?>
    <div class="col-sm-9" id="input_filedv">
        <input type="file" name="display_image" class="dropify" id="display_image" data-max-file-size="3M" data-allowed-file-extensions="png jpeg jpg" />
        <p>Support format: .png,.jpeg,.jpg</p>
    </div>
</div> -->
<div class="form-group row">
    <label for="upload_path" class="col-sm-3 col-form-label">File <span class="text-danger">*</span></label>
    <div class="col-sm-9" id="input_filedv">
        <input type="file" name="upload_path" class="dropify" id="upload_path" data-max-file-size="20M" data-allowed-file-extensions="png jpeg jpg pdf wav aif mp3 mid ogg vlc mp4 mov wmv avi"/>
        <p>Support format: png, jpeg, jpg, pdf,wav,aif,mp3,mid,ogg,vlc,mp4,mov,wmv,avi</p>
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>