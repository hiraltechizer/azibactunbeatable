<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Knowledge_world extends Admin_Controller
{
	private $module = 'knowledge_world';
	private $controller = 'master/knowledge_world';
	private $model_name = 'mdl_knowledge_world';
	private $settings = [
		'permissions'=> ['add','download','remove'],
		'paginate_index' => 4
	];
	 private $scripts = ['knowledge_world.js'];

	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];

		parent::__construct($this->module, $this->controller, $this->model_name, $this->settings,  $this->scripts);
	}
}
