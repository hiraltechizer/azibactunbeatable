<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Brand extends Admin_Controller
{
	private $module = 'brand';
	private $controller = 'master/brand';
	private $model_name = 'mdl_brand';
	private $settings = [
		'permissions'=> ['add', 'edit', 'download'],
		'paginate_index' => 4
	];
	 private $scripts = ['brand.js'];

	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];

		parent::__construct($this->module, $this->controller, $this->model_name, $this->settings,  $this->scripts);
	}

	function _get_strength_data($strength_id) {
        return $this->model->getstrengthData($strength_id);
    }

	function options(){
	//	echo 'dd';die;
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options($s_term, 'brand_name', [], $page * $limit, $limit,[],'','brand_name ASC');
		//echo '<pre>';print_r($this->db->last_query());die;
		$_opt_count = count($this->model->get_options($s_term, 'brand_name'));

		foreach($_options as $option){
			$new['id'] = $option->brand_id;
			$new['text'] = $option->brand_name;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

	 function strengthoptions()
    {
       
        $this->session->is_Ajax_and_logged_in();

        $limit = $this->dropdownlength;
        $page = intval($_POST['page']) - 1;
        $page = ($page <= 0) ? 0 : $page;

        $s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
        
        $id = (isset($_POST['id'])) ? (int) $this->input->post('id') : 0;

        if($id){ $filters['brand_id'] = $id; }

        $new = array(); $json['results'] = array();
        $filters = [];

        $_options = $this->model->get_brand_strength($id);
        $_opt_count = count($this->model->get_brand_strength($id));

        foreach($_options as $option){
            $new['id'] = $option->strength_id;
            $new['text'] = $option->strength;

            array_push($json['results'], $new);
        }
        
        $more = ($_opt_count > count($_options)) ? TRUE : FALSE;
        $json['pagination']['more'] = $more;

        echo json_encode($json);
    }

}
