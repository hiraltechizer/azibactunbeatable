<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Document_type extends Admin_Controller
{
	private $module = 'document_type';
	private $controller = 'master/document_type';
	private $model_name = 'mdl_document_type';
	private $settings = [
		'permissions'=> ['add', 'edit', 'download', 'upload'],
		'paginate_index' => 4
	];

	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];

		parent::__construct($this->module, $this->controller, $this->model_name, $this->settings);
	}

	function options(){
	
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options($s_term, 'document_type', [], $page * $limit, $limit,[],'','document_type ASC');
		//echo '<pre>';print_r($this->db->last_query());die;
		$_opt_count = count($this->model->get_options($s_term, 'document_type'));

		foreach($_options as $option){
			$new['id'] = $option->document_type_id;
			$new['text'] = $option->document_type;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

	function uploadcsv(){


		$this->session->is_Ajax_and_logged_in();
		/*upload csv file */

		if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
		}

		if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
		}

		$file = $_FILES['csvfile']['tmp_name'];
		$handle = fopen($file, "r");
		$cnt = 0; $newrows = 0;

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

			if(count($data) !== 1) { continue; }

			if(! $cnt){
                $cnt++; continue;
            }
            
            $document_type = trim($data[0]);
         

            if( empty($document_type)){
                continue;
            }

            if(! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $document_type)){
                continue;
			}
			
            $docrecord = $this->model->get_records(
                [ 
                    'document_type'=> $document_type
                ], 'document_type', ['document_type_id'], '', 1
            );

            if(count($docrecord)) {
                continue;
            }

            $insert['document_type'] = $document_type;
    
            $this->model->_insert($insert);
            $newrows++;
		}

		fclose($handle);
		echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
	}
}
