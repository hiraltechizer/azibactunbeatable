<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class State extends Admin_Controller
{
	private $module = 'state';
	private $controller = 'master/state';
	private $model_name = 'mdl_state';
	private $settings = [
		'permissions'=> ['add', 'edit', 'download', 'upload'],
		'paginate_index' => 4
	];

	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];

		parent::__construct($this->module, $this->controller, $this->model_name, $this->settings);
	}

	function options(){
	
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options($s_term, 'state_name', [], $page * $limit, $limit,[],'','state_name ASC');
		//echo '<pre>';print_r($this->db->last_query());die;
		$_opt_count = count($this->model->get_options($s_term, 'state_name'));

		foreach($_options as $option){
			$new['id'] = $option->state_id;
			$new['text'] = $option->state_name;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

	function uploadcsv(){


		$this->session->is_Ajax_and_logged_in();
		/*upload csv file */

		if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
		}

		if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
		}

		$file = $_FILES['csvfile']['tmp_name'];
		$handle = fopen($file, "r");
		$cnt = 0; $newrows = 0;

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

			if(count($data) !== 1) { continue; }

			if(! $cnt){
                $cnt++; continue;
            }
            
            $state_name = trim($data[0]);
         

            if( empty($state_name)){
                continue;
            }

            if(! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $state_name)){
                continue;
			}
			
            $staterecord = $this->model->get_records(
                [ 
                    'state_name'=> $state_name
                ], 'state', ['state_id'], '', 1
            );

            if(count($staterecord)) {
                continue;
            }

            $insert['state_name'] = $state_name;
    
            $this->model->_insert($insert);
            $newrows++;
		}

		fclose($handle);
		echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
	}
}
