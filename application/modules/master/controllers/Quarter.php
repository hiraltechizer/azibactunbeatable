<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Quarter extends Admin_Controller
{
	private $module = 'quarter';
	private $controller = 'master/quarter';
	private $model_name = 'mdl_quarter';
	private $settings = [
		'permissions'=> ['add', 'edit', 'download',  'remove'],
		'paginate_index' => 4
    ];

	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];
		parent::__construct($this->module, $this->controller, $this->model_name, $this->settings);
	}


	function _get_quarter_data($quarter_id) {
        return $this->model->getquarterData($quarter_id);
    }

	function options(){
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
		//$id = (isset($_POST['sku_id'])) ? (int) $this->input->post('sku_id') : 0;

		$filters = [];
		if(!empty($id)) {
			//$filters['sku_id'] = $id;
		}

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options($s_term, 'quarter_name', $filters, $page * $limit, $limit,[],'','quarter_name ASC');
		
        $_opt_count = count($this->model->get_options($s_term, 'quarter_name', $filters));
        
		foreach($_options as $option){
			$new['id'] = $option->quarter_id;
			$new['text'] = $option->quarter_name;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}


	function suboptions(){
	
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
		$id = (isset($_POST['id'])) ? (int) $this->input->post('id') : 0;

		$filters = [];
		//if(!empty($id)) {
			$filters['q.quarter_id'] = $id;
		//}

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options1($s_term, 'month', $filters, $page * $limit, $limit,[],'','month ASC');
		
        $_opt_count = count($this->model->get_options1($s_term, 'month', $filters));
        
		foreach($_options as $option){
			$new['id'] = $option->quarter_sub_id;
			$new['text'] = $option->month;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}


	function suboptions1(){
	
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
		$id = (isset($_POST['id'])) ? (int) $this->input->post('id') : 0;

		$filters = [];
		if(!empty($id)) {
			$filters['q.quarter_id'] = $id;
		}

		#get month id 
		$MonthInfo = $this->model->get_records(['quarter_id' => $id], 'speak_altan_speak', ['quarter_sub_id']);

		$qsubid = [];
		if(count($MonthInfo)) {
			foreach ($MonthInfo as $key => $value) {
				array_push($qsubid, $value->quarter_sub_id);
			}	
			$filters['qs.quarter_sub_id'] = implode(",", $qsubid);
		}


		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options1($s_term, 'month', $filters, $page * $limit, $limit,[],'','month ASC');
		
        $_opt_count = count($this->model->get_options1($s_term, 'month', $filters));
        
		foreach($_options as $option){
			$new['id'] = $option->quarter_sub_id;
			$new['text'] = $option->month;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

	function uploadcsv(){
		$this->session->is_Ajax_and_logged_in();
		/*upload csv file */

		if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
		}

		if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
		}

		$file = $_FILES['csvfile']['tmp_name'];
		$handle = fopen($file, "r");
		$cnt = 0; $newrows = 0;

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

			if(count($data) !== 3) { continue; }

			if(! $cnt){
                $cnt++; continue;
            }

            $quarter_name = trim($data[0]); 
           	
           	if(empty($quarter_name) ) {
				continue;
            }

          
            $insert['quarter_name'] = $quarter_name;
           
            $this->model->_insert($insert);

            $newrows++;
		}

		fclose($handle);

		echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
	}

	function activeoptions(){
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';
		//$id = (isset($_POST['sku_id'])) ? (int) $this->input->post('sku_id') : 0;

		$filters = ['status' => 'Active'];
		if(!empty($id)) {
			//$filters['sku_id'] = $id;
		}

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options($s_term, 'quarter_name', $filters, $page * $limit, $limit,[],'','quarter_name ASC');
		
        $_opt_count = count($this->model->get_options($s_term, 'quarter_name', $filters));
        
		foreach($_options as $option){
			$new['id'] = $option->quarter_id;
			$new['text'] = $option->quarter_name;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

}
