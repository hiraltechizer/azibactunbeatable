<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cities extends Admin_Controller
{
	private $module = 'cities';
	private $controller = 'master/cities';
	private $model_name = 'mdl_cities';
	private $settings = [
		'permissions'=> ['add', 'edit', 'download', 'upload'],
		'paginate_index' => 4
	];
	private $scripts = ['state.js'];

	function __construct() {
		$this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];

		parent::__construct($this->module, $this->controller, $this->model_name, $this->settings, $this->scripts);
	}

	function options(){
	//	echo 'dd';die;
		$this->session->is_Ajax_and_logged_in();

		$limit = $this->dropdownlength;
		$page = intval($_POST['page']) - 1;
		$page = ($page <= 0) ? 0 : $page;

		$s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';

		$new = array(); $json['results'] = array();

		$_options = $this->model->get_options($s_term, 'city_name', [], $page * $limit, $limit,[],'','city_name ASC');
		//echo '<pre>';print_r($this->db->last_query());die;
		$_opt_count = count($this->model->get_options($s_term, 'city_name'));

		foreach($_options as $option){
			$new['id'] = $option->city_id;
			$new['text'] = $option->city_name;

			array_push($json['results'], $new);
		}
		
		$more = ($_opt_count > count($_options)) ? TRUE : FALSE;
		$json['pagination']['more'] = $more;

		echo json_encode($json);
	}

	function uploadcsv(){


		$this->session->is_Ajax_and_logged_in();
		/*upload csv file */

		if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
		}

		if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
			echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
		}

		$file = $_FILES['csvfile']['tmp_name'];
		$handle = fopen($file, "r");
		$cnt = 0; $newrows = 0;

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

			if(count($data) !== 2) { continue; }

			if(! $cnt){
                $cnt++; continue;
            }
            
           
            $city_name = trim($data[0]);
         	 $state_name = trim($data[1]);

            if( empty($state_name) || empty($city_name)){
                continue;
            }

            if(! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $state_name) || ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $city_name)){
                continue;
			}
			
            $state_record = $this->model->get_records(
                [ 
                    'state_name'=> $state_name
                ], 'state', ['state_id'], '', 1
            );

            if(!count($state_record)) {
                continue;
            }

            $state_id = $state_record[0]->state_id;

            $cityInfo =$this->model->get_records(['state_id' => $state_id, 'city_name' => $city_name], 'cities', ['city_id']);

            if(count($cityInfo)) {
            	continue;
            }

            $insert['state_id'] = $state_id;
            $insert['city_name'] = $city_name;
    
            $this->model->_insert($insert);
            $newrows++;
		}

		fclose($handle);
		echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
	}
}
