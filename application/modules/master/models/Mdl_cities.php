<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_cities extends MY_Model {

	private $p_key = 'city_id';
    private $table = 'cities';
    private $tb_alias = 'c';
	private $fillable = ['city_name'];
	private $column_list = ['City', 'State', 'Created On'];
    private $csv_columns = ['City', 'State'];
    private $testcsv_columns = [];

	function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
	}
	function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }

    function get_column_list() {
        return $this->column_list;
	}

	function get_filters() {
        return [
			
			[
				'field_name'=>'city_name',
				'field_label'=> 'City',
			],
			[
				'field_name'=>'state_name',
				'field_label'=> 'State',
			]
        ];
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);
    	$q = $this->db->select('c.city_id, c.city_name,c.insert_dt, s.state_name, s.state_id')
        ->from('cities c')
        ->join('state s','s.state_id = c.state_id')
        ;
				
		if(sizeof($f_filters)) { 
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

		if(! $count) {
			$q->order_by('c.update_dt desc, c.city_id desc');
		}

		if(!empty($limit)) { $q->limit($limit, $offset); }        
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
		return $collection;
    }	
    
    function validate($type)
	{
       if($type == 'save') {
			return [
				[
					'field' => 'state_id',
					'label' => 'State',
					'rules' => 'trim|required|xss_clean'
                ],
				[
					'field' => 'city_name',
					'label' => 'City',
					'rules' => 'trim|required|valid_name|unique_record[add.table.cities.city_name.' . $this->input->post('city_name') . '.state_id.'. (int) $this->input->post('state_id') .']|xss_clean'
                ]
			];
		}

		if($type == 'modify') {
            
            return [
				[
					'field' => 'state_id',
					'label' => 'State',
					'rules' => 'trim|required|xss_clean'
                ],
				[
					'field' => 'city_name',
					'label' => 'City Name',
					'rules' => 'trim|required|valid_name|unique_record[edit.table.cities.city_name.' . $this->input->post('city_name') . '.state_id.'. (int) $this->input->post('state_id') .'.city_id.'. (int) $this->input->post('city_id') .']|xss_clean'
				],
			];
		}
    }

	function save(){
		/*Load the form validation Library*/
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validate('save'));
		
		if(! $this->form_validation->run()){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');
	        
	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}
		
        $data = $this->process_data($this->fillable, $_POST);
        
        $id = $this->_insert($data);
        
        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
	}
	
	function modify(){
		/*Load the form validation Library*/
		$this->load->library('form_validation');

		$is_Available = $this->check_for_posted_record($this->p_key, $this->table);
		if(! $is_Available['status']){ return $is_Available; }
		
		$this->form_validation->set_rules($this->validate('modify'));

		if(! $this->form_validation->run() ){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');

	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}		
		
        $data = $this->process_data($this->fillable, $_POST);

        $p_key = $this->p_key;
        $id = (int) $this->input->post($p_key);

        $status = (int) $this->_update([$p_key => $id], $data);

        if(! $status){
			$response['message'] = 'Internal Server Error';
			$response['status'] = FALSE;
			return $response;
		}

		$response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
	}

	function remove(){
		
		if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
			$ids = $this->input->post('ids');
			$response = $this->_delete($this->p_key, $ids, $this->table);

			$msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
			return ['msg'=> $msg];
		}

		return ['msg'=> 'No Records Selected'];
	}

	function _format_data_to_export($data){
		
		$resultant_array = [];
		
		foreach ($data as $rows) {
			$records['State'] = $rows['state_name'];
			$records['City'] = $rows['city_name'];
			$records['Created On'] = $rows['insert_dt'];
			
			array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}