<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_quarter extends MY_Model {

	private $p_key = 'quarter_id';
    private $table = 'quarter';
    private $tb_alias = 'q';
	private $fillable = ['quarter_name'];
	private $column_list = ['Quarter', 'Quarter Month', 'Status','Created On'];
    private $csv_columns = ['Quarter', 'Quarter Month','Status'];
    private $testcsv_columns = [];
    

	function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
	}

	function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
           	 [
                'field_name'=>'q|quarter_name',
                'field_label'=> 'Quarter',
            ],[],
             [
                'field_name'=>'q|status',
                'field_label'=> 'Status',
            ]
        ];
	}
	
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

    	$q = $this->db->select('q.quarter_name,q.quarter_id, q.status,q.insert_dt')
		->from('quarter q');

				
		if(sizeof($f_filters)) { 
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                 $key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

		if(! $count) {
			$q->order_by('q.quarter_id DESC,q.insert_dt DESC');
		}

		if(!empty($limit)) { $q->limit($limit, $offset); }        
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
		return $collection;
    }	
    
    function validate($type)
	{
		if($type == 'save') {
			return [
				
                [
					'field' => 'quarter_name',
					'label' => 'Quarter Name',
					'rules' => 'trim|required|valid_name|max_length[150]|xss_clean'
                ]
			];
		}

		if($type == 'modify') {
			return [
				
				[
					'field' => 'quarter_name',
					'label' => 'Quarter Name',
					'rules' => 'trim|required|valid_name|max_length[150]|xss_clean'
				]
			];
		}
    }

	function save(){

		/*Load the form validation Library*/
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validate('save'));
		
		$quarter = FALSE;
		if(!isset($_POST['month_id'])) {
			$quarter = TRUE;
		}
		

		if(!$this->form_validation->run() || $quarter) {
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');

	        if(! $quarter){
				$errors['month_id'] = '<label class="form-line error">Please select Quarter</label><br/><br/>';
			}
	        
	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}
		
        $data = $this->process_data($this->fillable, $_POST);
        	
        $update_data['status'] = 'Disable';
        $this->model->_update([], $update_data, 'quarter');	

        $id = $this->_insert($data);
        
        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        if($id) {

        	if(!$quarter) {

        		$temp_insert_data = [];
        		foreach ($_POST['month_id'] as $key => $value) {
        			$temp_insert_data['quarter_id'] = $id;
        			$temp_insert_data['month_id'] = $value;

        			$this->_insert($temp_insert_data, 'quarter_sub');
        		}
        	}
        }


        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
	}
	
	function modify(){
		/*Load the form validation Library*/
		$this->load->library('form_validation');

		$quarter = FALSE;
		if(!isset($_POST['month_id'])) {
			$quarter = TRUE;
		}

		$is_Available = $this->check_for_posted_record($this->p_key, $this->table);
		if(! $is_Available['status']){ return $is_Available; }
		
		$this->form_validation->set_rules($this->validate('modify'));

		if(! $this->form_validation->run() || $quarter){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');

	         if($quarter){
				$errors['month_id'] = '<label class="form-line error">Please select Quarter</label><br/><br/>';
			}

	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}		
		
        $data = $this->process_data($this->fillable, $_POST);

        $p_key = $this->p_key;
        $id = $quarter_id = (int) $this->input->post($p_key);

        $data['status'] = $qstatus	=	$this->input->post('status');
       
        if($qstatus == 'Active') {
        	
        	$update_data['status'] = 'Disable';
        	$this->model->_update([], $update_data, 'quarter');	
        }
       	
        $status = (int) $this->_update([$p_key => $id], $data);

        if(! $status){
			$response['message'] = 'Internal Server Error';
			$response['status'] = FALSE;
			return $response;
		}

		if(!empty($_POST['month_id'])) {

				#get already assign disease  info
       			$quarterinfo	=	$this->model->get_records(['quarter_id'	=>	$quarter_id],'quarter_sub', ['quarter_sub_id', 'month_id']);

   				$puIds = [];
   				#Iterate throught patients info to get unique id
   				if(!empty($quarterinfo)) {
   					foreach ($quarterinfo as $key => $val) {
   						array_push($puIds, $val->month_id);
   					}	
   				}

				$postpuIds	=	[];
				for($i=0;$i<count($_POST['month_id']);$i++)
	        	{

	        		$month_id = $_POST['month_id'][$i];
	        		//$diseaseData = $this->get_records([ 'quarter_id'=> $quarter ], 'quarter_sub', ['disease_id', 'disease_name']);

	        		array_push($postpuIds, $month_id);
	        	}

		        $diffrent_ids	=	[];
				if(count($puIds) != count($postpuIds)) {
					$diffrent_ids =  array_merge(array_diff($postpuIds, $puIds), array_diff($puIds, $postpuIds));
				}	

				
				if($diffrent_ids) {
					foreach ($diffrent_ids as $key => $value) {

						$ret = $this->_delete_by_criteria(['month_id' => $value, 'quarter_id' => $quarter_id], 'quarter_sub');

						if(!$ret) {
							$response['status'] = FALSE;
							$response['message'] = 'Error! record can not be deleted.';
							return $response;
						}

					}
				}		


				for($i=0;$i<count($_POST['month_id']);$i++)
		        {	
		        	$month_id = $_POST['month_id'][$i];
					
		        	//$diseaseData = $this->get_records([ 'quarter'=> $quarter ], 'disease', ['disease_id', 'disease_name']);

		        	$rsquarterInfo = $this->get_records(['quarter_id'=> $quarter_id, 'month_id' => $month_id], 'quarter_sub', ['quarter_sub_id']);


					$insert_quarter['month_id'] = $month_id;
					$insert_quarter['quarter_id'] = $quarter_id;
					
					if(!count($rsquarterInfo)) {
		        		$quarter_sub_id = (int) $this->_insert($insert_quarter, 'quarter_sub');
	        		} else {
	        			$ret = (int) $this->_update(['quarter_sub_id' => $rsquarterInfo[0]->quarter_sub_id], $insert_quarter, 'quarter_sub');
	        		}
				}
			}



		$response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
	}

	function remove(){
		
		if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
			$ids = $this->input->post('ids');
			$response = $this->_delete($this->p_key, $ids, $this->table);

			$msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
			return ['msg'=> $msg];
		}

		return ['msg'=> 'No Records Selected'];
	}

	function _format_data_to_export($data){
		
		$resultant_array = [];
		
		foreach ($data as $rows) {
			$records['Quarter Name'] = $rows['quarter_name'];

			$selcted_txt = '';
			$quartermonthInfo = $this->get_records(['quarter_id' => $rows['quarter_id']], 'quarter_sub', ['quarter_sub_id','month_id']);

			if(count($quartermonthInfo)) {

				$i = count($quartermonthInfo);
				foreach ($quartermonthInfo as $key => $value) {

					$monthInfo = $this->get_records(['month_id' => $value->month_id], 'month', ['month_id', 'month'], '', 1);

					$selcted_txt .= $monthInfo[0]->month;

					$i--;
					if($i > 0) {
						$selcted_txt .= ', ';
					}
					
					
				}
			}

			$records['Quarter Month'] = $selcted_txt;
			$records['Status']	=	$rows['status'];
			$records['Created On']	=	$rows['insert_dt'];
			array_push($resultant_array, $records);
		}
		return $resultant_array;
	}

	function getquarterData($quarter_id) {

		#get quarterInfo
		$quarterInfo = $this->get_records(['quarter_id' => $quarter_id], 'quarter', ['quarter_id'], '', 1);



		$quartermonthInfo = $this->get_records(['quarter_id' => $quarter_id], 'quarter_sub', ['quarter_sub_id','month_id'], 'month_id ASC');

		$selcted_txt = '';
			if(count($quartermonthInfo)) {

				$i = count($quartermonthInfo);
				foreach ($quartermonthInfo as $key => $value) {

					$monthInfo = $this->get_records(['month_id' => $value->month_id], 'month', ['month_id', 'month'], '', 1);

					$selcted_txt .= $monthInfo[0]->month;

					$i--;
					if($i > 0) {
						$selcted_txt .= ', ';
					}
					
					
				}
			}

			return $selcted_txt;

	}

	function quatermonth() {

		return ['1'	=> 'January',
				'2'	=>	'February',
				'3'	=>	'March',
				'4'	=>	'April',
				'5'	=>	'May',
				'6'	=>	'June',
				'7'	=>	'July',
				'8'	=>	'August',
				'9'	=>	'September',
				'10'	=>	'October',
				'11'	=>	'November',
				'12'	=>	'December'
				];
	}

	function get_options1($s_key = '', $field = '', $filters = [], $offset = 0, $limit = 0, $select = [], $table = ''){

		$q = $this->db->select('qs.*,m.*,q.*');
		$q->from("quarter_sub qs");
		$q->join("quarter q",'q.quarter_id = qs.quarter_id');
		$q->join("month m", 'm.month_id = qs.month_id');

		if(sizeof($filters)){
			$q->where($filters);
		}

		if(!empty($s_key)) {

			$where_condition1 = " (" . $field . " like '%". $q->escape_like_str($s_key) ."%') ";
			$q->where($where_condition1, NULL, FALSE);
		}
		// $p_key = $this->p_key;
		$q->order_by("qs.update_dt desc");

		if(!empty($limit)) { $q->limit($limit, $offset); }

		$collection = $q->get()->result();
		//echo '<pre>';
		//echo $this->db->last_query();die;
		return $collection;
    }
}