<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_knowledge_world extends MY_Model {

	private $p_key = 'knowledge_world_id';
    private $table = 'knowledge_world';
    private $tb_alias = 'kw';
	private $fillable = ['quarter_id','title','description','upload_path', 'document_type_id'];
	private $column_list = ['Quarter', 'File Type','Title','Discription','Display Image','Created On'];
    private $csv_columns = [];
    private $testcsv_columns = [];

	function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
	}
	function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }

    function get_column_list() {
        return $this->column_list;
	}

	function get_filters() {
        return [
             [
                'field_name'=>'q|quarter_name',
                'field_label'=> 'Quarter',
            ],
            [
                'field_name'=>'dt|document_type',
                'field_label'=> 'Document TYpe',
            ],
            [
                'field_name'=>'kw|title',
                'field_label'=> 'Title',
            ],
            [
                'field_name'=>'kw|description',
                'field_label'=> 'Description',
            ]
        ];
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);
    	$q = $this->db->select('kw.knowledge_world_id,kw.insert_dt,q.quarter_name,q.quarter_id,dt.document_type,kw.upload_path,kw.title,kw.document_type_id,kw.description,kw.display_image')
        ->from('knowledge_world kw')
       ->join('quarter q','q.quarter_id = kw.quarter_id')
       ->join('document_type dt','dt.document_type_id = kw.document_type_id')
        ;
				
		if(sizeof($f_filters)) { 
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                $key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

		if(! $count) {
			$q->order_by('kw.update_dt desc, kw.knowledge_world_id desc');
		}

		if(!empty($limit)) { $q->limit($limit, $offset); }        
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
		return $collection;
    }	
    
    function validate($type)
	{
       if($type == 'save') {
			return [
				[
					'field' => 'quarter_id',
					'label' => 'Quarter',
					'rules' => 'trim|required|xss_clean'
                ],
                [
					'field' => 'document_type_id',
					'label' => 'Document Type',
					'rules' => 'trim|required|xss_clean'
                ],
                [
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|max_length[150]|xss_clean'
                ]
                ,
                [
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|xss_clean'
                ]
			];
		}

		if($type == 'modify') {
            
            return [
            	[
					'field' => 'quarter_id',
					'label' => 'Quarter',
					'rules' => 'trim|required|xss_clean'
                ],
                [
					'field' => 'document_type_id',
					'label' => 'Document Type',
					'rules' => 'trim|required|xss_clean'
                ]
                ,
                [
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|max_length[150]|xss_clean'
                ],
                [
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|xss_clean'
                ]
			];
		}
    }

	function save(){

		/*Load the form validation Library*/
		$this->load->library('form_validation');
		 $this->load->library('get_id3');
		$this->load->helper('upload_media');
		$this->form_validation->set_rules($this->validate('save'));
		
		if(! $this->form_validation->run()){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');

	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}
		
		$quarter_id = $this->input->post('quarter_id');
		$document_type_id = $this->input->post('document_type_id');

		if(empty($_FILES['upload_path']['size'])) {
			$upload_errors['status'] = FALSE;
			$upload_errors['errors'] = ['upload_path' => '<label class="error">Files Required.</label>'];
			return $upload_errors;
		}

		/*if(empty($_FILES['display_image']['size'])) {
			$upload_errors['status'] = FALSE;
			$upload_errors['errors'] = ['display_image' => '<label class="error">Display Image Required.</label>'];
			return $upload_errors;
		}*/

		#get document type
		$documentTypeInfo = $this->get_records(['document_type_id' => $document_type_id], 'document_type', ['document_type']);

		$document_type = $documentTypeInfo[0]->document_type;

		$path = $_FILES['upload_path']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);


		$file_icon = '';
		$duration = '0';
		if(strtolower($document_type) == 'audio') {
			if(!in_array($ext,['wav','aif','mp3','mid','ogg','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload audio files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/audio.png";
		} else if(strtolower($document_type) == 'video') {
			if(!in_array($ext,['mp4','mov','wmv','avi','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload video files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/video.png";
		} else if(strtolower($document_type) == 'ppt') {
			if(!in_array($ext,['ppt','pptx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload ppt files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'pdf') {
			if(!in_array($ext,['pdf'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/pdf_file.png";
		}else if(strtolower($document_type) == 'spredsheet' || strtolower($document_type) == 'excel') {
			if(!in_array($ext,['xls','xlsx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'document' || strtolower($document_type) == 'doc') {
			if(!in_array($ext,['doc','docx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload doc files Only.</label>'];
				return $upload_errors;
			}
		} else {
			if(!in_array($ext,['png', 'jpeg', 'jpg'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload image files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/images_file.png";
		}

		$upload_filename = $this->UploadFile($quarter_id, $document_type_id);

		//$displayimage = $this->UploadDisplayImage($quarter_id, $document_type_id);

		$duration = '0';
		if(strtolower($document_type) == 'audio' || strtolower($document_type) == 'video') {
			$getID3 = new getID3;
			$FileInfo = $getID3->analyze($upload_filename);
			$duration_string = $FileInfo['playtime_string'];
			$duration =	$this->format_duration($duration_string);
		}

		$inserted_ids = [];
		if(!empty($upload_filename)) {
			$upload_result = [];
			$upload_result['quarter_id'] = $quarter_id;
			$upload_result['document_type_id']	=	$document_type_id;
			$upload_result['title']	=	$this->input->post('title');
			$upload_result['description']	=	$this->input->post('description');
			$upload_result['upload_path'] = $upload_filename;
			$upload_result['display_image'] = $file_icon;
			$upload_result['duration']		=	$duration;

			$id = $this->_insert($upload_result);
		}

        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
	}

	function format_duration($duration){

	    // The base case is A:BB
	    if(strlen($duration) == 4){
	        return "00:0" . $duration;
	    }
	    // If AA:BB
	    else if(strlen($duration) == 5){
	        return "00:" . $duration;
	    }   // If A:BB:CC
	    else if(strlen($duration) == 7){
	        return "0" . $duration;
	    }
	}
	function UploadFile($quarter_id, $document_type_id) {
		$upload_data = $errors = array();
        $uploadPath = "uploads/knowledge_world/$quarter_id/$document_type_id/files";
        $s3uploadPath = '';

        if(!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, TRUE);
        }

       $file_status = $this->upload_file_response("upload_path", $uploadPath, '', ['png', 'jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi']);

        if(isset($file_status['status'])){
            $file_status = is_array($file_status['data']) ? $file_status['data'] : (array)$file_status['data'];
            for($i=0;$i < count($file_status); ++$i){
                array_push($upload_data, $file_status[$i]) ;
            }
        } else {
            array_push($errors, $file_status['error']);
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
    
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $filename = '';
        if(count($upload_data)) {
            foreach ($upload_data as $key => $value) {
               $filename = $value;
            }
            
        } 
        return $filename;
	}

	function UploadDisplayImage($quarter_id, $document_type_id) {
		$upload_data = $errors = array();
        $uploadPath = "uploads/knowledge_world/$quarter_id/$document_type_id/displayimage";
        $s3uploadPath = '';

        if(!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, TRUE);
        }

       $file_status = $this->upload_file_response("display_image", $uploadPath, '', ['png', 'jpeg', 'jpg']);

        if(isset($file_status['status'])){
            $file_status = is_array($file_status['data']) ? $file_status['data'] : (array)$file_status['data'];
            for($i=0;$i < count($file_status); ++$i){
                array_push($upload_data, $file_status[$i]) ;
            }
        } else {
            array_push($errors, $file_status['error']);
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
    
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $filename = '';
        if(count($upload_data)) {
            foreach ($upload_data as $key => $value) {
               $filename = $value;
            }
            
        } 
        return $filename;
	}

	
	function modify(){
		/*Load the form validation Library*/
		$this->load->library('form_validation');

		$is_Available = $this->check_for_posted_record($this->p_key, $this->table);
		if(! $is_Available['status']){ return $is_Available; }
		
		$this->form_validation->set_rules($this->validate('modify'));

		$strength = FALSE;
		if(!isset($_POST['strength_id'])) {
			$strength = TRUE;
		}

		if(! $this->form_validation->run() || $strength){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');

	         if($strength){
				$errors['strength_id[]'] = '<label class="form-line error">Please select Strength</label><br/><br/>';
			}

	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}		

		$division_id = $this->input->post('division_id');
		
        $data = $this->process_data($this->fillable, $_POST);

        $p_key = $this->p_key;
        $id = $brand_id =  (int) $this->input->post($p_key);

        $status = (int) $this->_update([$p_key => $id], $data);

        if(! $status){
			$response['message'] = 'Internal Server Error';
			$response['status'] = FALSE;
			return $response;
		}

		if(!empty($_POST['strength_id'])) {

			#get already assign disease  info
   			$strengthinfo	=	$this->model->get_records(['brand_id'	=>	$brand_id],'brand_strength', ['brand_strength_id', 'strength_id']);

			$puIds = [];
			#Iterate throught patients info to get unique id
			if(!empty($strengthinfo)) {
				foreach ($strengthinfo as $key => $val) {
					array_push($puIds, $val->strength_id);
				}	
			}

			$postpuIds	=	[];
			for($i=0;$i<count($_POST['strength_id']);$i++)
        	{

        		$strength_id = $_POST['strength_id'][$i];
        		//$diseaseData = $this->get_records([ 'quarter_id'=> $quarter ], 'quarter_sub', ['disease_id', 'disease_name']);

        		array_push($postpuIds, $strength_id);
        	}

	        $diffrent_ids	=	[];
			if(count($puIds) != count($postpuIds)) {
				$diffrent_ids =  array_merge(array_diff($postpuIds, $puIds), array_diff($puIds, $postpuIds));
			}	

			
			if($diffrent_ids) {
				foreach ($diffrent_ids as $key => $value) {

					$ret = $this->_delete_by_criteria(['strength_id' => $value, 'brand_id' => $brand_id], 'brand_strength');

					if(!$ret) {
						$response['status'] = FALSE;
						$response['message'] = 'Error! record can not be deleted.';
						return $response;
					}

				}
			}		


			for($i=0;$i<count($_POST['strength_id']);$i++)
	        {	
	        	$strength_id = $_POST['strength_id'][$i];
				
	        	//$diseaseData = $this->get_records([ 'quarter'=> $quarter ], 'disease', ['disease_id', 'disease_name']);

	        	$rsbrandInfo = $this->get_records(['brand_id'=> $brand_id, 'strength_id' => $strength_id], 'brand_strength', ['brand_strength_id']);

	        	$insert_brand['division_id'] = $division_id;
				$insert_brand['strength_id'] = $strength_id;
				$insert_brand['brand_id'] = $brand_id;
				
				if(!count($rsbrandInfo)) {
	        		$brand_strength_id = (int) $this->_insert($insert_brand, 'brand_strength');
        		} else {
        			$ret = (int) $this->_update(['brand_strength_id' => $rsbrandInfo[0]->brand_strength_id], $insert_brand, 'brand_strength');
        		}
			}
		}

		$response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
	}

	function remove(){
		
		if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
			$ids = $this->input->post('ids');

			#get record for unlink folder
			$knowledgewordinfo = $this->get_records(['knowledge_world_id' => $ids], 'knowledge_world');

			$temp_array	=	[];
			if(count($knowledgewordinfo)) {
				foreach ($knowledgewordinfo as $key => $value) {
					$quarter_id 		= $value->quarter_id;
					$document_type_id	=	$value->document_type_id;
					$file_name			=	$value->upload_path;

					#deleted info 
					$response = $this->_delete($this->p_key, $value->knowledge_world_id, 'knowledge_world');		
					unlink(getcwd().'/'.$file_name);
					array_push($temp_array,$response);
				}	
			}
			
			$msg = ($temp_array) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
			return ['msg'=> $msg];
		}

		return ['msg'=> 'No Records Selected'];
	}

	function _format_data_to_export($data){
		
		$resultant_array = [];
		
		foreach ($data as $rows) {

			$OfileUrl = '';
	        if(file_exists('./'.$rows['upload_path'])) {
	            $OfileUrl = base_url().$rows['upload_path'];
	        } 

			$records['Quarter'] = $rows['quarter_name'];
			$records['File Type']	=	$rows['document_type'];
			$records['Title']	=	$rows['title'];
			$records['Description']	=	$rows['description'];
			$records['Files']		=	$OfileUrl;
			$records['Created On'] = $rows['insert_dt'];
			
			array_push($resultant_array, $records);
		}
		return $resultant_array;
	}


	function upload_file_response($file_name, $uploadPath, $s3uploadPath, $allowed_types = ['png', 'jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi']){

		$file_response = upload_media($file_name, $uploadPath, $allowed_types, 100000000000, 0, $s3uploadPath);

		$upload_file_count = !empty($_FILES[$file_name]['name']) ? 1:0;

		if($upload_file_count > 1){
			$temp = array();
			$temp['status'] = TRUE;
			for($i=0; $i < $upload_file_count; ++$i){
				if(!empty($file_response[$i]['errors'])){
					$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
					$temp['status'] = FALSE;
				} else {
					#echo 'asfsaf';die;
					$temp['data'][] = $file_response[$i]['file_name'];
				}
			}
			return $temp;
		}else{

			if(!empty($file_response['errors'])){
				$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
				$temp['status'] = FALSE;

				return $temp;
			}

			if($file_response) {

				$file_name = [];
				foreach ($file_response as $key => $value) {
					if(!empty($value['file_name'])){
						$temp['status'] = TRUE;
						array_push($file_name, $value['file_name']);
						
					}
				}
				$temp['data'] = $file_name;
				return $temp;
			}

			
		}
	}
}