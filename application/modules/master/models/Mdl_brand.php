<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_brand extends MY_Model {

	private $p_key = 'brand_id';
    private $table = 'brands';
    private $tb_alias = 'b';
	private $fillable = ['brand_name', 'division_id'];
	private $column_list = ['Division','Brand', 'Strength','Created On'];
    private $csv_columns = ['Brand Name'];
    private $testcsv_columns = [];

	function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
	}
	function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }

    function get_column_list() {
        return $this->column_list;
	}

	function get_filters() {
        return [
             [
                'field_name'=>'d|division_name',
                'field_label'=> 'Division',
            ],
            [
                'field_name'=>'b|brand_name',
                'field_label'=> 'Brand',
            ]
        ];
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);
    	$q = $this->db->select('b.brand_id, b.brand_name,d.division_id,d.division_name,b.insert_dt')
        ->from('brands b')
        ->join('division d','d.division_id = b.division_id')
        ;
				
		if(sizeof($f_filters)) { 
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                $key = str_replace('|', '.', $key);                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

		if(! $count) {
			$q->order_by('b.update_dt desc, b.brand_id desc');
		}

		if(!empty($limit)) { $q->limit($limit, $offset); }        
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
		return $collection;
    }	
    
    function validate($type)
	{
       if($type == 'save') {
			return [
				[
					'field' => 'division_id',
					'label' => 'Division',
					'rules' => 'trim|required|xss_clean'
                ],
				[
					'field' => 'brand_name',
					'label' => 'Brand',
					'rules' => 'trim|required|max_length[50]|valid_name|unique_key[brands.brand_name]|xss_clean'
                ]
			];
		}

		if($type == 'modify') {
            
            return [
            	[
					'field' => 'division_id',
					'label' => 'Division',
					'rules' => 'trim|required|xss_clean'
                ],
				[
					'field' => 'brand_name',
					'label' => 'Brand',
					'rules' => 'trim|required|max_length[50]|valid_name|unique_key[brands.brand_name.brand_id.'. (int) $this->input->post('brand_id') .']|xss_clean'
                ]
			];
		}
    }

	function save(){
		/*Load the form validation Library*/
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validate('save'));
		
		$strength = FALSE;
		if(!isset($_POST['strength_id'])) {
			$strength = TRUE;
		}

		if(! $this->form_validation->run() || $strength){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');
	        
	        if($strength){
				$errors['strength_id[]'] = '<label class="form-line error">Please select Strength</label><br/><br/>';
			}

	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}
		
		$division_id = $this->input->post('division_id');

        $data = $this->process_data($this->fillable, $_POST);

        $id = $this->_insert($data);
        
        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

       if(!$strength) {

			$temp_insert_data = [];
			foreach ($_POST['strength_id'] as $key => $value) {
				$temp_insert_data['brand_id'] = $id;
				$temp_insert_data['division_id'] = $division_id;
				$temp_insert_data['strength_id'] = $value;

				$this->_insert($temp_insert_data, 'brand_strength');
			}
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
	}
	
	function modify(){
		/*Load the form validation Library*/
		$this->load->library('form_validation');

		$is_Available = $this->check_for_posted_record($this->p_key, $this->table);
		if(! $is_Available['status']){ return $is_Available; }
		
		$this->form_validation->set_rules($this->validate('modify'));

		$strength = FALSE;
		if(!isset($_POST['strength_id'])) {
			$strength = TRUE;
		}

		if(! $this->form_validation->run() || $strength){
			$errors = array();	        
	        foreach ($this->input->post() as $key => $value)
	            $errors[$key] = form_error($key, '<label class="error">', '</label>');

	         if($strength){
				$errors['strength_id[]'] = '<label class="form-line error">Please select Strength</label><br/><br/>';
			}

	        $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
		}		

		$division_id = $this->input->post('division_id');
		
        $data = $this->process_data($this->fillable, $_POST);

        $p_key = $this->p_key;
        $id = $brand_id =  (int) $this->input->post($p_key);

        $status = (int) $this->_update([$p_key => $id], $data);

        if(! $status){
			$response['message'] = 'Internal Server Error';
			$response['status'] = FALSE;
			return $response;
		}

		if(!empty($_POST['strength_id'])) {

			#get already assign disease  info
   			$strengthinfo	=	$this->model->get_records(['brand_id'	=>	$brand_id],'brand_strength', ['brand_strength_id', 'strength_id']);

			$puIds = [];
			#Iterate throught patients info to get unique id
			if(!empty($strengthinfo)) {
				foreach ($strengthinfo as $key => $val) {
					array_push($puIds, $val->strength_id);
				}	
			}

			$postpuIds	=	[];
			for($i=0;$i<count($_POST['strength_id']);$i++)
        	{

        		$strength_id = $_POST['strength_id'][$i];
        		//$diseaseData = $this->get_records([ 'quarter_id'=> $quarter ], 'quarter_sub', ['disease_id', 'disease_name']);

        		array_push($postpuIds, $strength_id);
        	}

	        $diffrent_ids	=	[];
			if(count($puIds) != count($postpuIds)) {
				$diffrent_ids =  array_merge(array_diff($postpuIds, $puIds), array_diff($puIds, $postpuIds));
			}	

			
			if($diffrent_ids) {
				foreach ($diffrent_ids as $key => $value) {

					$ret = $this->_delete_by_criteria(['strength_id' => $value, 'brand_id' => $brand_id], 'brand_strength');

					if(!$ret) {
						$response['status'] = FALSE;
						$response['message'] = 'Error! record can not be deleted.';
						return $response;
					}

				}
			}		


			for($i=0;$i<count($_POST['strength_id']);$i++)
	        {	
	        	$strength_id = $_POST['strength_id'][$i];
				
	        	//$diseaseData = $this->get_records([ 'quarter'=> $quarter ], 'disease', ['disease_id', 'disease_name']);

	        	$rsbrandInfo = $this->get_records(['brand_id'=> $brand_id, 'strength_id' => $strength_id], 'brand_strength', ['brand_strength_id']);

	        	$insert_brand['division_id'] = $division_id;
				$insert_brand['strength_id'] = $strength_id;
				$insert_brand['brand_id'] = $brand_id;
				
				if(!count($rsbrandInfo)) {
	        		$brand_strength_id = (int) $this->_insert($insert_brand, 'brand_strength');
        		} else {
        			$ret = (int) $this->_update(['brand_strength_id' => $rsbrandInfo[0]->brand_strength_id], $insert_brand, 'brand_strength');
        		}
			}
		}

		$response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
	}

	function remove(){
		
		if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
			$ids = $this->input->post('ids');
			$response = $this->_delete($this->p_key, $ids, $this->table);

			$msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
			return ['msg'=> $msg];
		}

		return ['msg'=> 'No Records Selected'];
	}

	function _format_data_to_export($data){
		
		$resultant_array = [];
		
		foreach ($data as $rows) {

			$sel_strength = 	$this->getstrengthData($rows['brand_id']);

			$records['Brand'] = $rows['brand_name'];
			$records['Strength']	=	$sel_strength;
			$records['Division'] = $rows['division_name'];
			$records['Created On'] = $rows['insert_dt'];
			
			array_push($resultant_array, $records);
		}
		return $resultant_array;
	}


	function getstrengthData($brand_id) {

		$selcted_txt = '';
		$strengthInfo = $this->get_records(['brand_id' => $brand_id], 'brand_strength', ['brand_strength_id','strength_id']);

			if(count($strengthInfo)) {

				$i = count($strengthInfo);
				foreach ($strengthInfo as $key => $value) {

					$rsstrength  = $this->get_records(['strength_id' => $value->strength_id], 'strength', ['strength_id', 'strength'], '', 1);

					$selcted_txt .= $rsstrength[0]->strength;

					$i--;
					if($i > 0) {
						$selcted_txt .= ', ';
					}
					
					
				}
			}

		return $selcted_txt;

	}

	function get_brand_strength($brandId) {

    	$q = $this->db->select('bm.*,st.*,b.*')
		->from('brand_strength bm')
		->join('strength st','st.strength_id = bm.strength_id')
		->join('brand b','b.brand_id = bm.brand_id')
		->where('b.brand_id = '.$brandId);
        $collection = $q->get()->result();
		return $collection;
	}
}