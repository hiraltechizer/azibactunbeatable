<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ho extends Admin_Controller
{
    private $module = 'ho';
    private $controller = 'manpower/ho';
    private $model_name = 'mdl_ho';
    private $settings = [
        'permissions'=> ['add', 'edit', 'download', 'upload'],
        'paginate_index' => 4
    ];
    private $scripts = ['division.js'];

    function __construct() {
        $this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];
        
        parent::__construct($this->module, $this->controller, $this->model_name, $this->settings,$this->scripts);

    }

    function options(){
        $this->session->is_Ajax_and_logged_in();

        $limit = $this->dropdownlength;
        $page = intval($_POST['page']) - 1;
        $page = ($page <= 0) ? 0 : $page;

        $s_term = (isset($_POST['search'])) ? $this->db->escape_like_str($_POST['search']) : '';

        $new = array(); $json['results'] = array();

        $_options = $this->model->get_options($s_term, 'users_name', [], $page * $limit, $limit);
        $_opt_count = count($this->model->get_options($s_term, 'users_name'));

        foreach($_options as $option){
            $new['id'] = $option->users_id;
            $new['text'] = $option->users_name;

            array_push($json['results'], $new);
        }
        
        $more = ($_opt_count > count($_options)) ? TRUE : FALSE;
        $json['pagination']['more'] = $more;

        echo json_encode($json);
    }

    function uploadcsv(){
        $this->session->is_Ajax_and_admin_logged_in();
        /*upload csv file */

        if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
        }

        if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
        }

        $file = $_FILES['csvfile']['tmp_name'];
        $handle = fopen($file, "r");
        $cnt = 0; $newrows = 0;

        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

            if(count($data) !== 6) { continue; }

            if(! $cnt){
                $cnt++; continue;
            }
            
            $ho_name = trim($data[0]);
            $mobile = trim($data[1]);
            $emp_id = trim($data[2]);
            $username = trim($data[3]);
            $password = trim($data[4]);
            $division = trim($data[5]);

            if( empty($ho_name) || empty($emp_id) || empty($division) || empty($password) ||  empty($username)  || empty($mobile)){
                continue;
            }

            if( 
                ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $ho_name) 
                || ! preg_match('/^[a-zA-Z0-9]+$/', $emp_id) 
                 || ! preg_match('/^[a-zA-Z0-9]+$/', $username) 
            ){
                continue; 
            }
            
            if(!empty($mobile)) {
                if(! preg_match('/^[1-9][0-9]{9}$/', $mobile)) {
                    continue;
                }
            } else {
                $mobile = NULL;
            }
            
             $division_record = $this->model->get_records(['division_name'=> $division], 'division', ['division_id'], '', 1);
            if(!count($division_record)) {
                continue;
            }
            
            $division_id = $division_record[0]->division_id;
 

            if($mobile) {
                $emp_record = $this->model->get_or_records(
                    [ 
                        'users_mobile'=> $mobile,
                        'users_emp_id'=> $emp_id,
                    ], 'manpower', ['users_id'], '', 1
                );
            } else {
                $emp_record = $this->model->get_records(
                    [ 'users_emp_id'=> $emp_id ],
                    'manpower',
                    ['users_id'],
                    '',
                    1
                );
            }

            if(count($emp_record)) {
                continue;
            }

            //  if($mobile) {
            //     $rsemp_record = $this->model->get_or_records(['users_mobile'=> $mobile,'users_emp_id'=> $emp_id,'users_division_id' => $division_id], 'manpower', ['users_id'], '', 1);
            //     // echo $this->db->last_query();exit;
            // } else {
            //     $rsemp_record = $this->model->get_records([ 'users_emp_id'=> $emp_id,'users_division_id' => $division_id],'manpower',['users_id'],'',1);
            // }

            // if(count($rsemp_record)) {
            //     continue;
            // }

            $insert['users_name'] = $ho_name;
            
            if($mobile) {
                $insert['users_mobile'] = $mobile;
            }
            
            $insert['users_emp_id'] = $emp_id;
            $insert['users_username'] = $username;
            $insert['users_password'] = $password;
            $insert['users_type'] = "HO";
            $insert['users_division_id']  =   $division_id;

            $this->model->_insert($insert);
            $newrows++;
        }

        fclose($handle);
        echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
    }
}
