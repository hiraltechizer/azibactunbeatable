<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['users_id']; ?>
<tr>
    <td>
        <input type="checkbox" name="ids[]" value="<?php echo $id ?>" id="check_<?= $id ?>" class="form-check-input form-check-{color}" />
        <label for="check_<?= $id ?>"></label>
    </td>
     <td><?php echo $record['users_name'] ?></td>   
    <td><?php echo $record['users_mobile'] ?></td>   
    <td><?php echo $record['users_emp_id'] ?></td>   
    <td><?php echo $record['users_username'] ?></td>
    <td><?php echo $record['users_password'] ?></td>      
    <td><?php echo $record['area_name'] ?></td>   
    <td><?php echo $record['mgr_name'] ?></td>   
    <td><?php echo $record['region_name'] ?></td> 
    <td><?php echo $record['zone_name'] ?></td> 
    <td><?php echo $record['division_name'] ?></td>   
    <td><?php echo $record['insert_dt'] ?></td>
    <td>
        <div class="btn-group" role="group" aria-label="Basic example">
            
            <a class="blue-color" href="<?php echo base_url("$controller/edit/record/$id?c=$timestamp") ?>" title="Edit">
                <i class="fa fa-edit"></i>
            </a>
        </div>
    </td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>">
        <div class="row">
            <?php echo $this->ajax_pagination->create_links(); ?>    
        </div>
    </td>
</tr>