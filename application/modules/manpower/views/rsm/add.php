<?php echo form_open("$controller/save",array('class' => 'save-form')); ?>
<div class="form-group row">
    <label for="division_id" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="division_id" name="users_division_id" data-placeholder="Select Division">
            <option value="">Select Division</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_zone_id" class="col-sm-3 col-form-label">Zone Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm" id="zone_id" name="users_zone_id" data-placeholder="Select Zone">
            <option value="">Select Zone</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_region_id" class="col-sm-3 col-form-label">Region Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm" id="region_id" name="users_region_id" data-placeholder="Select Region">
            <option value="">Select Region</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_name" class="col-sm-3 col-form-label">Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_name" name="users_name" placeholder="User Name" maxlength="50">
    </div>
</div>
<div class="form-group row">
    <label for="users_mobile" class="col-sm-3 col-form-label">Mobile <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_mobile" name="users_mobile" placeholder="User Mobile" maxlength="10">
    </div>
</div>
<div class="form-group row">
    <label for="users_emp_id" class="col-sm-3 col-form-label">Emp ID <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_emp_id" name="users_emp_id" placeholder="User Employee ID" maxlength="10">
    </div>
</div>
<div class="form-group row">
    <label for="users_username" class="col-sm-3 col-form-label">Username <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_username" name="users_username" placeholder="User Username" maxlength="20">
    </div>
</div>
<div class="form-group row">
    <label for="users_password" class="col-sm-3 col-form-label">Password <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_password" name="users_password" placeholder="User Password" maxlength="20">
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>