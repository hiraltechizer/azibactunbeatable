<?php echo form_open("$controller/users_parent_modify",array('class' => 'save-form')); ?>
<input type="hidden" name="users_id" value="<?php echo $info[0]['users_id']; ?>" />


<div class="form-group row">
    <label for="users_area_id" class="col-sm-3 col-form-label">Area <span class="text-danger">*</span></label>
    <div class="col-sm-9">
       <select class="form-control form-control-sm" id="users_area_id" name="users_area_id" data-placeholder="Select Area">
           <option>Select Area</option>
            <option value="<?php echo $info[0]['area_id']; ?>" ><?php echo $info[0]['area_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_parent_id" class="col-sm-3 col-form-label">ASM Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
       <select class="form-control form-control-sm" id="users_parent_id" name="users_parent_id" data-placeholder="Select ASM">
            <option value="" ></option>
        </select>
    </div>
</div>

<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>