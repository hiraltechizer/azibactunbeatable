<?php echo form_open("$controller/modify",array('class' => 'save-form')); ?>
<input type="hidden" name="users_id" value="<?php echo $info[0]['users_id']; ?>" />

<div class="form-group row">
    <label for="division_id" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="division_id" name="users_division_id" data-placeholder="Select Division">
           <option value="<?php echo $info[0]['division_id']; ?>" selected="selected"><?php echo $info[0]['division_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_zone_id" class="col-sm-3 col-form-label">Zone <span class="text-danger">*</span></label>
    <div class="col-sm-9">
       <select class="form-control form-control-sm" id="zone_id" name="users_zone_id" data-placeholder="Select Zone">
            <option value="<?php echo $info[0]['zone_id']; ?>" selected="selected"><?php echo $info[0]['zone_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_region_id" class="col-sm-3 col-form-label">Region <span class="text-danger">*</span></label>
    <div class="col-sm-9">
       <select class="form-control form-control-sm" id="region_id" name="users_region_id" data-placeholder="Select Region">
            <option value="<?php echo $info[0]['region_id']; ?>" selected="selected"><?php echo $info[0]['region_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_area_id" class="col-sm-3 col-form-label">Area <span class="text-danger">*</span></label>
    <div class="col-sm-9">
       <select class="form-control form-control-sm" id="area_id" name="users_area_id" data-placeholder="Select Area">
            <option value="<?php echo $info[0]['area_id']; ?>" selected="selected"><?php echo $info[0]['area_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_area_id" class="col-sm-3 col-form-label">City <span class="text-danger">*</span></label>
    <div class="col-sm-9">
       <select class="form-control form-control-sm" id="city_id" name="users_city_id" data-placeholder="Select City">
            <option value="<?php echo $info[0]['city_id']; ?>" selected="selected"><?php echo $info[0]['city_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="users_name" class="col-sm-3 col-form-label">User Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_name" name="users_name" placeholder="User Name" value="<?php echo $info[0]['users_name']; ?>" maxlength="50" >
    </div>
</div>
<div class="form-group row">
    <label for="users_mobile" class="col-sm-3 col-form-label">Mobile <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_mobile" name="users_mobile" placeholder="User Mobile" value="<?php echo $info[0]['users_mobile']; ?>" maxlength="10" >
    </div>
</div>
<div class="form-group row">
    <label for="users_emp_id" class="col-sm-3 col-form-label">Emp ID <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_emp_id" name="users_emp_id" placeholder="User Employee ID" value="<?php echo $info[0]['users_emp_id']; ?>" maxlength="10" >
    </div>
</div>
<div class="form-group row">
    <label for="users_username" class="col-sm-3 col-form-label">Username <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_username" name="users_username" placeholder="User Username" maxlength="20" value="<?php echo $info[0]['users_username']; ?>">
    </div>
</div>
<div class="form-group row">
    <label for="users_password" class="col-sm-3 col-form-label">Password <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="users_password" name="users_password" placeholder="User Password" value="<?php echo $info[0]['users_password']; ?>" maxlength="10" >
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>