<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_repurchase extends MY_Model {
    public $p_key = 'visit_id';
    public $table = 'visit';
    
    function __construct() {
        parent::__construct($this->table);
    }

    function get_repurchase_records() {
        $q = $this->db->select('p.patient_id, DATEDIFF(ps.repurchase_date, date(now())) as diffDate,p.patient_name,p.patient_mobile_no,b.brand_name,m.users_name,m.users_mobile,d.doctor_name,ps.repurchase_date,m.users_parent_id')
        ->from('patient p')
        ->join('patient_sub ps', 'ps.patient_id = p.patient_id')
        ->join('manpower m', 'm.users_id = p.users_id')
        ->join('doctor d', 'd.doctor_id = p.doctor_id')
        ->join('brands b', 'b.brand_id = ps.brand_id')
        ;
         
        $q->where('p.unsubscribe', '0');
        $q->group_by('p.patient_id,b.brand_id');
        $q->having('diffDate',  7);



        $collection = $q->get()->result();
        //echo '<pre>';
        //print_r($this->db->last_query());die;

        return $collection;
    }
}