<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_healthtips extends MY_Model {
    
    function __construct() {
        parent::__construct();
    }

    function get_healthtips($f_filters = [], $keywords ='') {

    	$q = $this->db->select('p.patient_id,p.patient_name,p.patient_mobile_no, `ht`.`ht_id`, `htt`.`message`, `ht`.`ht_date`, `ht`.`ht_time`, `htt`.`language_id`')
        ->from("patient p")
        ->join('healthtips_translate htt', 'htt.language_id = p.language_id')
        ->join('healthtips ht','ht.ht_id = htt.ht_id')
        ;
       
				
		if(sizeof($f_filters)) { 
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}
        $q->where('p.unsubscribe = "0"');
        $q->where('htt.message IS NOT NULL');
        $q->group_by('p.patient_id');
		$q->order_by('ht.update_dt desc, ht.ht_id desc');
        
		$collection = $q->get()->result();
       // echo '<pre>';
        //print_r($this->db->last_query());die;
		return $collection;
	}
}