<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cron_healthtips extends Generic_Controller {
    private $model_name = 'mdl_healthtips';

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name, 'model');
        $this->load->helper('send_sms');
    }

    public function index() {
        $data = [];
        $current_datetime = date('Y-m-d H:i:00');
        $healthtips_details = $this->model->get_healthtips(['ht.ht_date' => date('Y-m-d'), 'ht.ht_time' => date('H:i:00')]);
            //echo '<pre>';
            //print_r($healthtips_details);die;
        if(!empty($healthtips_details)) {
            foreach ($healthtips_details as $key => $value) {
                
                $ht_id   = $value->ht_id;
                $message = $value->message;
                $ht_date = $value->ht_date;
                $ht_time = $value->ht_time;
                $language_id = $value->language_id;
                $translate_msg = $value->message;

                $patient_name = $value->patient_name;
                $patient_mobile = $value->patient_mobile_no;

                if(empty($translate_msg)) {
                    continue;
                }

                if(strtotime($current_datetime) == strtotime("$ht_date $ht_time")) {
                    $ptranslate_msg = "Dear $patient_name,".PHP_EOL."$translate_msg";
                    array_push($data, [
                        'send_to'   => $patient_mobile,
                        'sms_text'  => $ptranslate_msg,
                        'user_group'=> 'HEALTH-TIPS',
                        'selection_type' => 'single',
                        'insert_dt' => date('Y-m-d H:i:s'),
                        'update_dt' => date('Y-m-d H:i:s')
                    ]);
                }                
            }
            $healthtip_insert_batch = $this->model->_insert_batch($data, 'sms_queue');
        }
        exit;
    }
}