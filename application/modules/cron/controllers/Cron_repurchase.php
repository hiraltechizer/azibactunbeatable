<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cron_repurchase extends Generic_Controller {
    private $model_name = 'mdl_repurchase';

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name, 'model');
        $this->load->helper('send_sms');
    }

    public function index() {
        $current_date = date('Y-m-d');
        $patientData = $this->model->get_repurchase_records();

        foreach($patientData as $patient) {
           
            $patient_name       = $patient->patient_name;
            $patient_mobile_no  = $patient->patient_mobile_no;
            $users_name         =   $patient->users_name;
            $users_mobile       =   $patient->users_mobile;
            $doctor_name        =   $patient->doctor_name;
            $brand_name         =   $patient->brand_name;
            $users_parent_id    =   $patient->users_parent_id;
            
            if(!is_null($patient->repurchase_date) && $patient->repurchase_date != '0000-00-00' && $patient->repurchase_date != '1970-01-01') {
                
                $repurchase_date = date('Y-m-d', strtotime($patient->repurchase_date));
                $reminder_date = date('Y-m-d', strtotime("$repurchase_date -7 days"));

                if(strtotime($reminder_date) == strtotime($current_date)){

                    #sms to patient
                  //  $message = "Dear $patient_name, kindly purchase your medicine $brand_name on or before $repurchase_date. To unsubscribe from this alerts just type SDACT (space) your name and send it to 8082633637";
                 
                  $message = "Dear $patient_name, kindly purchase your medicine $brand_name on or before $repurchase_date. To unsubscribe from this alerts just type SDACT (space) your name and send it to 8082633637
Regards,
Ipca Uro";


                    echo $message.'<br/>';
                    send_sms($patient_mobile_no, $message, 'PATIENT-RREPURCHASE-REMINDER');

                    #tm revisit reminder
                   // $tm_sms =   "Dear $users_name, kindly assist your patient $patient_name about their next purchase of medicine $brand_name on or before $repurchase_date.";
                   
                   $tm_sms =   "Dear $users_name, kindly assist your patient $patient_name about their next purchase of medicine $brand_name on or before $repurchase_date under Sahyog.
Regards,
Ipca Uro";
                    echo $tm_sms.'<br/>';
                    
                    send_sms($users_mobile, $tm_sms, 'TM-REPURCHASE-REMINDER');

                    #send RMB SMS
                    $asmInfo = $this->model->get_records(['users_id' => $users_parent_id],'manpower',['users_name','users_mobile','users_parent_id']);
                    $asm_parent_id  =   $asmInfo[0]->users_parent_id;

                    $rsmInfo = $this->model->get_records(['users_id' => $asm_parent_id],'manpower',['users_name','users_mobile','users_parent_id']);

                    $rsm_parent_id  =   $rsmInfo[0]->users_parent_id;
                    $rsm_name   =   $rsmInfo[0]->users_name;
                    $rsm_mobile     =   $rsmInfo[0]->users_mobile;

                      #send sms to RSM
            if(!empty($rsm_mobile)) {
                
                $rsm_sms_text    =   "Dear $rsm_name, kindly ask your TM $users_name to assist patient $patient_name about their medicine $brand_name to repurchase on or before $repurchase_date under Sahyog.
Regards, 
Ipca Uro";

            $rsmsms_ret =  send_sms($rsm_mobile,$rsm_sms_text, 'RSM-REPURCHASE-REMINDER');    
            }
        }
            }
        }
    }
}