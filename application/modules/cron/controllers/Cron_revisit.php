<?php if( ! defined('BASEPATH')) exit('No Direct script access allowed');

class Cron_revisit extends Generic_Controller {
    
    private $model_name = 'mdl_revisit';

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name, 'model');
        $this->load->helper('send_sms');
    }

    public function index() {

        $current_date = date('Y-m-d');
        $patientData = $this->model->getAllRevisitPatients();

        foreach($patientData as $patient) {
           
            $patient_name = $patient->patient_name;
            $patient_mobile_no = $patient->patient_mobile_no;
            $users_name         =   $patient->users_name;
            $users_mobile       =   $patient->users_mobile;
            $doctor_name        =   $patient->doctor_name;
            
            if(!is_null($patient->revisit_date) && $patient->revisit_date != '0000-00-00' && $patient->revisit_date != '1970-01-01') {
                
                $revisit_date = date('Y-m-d', strtotime($patient->revisit_date));
                $reminder_date = date('Y-m-d', strtotime("$revisit_date -7 days"));

                if(strtotime($reminder_date) == strtotime($current_date)){

                    #sms to patient
                   // $message = "Dear $patient_name, kindly re-visit your doctor on or $revisit_date date to avoid the discontinuation of your treatment. To unsubscribe from the SAHYOG just type SDACT (space) your name and send it to 8082633637";

                   $message = "Dear $patient_name, kindly re-visit your doctor on or before $revisit_date to avoid the discontinuation of your treatment. To unsubscribe from the Sahyog just type SDACT (space) your name and send it to 8082633637.
Regards,
Ipca Uro";

                    echo $message.'<br/>';
                    send_sms($patient_mobile_no, $message, 'PATIENT-REVISIT-REMINDER');

                    #tm revisit reminder
                  //  $tm_sms =   "Dear $users_name, kindly assist your patient $patient_name about their $doctor_name revisit on or before $revisit_date";

                  $tm_sms =   "Dear $users_name, kindly assist your patient $patient_name about their Dr. $doctor_name revisit on or before $revisit_date under Sahyog.
Regards,
Ipca Uro";

                    echo $tm_sms.'<br/>';
                    send_sms($users_mobile, $tm_sms, 'TM-REVISIT-REMINDER');
                }
            }
        }
    }
}
?>