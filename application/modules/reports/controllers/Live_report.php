<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Live_report extends Reports_Controller
{
	private $module = 'live_report';
	private $controller = 'reports/live_report';
    private $model_name = 'mdl_live_report';
    private $columns = ['MR Name','HQ Name','Dr name','Speciality','SBU Code','Total No. Of Prescription','Image URL','Date and Time'];

	private $scripts =  ['select2.js','geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts

        );
	}

    // function index(){
    //     if( ! $this->session->is_logged_in() ){
    //         show_error("Forbidden", 403);
    //     }

    //     $sfilters = array();

    //     $offset = (int) $this->input->post('page');
    //     // print_r(print_r($_POST)); die();
    //     // $keywords = !empty($this->input->post('keywords'))?$this->input->post('keywords'):'';

    //     $post_array = $this->input->post();
    //     unset($post_array['page']);
    //     unset($post_array['search']);       
        
    //     $this->data['collection'] = $this->model->get_collection($count = FALSE, $sfilters, $post_array, $this->perPage, $offset);

    //     $this->data['plugins'] = ['paginate'];
    //     $totalRec = $this->model->get_totalcollection($count = TRUE, $sfilters, $post_array);
    //     // print_r($totalRec);
    //    //echo $this->db->last_query();die; 
    //     //$this->paginate($this->data['controller'], $totalRec, 4);
    //     //$this->data['plugins'] = ['paginate','fancybox'];
        
    //     /* columns for list */
    //     $table_columns = $this->columns;
    //     $this->data['all_action'] = FALSE;

    //     $this->set_defaults([
    //         'listing_url'=> $this->controller . '/index', 
    //         'download_url'=> $this->controller . '/download' ,
    //         'module_title'=> $this->module
    //     ]);
    //     $this->set_view_columns($table_columns);
    //     /* END columns */
    //     $records_view = $this->data['controller'].'/records';
        
    //     $role = $this->session->get_field_from_session('role');

    //     $this->data['permissions'] = ['download'];
        
    //     $template = ( in_array($role, ['SA', 'A'])) ? '_admin' : '_user';

    //     $filter_columns = $this->model->get_filters();
        
    //     $this->data['show_filters'] = TRUE;  
    //     $this->data['date_filters'] = FALSE;

    //     $this->set_view_columns($table_columns, [], $filter_columns);

    //     $this->data['js'] = $this->scripts;

       
    //     if($this->input->post('search') == TRUE) {
    //         $this->load->view($records_view, $this->data);
    //     } else {
    //         $this->data['records_view'] = $records_view;
    //         $this->set_view($this->data, 'template/components/container/lists', $template);
    //     }
    // }
}
