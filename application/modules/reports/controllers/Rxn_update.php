<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rxn_update extends Reports_Controller
{
	private $module = 'rxn_update';
	private $controller = 'reports/rxn_update';
    private $model_name = 'mdl_rxn_update';
    private $columns = ["MR Name","HQ Name","Dr name","Speciality","SBU Code","Total No. Of Prescription"];

	private $scripts =  ['select2.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts

        );
        $this->set_defaults();
	}

    function downloadpdf() {
        $this->load->library('zip');

        $pdfurl = $this->input->get('param');
        
        if(!$pdfurl) { return; }

        $pdf_download = json_decode(base64_decode($pdfurl));

        $uploaded_data = [];
        if(!empty($pdf_download)) {
            $temp_url = $pdf_download->tmp_str;
            array_push($uploaded_data, $temp_url);
        }
        
        if(!empty($uploaded_data)) {
            
            foreach ($uploaded_data as $key => $value) {

                print_r($value);die;
                $this->zip->read_file($value, FALSE);
            }

            $this->zip->download("$type-".date('d-m-Y-H-i-s').".zip");
            return;
        }

        return;
    }
     function getTotalcount() {

        $sfilters = array();
        $post_array = $this->input->post();
        $offset = (int) $this->input->post('page');
        
        unset($post_array['page']);
        unset($post_array['search']);     

        $total_count =  $this->model->get_collection($count = TRUE, $sfilters, $post_array);
        $result = ['total_record' => $total_count];
        echo json_encode($result);
    }
}
