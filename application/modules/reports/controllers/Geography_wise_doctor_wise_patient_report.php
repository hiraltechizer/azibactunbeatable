<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Geography_wise_doctor_wise_patient_report extends Reports_Controller
{
	private $module = 'geography_wise_doctor_wise_patient_report';
	private $controller = 'reports/geography_wise_doctor_wise_patient_report';
    private $model_name = 'mdl_geography_wise_doctor_wise_patient_report';
    private $columns = ['ZSM Name','ZSM Emp code','Zone Name',  'RSM Name', 'RSM emp code','Region Name','ASM Name','ASM Emp code','Area Name','MR Name','MR Emp code','HQ Name','Booklet code','Doctor Name','Doctor City','Active or Inactive doctor','Patient screened count For HbA1c','Patient screened count For Lipid', 'Patient screened count For Both','Ongoing Patient count YTD'];

	private $scripts =  ['select2.js','geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts,

        );
	}

    function getTotalcount() {

        $sfilters = array();
        $post_array = $this->input->post();
        $offset = (int) $this->input->post('page');
        
        unset($post_array['page']);
        unset($post_array['search']);     

        $total_count =  $this->model->get_collection($count = TRUE, $sfilters, $post_array);
        $result = ['total_record' => $total_count];
        echo json_encode($result);
    }
}
