<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ranking_report extends Reports_Controller
{
	private $module = 'ranking_report';
	private $controller = 'reports/ranking_report';
    private $model_name = 'mdl_ranking_report';
    private $columns = ["MR Name","HQ Name","Cumulative Prescription","Rank as per the Prescrption"];

	private $scripts =  ['select2.js','geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts

        );
	}

    
}
