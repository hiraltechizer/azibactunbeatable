<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Geography_wise_patient_count_report extends Reports_Controller
{
	private $module = 'geography_wise_patient_count_report';
	private $controller = 'reports/geography_wise_patient_count_report';
    private $model_name = 'mdl_geography_wise_patient_count_report';
    private $columns = ['Name','Mobile no.','Designation','Emp code','HQ Name','Booklet code','Doctor Name','New Patient on therapy in a month','HBA1C Patient Count', 'Lipid Patient Count', 'Both Patient Count', 'Ongoing patient count'];

	private $scripts =  ['select2.js','geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts,

        );
	}

    function getTotalcount() {

        $sfilters = array();
        $post_array = $this->input->post();
        $offset = (int) $this->input->post('page');
        
        unset($post_array['page']);
        unset($post_array['search']);     

        $total_count =  $this->model->get_collection($count = TRUE, $sfilters, $post_array);
        $result = ['total_record' => $total_count];
        echo json_encode($result);
    }
}
