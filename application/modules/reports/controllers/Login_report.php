<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login_report  extends Reports_Controller
{
	private $module = 'login_report';
	private $controller = 'reports/login_report';
    private $model_name = 'mdl_login_report';
    private $columns = ['Employee name', 'Mobile number', 'Emp id', 'Users Type', 'City', 'Area', 'Region', 'Zone','Login Date and Time'];
	
    private $scripts = ['geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts
        );
	}


}
