<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Repurchase_reminder  extends Reports_Controller
{
	private $module = 'repurchase_reminder';
	private $controller = 'reports/repurchase_reminder';
    private $model_name = 'mdl_repurchase_reminder';
    private $columns = ['ZSM Name','ZSM Emp code','Zone Name','RSM Name','RSM emp code','Region Name','ASM Name','ASM Emp code','Area Name','MR Name','MR Emp code','MR HQ name','Patient Code','Booklet code','Dr. Name','Patient City','Language','Brand Name','Call Date','Call status','Repurchase Date','Call counts'];
    private $scripts = ['geo_wise_filers.js'];
    
    
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts
        );
	}

   /*  function _get_geo_data($id,$userType) { 
        return $this->model->getGeoData($id,$userType);
    } */
}
