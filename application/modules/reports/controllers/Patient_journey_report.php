<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Patient_journey_report extends Reports_Controller
{
	private $module = 'patient_journey_report';
	private $controller = 'reports/patient_journey_report';
    private $model_name = 'mdl_patient_journey_report';
    private $columns = ['ZSM Name','ZSM Emp code','Zone Name',  'RSM Name', 'RSM emp code','Region Name','ASM Name','ASM Emp code','Area Name','MR Name','MR Emp code','HQ Name','Patient Code','Coupon code','Booklet code','Doctor Name','Doctor City','Brand name','Consent(Y/N)','Age','Gender','BMI score','Rxn verified','Invoice verified','Purchase count in Qty','Therapy status','Health tips sent count','Revisit reminder sent count','Repurchase reminder sent count','Diet chart sent count','Test done with results'];

	private $scripts =  ['select2.js','geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts,

        );
	}

    function index() {
        if( ! $this->session->is_logged_in() ){
            show_error("Forbidden", 403);
        }

        $sfilters = array();

        $offset = (int) $this->input->post('page');
        // print_r(print_r($_POST)); die();
        // $keywords = !empty($this->input->post('keywords'))?$this->input->post('keywords'):'';

        $post_array = $this->input->post();
        unset($post_array['page']);
        unset($post_array['search']);       
        
        $this->data['collection'] = $this->model->get_collection($count = FALSE, $sfilters, $post_array, $this->perPage, $offset);
        $totalRec = $this->model->get_collection($count = TRUE, $sfilters, $post_array);
        $this->data['totalRec'] =   $totalRec;
        // print_r($totalRec);
       //echo $this->db->last_query();die; 
        $this->paginate($this->data['controller'], $totalRec, 4);
        $this->data['plugins'] = ['paginate','fancybox'];
        
        /* columns for list */
        $table_columns = $this->columns;
        $this->data['all_action'] = FALSE;

        $this->set_defaults([
            'listing_url'=> $this->controller . '/index', 
            'download_url'=> $this->controller . '/download' ,
            'module_title'=> $this->module
        ]);
        $this->set_view_columns($table_columns);
        /* END columns */
        $records_view = $this->data['controller'].'/records';
        
        $role = $this->session->get_field_from_session('role');

        $this->data['permissions'] = ['download'];
        
        $template = ( in_array($role, ['SA', 'A'])) ? '_admin' : '_user';

        $filter_columns = $this->model->get_filters();
        
        $this->data['show_filters'] = TRUE;  
        $this->data['date_filters'] = FALSE;

        $this->data['userstypeInfo'] = $this->model->get_records(['users_type !=' => 'HO'], 'manpower',['DISTINCT(users_type)']);

        $this->data['zoneInfo'] = $this->model->get_records(['is_deleted' => '0'], 'zone',['zone_id','zone_name']);
        $this->data['regionInfo'] = $this->model->get_records(['is_deleted' => '0'], 'region',['region_id','region_name']);
        $this->data['areaInfo'] = $this->model->get_records(['is_deleted' => '0'], 'area',['area_id','area_name']);
        $this->data['cityInfo'] = $this->model->get_records(['is_deleted' => '0'], 'city',['city_id','city_name']);        

       $this->set_view_columns($table_columns, [], $filter_columns);

        $this->data['js'] = $this->scripts;
        $this->data['total_record'] =   $totalRec;

        if($this->input->post('search') == TRUE) {
            $this->load->view($records_view, $this->data);
        } else {
            $this->data['records_view'] = $records_view;
            $this->set_view($this->data, 'template/components/container/lists', $template);
        }
    }

    function _getpatientrequiredcnt($pId) {
        return $this->model->get_patientreuiredcount($pId);
    }

     function getTotalcount() {

        $sfilters = array();
        $post_array = $this->input->post();
        $offset = (int) $this->input->post('page');
        
        unset($post_array['page']);
        unset($post_array['search']);     

        $total_count =  $this->model->get_collection($count = TRUE, $sfilters, $post_array);
        $result = ['total_record' => $total_count];
        echo json_encode($result);
    }
}
