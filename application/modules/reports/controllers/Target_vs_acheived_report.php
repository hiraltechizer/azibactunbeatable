<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Target_vs_acheived_report extends Reports_Controller
{
	private $module = 'target_vs_acheived_report';
	private $controller = 'reports/target_vs_acheived_report';
    private $model_name = 'mdl_target_vs_acheived_report';
    private $columns = ['ZSM Name','ZSM Emp code','Zone Name',  'RSM Name', 'RSM emp code','Region Name','ASM Name','ASM Emp code','Area Name','MR Name','MR Emp code','HQ Name','Target Doctor Count','Achieved Doctor Count','Target Patient Count','Achieved Patient Count'];

	private $scripts =  ['select2.js','geo_wise_filers.js'];

    function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts,

        );
	}

    function getzoneByregion(){
        $zone_id      = $this->input->post('zone_id');
        $regionRecords = $this->model->get_records(['zone_id' => $zone_id], 'region');
        echo json_encode($regionRecords);
    }
    
    function getreagionByarea(){    
        $region_id      = $this->input->post('region_id');
        $areaRecords = $this->model->get_records(['region_id' => $region_id], 'area');
        echo json_encode($areaRecords); 
    }
    
    function getareaBycity(){   
        $area_id      = $this->input->post('area_id');
        $cityRecords = $this->model->get_records(['area_id' => $area_id], 'city');
        echo json_encode($cityRecords); 
    }

     function getTotalcount() {

        $sfilters = array();
        $post_array = $this->input->post();
        $offset = (int) $this->input->post('page');
        
        unset($post_array['page']);
        unset($post_array['search']);     

        $total_count =  $this->model->get_collection($count = TRUE, $sfilters, $post_array);
        $result = ['total_record' => $total_count];
        echo json_encode($result);
    }
}
