<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Patient_adherence_report extends Reports_Controller
{
	private $module = 'patient_adherence_report';
	private $controller = 'reports/patient_adherence_report';
    private $model_name = 'mdl_patient_adherence_report';
    private $columns = ['ZSM Name','ZSM Emp code','Zone Name',  'RSM Name', 'RSM emp code','Region Name','ASM Name','ASM Emp code','Area Name','MR Name','MR Emp code','HQ Name','Patient Code','Enrollment date','Coupon code','Booklet code','Doctor Name','Doctor city','Diet chart shared (Y/N)','Test done','Test coupon code','Brand name'];

	private $scripts =  ['select2.js','geo_wise_filers.js'];
	function __construct() {
        
		parent::__construct(
            $this->module, 
            $this->controller, 
            $this->model_name, 
            $this->columns,
            $this->scripts,

        );
	}

    function _getpatientrequiredinfo($PId) {
        return $this->model->getPatientRequiredInfo($PId);
    }

     function pdfdownload(){
        #$this->session->is_Ajax_and_logged_in();

        $this->load->library('zip');

        $patient_id = $this->input->get('pid');
    
        if(!$patient_id) { return; }
        
        $pdf_download = $this->model->get_records(['patient_id' => $patient_id], 'patient_diet_chart_pdf' , ['upload_path']);
        
        $uploaded_data = [];
        if(!empty($pdf_download)) {
            foreach ($pdf_download as $key => $value) {
                        
                if(file_exists(getcwd().'/'.$value->upload_path)) {
                    
                    array_push($uploaded_data,$value->upload_path);
                }
            }
        }
        
        if(!empty($uploaded_data)) {
            
            foreach ($uploaded_data as $key => $value) {
                $this->zip->read_file($value, FALSE);
            }

            $this->zip->download("$type-".date('d-m-Y-H-i-s').".zip");
            return;
        }

        return;
    }
}
