<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_ranking_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

	
    	$q = $this->db->select("pre.users_id,`m`.`users_name`, `c`.`city_name`,
        sum(pre.rxn_count) AS total_rxn_count,
        case when pre.rxn_count > 0 THEN (RANK() OVER (ORDER BY total_rxn_count DESC, pre.insert_dt ASC)) ELSE 0 END AS mr_rank")
        ->from('`prescription` `pre`')
        ->join('`doctor` `d`', '`d`.`doctor_id` = `pre`.`doctor_id`')
        ->join('`manpower` `m`', '`m`.`users_id` = `pre`.`users_id`')
        ->join('`city` `c`', '`m`.`users_city_id` = `c`.`city_id`')
        ->join('`speciality` `s`', '`s`.`speciality_id` = `d`.`speciality_id`') 
        ->join('cities cts', 'cts.city_id = d.city_id','left') 
        ->join('`speciality` `sp`', '`sp`.`speciality_id` = `d`.`speciality_id`','left') 
       ;
	    
        
       if(sizeof($f_filters)) { 
        foreach ($f_filters as $key=>$value) { 
            
            if($key == 'from_date' || $key == 'to_date') {
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE(pre.insert_dt) >=', date('Y-m-d', strtotime($value)));
                } 

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE(pre.insert_dt) <=', date('Y-m-d', strtotime($value)));
                }    
            } else {
                if(strpos($value, ",") !== FALSE) {
                   $q->where("$key IN (".$value.")"); 
                
                } else {
                    //echo 'ppp';die;
                    $q->where("$key", $value); 
                }
               // $q->where("$key", $value); 
            }
            //$q->where("$key", $value); 
        }
    }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                $key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE(pre.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE(pre.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                if($key == 'z.zone_id' || $key == 'r.region_id' || $key == 'a.area_id' || $key == 'c.city_id' ){
                  $this->db->where("$key", $value);
                }else{
                  $this->db->like($key, $value);
                }
            }
        }

       $q->group_by('pre.users_id');
     //  $q->order_by('p.update_dt desc, p.doctor_id desc');
        if(! $count) {
          $q->order_by('total_rxn_count desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

    //     echo '<pre>';
    //   print_r($this->db->last_query());exit;
		return $collection;
    }


    
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }
       
		foreach ($data as $rows) {

			 $records['MR Name'] = $rows['users_name'];
             $records['HQ Name'] = $rows['city_name'];
             $records['Cumulative Prescription'] = $rows['total_rxn_count'];
             $records['Rank as per the Prescrption'] = $rows['mr_rank'];
        
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}