<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_login_report extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
           
            array_push($new_filters, 'from_date');
        }
        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
		}
        return $new_filters;
    }

    function get_filters() {
        return [
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'Name',
            ],
             [
                'field_name'=>'m|users_mobile',
                'field_label'=> 'Mobile Number',
            ],
            [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'EmpId',
            ],
            [
                'field_name'=>'m|users_type',
                'field_label'=> 'Type',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'City',
            ],
             [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area',
            ],
             [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region',
            ],
             [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone',
            ],
            
            
        ];
    }

  

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

        $field_filters = $this->get_filters_from($rfilters);

        $q = $this->db->select("m.users_id,m.users_name,m.users_mobile,m.users_emp_id,m.users_type, asm.users_name AS reporting_manager,c.city_name,a.area_name,r.region_name,z.zone_name, MAX(ul.insert_dt) AS insert_dt")
        ->from('last_login_log ul')
       ->join('manpower m', 'm.users_id = ul.users_id','left')
       ->join('manpower asm', 'asm.users_id = m.users_parent_id','left')
       ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id','left')
       ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id','left')
       ->join('city c', 'c.city_id = m.users_city_id','left')
       ->join('area a', 'a.area_id = m.users_area_id','left')
        ->join('region r', 'r.region_id = m.users_region_id','left')
        ->join('zone z', 'z.zone_id = m.users_zone_id','left');
        
        
        // $q->where('ul.is_deleted', '0');
      
        if(sizeof($f_filters)) {
            //echo 'sss';die;
            foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                $key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE(ul.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE(ul.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                if($key == 'z.zone_id' || $key == 'r.region_id' || $key == 'a.area_id' || $key == 'c.city_id' ){
                  $this->db->where("$key", $value);
                }else{
                  $this->db->like($key, $value);
                }
            }
        }
        $q->group_by('m.users_id');
        //$q->where('u.users_type != "HO"');
        $q->order_by('ul.login_id DESC, ul.update_dt DESC');

        if(!empty($limit)) { $q->limit($limit, $offset); }
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
// echo '<pre>';
// echo $this->db->last_query();exit;
        return $collection;
    }
    
    function _format_data_to_export($data){
        
        $resultant_array = [];


        foreach ($data as $rows) {

            $records['Name'] = $rows['users_name'];
            $records['Mobile Number']   = $rows['users_mobile'];
            $records['EmpId']   =    $rows['users_emp_id'];
            $records['User Type']   =   $rows['users_type'];
            $records['City Name'] = $rows['city_name'];
            $records['Area Name'] = $rows['area_name'];
            $records['Region Name'] = $rows['region_name'];
            $records['Zone Name'] = $rows['zone_name'];
            $records['DateTime'] = $rows['insert_dt'];
            
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }

    

}