<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_patient_journey_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('users_type', $filters))  {
            array_push($new_filters, 'users_type');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }
        return $new_filters;
	}
	
	

    function get_filters() {
        return [
        	 [
                'field_name'=>'zsm|users_name',
                'field_label'=> 'ZSM Name',
            ],
             [
                'field_name'=>'zsm|users_emp_id',
                'field_label'=> 'ZSM Emp code',
            ],
             [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone Name',
            ],
             [
                'field_name'=>'rsm|users_name',
                'field_label'=> 'RSM Name',
            ],
             [
                'field_name'=>'rsm|users_emp_id',
                'field_label'=> 'RSM Emp code',
            ],
             [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region Name',
            ],
            [
                'field_name'=>'asm|users_name',
                'field_label'=> 'ASM Name',
            ],
             [
                'field_name'=>'asm|users_emp_id',
                'field_label'=> 'ASM Emp code',
            ],
             [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area Name',
            ],
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            [
                'field_name'=>'p|patient_code',
                'field_label'=> 'Patient Code',
            ]

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

		#get latest insert dt from table
		
    	$q = $this->db->select("p.patient_code, p.patient_id,p.patient_age,p.patient_gender,dcc.diet_coupon_code, bc.booklet_code,
d.doctor_name, cts.city_name AS doc_city_name, m.users_name AS mr_name,m.users_emp_id AS mr_emp_code,c.city_name,asm.users_name AS asm_name,
asm.users_emp_id AS asm_emp_code,a.area_name,rsm.users_name AS rsm_name,rsm.users_emp_id AS rsm_emp_code,
r.region_name,zsm.users_name AS zsm_name,zsm.users_emp_id AS zsm_emp_code,z.zone_name,(SELECT GROUP_CONCAT(b.brand_name) FROM therapy t1 JOIN brand b ON b.brand_id = t1.brand_id 
WHERE t1.patient_id = p.patient_id) AS brand_name,cd.consent_doc_id")
    	->from('patient p')
    	 ->join('patient_doctor pd', 'pd.patient_id = p.patient_id')
        ->join('doctor d', 'd.doctor_id = pd.doctor_id')
        ->join('call_tracking ct', 'ct.patient_id = p.patient_id')
        ->join('cities cts', 'cts.city_id = d.city_id')
         ->join('consent_document cd', 'cd.patient_id =p.patient_id','LEFT')
        ->join('patient_coupon_code pcc', 'pcc.patient_id = p.patient_id')
        ->join('diet_coupon_code dcc', 'dcc.diet_coupon_code_id = pcc.diet_coupon_code_id')
        ->join('booklet_code bc', 'bc.booklet_code_id =dcc.booklet_code_id')
        ->join('manpower m', 'm.users_id = d.users_id')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id')
        ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id')
        ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id')
        ->join('city c', 'c.city_id = m.users_city_id')
        ->join('area a', 'a.area_id = asm.users_area_id')
        ->join('region r', 'r.region_id = rsm.users_region_id')
        ->join('zone z', 'z.zone_id = zsm.users_zone_id')
        ;
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$date_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {
			
			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);
                /*if($key == 'from_date' && $value) {
                	$date_search_flag = TRUE;
                    $this->db->where('DATE(hsa.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                	$date_search_flag = TRUE;
                    $this->db->where('DATE(hsa.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                } */

              if(!empty($value)) {
                    $this->db->like($key, $value);      
                }
            }
        }
        $q->where_in('ct.call_status_id', [5,11]);
		$q->group_by('p.patient_id');

        if(! $count) {
           $q->order_by('p.update_dt desc, p.patient_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

	//echo '<pre>';
	//echo $this->db->last_query();
	//die;	//print_r($collection);die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

            $prequiredInfo  =   $this->get_patientreuiredcount($rows['patient_id']);
            $patient_bmi = 0;
        if(isset($prequiredInfo[0]['patient_bmi'])) {
            $patient_bmi = $prequiredInfo[0]['patient_bmi'];
        }

        $rxn_verify = 'No';
        if(isset($prequiredInfo[0]['rxn_verify'])) {
            $rxn_verify = $prequiredInfo[0]['rxn_verify'];
        } 
        
        $invoice_verify = 'No';
        if(isset($prequiredInfo[0]['invoice_verify'])) {
            $invoice_verify = $prequiredInfo[0]['invoice_verify'];
        }        

        $purchase_quantity = 0;
        if(isset($prequiredInfo[0]['purchase_quantity'])) {
            $purchase_quantity = $prequiredInfo[0]['purchase_quantity'];
        } 

        $status = 'Pending';
        if(isset($prequiredInfo[0]['status'])) {
            $status = $prequiredInfo[0]['status'];
        } 

        $healthtips_cnt = 0;
        if(isset($prequiredInfo[0]['healthtips_cnt'])) {
            $healthtips_cnt = $prequiredInfo[0]['healthtips_cnt'];
        }

        $rev_call = 0;
        if(isset($prequiredInfo[0]['rev_call'])) {
            $rev_call = $prequiredInfo[0]['rev_call'];
        } 

        $rep_call = 0;
        if(isset($prequiredInfo[0]['rep_call'])) {
            $rep_call = $prequiredInfo[0]['rep_call'];
        }

        $total_count_share_diet_chart = 0;
        if(isset($prequiredInfo[0]['total_count_share_diet_chart'])) {
            $total_count_share_diet_chart = $prequiredInfo[0]['total_count_share_diet_chart'];
        }

        $test_type = '';
        if(isset($prequiredInfo[0]['test_type'])) {
            $test_type = $prequiredInfo[0]['test_type'];
        }

        $consent = 'No';
        if(!empty($record['consent_doc_id'])) {
            $consent = 'Yes';
        }



			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ASM Name']    = $rows['asm_name'];
            $records['ASM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['MR Name']  =   $rows['mr_name'];
            $records['MR Emp code'] 	= $rows['mr_emp_code'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Patient Code']    = $rows['patient_code'];
            $records['Coupon code']        =   $rows['diet_coupon_code'];
            $records['Booklet code']        =   $rows['booklet_code'];
            $records['Doctor Name']        =   $rows['doctor_name'];
            $records['Doctor City']        =   $rows['doc_city_name'];
            $records['Brand name']        =   $rows['brand_name'];
            $records['Consent(Y/N)']        =   $consent;
            $records['Age']        =    $rows['patient_age'];
            $records['Gender']        =     $rows['patient_gender'];
            $records['BMI score']        =   $patient_bmi;
            $records['Rxn verified']        =   $rxn_verify;
            $records['Invoice verified']        =   $invoice_verify;
            $records['Purchase count in Qty']        =   $purchase_quantity;
            $records['Therapy status']        =   $status;
            $records['Health tips sent count']     =    $healthtips_cnt; 
            $records['Revisit reminder sent count']      = $rev_call;
            $records['Repurchase reminder sent count']   = $rep_call;
            $records['iet chart sent count']             = $total_count_share_diet_chart;
            $records['Test done with results']           = $test_type;

			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}

    function get_patientreuiredcount($pId) {

        $q = $this->db->select('tt.test_type, p.patient_id,t.status,pt.hba1c_result,pt.tc_result,pt.ldl_result,pt.hdl_result,pt.tg_result, 
pdt.total_count_share_diet_chart,t.is_approved as rxn_verify, pu.is_approved as invoice_verify,pdt.patient_bmi,
 (SELECT sum(purchase.quantity_pills) from purchase WHERE purchase.patient_id = p.patient_id) as purchase_quantity,
 (SELECT count(calls.repurchase_history_id) from repurchase_call_history calls WHERE calls.patient_id = p.patient_id) as rep_call,
 (SELECT count(rcalls.revisite_history_id) from revisite_call_history rcalls WHERE rcalls.patient_id = p.patient_id) as rev_call,
    (SELECT COUNT(sl.id) FROM sms_log sl WHERE sl.mobile = p.mobile_1 AND sl.msg_for = "HEALTH-TIPS") AS healthtips_cnt')
        ->from('patient p')
        ->join('patient_test pt','p.patient_id = pt.patient_id')
        ->join('test_type tt','tt.test_type_id = pt.test_type_id')
        ->join('patient_diet_track pdt','pdt.patient_id = p.patient_id','LEFT')
        ->join('therapy t','t.patient_id = p.patient_id','LEFT')
        ->join('purchase pu','pu.patient_id = p.patient_id','LEFT')
        ->where('p.patient_id', $pId)
        ->group_by('p.patient_id');

        $collection = $q->get()->result_array();
        return $collection;


    }
}