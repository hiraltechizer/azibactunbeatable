<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_target_vs_acheived_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

         if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
        	  [
                'field_name'=>'zsm|users_name',
                'field_label'=> 'ZSM Name',
            ],
             [
                'field_name'=>'zsm|users_emp_id',
                'field_label'=> 'ZSM Emp code',
            ],
             [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone Name',
            ],
             [
                'field_name'=>'rsm|users_name',
                'field_label'=> 'RSM Name',
            ],
             [
                'field_name'=>'rsm|users_emp_id',
                'field_label'=> 'RSM Emp code',
            ],
             [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region Name',
            ],
            [
                'field_name'=>'asm|users_name',
                'field_label'=> 'ASM Name',
            ],
             [
                'field_name'=>'asm|users_emp_id',
                'field_label'=> 'ASM Emp code',
            ],
             [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area Name',
            ],
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);


        $q_str = '';
        $q_str1 = '';
        if(is_array($rfilters) && count($rfilters) ) {          
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                $key = str_replace('|', '.', $key);

                if($key == 'from_date') {
                    if(!empty($value)) {
                        $q_str .= 'AND bc1.update_dt >= "'.date("Y-m-d", strtotime($value)).'"';
                        $q_str1 .= 'AND t.insert_dt >= "'. date("Y-m-d", strtotime($value)).'"';    
                    }
                }

                if($key == 'to_date') {

                    if(!empty($value)) {
                         $q_str .= 'AND bc1.update_dt <= "'. date("Y-m-d", strtotime($value)).'"';
                         $q_str1 .= 'AND t.insert_dt >= "'. date("Y-m-d", strtotime($value)).'"';      
                    }
                }
            }
        }

		#get latest insert dt from table
		
    	$q = $this->db->select("m.users_name as mr_name,m.users_emp_id as mr_emp_code, asm.users_name AS asm_name,
asm.users_emp_id AS asm_emp_code, rsm.users_name AS rsm_name, rsm.users_emp_id AS rsm_emp_code,
zsm.users_name AS zsm_name,zsm.users_emp_id AS zsm_emp_code, c.city_name,a.area_name,
r.region_name,z.zone_name ,
(SELECT COUNT(bc.booklet_code_id) FROM booklet_code bc WHERE bc.users_id = m.users_id) AS total_doc_count,
(SELECT COUNT(bc1.booklet_code_id) FROM booklet_code bc1 JOIN doctor d ON d.doctor_id = bc1.doctor_id WHERE bc1.users_id = m.users_id ".$q_str.") AS total_doc_achive,
(SELECT COUNT(dcc.diet_coupon_code_id) FROM booklet_code bc2 JOIN diet_coupon_code dcc ON dcc.booklet_code_id = bc2.booklet_code_id WHERE bc2.users_id = m.users_id) AS total_pat_count,(SELECT COUNT(pcc.patient_coupon_code_id) FROM patient_coupon_code pcc 
JOIN diet_coupon_code dcc ON dcc.diet_coupon_code_id = pcc.diet_coupon_code_id
JOIN booklet_code bc ON bc.booklet_code_id = dcc.booklet_code_id
JOIN call_tracking ct ON ct.patient_id = pcc.patient_id
JOIN therapy t ON t.patient_id = pcc.patient_id
WHERE t.`status` = 'ongoing' AND ct.call_status_id NOT IN (1,2,3,4) 
AND bc.users_id = m.users_id ".$q_str1.") AS acheive_patient_count")
    	->from('manpower m')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id')
        ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id')
        ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id')
        ->join('city c', 'c.city_id = m.users_city_id')
        ->join('area a', 'a.area_id = asm.users_area_id')
        ->join('region r', 'r.region_id = rsm.users_region_id')
        ->join('zone z', 'z.zone_id = zsm.users_zone_id');
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$date_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);

                if($key != 'from_date' && $key != 'to_date') {
                    if(!empty($value)) {
                        $this->db->like($key, $value);      
                    }
                }
            }
        }
        $q->where('m.users_type','MR');
        $q->where('m.is_deleted', '0');
        $q->group_by('m.users_id');
		
        if(! $count) {
           $q->order_by('m.update_dt desc, m.users_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

        //echo '<pre>';
      //  print_r($this->db->last_query());die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ASM Name']    = $rows['asm_name'];
            $records['ASM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['MR Name']  =   $rows['mr_name'];
            $records['MR Emp code'] 	= $rows['mr_emp_code'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Target Doctor Count']    = $rows['total_doc_count'];
            $records['Achieved Doctor Count']    = $rows['total_doc_count'];
            $records['Target Patient Count']    = $rows['total_pat_count'];
            $records['Achieved Patient Count']  = $rows['acheive_patient_count']; 
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}