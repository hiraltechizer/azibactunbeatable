<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_tm_wise_patient_recruited_count extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
            [
                'field_name'=>'m|users_mobile',
                'field_label'=> 'MR Mobile',
            ],
            [],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);
        $m = date('m', strtotime(date('Y-m-d', time())));
		$y = date('Y', strtotime(date('Y-m-d', time())));

            $subsql = "SELECT b.brand_id,b.brand_name,division_id
            FROM brands b
            ORDER BY brand_id ASC ";
            
            $subcollection = $this->db->query($subsql)->result_array();

            $temp_array	=	array();
            $temp_npt	=	array();
            $temp_pot	=	array();

                foreach($subcollection as $brand_info) {
 
              $temp_str	=	"count(DISTINCT(case when ps.brand_id = ".$brand_info['brand_id']." then ps.patient_id END)) AS '".$brand_info['brand_name']."'";



              $temp_str_npt	=	"count(DISTINCT(case when ps.brand_id = ".$brand_info['brand_id']." AND MONTH(p.insert_dt) = $m AND YEAR(p.insert_dt) = $y then ps.patient_id END)) AS 'npt_".$brand_info['brand_name']."'";

              $temp_str_pot	=	"count(DISTINCT(case when ps.brand_id = ".$brand_info['brand_id']." AND MONTH(p.insert_dt) < $m AND YEAR(p.insert_dt) = $y then ps.patient_id END)) AS 'pot_".$brand_info['brand_name']."'";

                
                array_push($temp_array, $temp_str);
                array_push($temp_npt, $temp_str_npt);
                array_push($temp_pot, $temp_str_pot);

                }
                $binfo1 = '';
        $binfo2 = '';
        $binfo3 = '';
			$joinstr = '';
            $joinstrnpt = '';
            $joinstrpot = '';
			if(!empty($temp_array)) {
				$joinstr = ",";
                $joinstrnpt = ",";
                $joinstrpot = ",";
				$binfo1 = implode(",", $temp_array);
                $binfo2 = implode(",", $temp_npt);
                $binfo3 = implode(",", $temp_pot);
			}
    	$q = $this->db->select("m.users_id,m.users_name,m.users_mobile,m.users_type,c.city_name as mr_city,m.users_emp_id,asm.users_name AS asm_name,
        asm.users_emp_id AS asm_emp_code,a.area_name,rsm.users_name AS rsm_name,rsm.users_emp_id AS rsm_emp_code,
        r.region_name,zsm.users_name AS zsm_name,zsm.users_emp_id AS zsm_emp_code,z.zone_name,".$joinstr.implode(",", $temp_array).",".$joinstrnpt.implode(",", $temp_npt).",".$joinstrpot.implode(",", $temp_pot)."")
    	->from('manpower m')
		->join('manpower asm', 'asm.users_id = m.users_parent_id','left')
		->join('manpower rsm', 'rsm.users_id = asm.users_parent_id','left')
		->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id','left')
		->join('city c', 'c.city_id = m.users_city_id','LEFT')
		->join('area a', 'a.area_id = c.area_id','LEFT')
		->join('region r', 'r.region_id = a.region_id','LEFT')
		->join('zone z', 'z.zone_id = r.zone_id','LEFT')
		->join('doctor d', 'd.users_id = m.users_id')
        ->join('`patient` `p`', '`p`.`users_id` = `m`.`users_id`','left')
        ->join('patient_sub ps', 'ps.patient_id = p.patient_id','left');
	    
        
		if(sizeof($f_filters)) {

            foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                $key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE(ps.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE(ps.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                if($key == 'z.zone_id' || $key == 'r.region_id' || $key == 'a.area_id' || $key == 'c.city_id' ){
                  $this->db->where("$key", $value);
                }else{
                  $this->db->like($key, $value);
                }
            }
        }

        $q->where('m.is_deleted','0');
		$q->group_by('m.users_id');
        $q->order_by('ps.update_dt desc');
        if(! $count) {
          // $q->order_by('d.update_dt desc, d.doctor_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

    //   echo '<pre>';
    //   print_r($this->db->last_query());exit;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }
        $report_brands_record = $this->model->get_records([],'brands',['brand_name']); 
		foreach ($data as $rows) {

			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ABM Name']    = $rows['asm_name'];
            $records['ABM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['TM Name']  =   $rows['users_name'];
            $records['TM Emp code'] 	= $rows['users_emp_id'];
            $records['TM Emp code'] 	= $rows['users_mobile'];
			$records['TM City Name']    = $rows['mr_city'];

            if($report_brands_record) { 
                foreach($report_brands_record as $info) { 
                $brands = $info->brand_name;
        
                $records["NPT - Patient Recruited count of ".$brands.""]    = $rows["npt_".$brands.""];
                $records["POT - Patient Recruited count of ".$brands.""]    = $rows["pot_".$brands.""];
            }} 
            
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}