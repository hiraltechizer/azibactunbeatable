<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_geography_wise_patient_count_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

         if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        if(array_key_exists('b|brand_id', $filters))  {
            array_push($new_filters, 'b|brand_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'m|users_mobile',
                'field_label'=> 'MR Mobile',
            ],[],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],[],
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'Doctor Name',
            ]

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

        $q_str2 = '';
         $q_str = '';
         $q_str3 = '';
         $q_str1 = '';
        if(is_array($rfilters) && count($rfilters) ) {          
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                $key = str_replace('|', '.', $key);

                if($key == 'from_date') {
                    if(!empty($value)) {
                        $q_str = ' AND t.update_dt >= "'.date("Y-m-d", strtotime($value)).'"'; 
                        $q_str1 = ' AND pt.update_dt >= "'.date("Y-m-d", strtotime($value)).'"';
                        $q_str3 = ' AND t.update_dt >= "'.date("Y-m-d", strtotime($value)).'"';    
                    }
                }

                if($key == 'to_date') {

                    if(!empty($value)) {
                         $q_str .= ' AND t.update_dt <= "'. date("Y-m-d", strtotime($value)).'"';
                         $q_str1 .= ' AND pt.update_dt <= "'.date("Y-m-d", strtotime($value)).'"';
                         $q_str3 .= ' AND t.update_dt <= "'. date("Y-m-d", strtotime($value)).'"';   
                    } 
                }

                if($key == 'b.brand_id' && !empty($value)) {
                    $q_str2 .= ' AND t.brand_id ='.$value;
                }
            }
        } else {

            $q_str = ' AND MONTH(t.update_dt) = MONTH(CURRENT_DATE()) AND YEAR(t.update_dt) = YEAR(CURRENT_DATE())';
            $q_str1 = ' AND MONTH(pt.update_dt) = MONTH(CURRENT_DATE()) AND YEAR(pt.update_dt) = YEAR(CURRENT_DATE())';
            $q_str3 = ' AND DATE(t.update_dt) <= CURDATE()';
        }

		#get latest insert dt from table
		
    	$q = $this->db->select("m.users_name AS mr_name,m.users_mobile AS mr_mobile,m.users_emp_id AS mr_emp_code,c.city_name,m.users_type AS mr_type,
asm.users_name AS asm_name,asm.users_mobile AS asm_mobile, asm.users_emp_id AS asm_emp_code,a.area_name,
rsm.users_name AS rsm_name,rsm.users_mobile AS rsm_mobile,rsm.users_emp_id AS rsm_emp_code,r.region_name,
zsm.users_name AS zsm_name,zsm.users_mobile AS zsm_mobile,zsm.users_emp_id AS zsm_emp_code,z.zone_name,
d.doctor_name, (SELECT GROUP_CONCAT(bc.booklet_code) FROM booklet_code bc WHERE bc.doctor_id = d.doctor_id) AS booklet_code,
(SELECT COUNT(DISTINCT(p.patient_id)) FROM patient p JOIN patient_doctor pd ON pd.patient_id = p.patient_id
JOIN therapy t ON t.patient_id = p.patient_id 
JOIN call_tracking ct ON ct.patient_id = p.patient_id
WHERE t.`status` ='ongoing' AND  ct.call_status_id IN (5,11) AND pd.doctor_id = d.doctor_id ".$q_str2.$q_str." ) AS total_patient_count,
 (SELECT COUNT(DISTINCT(pt.patient_id)) FROM  patient_test pt 
WHERE pt.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd 
JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id 
WHERE pd.doctor_id = d.doctor_id AND t.`status` ='ongoing' AND ct.call_status_id IN (5,11) ".$q_str2." ) 
AND pt.test_type_id = 1 ".$q_str1." ) AS hba1c_count,
(SELECT COUNT(DISTINCT(pt.patient_id)) FROM  patient_test pt 
WHERE pt.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd 
JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id 
WHERE pd.doctor_id = d.doctor_id AND ct.call_status_id IN (5,11)  AND t.`status` ='ongoing' ".$q_str2.")
AND pt.test_type_id = 2 ".$q_str1." ) AS lipid_count,
(SELECT COUNT(DISTINCT(pt.patient_id)) FROM  patient_test pt 
WHERE pt.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd 
JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id 
WHERE pd.doctor_id = d.doctor_id AND ct.call_status_id IN (5,11) AND t.`status` ='ongoing' ".$q_str2.") 
AND pt.test_type_id = 3 ".$q_str1." ) AS both_count,
(SELECT COUNT(DISTINCT(p.patient_id)) FROM patient p 
JOIN patient_doctor pd1 ON pd1.patient_id = p.patient_id
JOIN therapy t ON t.patient_id = p.patient_id
JOIN call_tracking ct ON ct.patient_id = p.patient_id 
WHERE pd1.doctor_id = d.doctor_id AND t.`status` = 'ongoing' ".$q_str2." AND ct.call_status_id IN (5,11)
".$q_str3." ) AS patient_count")
    	->from('manpower m')
        ->join('doctor d', 'd.users_id = m.users_id')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id')
        ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id')
        ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id')
        ->join('city c', 'c.city_id = m.users_city_id')
        ->join('area a', 'a.area_id = asm.users_area_id')
        ->join('region r', 'r.region_id = rsm.users_region_id')
        ->join('zone z', 'z.zone_id = zsm.users_zone_id');
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$date_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {
			
			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);
                
                if($key != 'from_date' && $key != 'to_date' && $key != 'b.brand_id') {
                    if(!empty($value)) {
                        $this->db->like($key, $value);      
                    }
                }
            }
        }
       $q->where('d.is_deleted', '0');
        $q->group_by('d.doctor_id');
		
        if(! $count) {
           $q->order_by('d.update_dt desc, d.doctor_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

       // echo '<pre>';
        //print_r($this->db->last_query());die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

            $active_doc = 'Inactive';
            if(!empty($rows['patient_doctor_id'])) {
                $active_doc = 'Active';
            }

			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ASM Name']    = $rows['asm_name'];
            $records['ASM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['MR Name']  =   $rows['mr_name'];
            $records['MR Emp code'] 	= $rows['mr_emp_code'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Booklet code']    =   $rows['booklet_code'];
            $records['Doctor Name']    = $rows['doctor_name'];
            $records['New Patient on therapy in a month']    = $rows['total_patient_count'];
            $records['HbA1c Patient Count']    = $rows['hba1c_count']; 
            $records['Lipid Patient Count']   =   $rows['lipid_count'];
            $records['Both Patient Count']   =   $rows['both_count'];
            $records['Ongoing patient count']   =   $rows['patient_count'];
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}