<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_test_done_wise_patients_rank_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        if(array_key_exists('b|brand_id', $filters))  {
            array_push($new_filters, 'b|brand_id');
        }
        if(array_key_exists('mth|month_id', $filters))  {
            array_push($new_filters, 'mth|month_id');
        }

        if(array_key_exists('yr|year', $filters))  {
            array_push($new_filters, 'yr|year');
        }

        if(array_key_exists('pt|test_type_id', $filters))  {
            array_push($new_filters, 'pt|test_type_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
            [
                'field_name'=>'m|users_mobile',
                'field_label'=> 'MR Mobile',
            ],
            [
                'field_name'=>'m|users_type',
                'field_label'=> 'Type',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ]
        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

		#get latest insert dt from table
		
    	$q = $this->db->select("m.users_name,m.users_mobile,m.users_type,m.users_emp_id,c.city_name,MONTHNAME(pt.update_dt) AS month_name,YEAR(pt.update_dt) AS year_name,COUNT(DISTINCT(p.patient_id)) AS patient_testcount,DENSE_RANK() OVER (ORDER BY patient_testcount DESC) AS rank ")
    	->from('patient p')
    	->join('patient_doctor pd', 'pd.patient_id = p.patient_id')
        ->join('patient_test pt', 'pt.patient_id = p.patient_id')
        ->join('doctor d', 'd.doctor_id = pd.doctor_id')
        ->join('manpower m', 'm.users_id = d.users_id')
        ->join('city c', 'c.city_id = m.users_city_id')
        ->join('area a', 'a.area_id = c.area_id')
        ->join('region r', 'r.region_id = a.region_id')
        ->join('zone z', 'z.zone_id = r.zone_id')
        ->join('call_tracking ct', 'ct.patient_id = p.patient_id')
        ;
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$year_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {
			
			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);


                if($key == 'yr.year' && $value) {
                   $this->db->where('YEAR(pt.update_dt)', $value); 
                   $year_search_flag = TRUE;
                }

                if($key == 'mth.month_id' && $value) {
                    $this->db->where('MONTH(pt.update_dt)', $value);
                }

                if($key != 'yr.year' && $key != 'mth.month_id'){ 
                    if(!empty($value)) {
                        $this->db->where($key, $value);      
                    }
                }
            }
        }

        if(!$year_search_flag) {
            $q->where('YEAR(pt.update_dt)', date('Y'));            
        }

        $q->where('p.lab_test', 'Yes');
         $q->where_in('ct.call_status_id', [5,11]);
        $q->group_by('MONTH(pt.update_dt),YEAR(pt.update_dt)');
		
        if(! $count) {
           $q->order_by('patient_testcount desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

	//echo '<pre>';
	//echo $this->db->last_query();
	//die;	//print_r($collection);die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

			$records['Employee Name']  =   $rows['users_name'];
            $records['Mobile no.']  =   $rows['users_mobile'];
            $records['Designation'] =   $rows['users_type'];
            $records['Emp code'] 	= $rows['users_emp_id'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Month Name']    = $rows['month_name'];
            $records['Year Name']    = $rows['year_name'];    
            $records['Patient count']    = $rows['patient_testcount'];
            $records['Rank'] = $rows['rank'];
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}