<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_live_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        if(array_key_exists('b|brand_id', $filters))  {
            array_push($new_filters, 'b|brand_id');
        }

        if(array_key_exists('ct|sub_comment_id', $filters))  {
            array_push($new_filters, 'ct|sub_comment_id');
        }

        if(array_key_exists('mth|month_id', $filters))  {
            array_push($new_filters, 'mth|month_id');
        }

        if(array_key_exists('yr|year', $filters))  {
            array_push($new_filters, 'yr|year');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'name',
            ],
             [
                'field_name'=>'s|speciality',
                'field_label'=> 'speciality',
            ],
             [
                'field_name'=>'d|sbu_code',
                'field_label'=> 'SBU Code',
            ]
        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

		#get latest insert dt from table
		
    	
        $q = $this->db->select('pre.presc_id,pre.doctor_id,pre.rxn_count,pre.rxn_path,pre.insert_dt,d.doctor_name,d.sbu_code,s.speciality,m.users_name,c.city_name')
        ->from('prescription pre')
        ->join('doctor d','d.doctor_id = pre.doctor_id')
        // ->join('ho_connect_fields cfield', 'cfield.ho_conn_id = conn.ho_conn_id');
        ->join('manpower m', 'm.users_id = pre.users_id')
        ->join('city c', 'm.users_city_id = c.city_id')
        ->join('speciality s', 's.speciality_id = d.speciality_id')
        ->join('manpower asm' ,'asm.users_id = m.users_parent_id')
        ->join('manpower rsm' ,'rsm.users_id = asm.users_parent_id')
        ->join('manpower zsm' ,'zsm.users_id = rsm.users_parent_id')
        ;
	
		if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(pre.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(pre.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE(pre.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE(pre.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }
      

    
		
        if(! $count) {
            $q->order_by('pre.update_dt desc, pre.presc_id desc');
           
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

	// echo '<pre>';
	// echo $this->db->last_query();
	// die;	
    //print_r($collection);die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

            $rxn_path_url = '';
			$temp_rxn_path = explode(",", $rows['rxn_path']);
	        foreach ($temp_rxn_path as $key => $rxn_path) { 
				if(!empty($rxn_path)){
					$rxn_path_url .= base_url().$rxn_path.' ,';
				}	
			}

			 $records['MR Name'] = $rows['users_name'];
             $records['HQ'] = $rows['city_name'];
             $records['Doctor Name'] = $rows['doctor_name'];
            
             $records['Speciality'] = $rows['speciality'];
             $records['Doctor SBU Code'] = $rows['sbu_code'];
             $records['Total No. Of Prescription'] = $rows['rxn_count'];
             $records['Rxn Images Paths'] = $rxn_path_url;
            $records['DateTime'] = $rows['insert_dt'];
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}


    function get_role_wise_city($users_id,$role){
     
		$q = $this->db->select("GROUP_CONCAT(DISTINCT (c.city_id)) as city_id")
				->from('manpower m')
				->join('region r' ,'r.region_id = m.users_region_id')
                ->join('area a' ,'a.region_id = r.region_id')
                ->join('city c' ,'c.area_id = a.area_id')
               
                ;
		$q->where('m.users_id',$users_id);
        // $q->group_by('m.users_city_id ');
		$q->order_by('m.users_id desc');
		$collection1 = $q->get();
	   $collection = $collection1->result_array();
    //    echo $this->db->last_query();exit;
		return $collection;
    }


    function get_zsm_wise_city($users_id,$role){
     
		$q = $this->db->select("GROUP_CONCAT(DISTINCT(c.city_id)) as city_id")
				->from('manpower m')
				->join('manpower asm' ,'asm.users_id = m.users_parent_id')
                ->join('manpower rsm' ,'rsm.users_id = asm.users_parent_id')
                ->join('manpower zsm' ,'zsm.users_id = rsm.users_parent_id')
                ->join('city c' ,'c.city_id = m.users_city_id')
                ;
		$q->where('zsm.users_id',$users_id);
        // $q->group_by('m.users_city_id ');
		// $q->order_by('m.users_id desc');
		$collection1 = $q->get();
	   $collection = $collection1->result_array();
    //    echo $this->db->last_query();exit;
		return $collection;
    }
}