<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_dropout_patients_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        if(array_key_exists('b|brand_id', $filters))  {
            array_push($new_filters, 'b|brand_id');
        }

        if(array_key_exists('ct|sub_comment_id', $filters))  {
            array_push($new_filters, 'ct|sub_comment_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
        	 [
                'field_name'=>'zsm|users_name',
                'field_label'=> 'ZSM Name',
            ],
             [
                'field_name'=>'zsm|users_emp_id',
                'field_label'=> 'ZSM Emp code',
            ],
             [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone Name',
            ],
             [
                'field_name'=>'rsm|users_name',
                'field_label'=> 'RSM Name',
            ],
             [
                'field_name'=>'rsm|users_emp_id',
                'field_label'=> 'RSM Emp code',
            ],
             [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region Name',
            ],
            [
                'field_name'=>'asm|users_name',
                'field_label'=> 'ASM Name',
            ],
             [
                'field_name'=>'asm|users_emp_id',
                'field_label'=> 'ASM Emp code',
            ],
             [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area Name',
            ],
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            [
                'field_name'=>'p|patient_code',
                'field_label'=> 'Patient Code',
            ],
             [
                'field_name'=>'tt|test_type',
                'field_label'=> 'Test Type',
            ],
            [
                'field_name'=>'t|comment',
                'field_label'=> 'Reason',
            ],
            
            

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

		#get latest insert dt from table
		
    	$q = $this->db->select("m.users_name AS mr_name,m.users_emp_id AS mr_emp_code,c.city_name,asm.users_name AS asm_name,
asm.users_emp_id AS asm_emp_code,a.area_name,rsm.users_name AS rsm_name,rsm.users_emp_id AS rsm_emp_code,
r.region_name,zsm.users_name AS zsm_name,zsm.users_emp_id AS zsm_emp_code,z.zone_name,p.patient_code,sc.reason,
ct.insert_dt AS enrollment_date,ct.update_dt AS dropuout_date, b.brand_name,t.duration")
    	->from('patient p')
    	->join('call_tracking ct', 'ct.patient_id = p.patient_id')
        ->join('sub_comment sc', 'sc.sub_comment_id = ct.sub_comment_id','LEFT')
        ->join('patient_doctor pd', 'pd.patient_id = p.patient_id','LEFT')
        ->join('therapy t', 't.patient_id = p.patient_id','LEFT')
        ->join('brand b', 'b.brand_id = t.brand_id','LEFT')
        ->join('manpower m', 'm.users_id = pd.users_id','LEFT')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id','LEFT')
        ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id','LEFT')
        ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id','LEFT')
        ->join('city c', 'c.city_id = m.users_city_id','LEFT')
        ->join('area a', 'a.area_id = asm.users_area_id', 'LEFT')
        ->join('region r', 'r.region_id = rsm.users_region_id', 'LEFT')
        ->join('zone z', 'z.zone_id = zsm.users_zone_id', 'LEFT');
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$date_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {
			
			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                	$date_search_flag = TRUE;
                    $this->db->where('DATE(ct.update_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                	$date_search_flag = TRUE;
                    $this->db->where('DATE(ct.update_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

               if($key != 'from_date' && $key != 'to_date') {

                    if($key == 'b.brand_id') {
                        if(!empty($value)) {
                            $this->db->like('t.brand_id', $value);      
                        }
                    } else {
                        if(!empty($value)) {
                            $this->db->like($key, $value);      
                        }
                    }
                }
            }
        }
        $q->where('ct.call_status_id', '6');
        $q->group_by('t.brand_id,p.patient_id');
		
        if(! $count) {
           $q->order_by('p.update_dt desc, p.patient_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

	//echo '<pre>';
	//echo $this->db->last_query();
	//die;	//print_r($collection);die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ASM Name']    = $rows['asm_name'];
            $records['ASM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['MR Name']  =   $rows['mr_name'];
            $records['MR Emp code'] 	= $rows['mr_emp_code'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Patient Code']    = $rows['patient_code'];
            $records['Brand Name']    = $rows['brand_name'];
            $records['Reason']    = $rows['reason'];
            $records['Enrolled date']  = $rows['enrollment_date']; 
            $records['Dropped out Date'] = $rows['dropuout_date'];
            $records['Therapy duration'] = $rows['duration'];
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}