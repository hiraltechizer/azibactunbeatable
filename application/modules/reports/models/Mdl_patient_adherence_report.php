<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_patient_adherence_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

          if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }
        return $new_filters;
	}
	
	

    function get_filters() {
        return [
        	 [
                'field_name'=>'zsm|users_name',
                'field_label'=> 'ZSM Name',
            ],
             [
                'field_name'=>'zsm|users_emp_id',
                'field_label'=> 'ZSM Emp code',
            ],
             [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone Name',
            ],
             [
                'field_name'=>'rsm|users_name',
                'field_label'=> 'RSM Name',
            ],
             [
                'field_name'=>'rsm|users_emp_id',
                'field_label'=> 'RSM Emp code',
            ],
             [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region Name',
            ],
            [
                'field_name'=>'asm|users_name',
                'field_label'=> 'ASM Name',
            ],
             [
                'field_name'=>'asm|users_emp_id',
                'field_label'=> 'ASM Emp code',
            ],
             [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area Name',
            ],
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],
            [
                'field_name'=>'p|patient_code',
                'field_label'=> 'Patient Code',
            ],[],
             [
                'field_name'=>'cc|coupon_code',
                'field_label'=> 'Coupon Code',
            ],
            [
                'field_name'=>'bc|booklet_code',
                'field_label'=> 'Booklet Code',
            ],
            
            

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

		#get latest insert dt from table
		
    	$q = $this->db->select("zsm.users_name AS zsm_name,zsm.users_emp_id AS zsm_emp_code,z.zone_name,rsm.users_name AS rsm_name,
rsm.users_emp_id AS rsm_emp_code,r.region_name,asm.users_name AS asm_name,asm.users_emp_id AS asm_emp_code,
a.area_name,m.users_name AS mr_name,m.users_emp_id AS mr_emp_code,c.city_name,pdt.diet_chart_send,
p.patient_id,p.patient_code,ct.update_dt, dcc.diet_coupon_code, bc.booklet_code, d.doctor_name,
cts.city_name AS doc_city_name,p.lab_test,tt.test_type,cc.coupon_code,(SELECT GROUP_CONCAT(b.brand_name) FROM therapy t1 JOIN brand b ON b.brand_id = t1.brand_id 
WHERE t1.patient_id = p.patient_id) AS brand_name")
    	->from('patient p')
        ->join('call_tracking ct', 'ct.patient_id = p.patient_id')
    	->join('therapy t', 't.patient_id = p.patient_id')
        ->join('patient_doctor pd', 'pd.patient_id = p.patient_id')
        ->join('doctor d', 'd.doctor_id = pd.doctor_id')
        ->join('cities cts', 'cts.city_id = d.city_id')
        ->join('patient_coupon_code pcc','pcc.patient_id = p.patient_id')
        ->join('diet_coupon_code dcc', 'dcc.diet_coupon_code_id = pcc.diet_coupon_code_id')
        ->join('booklet_code bc', 'bc.booklet_code_id =dcc.booklet_code_id')
        ->join('patient_test pt', 'pt.patient_id = p.patient_id','LEFT')
        ->join('test_type tt', 'tt.test_type_id = pt.test_type_id','LEFT')
        ->join('coupon_code cc', 'cc.coupon_code_id = pt.coupon_code_id','LEFT')
        ->join('patient_diet_track pdt', 'pdt.patient_id = p.patient_id','LEFT')
        ->join('manpower m', 'm.users_id = d.users_id')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id')
        ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id')
        ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id')
        ->join('city c', 'c.city_id = m.users_city_id')
        ->join('area a', 'a.area_id = asm.users_area_id')
        ->join('region r', 'r.region_id = rsm.users_region_id')
        ->join('zone z', 'z.zone_id = zsm.users_zone_id')
        ;
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$date_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {
			
			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                	$date_search_flag = TRUE;
                    $this->db->where('DATE(p.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                	$date_search_flag = TRUE;
                    $this->db->where('DATE(p.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value)) {
                    $this->db->like($key, $value);      
                }
            }
        }

        $q->where('t.status','ongoing');
        $q->where_in('ct.call_status_id', [5,11]);
		$q->group_by('p.patient_id');

        if(! $count) {
           $q->order_by('p.update_dt desc, p.patient_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

	//echo '<pre>';
	//echo $this->db->last_query();
	//die;	//print_r($collection);die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

            $diet_chart_send = 'No';
            if(!empty($rows['diet_chart_send'])) {
                $diet_chart_send = 'Yes';
            }

            $test_done = 'No';
            if(!empty($rows['patient_test_id'])){
                $test_done = 'Yes';
            }

			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ASM Name']    = $rows['asm_name'];
            $records['ASM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['MR Name']  =   $rows['mr_name'];
            $records['MR Emp code'] 	= $rows['mr_emp_code'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Patient Code']    = $rows['patient_code'];
            $records['Enrollment date']    = $rows['update_dt'];
            $records['Coupon code']    = $rows['diet_coupon_code'];
            $records['Booklet code']  = $rows['booklet_code']; 
            $records['Doctor Name'] = $rows['doctor_name'];
            $records['Doctor city'] = $rows['doc_city_name'];
            $records['Diet chart shared (Y/N)'] = $diet_chart_send;
            $records['Test done'] = $test_done;
            $records['Test coupon code'] = $rows['coupon_code'];
            $records['Brand name'] = $rows['brand_name'];
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}