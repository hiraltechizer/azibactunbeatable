<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_geography_wise_doctor_wise_patient_report extends MY_Model {

    function __construct() {
        parent::__construct();
	}

	function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

         if(array_key_exists('z|zone_id', $filters))  {
            array_push($new_filters, 'z|zone_id');
        }
        
        if(array_key_exists('r|region_id', $filters))  {
            array_push($new_filters, 'r|region_id');
        }
        
        if(array_key_exists('a|area_id', $filters))  {
            array_push($new_filters, 'a|area_id');
        }
        
        if(array_key_exists('c|city_id', $filters))  {
            array_push($new_filters, 'c|city_id');
        }

        if(array_key_exists('b|brand_id', $filters))  {
            array_push($new_filters, 'b|brand_id');
        }

        return $new_filters;
	}
	
	

    function get_filters() {
        return [
        	  [
                'field_name'=>'zsm|users_name',
                'field_label'=> 'ZSM Name',
            ],
             [
                'field_name'=>'zsm|users_emp_id',
                'field_label'=> 'ZSM Emp code',
            ],
             [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone Name',
            ],
             [
                'field_name'=>'rsm|users_name',
                'field_label'=> 'RSM Name',
            ],
             [
                'field_name'=>'rsm|users_emp_id',
                'field_label'=> 'RSM Emp code',
            ],
             [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region Name',
            ],
            [
                'field_name'=>'asm|users_name',
                'field_label'=> 'ASM Name',
            ],
             [
                'field_name'=>'asm|users_emp_id',
                'field_label'=> 'ASM Emp code',
            ],
             [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area Name',
            ],
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'MR Emp code',
            ],
             [
                'field_name'=>'c|city_name',
                'field_label'=> 'HQ Name',
            ],[],
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'Doctor Name',
            ],
            [
                'field_name'=>'cts|city_name',
                'field_label'=> 'City Name',
            ]

        ];
    }

	function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

		$field_filters = $this->get_filters_from($rfilters);

        $q_str = '';
        $q_str1 = '';
        $q_str2 = '';

        if(is_array($rfilters) && count($rfilters) ) {          
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
                
                $key = str_replace('|', '.', $key);

                if($key == 'from_date') {
                    if(!empty($value)) {
                        $q_str .= 'AND pt.insert_dt >= "'.date("Y-m-d", strtotime($value)).'"'; 
                        $q_str1 .= 'AND t.insert_dt >= "'.date("Y-m-d", strtotime($value)).'"';   
                    }
                }

                if($key == 'to_date') {

                    if(!empty($value)) {
                         $q_str .= 'AND pt.insert_dt <= "'. date("Y-m-d", strtotime($value)).'"';
                         $q_str1 .= 'AND t.insert_dt >= "'.date("Y-m-d", strtotime($value)).'"';   
                    }
                }

                if($key == 'b.brand_id' && !empty($value)) {
                    $q_str2 .= ' AND t.brand_id ='.$value;
                }
            }
        }

		#get latest insert dt from table
		
    	$q = $this->db->select("d.doctor_id,m.users_name AS mr_name,m.users_emp_id AS mr_emp_code, asm.users_name AS asm_name,asm.users_emp_id AS asm_emp_code, rsm.users_name AS rsm_name, rsm.users_emp_id AS rsm_emp_code,
zsm.users_name AS zsm_name,zsm.users_emp_id AS zsm_emp_code, c.city_name,a.area_name,
r.region_name,z.zone_name, d.doctor_name, cts.city_name as doc_city_name,
(SELECT GROUP_CONCAT(bc.booklet_code) FROM booklet_code bc WHERE bc.doctor_id = d.doctor_id) AS booklet_code,
pd.patient_doctor_id, (SELECT COUNT(pt.patient_id) FROM  patient_test pt 
WHERE pt.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id WHERE pd.doctor_id = d.doctor_id AND t.`status` ='ongoing' AND ct.call_status_id IN (5,11) ".$q_str2." ) AND pt.test_type_id = 1 ".$q_str." ) AS hba1c_count,
(SELECT COUNT(pt.patient_id) FROM  patient_test pt 
WHERE pt.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id WHERE pd.doctor_id = d.doctor_id AND t.`status` ='ongoing' AND ct.call_status_id IN (5,11) ".$q_str2." ) AND pt.test_type_id = 2 ".$q_str." ) AS lipid_count,
(SELECT COUNT(pt.patient_id) FROM  patient_test pt 
WHERE pt.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id WHERE pd.doctor_id = d.doctor_id AND t.`status` ='ongoing' AND ct.call_status_id IN (5,11) ".$q_str2." ) AND pt.test_type_id = 3 ".$q_str." ) AS both_count,
(SELECT COUNT(DISTINCT(t.patient_id)) 
FROM therapy t 
JOIN call_tracking ct ON ct.patient_id = t.patient_id
WHERE t.`status` = 'ongoing' AND ct.call_status_id IN (5,11) AND t.patient_id IN (SELECT DISTINCT(pd.patient_id) FROM patient_doctor pd JOIN therapy t ON t.patient_id = pd.patient_id JOIN call_tracking ct ON ct.patient_id = pd.patient_id WHERE pd.doctor_id = d.doctor_id AND t.`status` ='ongoing' AND ct.call_status_id IN (5,11) ".$q_str2." ) ".$q_str1." ) AS total_active_pat")
    	->from('doctor d')
        ->join('cities cts', 'cts.city_id = d.city_id')
        ->join('patient_doctor pd', 'pd.doctor_id = d.doctor_id','LEFT')
        ->join('manpower m', 'm.users_id = d.users_id')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id')
        ->join('manpower rsm', 'rsm.users_id = asm.users_parent_id')
        ->join('manpower zsm', 'zsm.users_id = rsm.users_parent_id')
        ->join('city c', 'c.city_id = m.users_city_id')
        ->join('area a', 'a.area_id = asm.users_area_id')
        ->join('region r', 'r.region_id = rsm.users_region_id')
        ->join('zone z', 'z.zone_id = zsm.users_zone_id');
	
		if(sizeof($f_filters)) {
			foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
		}

		$date_search_flag = FALSE;
		if(is_array($rfilters) && count($rfilters) ) {
			
			
            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);
                
                if($key != 'from_date' && $key != 'to_date' && $key != 'b.brand_id') {
                    if(!empty($value)) {
                        $this->db->like($key, $value);      
                    }
                }
            }
        }
       $q->where('d.is_deleted', '0');
        $q->group_by('d.doctor_id');
		
        if(! $count) {
           $q->order_by('d.update_dt desc, d.doctor_id desc');
        }

		if(!empty($limit)) { $q->limit($limit, $offset); }
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();

        //echo '<pre>';
        //print_r($this->db->last_query());die;
		return $collection;
    }
    
	function _format_data_to_export($data){
		
		$resultant_array = [];
		$role = $this->session->get_field_from_session('role','user');

        if(empty($role)) {
            $role = $this->session->get_field_from_session('role');
        }

		foreach ($data as $rows) {

            $active_doc = 'Inactive';
            if(!empty($rows['patient_doctor_id'])) {
                $active_doc = 'Active';
            }

			$records['ZSM Name']    = $rows['zsm_name'];
            $records['ZSM Emp code']    = $rows['zsm_emp_code'];
            $records['Zone Name']          = $rows['zone_name'];
            $records['RSM Name']    = $rows['rsm_name'];
            $records['RSM emp code']    = $rows['rsm_emp_code'];
            $records['Region Name']    = $rows['region_name'];
            $records['ASM Name']    = $rows['asm_name'];
            $records['ASM Emp code']    = $rows['asm_emp_code'];
            $records['Area Name']    = $rows['area_name'];
            $records['MR Name']  =   $rows['mr_name'];
            $records['MR Emp code'] 	= $rows['mr_emp_code'];
			$records['HQ Name']    = $rows['city_name'];
            $records['Booklet code']    =   $rows['booklet_code'];
            $records['Doctor Name']    = $rows['doctor_name'];
            $records['Doctor City']    = $rows['doc_city_name'];
            $records['Active or Inactive doctor']    = $active_doc;
            $records['Patient screened count For HbA1c']    = $rows['hba1c_count']; 
            $records['Patient screened count For Lipid']   =   $rows['lipid_count'];
            $records['Patient screened count For Both']   =   $rows['both_count'];
            $records['Ongoing Patient count YTD']   =   $rows['total_active_pat'];
			
            array_push($resultant_array, $records);
		}
		return $resultant_array;
	}
}