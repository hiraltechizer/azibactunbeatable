<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_repurchase_reminder extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_filters() {
        return [
            [
                'field_name'=>'zsm|users_name',
                'field_label'=> 'Name',
            ],
             [
                'field_name'=>'zsm|users_emp_id',
                'field_label'=> 'Users Emp',
            ],
            [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone',
            ],
            [
                'field_name'=>'rsm|users_name',
                'field_label'=> 'Name',
            ],
             [
                'field_name'=>'rsm|users_emp_id',
                'field_label'=> 'Users Emp',
            ],
            [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region',
            ],
            [
                'field_name'=>'asm|users_name',
                'field_label'=> 'Name',
            ],
             [
                'field_name'=>'asm|users_emp_id',
                'field_label'=> 'Users Emp',
            ],
            [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area',
            ],
            [
                'field_name'=>'m|users_name',
                'field_label'=> 'Name',
            ],
             [
                'field_name'=>'m|users_emp_id',
                'field_label'=> 'Users Emp',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'city',
            ],

            [
                'field_name'=>'p|patient_code',
                'field_label'=> 'Code',
            ],
            [
                'field_name'=>'dcc|coupon_code',
                'field_label'=> 'Coupon',
            ],
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'doctor',
            ],
            
        ];
    }

   /* function getGeoData($id,$userType) {

        if($userType == "MR") {
        $q = $this->db->select('m.*,z.zone_id, z.zone_name, r.region_id, r.region_name, 
            a.area_id, a.area_name, c.city_id, c.city_name,asm.users_name as reporting_manager')
        ->from('manpower m')
        ->join('manpower asm', 'asm.users_id = m.users_parent_id')
        ->join('city c', 'm.users_city_id = c.city_id')
        ->join('area a', 'c.area_id = a.area_id')
        ->join('region r', 'a.region_id = r.region_id')
        ->join('zone z', 'r.zone_id = z.zone_id');
        } else if($userType == "ASM") {
            $q = $this->db->select('m.*,z.zone_id, z.zone_name, r.region_id, r.region_name,a.area_id, a.area_name,rsm.users_name as reporting_manager')
                ->from('manpower m')  
                ->join('manpower rsm', 'rsm.users_id = m.users_parent_id')
                ->join('area a', 'm.users_area_id = a.area_id')
                ->join('region r', 'a.region_id = r.region_id')
                ->join('zone z', 'r.zone_id = z.zone_id'); 
        } else if($userType == "RSM") {
            $q = $this->db->select('m.*,z.zone_id, z.zone_name, r.region_id, r.region_name,zsm.users_name as reporting_manager')
                ->from('manpower m') 
                ->join('manpower zsm', 'zsm.users_id = m.users_parent_id')
                ->join('region r', 'm.users_region_id = r.region_id')
                ->join('zone z', 'r.zone_id = z.zone_id'); 
        } else if($userType == "ZBM") {
           $q = $this->db->select('m.*,z.zone_id, z.zone_name')
                ->from('manpower m')  
                ->join('zone z', 'm.users_zone_id = z.zone_id'); 
        } else if($userType == "HO") {
           $q = $this->db->select('m.*')
                ->from('manpower m'); 
        }
       
        
        $q->where('m.users_id',$id);       
               
        //$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        $collection = $q->get()->result_array();
        
        return $collection;

    } */

    // function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

    //     $q = $this->db->select('adh2.*, adh2.insert_dt AS commentUpdateDate, 
    //     adh2.comment AS revisitcomment, 
    //     ( SELECT COUNT(adh3.repurchase_history_id) 
    //       FROM repurchase_call_history adh3 
    //        WHERE adh3.patient_id = adh2.patient_id 
    //        AND adh3.call_status_id IS NOT NULL 
    //        GROUP BY adh3.patient_id) AS total_calls, 
    //        (SELECT MIN(rc.insert_dt )
    //        FROM repurchase_call_history rc
    //        WHERE rc.patient_id = adh2.patient_id
    //        ) AS firts_remin,
    //         (SELECT MIN(rc.call_days )
    //        FROM repurchase_call_history rc
    //        WHERE rc.patient_id = adh2.patient_id
    //        ) AS firts_call_type,
    //        p.name_1, p.mobile_1, p.name_2, p.mobile_2, p.lang_code, 
    //        add.address_line, add.pincode, p.patient_code, p.update_dt AS comm_up_dt, 
    //        p.mode, p.type, p.patient_reminder, p.caretaker_reminder,cs.status_name,
    //        lan.language_id, lan.language_name,cit.city_name',FALSE)
    // ->from('`repurchase_call_history` `adh2`')
    // ->join('patient p','p.patient_id = adh2.patient_id', 'LEFT')			
    // ->join('address add','add.patient_id = p.patient_id', 'LEFT')
    // ->join('cities cit','cit.city_id = add.city', 'LEFT')
    // ->join('call_status cs','cs.status_id = adh2.call_status_id','LEFT')
    // ->join('language lan', 'lan.language_id = p.lang_code', 'LEFT')		
    // ->where('
    //         adh2.repurchase_history_id IN (
    //         SELECT MAX(adh.repurchase_history_id)
    //         FROM repurchase_call_history adh
    //         GROUP BY adh.patient_id) AND 
    //         (`adh2`.`call_status_id` NOT IN (10,11,5,3,4,6,12,13,14) OR adh2.call_status_id IS NULL)
    //     ');
           
    //     if(sizeof($f_filters)) {
    //         foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
    //     }
    
    //     if(is_array($rfilters) && count($rfilters) ) {
    
    //         foreach($rfilters as $key=> $value) {
    //             if(!in_array($key, $field_filters)) {
    //                 continue;
    //             }
                
    //             $key = str_replace('|', '.', $key);
    //             if($key == 'from_date' && $value) {
    //                 $this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
    //                 continue;
    //             }
    
    //             if($key == 'to_date' && $value) {
    //                 $this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
    //                 continue;
    //             }
    
    //             if(!empty($value))
    //                 $this->db->like($key, $value);
    //         }
    //     }
    //     $q->group_by('adh2.patient_id');	
    //     if(! $count) {
    //         $q->order_by('commentUpdateDate desc');
    //         //$q->order_by('sms.insert_dt desc');
    //     }
    
    //     if(!empty($limit)) { $q->limit($limit, $offset); }
    
    //     // print_r($this->db->get_compiled_select());exit;
    //     $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
    //     echo '<pre>';
    //     echo $this->db->last_query();die;
    //     return $collection;
    // }
    
    function _format_data_to_export($data){
        
        $resultant_array = [];


        foreach ($data as $rows) {

            $records['TLM Name'] = $rows['tlm_name'];
            $records['TLM Emp Code']   = $rows['tim_empcode'];
            $records['Zone Name']   =    $rows['zone_name'];
            $records['SLM Name']   =   $rows['slm_name'];
            $records['SLM Emp Code'] = $rows['slm_empcode'];
            $records['Region Name'] = $rows['region_name'];
            $records['FLM Name'] = $rows['flm_name'];
            $records['FLM Emp Code'] = $rows['flm_name'];
            $records['Area Name'] = $rows['area_name'];
            $records['SO Name'] = $rows['users_name'];
            $records['SO Emp Code'] = $rows['users_emp_id'];
            $records['SO HQ Name'] = $rows['city_name'];


            $records['Patient Code'] = $rows['patient_code'];
            $records['Coupon Code']   = $rows['coupons'];
            $records['Dr. Name']   =    $rows['doctors'];
            $records['Patient City']   =   $rows['pcity'];
            $records['Language'] = $rows['language_name'];
            $records['Call Date'] = $rows['firts_remin'];
            $records['Call status (drop down)'] = $rows['status_name'];
            $records['Repurchase Date'] = $rows['reminder_date'];
            $records['Call counts'] = $rows['total_calls'];
           

            
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }

    

//     function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0){

// 		$current_date = date('Y-m-d');
// 		// $this->load->model('cron/Mdl_repurchase', 'model');
//         $repurchase_records = $this->get_repurchase_records();
//         $abc = array();
//          if(count($repurchase_records)) {
//          	$i=0;
//             foreach ($repurchase_records as $value) {
//             	$patient_id = $value->patient_id;
// 		        $type = $value->type;
// 		        $patient_name = ($type == "patient") ? $value->name_1 : $value->name_2;
// 		        $patient_mobile = ($type == "patient") ? $value->mobile_1 : $value->mobile_2;
// 		        $caretaker_name = ($type == "caretaker") ? $value->name_1 : $value->name_2;
// 		        $caretaker_mobile = ($type == "caretaker") ? $value->mobile_1 : $value->mobile_2;
// 		        $dose = (int)$value->dose;
//                 $pending_pills = !empty($value->pending_pills) ? (int)$value->pending_pills : 0;
//                 $quantity_pills = (int)$value->quantity_pills;
//                 $purchase_date = $value->purchase_date;

//                 $no_of_days_for_repurchase = (($quantity_pills / $dose) + ($pending_pills / $dose)) - 1;

//                 $reminder_date = date('Y-m-d', strtotime("$purchase_date +$no_of_days_for_repurchase days"));
//                 $patient_delivered = $this->model->get_tracking_patient(['p.patient_id' => $patient_id, 'c.insert_dt >=' => $purchase_date]);

//                 if(count($patient_delivered)) {
//                     $total_pills_delivered = $patient_delivered[0]->total_delivered;
                    
//                     if($total_pills_delivered) {
//                         $reminder_date = date('Y-m-d', strtotime("$reminder_date +$total_pills_delivered days"));
//                     }
//                 }
//                 // ----------------
//                 $reminder_date_5 = date('Y-m-d', strtotime("$reminder_date -5 days"));
//                 $reminder_date_2 = date('Y-m-d', strtotime("$reminder_date -2 days"));
//                 if(strtotime($reminder_date) == strtotime($current_date)){
//                 	/*$abc.push({$patient_mobile});*/
//                 	$abc[$i]['mobile'] = $patient_mobile;
//                 	$abc[$i]['patient_name'] = $patient_name;
//                 	$abc[$i]['caretaker_name'] = $caretaker_name;
//                 	$abc[$i]['caretaker_mobile'] = $caretaker_mobile;
//                 	$abc[$i]['dose'] = $dose;
//                 	$abc[$i]['pending_pills'] = $pending_pills;
//                 	$abc[$i]['purchase_date'] = $purchase_date;
//                 	$abc[$i]['reminder_date'] = $reminder_date;
//                 	$abc[$i]['quantity_pills'] = $quantity_pills;
//                 	//$abc[$i]['caretaker_mobile'] = $caretaker_mobile;

//                 	$i++;
//                 }
// 			}
// 	}

// 	return $abc;	
// }


function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0 ) {

	$field_filters = $this->get_filters_from($rfilters);

	$q = $this->db->select('adh2.*, adh2.insert_dt AS commentUpdateDate, 
	adh2.comment AS revisitcomment, 
	( SELECT COUNT(adh3.repurchase_history_id) 
	  FROM repurchase_call_history adh3 
	   WHERE adh3.patient_id = adh2.patient_id 
	   AND adh3.call_status_id IS NOT NULL 
	   GROUP BY adh3.patient_id) AS total_calls, 
	   (SELECT MIN(rc.insert_dt )
	   FROM repurchase_call_history rc
	   WHERE rc.patient_id = adh2.patient_id
	   ) AS firts_remin, GROUP_CONCAT(DISTINCT (dcc.diet_coupon_code)) as coupons,
       GROUP_CONCAT(DISTINCT (d.doctor_name)) as doctors,
	   p.name_1, p.mobile_1, p.name_2, p.mobile_2, 
	   add.address_line, add.pincode, p.patient_code, p.update_dt AS comm_up_dt, 
	   p.mode, p.type, t.patient_reminder, t.caretaker_reminder,cs.status_name,
	   lan.language_id, lan.language_name,cit.city_name as pcity,zsm.users_name AS tlm_name,zsm.users_emp_id AS tim_empcode,z.zone_name,
       rsm.users_name AS slm_name,rsm.users_emp_id AS slm_empcode,r.region_name,
       asm.users_name AS flm_name,asm.users_emp_id AS flm_empcode,a.area_name,m.users_name,m.users_emp_id,c.city_name,d.doctor_name',FALSE)
->from('`repurchase_call_history` `adh2`')
->join('patient p','p.patient_id = adh2.patient_id', 'LEFT')
->join('therapy t', 't.patient_id = p.patient_id')	
->join('address add','add.patient_id = p.patient_id', 'LEFT')
->join('cities cit','cit.city_id = add.city', 'LEFT')
->join('call_status cs','cs.status_id = adh2.call_status_id','LEFT')
->join('language lan', 'lan.language_id = p.language_id', 'LEFT')	
->join('patient_coupon_code pcc','pcc.patient_id = p.patient_id','LEFT')
->join('diet_coupon_code dcc','dcc.diet_coupon_code_id = pcc.diet_coupon_code_id','LEFT')
->join('patient_doctor pd', 'pd.patient_id = p.patient_id', 'LEFT')
->join('doctor d', 'd.doctor_id = pd.doctor_id', 'LEFT')
->join('manpower m','m.users_id = d.users_id','LEFT')
->join('manpower asm','asm.users_id = m.users_parent_id','LEFT')
->join('manpower rsm','rsm.users_id = asm.users_parent_id','LEFT')
->join('manpower zsm','zsm.users_id = rsm.users_parent_id','LEFT')
->join('city c','c.city_id = m.users_city_id','LEFT')
->join('area a','a.area_id = c.area_id','LEFT')
->join('region r','r.region_id = a.region_id','LEFT')
->join('zone z','z.zone_id = r.zone_id','LEFT')	
->where('
		adh2.repurchase_history_id IN (
		SELECT MAX(adh.repurchase_history_id)
		FROM repurchase_call_history adh
		GROUP BY adh.patient_id) AND 
		(`adh2`.`call_status_id` NOT IN (11,5,3,4,6,12,13,14) OR adh2.call_status_id IS NULL)
	');
	   
	if(sizeof($f_filters)) {
		foreach ($f_filters as $key=>$value) { $q->where("$key", $value); }
	}

	if(is_array($rfilters) && count($rfilters) ) {

		foreach($rfilters as $key=> $value) {
			if(!in_array($key, $field_filters)) {
				continue;
			}
			
			$key = str_replace('|', '.', $key);
			if($key == 'from_date' && $value) {
				$this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
				continue;
			}

			if($key == 'to_date' && $value) {
				$this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
				continue;
			}

			if(!empty($value))
				$this->db->like($key, $value);
		}
	}
	$q->group_by('adh2.patient_id');	
	if(! $count) {
		$q->order_by('commentUpdateDate desc');
		//$q->order_by('sms.insert_dt desc');
	}

	if(!empty($limit)) { $q->limit($limit, $offset); }

	// print_r($this->db->get_compiled_select());exit;
	$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
	
	echo '<pre>';
	echo $this->db->last_query();die;
	return $collection;
}	

function get_repurchase_records() {
    $q = $this->db->query("SELECT 
    pt.name_1,pt.name_2,pt.mobile_1,pt.mobile_2,pt.type,th.patient_reminder,th.caretaker_reminder,
    `p`.`purchase_id`, `p`.`patient_id`,th.brand_id,b.brand_name,
    `p`.`therapy_id`, `p`.`purchase_date`, `p`.`quantity_pills`, `p`.`pending_pills`,
    th.dose
    FROM `purchase` p
    JOIN therapy th ON th.therapy_id = p.therapy_id
    JOIN patient pt ON pt.patient_id = p.patient_id
    LEFT JOIN brand b ON th.brand_id = b.brand_id
    WHERE 1 = 1 AND purchase_id IN (
            SELECT 
                MAX(purchase_id) 
            FROM purchase 
            WHERE 1 = 1
            AND `is_approved` = 'yes' 
            GROUP BY `patient_id`
        )
    AND `th`.`is_approved` = 'yes'  
    AND  `th`.`status` = 'ongoing'
    AND `b`.`is_deleted` = 0
    ORDER BY  patient_id");
    $collection = $q->result();
    return $collection;
}




}