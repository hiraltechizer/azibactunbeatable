<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { ?>
<tr>
    <?php $id = $record['patient_id']; ?>
    <td><?php echo $record['zsm_name'] ?></td>
    <td><?php echo $record['zsm_emp_code'] ?></td>
    <td><?php echo $record['zone_name'] ?></td>
    <td><?php echo $record['rsm_name'] ?></td>
    <td><?php echo $record['rsm_emp_code'] ?></td>
    <td><?php echo $record['region_name'] ?></td>
    <td><?php echo $record['asm_name'] ?></td>
    <td><?php echo $record['asm_emp_code'] ?></td>
    <td><?php echo $record['area_name'] ?></td>
    <td><?php echo $record['mr_name'] ?></td>
    <td><?php echo $record['mr_emp_code'] ?></td>
    <td><?php echo $record['city_name'] ?></td>
    <td><?php echo $record['patient_code'] ?></td>
    <td><?php echo date('Y-m-d H:i:s',strtotime($record['update_dt'])); ?></td>
    <td><?php echo $record['diet_coupon_code'] ?></td>
    <td><?php echo $record['booklet_code'] ?></td>
    <td><?php echo $record['doctor_name'] ?></td>
    <td><?php echo $record['doc_city_name'] ?></td>
    <td>
        <?php if($role == 'SA' || $role == 'A') { ?>
            <?php if(!empty($record['diet_chart_send'])) { ?> 
                <a href="<?php echo base_url("reports/patient_adherence_report/pdfdownload?pid=$id") ?>" download>Download</a>
            <?php } ?>
        <?php } else { ?>
            <?php if(!empty($record['diet_chart_send'])) { ?>
            Yes
            <?php } else { ?>    
            No
            <?php } ?>
        <?php } ?>
    </td>
    <td>
        <?php if(!empty($record['patient_test_id'])) { ?>
            Yes
        <?php } else { ?>
            No
        <?php } ?>
    <td><?php echo $record['coupon_code'] ?></td>
    <td><?php echo $record['brand_name'] ?></td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= count($columns)  ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= count($columns) ?>"><?php echo $this->ajax_pagination->create_links(); ?></td>
</tr>