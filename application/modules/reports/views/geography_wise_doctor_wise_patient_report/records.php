<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { ?>
<tr>
    <td><?php echo $record['zsm_name'] ?></td>
    <td><?php echo $record['zsm_emp_code'] ?></td>
    <td><?php echo $record['zone_name'] ?></td>
    <td><?php echo $record['rsm_name'] ?></td>
    <td><?php echo $record['rsm_emp_code'] ?></td>
    <td><?php echo $record['region_name'] ?></td>
    <td><?php echo $record['asm_name'] ?></td>
    <td><?php echo $record['asm_emp_code'] ?></td>
    <td><?php echo $record['area_name'] ?></td>
    <td><?php echo $record['mr_name'] ?></td>
    <td><?php echo $record['mr_emp_code'] ?></td>
    <td><?php echo $record['city_name'] ?></td>
    <td><?php echo $record['booklet_code'] ?></td>
    <td><?php echo $record['doctor_name'] ?></td>
    <td><?php echo $record['doc_city_name'] ?></td>
    <td>
        <?php if(!empty($record['patient_doctor_id'])) { ?>
            Active
        <?php } else { ?>
            Inactive
        <?php } ?>             
    </td>
    <td><?php echo $record['hba1c_count'] ?></td>
    <td><?php echo $record['lipid_count'] ?></td>
     <td><?php echo $record['both_count'] ?></td>
     <td><?php echo $record['total_active_pat'] ?></td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= count($columns)  ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= count($columns) ?>"><?php echo $this->ajax_pagination->create_links(); ?></td>
</tr>