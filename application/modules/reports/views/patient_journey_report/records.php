<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { ?>
<tr>
    <?php $prequiredInfo   =   Modules::run('reports/patient_journey_report/_getpatientrequiredcnt',$record['patient_id']);  

        $patient_bmi = 0;
        if(isset($prequiredInfo[0]['patient_bmi'])) {
            $patient_bmi = $prequiredInfo[0]['patient_bmi'];
        }

        $rxn_verify = 'No';
        if(isset($prequiredInfo[0]['rxn_verify'])) {
            $rxn_verify = $prequiredInfo[0]['rxn_verify'];
        } 
        
        $invoice_verify = 'No';
        if(isset($prequiredInfo[0]['invoice_verify'])) {
            $invoice_verify = $prequiredInfo[0]['invoice_verify'];
        }        

        $purchase_quantity = 0;
        if(isset($prequiredInfo[0]['purchase_quantity'])) {
            $purchase_quantity = $prequiredInfo[0]['purchase_quantity'];
        } 

        $status = 'Pending';
        if(isset($prequiredInfo[0]['status'])) {
            $status = $prequiredInfo[0]['status'];
        } 

        $healthtips_cnt = 0;
        if(isset($prequiredInfo[0]['healthtips_cnt'])) {
            $healthtips_cnt = $prequiredInfo[0]['healthtips_cnt'];
        }

        $rev_call = 0;
        if(isset($prequiredInfo[0]['rev_call'])) {
            $rev_call = $prequiredInfo[0]['rev_call'];
        } 

        $rep_call = 0;
        if(isset($prequiredInfo[0]['rep_call'])) {
            $rep_call = $prequiredInfo[0]['rep_call'];
        }

        $total_count_share_diet_chart = 0;
        if(isset($prequiredInfo[0]['total_count_share_diet_chart'])) {
            $total_count_share_diet_chart = $prequiredInfo[0]['total_count_share_diet_chart'];
        }

        $test_type = '';
        if(isset($prequiredInfo[0]['test_type'])) {
            $test_type = $prequiredInfo[0]['test_type'];
        }



    ?>
    <td><?php echo $record['zsm_name'] ?></td>
    <td><?php echo $record['zsm_emp_code'] ?></td>
    <td><?php echo $record['zone_name'] ?></td>
    <td><?php echo $record['rsm_name'] ?></td>
    <td><?php echo $record['rsm_emp_code'] ?></td>
    <td><?php echo $record['region_name'] ?></td>
    <td><?php echo $record['asm_name'] ?></td>
    <td><?php echo $record['asm_emp_code'] ?></td>
    <td><?php echo $record['area_name'] ?></td>
    <td><?php echo $record['mr_name'] ?></td>
    <td><?php echo $record['mr_emp_code'] ?></td>
    <td><?php echo $record['city_name'] ?></td>
    <td><?php echo $record['patient_code'] ?></td>
    <td><?php echo $record['diet_coupon_code'] ?></td>
    <td><?php echo $record['booklet_code'] ?></td>
    <td><?php echo $record['doctor_name'] ?></td>
    <td><?php echo $record['doc_city_name'] ?></td>
    <td><?php echo $record['brand_name'] ?></td>
    <td>
        <?php if(!empty($record['consent_doc_id'])) { ?>
            Yes
        <?php } else {?>
            No
         <?php } ?>   
    </td>
    <td><?php echo $record['patient_age'] ?></td>
    <td><?php echo $record['patient_gender'] ?></td>
    <td><?php echo $patient_bmi ?></td>
    <td><?php echo $rxn_verify ?></td>
    <td><?php echo $invoice_verify ?></td>
    <td><?php echo $purchase_quantity ?></td>
    <td><?php echo $status ?></td>
    <td><?php echo $healthtips_cnt ?></td>
    <td><?php echo $rev_call ?></td>
    <td><?php echo $rep_call ?></td>
    <td><?php echo $total_count_share_diet_chart ?></td>
    <td><?php echo $test_type ?></td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= count($columns)  ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= count($columns) ?>"><?php echo $this->ajax_pagination->create_links(); ?>
    </td>
</tr>
