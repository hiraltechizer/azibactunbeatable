<html>
    <head>
        <title>Wheel of fortune Wheel</title>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/winwheel.css" type="text/css" />
        <script src='<?php echo base_url() ?>assets/resources/winwheel.js'></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<style>
.demo{width:417px; height:417px; position:relative; margin:50px auto} 
#disk{width:417px; height:417px; background:url(disk.jpg) no-repeat} 
#start{width:163px; height:320px; position:absolute; top:46px; left:130px;} 
#start img{cursor:pointer} 

</style>
    </head>
    <body>
        <div align="center">
           
            <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <div class="power_controls">
                    <br />
                        <br />
                        <table class="power" cellpadding="10" cellspacing="0">
                            <tr>
                                <th align="center">Power</th>
                            </tr>
                            <tr>
                                <td width="78" align="center" id="pw3" onClick="powerSelected(3);">High</td>
                            </tr>
                            <tr>
                                <td align="center" id="pw2" onClick="powerSelected(2);">Med</td>
                            </tr>
                            <tr>
                                <td align="center" id="pw1" onClick="powerSelected(1);">Low</td>
                            </tr>
                        </table>
                        
                        <img id="spin_button" src="<?php echo base_url(); ?>assets/images/spin_off.png" alt="Spin" onClick="startSpin();" />
                        <br /><br />
                        &nbsp;&nbsp;<a href="#" onClick="resetWheel(); return false;">Play Again</a><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(reset)
                    </div>
                </td>
                <td width="438" height="582" class="the_wheel" align="center" valign="center">
                    <canvas id="canvas" width="434" height="434">
                        <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please try another.</p>
                    </canvas>
                </td>
            </tr>
        </table>

        <div class="demo"> 
    <div id="disk"></div> 
    <div id="start"><img src="<?php echo base_url(); ?>assets/images/spin_off.png" id="startbtn"></div> 
</div> 
        <script>
            
            let theWheel = new Winwheel({
                'outerRadius'     : 212,  
                'innerRadius'     : 75, 
                'textFontSize'    : 24,  
                'textOrientation' : 'horizontal', 
                'textAlignment'   : 'outer',    
                'numSegments'     : 5,         
                'segments'        :           
                [                    
                   {'fillStyle' : '#f78d05', 'text' : 'Pearl','textFontSize' : 18, 'textFillStyle' : '#ffffff'},
                   {'fillStyle' : '#004690', 'text' : 'Ruby','textFontSize' : 18, 'textFillStyle' : '#ffffff'},
                   {'fillStyle' : '#f6989d', 'text' : 'Emerald','textFontSize' : 16, 'textFillStyle' : '#ffffff'},
                   {'fillStyle' : '#00aef0', 'text' : 'Saphire','textFontSize' : 16, 'textFillStyle' : '#ffffff'},
                   {'fillStyle' : '#f26522', 'text' : 'Diamond','textFontSize' : 16, 'textFillStyle' : '#ffffff'}
                ],
                'animation' :           
                {
                    'type'     : 'spinToStop',
                    'duration' : 10,   
                    'spins'    : 3,     
                    'callbackFinished' : alertPrize,
                    // 'callbackSound'    : playSound,   
                    'soundTrigger'     : 'pin' 
                },
                'pins' :				
                {
                    'number'     : 5,
                    'fillStyle'  : 'silver',
                    'outerRadius': 4,
                }
            });

            // Loads the tick audio sound in to an audio object.
            // let audio = new Audio('<?php echo base_url(); ?>assets/images/tick.mp3');

            // This function is called when the sound is to be played.
            // function playSound()
            // {
            //     // Stop and rewind the sound if it already happens to be playing.
            //     audio.pause();
            //     audio.currentTime = 0;

            //     // Play the sound.
            //     audio.play();
            // }

            // Vars used by the code in this page to do power controls.
            let wheelPower    = 0;
            let wheelSpinning = false;

            // -------------------------------------------------------
            // Function to handle the onClick on the power buttons.
            // -------------------------------------------------------
            function powerSelected(powerLevel)
            {
                // Ensure that power can't be changed while wheel is spinning.
                if (wheelSpinning == false) {
                    // Reset all to grey incase this is not the first time the user has selected the power.
                    document.getElementById('pw1').className = "";
                    document.getElementById('pw2').className = "";
                    document.getElementById('pw3').className = "";

                    // Now light up all cells below-and-including the one selected by changing the class.
                    if (powerLevel >= 1) {
                        document.getElementById('pw1').className = "pw1";
                    }

                    if (powerLevel >= 2) {
                        document.getElementById('pw2').className = "pw2";
                    }

                    if (powerLevel >= 3) {
                        document.getElementById('pw3').className = "pw3";
                    }

                    // Set wheelPower var used when spin button is clicked.
                    wheelPower = powerLevel;

                    // Light up the spin button by changing it's source image and adding a clickable class to it.
                    document.getElementById('spin_button').src = "spin_on.png";
                    document.getElementById('spin_button').className = "clickable";
                }
            }

            // -------------------------------------------------------
            // Click handler for spin button.
            // -------------------------------------------------------
            function startSpin()
            {
               
                // Ensure that spinning can't be clicked again while already running.
                if (wheelSpinning == false) {
                    // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                    // to rotate with the duration of the animation the quicker the wheel spins.
                    if (wheelPower == 1) {
                        alert("1");
                        theWheel.animation.spins = 3;
                    } else if (wheelPower == 2) {
                        alert("2");
                        theWheel.animation.spins = 6;
                    } else if (wheelPower == 3) {
                        alert("3");
                        theWheel.animation.spins = 10;
                    }

                    // Disable the spin button so can't click again while wheel is spinning.
                    document.getElementById('spin_button').src       = "<?php echo base_url(); ?>assets/images/spin_off.png";
                    document.getElementById('spin_button').className = "";

                    // Begin the spin animation by calling startAnimation on the wheel object.
                    theWheel.startAnimation();

                    // Set to true so that power can't be changed and spin button re-enabled during
                    // the current animation. The user will have to reset before spinning again.
                    wheelSpinning = true;
                }
            }

            // -------------------------------------------------------
            // Function for reset button.
            // -------------------------------------------------------
            function resetWheel()
            {
                theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                theWheel.draw();                // Call draw to render changes to the wheel.

                document.getElementById('pw1').className = "";  // Remove all colours from the power level indicators.
                document.getElementById('pw2').className = "";
                document.getElementById('pw3').className = "";

                wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
            }

            // -------------------------------------------------------
            // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
            // -------------------------------------------------------
            function alertPrize(indicatedSegment)
            {
                // Just alert to the user what happened.
                // In a real project probably want to do something more interesting than this with the result.
                if (indicatedSegment.text == 'LOOSE TURN') {
                    alert('Sorry but you loose a turn.');
                } else if (indicatedSegment.text == 'BANKRUPT') {
                    alert('Oh no, you have gone BANKRUPT!');
                } else {
                    alert("You have won " + indicatedSegment.text);
                }
            }
        </script>
    </body>
</html>