<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['ho_conn_id']; ?>
    <tr>
    <td>
        <input type="checkbox" name="ids[]" value="<?php echo $id ?>" id="check_<?= $id ?>" class="form-check-input form-check-{color}" />
        <label for="check_<?= $id ?>"></label>
    </td>
     <?php 
        $fileUrl = '';
        if(file_exists('./'.$record['display_image'])) {
            $fileUrl = base_url().$record['display_image'];
        } 

        $OfileUrl = '';
        if(file_exists('./'.$record['upload_path'])) {
            $OfileUrl = base_url().$record['upload_path'];
        }         
    ?>
    <td><?php echo $record['document_type'] ?></td>
    <td><?php echo $record['title'] ?></td>
    <td><?php echo $record['description'] ?></td>
    <td>
        <?php if(!empty($fileUrl) ) { ?>

             <a target="_blank" href="<?php echo $OfileUrl; ?>" style="" data-fancybox="images" data-fancybox >
                <img src="<?php echo $fileUrl; ?>" alt="" style="width:50px;height: 50px;border-radius: 0%;">
            </a>
        <?php } ?>
    </td>
    <td><?php echo ucfirst($record['status']); ?></td>
    <td><?php echo $record['insert_dt'] ?></td>
    <td>
    	<?php if($record['status'] == 'drafts'){ ?>
        <div class="btn-group" role="group" aria-label="Basic example">
        	
            <a class="blue-color" href="<?php echo base_url("$controller/edit/record/$id?c=$timestamp") ?>" title="Edit">
                <i class="fa fa-edit"></i>
            </a>
        </div>
        <?php } ?>
    </td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>">
        <div class="row">
            <?php echo $this->ajax_pagination->create_links(); ?>    
        </div>
    </td>
</tr>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<link href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" rel="stylesheet">

<script src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
      
        $('[data-fancybox="images"]').fancybox({
  buttons : [
    'download',
    'thumbs',
    'close'
  ]
});
    </script>