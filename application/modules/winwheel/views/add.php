<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/winwheel.css" type="text/css" />
        <script src='<?php echo base_url() ?>assets/resources/winwheel.js'></script>
<script src="<?php echo base_url() ?>assets/resources/TweenMax.min.js"></script>
<style>
    .demo {
        width: 417px;
        height: 417px;
        position: relative;
        margin: 50px auto
    }

    #disk {
        width: 417px;
        height: 417px;
        background: url(disk.jpg) no-repeat
    }

    #start {
        width: 163px;
        height: 320px;
        position: absolute;
        top: 46px;
        left: 130px;
    }

    #start img {
        cursor: pointer
    }
    #swal2-title {
        font-family: Roboto-medium;
        font-style: normal;
        font-weight: 500;
        font-size: 23px;
        line-height: 27px;
        padding: 22px 15px;
        color: #0088BA;
    }

    #swal2-content {
        font-family: Roboto-Regular;
        font-style: normal;
        font-weight: normal;
        font-size: 17px;
        line-height: 25px;
        display: flex;
        align-items: center;
        text-align: center;

        color: #A9A9A9;
    }

    .swal2-confirm {
        width: 138px;
        height: 40px;
        background-color: #5087C6 !important;
        border-radius: 34px !important;
    }
    .bg-success, .settings-panel .color-tiles .tiles.success, .datepicker-custom .datepicker.datepicker-inline .datepicker-days .table-condensed tbody tr td.day.today:before, .swal2-modal .swal2-buttonswrapper .swal2-styled.swal2-confirm{
        background-color: #5087C6 !important;
        border-color:  #5087C6 !important;
    }
    .bg-success, .settings-panel .color-tiles .tiles.success:hover, 
   
     .swal2-modal .swal2-buttonswrapper .swal2-styled.swal2-confirm:hover{
        background-color: #5087C6 !important;
        border-color: #5087C6 !important;
    }
    a.bg-success:hover, .settings-panel .color-tiles a.tiles.success:hover, .swal2-modal .swal2-buttonswrapper a.swal2-styled.swal2-confirm:hover, a.bg-success:focus, .settings-panel .color-tiles a.tiles.success:focus, .swal2-modal .swal2-buttonswrapper a.swal2-styled.swal2-confirm:focus, button.bg-success:hover, .settings-panel .color-tiles button.tiles.success:hover, .swal2-modal .swal2-buttonswrapper button.swal2-styled.swal2-confirm:hover, button.bg-success:focus, .settings-panel .color-tiles button.tiles.success:focus, .swal2-modal .swal2-buttonswrapper button.swal2-styled.swal2-confirm:focus {
    background-color:#5087C6 !important;
}
    @media (max-width: 399px) {
        #canvas {
            width: 320px;
            height: 320px;
        }

        td.the_wheel {
            background-size: 82% 72%;
        }

        td #canvas {
            margin-top: -55px;
        }

        .spin-wheel-wrap {
            margin-top: -50px;
        }
    }


</style>

<div align="center" class="spin-wheel-wrap">

    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <!-- <td>
                    <div class="power_controls">
                    <br />
                        <br />
                        <table class="power" cellpadding="10" cellspacing="0" style="display:none">
                            <tr>
                                <th align="center">Power</th>
                            </tr>
                            <tr>
                                <td width="78" align="center" id="pw3" onClick="powerSelected(3);">High</td>
                            </tr>
                            <tr>
                                <td align="center" id="pw2" onClick="powerSelected(2);">Med</td>
                            </tr>
                            <tr>
                                <td align="center" id="pw1" onClick="powerSelected(1);">Low</td>
                            </tr>
                        </table>
                        
                        <?php



                        ?>
                        <img id="spin_button" src="<?php echo base_url(); ?>assets/images/spin_off.png" alt="Spin" onClick="startSpin(<?php echo $users_id ?>);" />
                        <br /><br />
                        &nbsp;&nbsp;<a href="#" onClick="resetWheel(); return false;">Play Again</a><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(reset)
                    </div>
                </td> -->
            <td width="500" height="450" class="the_wheel" align="center" valign="center">
                <canvas id="canvas" width="434" height="434">
                    <p align="center">Sorry, your browser doesn't support canvas. Please try another.</p>
                </canvas>
            </td>


        </tr>
        <tr>
            <td>
                <div class="power_controls text-center">
                    <br />
                    <br />
                    <table class="power" cellpadding="10" cellspacing="0" style="display:none">
                        <tr>
                            <th align="center">Power</th>
                        </tr>
                        <tr>
                            <td width="78" align="center" id="pw3" onClick="powerSelected(3);">High</td>
                        </tr>
                        <tr>
                            <td align="center" id="pw2" onClick="powerSelected(2);">Med</td>
                        </tr>
                        <tr>
                            <td align="center" id="pw1" onClick="powerSelected(1);">Low</td>
                        </tr>
                    </table>

                    <?php



                    ?>
                    <a id="spin_button" class="btn btn-spin" onClick="startSpin(<?php echo $users_id ?>);">Spin</a>

                    &nbsp;&nbsp;<a class="btn btn-spin" href="#" onClick="resetWheel(); return false;">Reset</a>
                    <!-- <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(reset) -->
                </div>
            </td>
        </tr>
    </table>




    <script>
        let theWheel = new Winwheel({
            'outerRadius': 150,
            'innerRadius': 55,
            'textFontSize': 18,
            'textOrientation': 'horizontal',
            'textAlignment': 'outer',
            'numSegments': 5,
            'segments': [{
                    'fillStyle': 'rgba(232, 150, 6, 1)',
                    'text': 'Pearl',
                    'textFontSize': 18,
                    'textFillStyle': '#ffffff'
                },
                {
                    'fillStyle': 'rgba(147, 2, 43, 1)',
                    'text': 'Ruby',
                    'textFontSize': 18,
                    'textFillStyle': '#ffffff'
                },
                {
                    'fillStyle': 'rgba(38, 124, 69, 1)',
                    'text': 'Emerald',
                    'textFontSize': 16,
                    'textFillStyle': '#ffffff'
                },
                {
                    'fillStyle': 'rgba(30, 1, 74, 1)',
                    'text': 'Saphire',
                    'textFontSize': 16,
                    'textFillStyle': '#ffffff'
                },
                {
                    'fillStyle': 'rgba(4, 123, 155, 1)',
                    'text': 'Diamond',
                    'textFontSize': 16,
                    'textFillStyle': '#ffffff'
                }
            ],
            'animation': {
                'type': 'spinToStop',
                'duration': 10,
                'spins': 3,
                'callbackFinished': alertPrize,
                // 'soundTrigger'     : 'pin' 
            },
            'pins': {
                'number': 5,
                'fillStyle': 'silver',
                'outerRadius': 4,
                'responsive': true
            }
        });


        let wheelPower = 0;
        let wheelSpinning = false;

        function powerSelected(powerLevel) {
            if (wheelSpinning == false) {

                document.getElementById('pw1').className = "";
                document.getElementById('pw2').className = "";
                document.getElementById('pw3').className = "";

                if (powerLevel >= 1) {
                    document.getElementById('pw1').className = "pw1";
                }

                if (powerLevel >= 2) {
                    document.getElementById('pw2').className = "pw2";
                }

                if (powerLevel >= 3) {
                    document.getElementById('pw3').className = "pw3";
                }


                wheelPower = powerLevel;


                document.getElementById('spin_button').src = "spin_on.png";
                document.getElementById('spin_button').className = "clickable";
            }
        }

        function startSpin($users_id) {

            // $('#preloader').show();
            if (wheelSpinning == false) {

                if (wheelPower == 1) {

                    theWheel.animation.spins = 3;
                } else if (wheelPower == 2) {

                    theWheel.animation.spins = 6;
                } else if (wheelPower == 3) {

                    theWheel.animation.spins = 10;
                }

                document.getElementById('spin_button').src = "<?php echo base_url(); ?>assets/images/spin_off.png";
                document.getElementById('spin_button').className = "btn btn-spin";

                theWheel.startAnimation();

                wheelSpinning = true;
            }
        }

        function resetWheel() {
            theWheel.stopAnimation(false);
            theWheel.rotationAngle = 0;
            theWheel.draw();
            document.getElementById('pw1').className = "";
            document.getElementById('pw2').className = "";
            document.getElementById('pw3').className = "";

            wheelSpinning = false;
        }

        function alertPrize(indicatedSegment) {
            $('#preloader').show();
            $.ajax({
                type: "POST",
                url: baseUrl + 'winwheel/getprice',
                data: {
                    'token': $('meta[name=csrf-token]').attr("content")
                },
                dataType: "json",
                beforeSend: function(xhr, opts) {
                    $('#preloader').show();
                },
                success: function(data) {
                    console.log(data);
                    $('#preloader').hide();

                    if (data) {
                        swal({
                            title: "Congratulations !",
                            text: 'You have been qualified for "'+ data +'" slab',
                            imageUrl: "<?php echo base_url("assets/images/trophy.png") ?>",
                            imageWidth: 250,
                            imageHeight: 250,
                        });
                        $('#preloader').hide();
                    } else {

                        swal({
                            title: 'Oops!!',
                            text: "You need to achieve more to fall under any slab. Better luck next time",
                            imageUrl: "<?php echo base_url("assets/images/sad.png") ?>",
                            imageWidth: 250,
                            imageHeight: 250,
                        });
                        $('#preloader').hide();
                    }
                }
            });
        }
    </script>