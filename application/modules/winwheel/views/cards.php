<style>
    .tbl-wrap table>thead tr:first-child {
        background-color: #fff;
        color: #fff;
    }
</style>

<?php $i = 1;
if (sizeof($collection)) : ?>
    <div id="tbody" class="row clearfix">
        <?php foreach ($collection as $record) {
            $id = $record['ho_conn_id']; ?>
            <div class="col-md-4">
                <div class=" box-body" style="padding: 15px">
                    <?php if (in_array('edit', $permissions)) : ?>
                        <?php if ($role == 'HO') : ?>
                            <?php if ($record['status'] == 'drafts') : ?>
                                
                                    <a class="" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit" style="position:absolute;right:3px;top:5px" href='<?php echo base_url("$controller/edit/record/$id?c=$timestamp") ?>' title="Edit <?php ucfirst($module_title) ?>"><i class="material-icons" style="font-size:18px;color:#06b1f0">edit</i></a>
                              
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="col-sm-12">
                  
                        <?php
                        $fileUrl = '';
                        if (file_exists('./' . $record['display_image'])) {
                            $fileUrl = base_url() . $record['display_image'];
                        }
                        $OfileUrl = '';
                        if (file_exists('./' . $record['upload_path'])) {
                            $OfileUrl = base_url() . $record['upload_path'];
                        }
                        ?>
                        <a target="_blank" href="<?php echo $OfileUrl; ?>" 
                        style="width:45px" data-fancybox="images" data-fancybox><img src="<?php echo $fileUrl; ?>" alt="" style="width:40px;height: 40px;border-radius: 0%;"></a>
                    
                    <div class="time-txt">
                            <?php echo date('d-m-Y', strtotime($record['insert_dt'])) ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="dash-panel-ttl">
                            <?php echo word_limiter($record['title'], 5); ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="dash-panel-text">
                            <?php echo word_limiter($record['description'], 15);
                            ?>
                        </div>
                       

                        <div class="text-right" >
                            <a class=" read-more-text" href="<?php echo base_url("$controller/notifcation_display/view/$id?c=$timestamp") ?>"> Read More </a>
                        </div>

                    </div>
             
                </div>
            </div>
        <?php $i++;
        } ?>
        <div class="clearfix"></div>
        <div class="col-md-12 text-center pagination-wrap">
            <?php echo $this->ajax_pagination->create_links(); ?>
        </div>
    </div>
<?php else : ?>
    <div class="col-md-12 text-center"><i>No Record Found</i>
        <div>
        <?php endif; ?>

        <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
        <link href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" rel="stylesheet">

        <script src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript">
            $('[data-fancybox="images"]').fancybox({
                buttons: [
                    'download',
                    'thumbs',
                    'close'
                ]
            });
        </script>