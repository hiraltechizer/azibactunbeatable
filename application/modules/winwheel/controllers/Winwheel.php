<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Winwheel extends User_Controller
{
	private $module = 'winwheel';
    private $model_name = 'mdl_winwheel';
    private $controller = 'winwheel';
    private $settings = [
        'permissions'=> ['add'],
        'pagination_index' => 3,
        'filters' => ['column_filters'=> FALSE, 'date_filters'=> FALSE]
    ];
    private $scripts = ['knowledge_world.js'];

   
	function __construct() {
         parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        $this->load->helper('text');
        // $this->load->model('master/mdl_speciality', 'mdl_speciality');
        $this->load->model('sales_performance/mdl_sales_performance','performance');
        $this->set_defaults();
    }

     function uploadcsv(){
        $this->session->is_Ajax_and_admin_logged_in();
        /*upload csv file */

        if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
        }

        if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
        }

        $file = $_FILES['csvfile']['tmp_name'];
        $handle = fopen($file, "r");
        $cnt = 0; $newrows = 0;

        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){

            if(count($data) !== 4) { continue; }

            if(! $cnt){
                $cnt++; continue;
            }
            
            $doctor_name = trim($data[0]);
            $doctor_mobile = trim($data[1]);
            $speciality = trim($data[2]);
            $users_mobile = trim($data[3]);
           
            if(empty($doctor_name) || empty($doctor_mobile) || empty($users_mobile) || empty($speciality)){
                continue;
            }

            if( 
                ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $doctor_name)
                || ! preg_match('/^[1-9][0-9]{9}$/', $doctor_mobile) 
                || ! preg_match('/^[1-9][0-9]{9}$/', $users_mobile)
                || ! preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $speciality) 
            ){
                continue;
            }
            
            $DrInfo = $this->model->get_records(['doctor_mobile'=> $doctor_mobile], 'doctor', ['doctor_id'], '', 1);
            if(count($DrInfo)) {
                continue;
            }

            $MRInfo = $this->model->get_records(['users_mobile'=> $users_mobile], 'manpower', ['users_id'], '', 1);
            if(!count($MRInfo)) {
                continue;
            }
            
            $users_id = $MRInfo[0]->users_id;

            #check for speciality
            $SpecialityInfo = $this->model->get_records(['speciality'=> $speciality], 'speciality', ['speciality_id'], '', 1);
            if(!count($SpecialityInfo)) {
                continue;
            }
            
            $speciality_id = $SpecialityInfo[0]->speciality_id;            

            $insert['users_id'] = $users_id;
            $insert['doctor_name'] = $doctor_name;
            $insert['doctor_mobile'] = $doctor_mobile;
            $insert['speciality_id'] = $speciality_id;
          

            $this->model->_insert($insert);

            $newrows++;
        }

        fclose($handle);

        echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
    }


    function getprice(){
  
        $users_id = $this->session->get_field_from_session('user_id', 'user');
        $role =  $this->session->get_field_from_session('role','user');
        $userInfo = $this->model->get_records(['users_id' => $users_id], 'manpower', ['users_city_id']);
       
        $sfilters = ['c.city_id' => $userInfo[0]->users_city_id];
        
        $collection = $this->performance->get_collection($count = FALSE, $sfilters);
        // print_r($collection[0]['city_name']);exit;
        $price = "";
       
        if ($collection[0]['pearl'] <= $collection[0]['sale_arc'] && $collection[0]['sale_arc'] <= $collection[0]['ruby']  ) {
                $price = "Pearl";

        }elseif($collection[0]['ruby'] <= $collection[0]['sale_arc'] && $collection[0]['sale_arc'] <= $collection[0]['emerald'] ){
               $price = "Ruby";

        }elseif($collection[0]['emerald'] <= $collection[0]['sale_arc'] && $collection[0]['sale_arc'] <= $collection[0]['saphire']){
               $price = "Emerald";

        }elseif($collection[0]['saphire'] <= $collection[0]['sale_arc'] && $collection[0]['sale_arc'] <= $collection[0]['diamond']){
               $price = "Saphire";

        }elseif($collection[0]['diamond'] <= $collection[0]['sale_arc']){
               $price = "Diamond";
        }else{
           $price = "";
        }

        echo json_encode($price);
    }
}
