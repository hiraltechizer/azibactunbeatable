<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Template extends MX_Controller {

	function __construct() {
		parent::__construct();
		/*$this->load->library('pwa');
		Pwa::init();*/
	}

	function _admin($data){
		$data['role'] = $role = $this->session->get_field_from_session('role');

		if( ! in_array($role, ['SA', 'A'])){
			show_error('You do not have permission to access content of the page', 403, 'Unauthorized Access');
		}

		$this->load->view('admin', $data);		
	}

	function _ho($data) {
		$data['role'] = $role = $this->session->get_field_from_session('role', 'user');

		if( ! in_array($role, ['DT','HO'])) {
			show_error('You do not have permission to access content of the page', 403, 'Unauthorized Access');
		}

		$this->load->view('ho', $data);
	}

	function _user($data) {
		$data['user_role'] = $role = $this->session->get_field_from_session('role', 'user');

		if( ! in_array($role, ['MR', 'HO','RSM','ASM','ZSM'])) {
			show_error('You do not have permission to access content of the page', 403, 'Unauthorized Access');
		}

		$this->load->view('user', $data);
	}

	function _login($data){
		$this->load->view('login', $data);
    }
    
	function _one_page($data){
		$this->load->view('one-page', $data);
	}

	function _ho_login($data) {
		$this->load->view('hologin', $data);
	}

	function _get_post_modal(){
		$this->load->view('get_post_modal');
	}


}