<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Azibact Unbeatable</title>
    <!-- <link rel="icon" href="../../favicon.ico" type="image/x-icon"> -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/iconfonts/font-awesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
    <!-- endinject -->
  
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png" />
    <link rel="manifest" href="<?php echo base_url(); ?>assets/manifest.json">
</head>
<style>
.content-wrapper {
    background: #FFFFFF;
    box-shadow: 0px 3px 29px rgba(0, 0, 0, 0.58);
    z-index: 99;
}

.page-body-wrapper {
    /* padding: 100px; */
    background: #F3F3F3;
}

.vector-column {
    background: #EDEDED;
}

.auth form .auth-form-btn {
    height: 50px;
    line-height: 0;
}

.btn-orange {
    background: linear-gradient(89.83deg, #F5BF6D -17.64%, #E8951E 69.26%);
    color: #fff;
    font-size: 18px;
    width: 145px;
}

.text-primary {
    color: #f5a623 !important;
}

.top-vector {
    position: absolute;
    right: 0;
}

.bottom-vector {
    position: absolute;
    left: 0;
    bottom: 0;
}

@media only screen and (max-width:800px) {
    .vector-column {
        display: none !important;
    }

    .page-body-wrapper {
        padding: 30px;

    }
}
</style>

<body class="">
    <!-- <img src="<?php echo base_url(); ?>assets/images/top-vector.png" class="top-vector" alt=""> -->

    <div class="container-fluid page-body-wrapper user-login-bg full-page-wrapper">
        <!-- <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg"> -->
        <div class="row flex-grow justify-content-center  align-items-center">
            <div class="col-md-7 col-lg-7 ">

                <div class="auth-form-transparent text-left p-3">
                    <div class="brand-logo text-center pb-5">
                        <img src="<?php echo base_url(); ?>assets/images/login/ipca_logo-.png" alt="">

                        <!-- <h4 class="text-center"><?= $this->config->item('title') ?></h4> -->
                        <!-- <h5 class="text-center"><?php echo strtoupper($module) ?></h5> -->
                    </div>
                    <?php
                    if(!isset($module)){
                        $module = $this->uri->segment(1);
                    }

                    if(!isset($viewFile)){
                        $viewFile = $this->uri->segment(2);
                    }

                    if( $module != '' && $viewFile != '' ){
                        $path = $controller . '/' . $viewFile;
                        echo $this->load->view($path);
                    }
            ?>
                    <br>
                    <button name="addToHome" id="btnAdd" class="addToHome btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light waves-effect" style="display:none">Install Application</button> 
                </div>
               
            </div>

        </div>

        <!-- page-body-wrapper ends -->
    </div>
    <!-- <img src="<?php echo base_url(); ?>assets/images/bottom-vector.png" class="bottom-vector" alt=""> -->
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?php echo base_url() ?>assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?php echo base_url() ?>assets/js/off-canvas.js"></script>
    <script src="<?php echo base_url() ?>assets/js/hoverable-collapse.js"></script>
    <script src="<?php echo base_url() ?>assets/js/misc.js"></script>
    <script src="<?php echo base_url() ?>assets/js/settings.js"></script>
    <script src="<?php echo base_url() ?>assets/js/todolist.js"></script>
    <!-- endinject -->
</body>


<script type="text/javascript">
if ("serviceWorker" in navigator) {

  if (navigator.serviceWorker.controller) {

    console.log("[PWA Builder] active service worker found, no need to register");

  } else {

    // Register the service worker

    navigator.serviceWorker

      .register("<?php echo base_url(); ?>pwabuilder-sw.js", {

        scope: "./"

      })

      .then(function (reg) {

        console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);

      });

  }

}

var deferredPrompt;
var btnSave = document.querySelectorAll('.addToHome')[0];

    window.addEventListener('beforeinstallprompt', function(e) {
      console.log('beforeinstallprompt Event fired');
      //e.preventDefault();   //I even try with this uncommented no luck so far

      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      btnSave.style.display = 'block';
      return false;
    });

    btnSave.addEventListener('click', function() {
      if(deferredPrompt !== undefined) {
        // The user has had a postive interaction with our app and Chrome
        // has tried to prompt previously, so let's show the prompt.
        deferredPrompt.prompt();

        // Follow what the user has done with the prompt.
        deferredPrompt.userChoice.then(function(choiceResult) {

          console.log(choiceResult.outcome);

          if(choiceResult.outcome == 'dismissed') {
            console.log('User cancelled home screen install');
          }
          else {
            console.log('User added to home screen');
          }

          // We no longer need the prompt.  Clear it up.
          deferredPrompt = null;
        });
      }
    });

    window.addEventListener('appinstalled', (evt) => {
      app.logEvent('a2hs', 'installed');
      console.log("dfadf    ");
    });
    
</script>


</html>