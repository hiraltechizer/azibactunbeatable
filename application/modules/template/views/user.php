<!DOCTYPE html>
<html lang="en">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="<?= $this->security->get_csrf_hash(); ?>">
  <title><?= (!empty($settings['app_name'])) ? $settings['app_name'] : strtoupper($module) ?></title>

  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/iconfonts/font-awesome/css/all.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/css/vendor.bundle.addons.css">
  <!-- plugins:js -->
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/baguetteBox.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/fluid-gallery.css">


  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
  <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>


  <?php if (isset($plugins) && in_array('chosen', $plugins)) : ?>
    <!-- SELECT2 plugins CSS loaded -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/chosen/chosen.css" />
  <?php endif; ?>
  <?php if (isset($plugins) && in_array('fancybox', $plugins)) : ?>
    <!-- FANCYBOX plugins CSS loaded -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/fancybox/dist/jquery.fancybox.css" /><?php endif; ?>

  <link type="text/css" rel="stylesheet" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap-material-datetimepicker.css">
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png" />
  <!-- endinject -->
  <script type="text/javascript">
    var baseUrl = "<?php echo base_url() ?>";
  </script>
</head>

<body class="sidebar-dark">
  <div class="container-scroller">
    <?php echo $this->load->view('components/loader'); ?>

    <!-- Overlay For Sidebars -->

    <!-- partial:partials/_navbar.html -->
    <!-- Top Bar -->
    <?php echo $this->load->view('components/user/top-bar'); ?>
    <!-- #Top Bar -->
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <!-- Side Bar -->
      <?php echo $this->load->view('components/user/sidebar'); ?>
      <!-- #Side Bar -->
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper user-content-wrapper">

          <?php
          if (!isset($module)) {
            $module = $this->uri->segment(1);
          }

          if (!isset($viewFile)) {
            $viewFile = $this->uri->segment(2);
          }

          if ($viewFile == 'template/pages/shared-lists') {
            echo $this->load->view($viewFile);
          }
          /* elseif( $module != '' && $viewFile != '' ){
                $path = $module. '/' . $viewFile;
                echo $this->load->view($path);
            } */ else {
            $path = $viewFile;
            echo $this->load->view($path);
          }

          ?>
 <div class="col-xs-12 col-sm-12 text-center pad-t-100">
            <img src="<?php echo base_url(); ?>web/images/logo.png" alt="">
          </div>

        </div>

        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">
              &copy;
              <?= (date('Y') - 1) ?> - <?= date('Y') ?>
              <?php echo $this->session->get_field_from_session('role_label') ?> Panel - <?= config_item('title') ?>
            </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
              <b>Version: </b> 1.0
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->


  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>assets/vendors/js/vendor.bundle.addons.js"></script>
  <script src="<?php echo base_url() ?>assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert2.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/misc.js"></script>
  <script src="<?php echo base_url() ?>assets/js/settings.js"></script>
  <script src="<?php echo base_url() ?>assets/js/todolist.js"></script>
  <script src="<?php echo base_url() ?>assets/js/bootstrap-material-datetimepicker.js"></script>
  <script type="text/javascript">
    var controller = "<?php echo $controller ?>";
  </script>
  <?php if (isset($plugins) && in_array('select2', $plugins)) : ?>
    <script src="<?php echo base_url() ?>assets/js/select2.js"></script>
  <?php endif; ?>



  <?php if (isset($plugins) && in_array('chosen', $plugins)) : ?>
    <script src="<?php echo base_url() ?>assets/plugins/chosen/chosen.jquery.js"></script>
  <?php endif; ?>
  <?php if (isset($plugins) && in_array('fancybox', $plugins)) : ?>
    <!-- FANCYBOX plugins JS loaded and initialized -->
    <script src="<?php echo base_url(); ?>assets/js/fancybox/dist/jquery.fancybox.js"></script>
    <script type="text/javascript">
      $(".fancybox").fancybox();

      $('.fancybox').fancybox({
        buttons: [
          'download',
          'thumbs',
          'close'
        ]
      });

      $(".fancybox.video").fancybox({
        width: 600,
        height: 400,
        type: 'iframe'
      });
    </script>
  <?php endif; ?>
  <?php if (isset($js) && sizeof($js)) : foreach ($js as $javascript) : ?>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/resources/<?php echo $javascript ?>?ver=<?php echo $timestamp ?>"></script>
  <?php endforeach;
  endif; ?>

  <?php if (isset($plugins) && in_array('paginate', $plugins)) : ?>
    <script src="<?php echo base_url() ?>assets/resources/paginate.js"></script>
  <?php endif; ?>

  <?php if (isset($plugins) && in_array('baguetteBox', $plugins)) : ?>
    <script src="<?php echo base_url() ?>assets/js/baguetteBox.min.js"></script>
    <script>
      baguetteBox.run('.tz-gallery');
    </script>
  <?php endif; ?>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!--<script src="<?php echo base_url() ?>assets/js/dashboard.js"></script> -->
  <!-- End custom js for this page-->
  <script type="text/javascript">
    $(document).ready(function() {
      $("#sidebar>ul.nav>li.nav-item").removeClass("active");
      $("#sidebar>ul.nav>li.nav-item>div.collapse").removeClass("show");
      var result1 = controller.split('/');
      // alert(result1[1]);
      $("#sidebar>ul.nav>li." + result1[1]).addClass("active");
      $("#sidebar>ul.nav>li.nav-item>div." + result1[1]).addClass("show");

    });
  </script>

</html>