<div class="modal fade in" id="addRevisit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body padding-0">
                <?php $this->load->view($revisit_view); ?>
            </div>
        </div>
    </div>
</div>