<div class="modal fade in" id="editModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body padding-0">
                <?php $this->load->view($edit_view); ?>
            </div>
        </div>
    </div>
</div>