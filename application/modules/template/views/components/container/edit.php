<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><?= strtoupper(str_replace('_', ' ',$section_title)) ?></h4>
          <?php if(sizeof($info)) : ?>
				<?php $this->load->view("$controller/edit"); ?>
			<?php else: ?>
				<h4>No Record Found !!!</h4>
			<?php endif; ?>
        </div>
      </div>
    </div>
</div>
