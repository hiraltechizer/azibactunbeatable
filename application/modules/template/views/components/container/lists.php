<div class="page-header border-bottom-gray">
    <?php if ($controller == 'prescription') { ?>
        <h3 class="sale-performance-ttl"><?= (str_word_count($module_title) === 1) ? 'Upload ' . ucfirst($module_title) : ucwords(str_replace('_', ' ', $module_title)) ?>
        </h3>
    <?php } else {  ?>
        
        <h3 class="sale-performance-ttl"><?= $module_title == "quarter_target" ?(str_word_count($module_title) === 1) ? 'Manage ' . ucfirst($module_title) : ucwords(str_replace('_target', ' Slab', $module_title)) : (str_word_count($module_title) === 1) ? 'Manage ' . ucfirst($module_title) : ucwords(str_replace('_', ' ', $module_title)) ?>
        
        <?php if ($controller == 'sales_performance') { 
         if($role == 'ASM'){?>
              <p>
                Minimum 50% team members should qualify for ABM to get the reward (Provided Area should be on 100% Achievement with growth)
             </p> 
            <?php }elseif($role == 'RSM'){ ?>
                <p>
                 Minimum 50% BOs should qualify for RBM to get the reward (Provided Region should be on 100% Achievement with growth)
                </p> 
            <?php } }?>
        </h3>
        <br/>
        
    <?php } ?>

    <?php if (isset($permissions) && count($permissions)) : ?>
        <?php echo $this->load->view('template/components/action-btns'); ?>
    <?php endif; ?>
    <div>
    
    </div>
</div>

<div class="row">
    <?php echo $this->load->view('template/components/table-listing') ?>
</div>
<?php if ($controller == 'sales_performance') { 
         if($role == 'MR'){?>
              <!-- <div class="text-center" style="color:#0088ba;font-weight:bold">You will be eligible for the concerned slab on achievement of your Q2 target with growth.</div>  -->
            <?php } }?>
<?php echo $this->load->view('template/popup/csv-popup-box') ?>
<?php echo Modules::run('template/_get_post_modal'); ?>
<script type="text/javascript">
    var listing_url = "<?php echo $listing_url ?>";
    var download_url = "<?php echo $download_url ?>";
</script>