<style>
.filter{
    padding: 20px 20px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 26px;
    position: absolute;
    top: 8px;
    right: 1px;
    width: 20px;
}
</style>
<?php if($controller == 'sms') { ?>
    <?php echo $this->load->view('sms/sms_balance') ?>  
<?php } ?>
<?php if(isset($date_filters) && $date_filters): ?>

<div class="col-12">
   <div class="card">
       <div class="row">
          <div class="col-md-6 col-lg-6">
            <div class="card-body">
              <h4 class="card-title">From Date</h4>
                <div class="input-group date">
                  <input type="text" class="form-control filters" name="from_date" id="from_date" autocomplete="off">
                  <span class="input-group-addon input-group-append border-left">
                    <span class="far fa-calendar input-group-text"></span>
                  </span>
                </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-6">
            <div class="card-body">
              <h4 class="card-title">To Date</h4>
             <div class="input-group date">
                  <input type="text" class="form-control filters" name="to_date" id="to_date" autocomplete="off" <?php if(!empty($date_value)) {?> value="<?php echo $date_value; ?>" <?php } ?> >
                  <span class="input-group-addon input-group-append border-left">
                    <span class="far fa-calendar input-group-text"></span>
                  </span>
                </div>
          </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<?php $url_report =  $this->uri->segment(1);
   if($url_report == 'reports_no'){ ?>
<div class="col-12">
   <div class="card">
     
         <div class="row ">
            <div class="col-md-3">
               <div class="card-body">
                  <?php $zone_list = $this->model->get_records([],'zone'); ?>
                  <label for="">Zone Name</label><small class="req"></small>
                  <select class="report_filter form-control filters" name="zone_id" id="zone_id" data-id="report_filter_count" data-type="zone_id" >
                     <option value="">Zone</option>
                     <?php foreach ($zone_list as $zone) {  ?>
                     <option value="<?php echo $zone->zone_id ?>"<?php if (set_value('zone_id') == $zone->zone_id) { echo "selected=selected"; } ?>>
                        <?php echo $zone->zone_name ?>
                     </option>
                     <?php } ?>
                  </select>
               </div>
            </div>
            <div class="col-lg-3 col-md-3">
               <div class="card-body">
                  <label for="">Region Name</label><small class="req"></small>
                  <select class="report_filter form-control filters" id="region_id" name="region_id" data-id="report_filter_count" data-type="region_id" >
                     <option value="" > Region Name </option>
                  </select>
               </div>
            </div>

            <div class="col-lg-3 col-md-3">
               <div class="card-body">
                  <label for="">Area Name</label><small class="req"></small>
                  <select class="report_filter form-control filters" id="area_id" name="area_id" data-id="report_filter_count" data-type="area_id" >
                     <option value="" > Area Name </option>
                  </select>
               </div>
            </div>

            <div class="col-lg-3 col-md-3">
               <div class="card-body">
                  <label for="">City Name</label><small class="req"></small>
                  <select class="report_filter form-control filters" id="city_id" name="city_id" data-id="report_filter_count" data-type="city_id" >
                     <option value="" > city Name </option>
                  </select>
               </div>
            </div>
            </div>
   
   </div>
</div>

            <?php } ?>
<div class="col-lg-12 grid-margin stretch-card mt-4">
    <div class="card">
      <?php echo form_open("$controller/remove",array('id'=>'frm_delete', 'name'=>'frm_delete')); ?>
        <div class="card-body">
          <div class="">
            <table class="table table-hover">
              <thead>
                <tr>
                  <?php if(! isset($all_action) || $all_action): ?>
                  <!-- <th>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input form-check-{color}" id="checkall">      
                      </label>
                    </div>
                  </th> -->
                  <?php endif ?>
                  <?php foreach ($columns as $headers) { ?>
                  <th><?= $headers ?></th>
                  <?php } ?>
                   <?php if(! isset($all_action) || $all_action): ?>

                    <?php if(in_array('edit', $permissions)) : ?>
                      <th> Action</th>
                    <?php endif; ?>
            
                <?php endif; ?>
                </tr>
                <?php if(isset($show_filters) ? $show_filters : FALSE): ?>
                <tr>
                    <?php if(! isset($all_action) || $all_action): ?>
                    <td></td>
                    <?php endif; ?>

                    <?php foreach ($filter_columns as $filters): ?>
                    <td>
                        <?php if( isset($filters['show']) ? $filters['show'] : TRUE) : ?>
                            <?php if(!count($filters)) { 
                                echo "</td>";
                             continue; } ?>
                                <input
                                    type="<?= (!isset($filters['type'])) ? 'text' : $filters['type'] ?>" 
                                    name="<?= $filters['field_name'] ?>" 
                                    id="<?= $filters['field_name'] ?>" 
                                    placeholder="<?= $filters['field_label'] ?>" 
                                    class="form-control filters" 
                                    autocomplete="off" 
                                />
                            <?php endif; ?>
                    </td>
                    <?php endforeach; ?>
                    <?php if(! isset($all_action) || $all_action): ?>
                    <td>&nbsp;</td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
              </thead>
              <tbody id="tbody">
                <?php echo $this->load->view($records_view); ?>                       
              </tbody>
            </table>

          </div>
           <?php if(isset($permissions) && count($permissions)) : if(in_array('remove', $permissions)) {  ?>
           <a class="btn btn-danger btn-icon-text deleteAction" href="#" data-type="ajax-loader"><i class="fa fa-trash btn-icon-prepend"></i> <span>Delete</span></a>
          <?php } endif; ?>
        </div>
      <?php echo form_close(); ?>
  </div>
</div>
<script>
$(document).ready(function() {
    $(".js-example-basic-multiple").each((i,e) => {
      $(e).select2({
        placeholder: 'Select ' + $(e).attr('placeholder')
      });
    });

});
</script>