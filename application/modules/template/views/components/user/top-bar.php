<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row default-layout-navbar navbar-warning user-sidebar ">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <img src="<?php echo base_url(); ?>assets/images/login/ipca_logo.png" alt="" class="nav-logo">
    
        <?php if($role == 'HO' || $role == 'DT')  { ?>
        <?php $url = base_url('dashboard/user'); ?>
        <?php } else { ?>
        <?php $url = 'javascript:void(0)'; ?>    
        <?php } ?>    

        <a class="navbar-brand brand-logo" href="<?= $url ?>" style="color: #FFFFFF;">
            <h6><? = (!empty($settings['app_name'])) ? $settings['app_name'] : config_item('title') . ''; ?>< /h6>
         
        </a>
        <a class="navbar-brand brand-logo-mini" href="<?= $url ?>" style="color: #FFFFFF;">
              <h6><? = (!empty($settings['app_name'])) ? $settings['app_name'] : config_item('title') . ''; ?></h6>
        </a>
    </div>
    <div class="navbar-menu-wrapper user-navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <!-- <span class="fas fa-bars"></span> -->
          <img src="<?php echo base_url(); ?>web/images/bars.svg" alt="" class="">
        </button>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                
              <img src="<?php echo $imagename; ?>" alt="profile"/>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item" href="<?php echo base_url() ?>dashboard/user/logout">
                <i class="fas fa-power-off blue-color"></i>
                Logout
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <!-- <span class="fas fa-bars"></span> -->
          <img src="<?php echo base_url(); ?>web/images/bars.svg" alt="" class="">
        </button>
    </div>
</nav>