<nav class="sidebar sidebar-offcanvas user-sidebar" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link" style="background: #353535;">
                <div class="profile-image">
                    <img src="<?php echo $imagename; ?>" alt="image"/>
                </div>
                <div class="profile-name">
                    <p class="name">
                        Welcome <?php echo ucwords($this->session->get_field_from_session('user_name', 'user')); ?>
                    </p>
                    <p class="designation">
                       <?php echo $this->session->get_field_from_session('role_label', 'user') ?>
                    </p>
                </div>
            </div>
        </li>

        <li class="nav-item <?php echo ($mainmenu == 'dashboard') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url('dashboard/user') ?>">
                <i class="fa fa-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <?php if($role == 'MR' || $role == 'ASM' || $role == 'RSM' || $role == 'ZSM') { ?>

             <li class="nav-item <?php echo ($mainmenu == 'sales_performance') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("sales_performance/lists?t=$timestamp") ?>">
                <i class="fa fa-comment menu-icon"></i>
                <span class="menu-title">Sales Performance</span>
            </a>
            </li>
<?php } ?>

 <?php if($role == 'MR') { ?>
 <li class="nav-item <?php echo ($mainmenu == 'prescription') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("prescription/lists?t=$timestamp") ?>">
                <i class="fa fa-user-md menu-icon"></i>
                <span class="menu-title">Upload Prescription</span>
            </a>
            </li>  
            <?php } ?>

        <?php if($role == 'HO' || $role == 'MR'  || $role == 'ASM' || $role == 'RSM' || $role == 'ZSM') { ?>
            
           <li class="nav-item <?php echo ($mainmenu == 'ho_connect') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("ho_connect/lists?t=$timestamp") ?>">
                <i class="fa fa-user-md menu-icon"></i>
                <span class="menu-title">HO Connect</span>
            </a>
        </li>  
        <?php } ?>
        
        <?php if($role == 'MR') { ?>
            
           <!-- <li class="nav-item <?php echo ($mainmenu == 'winwheel') ? 'active': ''; ?>">
              <a class="nav-link" href="<?php echo base_url("winwheel/add") ?>">
                  <i class="fa fa-user-md menu-icon"></i>
                  <span class="menu-title">Spin O Win</span>
              </a>
          </li>  -->
          <?php } ?>
        <?php if($role == 'HO' || $role == 'MR' || $role == 'ASM' || $role == 'RSM' || $role == 'ZSM') { ?>    
         <li class="nav-item <?php echo (in_array($menu, ['live_report','rxn_update','login_report','ranking_report'])) ? $menu: ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="false" aria-controls="reports">
              <i class="fa fa-users menu-icon"></i>
              <span class="menu-title">Reports</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php echo (in_array($menu, ['live_report','rxn_update','login_report','ranking_report'])) ? $menu: ''; ?>" id="reports">
              <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/live_report") ?>">Live Report</a></li>

              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/rxn_update") ?>">RXN Update Report</a></li>
              <?php if($role == 'HO') : ?>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/ranking_report") ?>">Ranking Report</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/login_report") ?>">Login Report</a></li>

              <?php endif; ?>
              </ul>
              </div>
        </li>  
        <?php } ?> 
       
    </ul>
</nav>