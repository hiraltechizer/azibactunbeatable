<style type="text/css">
  .breadcrumb.breadcrumb-custom li.active_li  {
      background: #F5A623 !important;
  }
  .breadcrumb.breadcrumb-custom li.active_li a {
      border: 0px !important;
  }

  .breadcrumb.breadcrumb-custom li.active_li a:before,.breadcrumb.breadcrumb-custom li.active_li a:after {
    border: 0px !important;
  }
</style>
<div class="row">
  <div class="col-md-12 col-lg-12">
     <div class="card">
    <div class="template-demo">
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb breadcrumb-custom">
                  <li class="breadcrumb-item <?php if($tab == 'basic_details') { ?> active_li <?php } ?>">
                      <a href='<?php echo ($tab != 'basic_details') ? base_url("patient/edit/record/$patient_id?c=$timestamp") : "#basic_details" ?>' >Basic Details</a>
                  </li>
                  <?php if(isset($is_new_patient) && ! $is_new_patient) : ?>
                  <li class="breadcrumb-item <?php if($tab == 'therapy_details') { ?> active_li <?php } ?>">
                      <a href='<?php echo ($tab != 'therapy_details') ? base_url("therapy/lists/patient/$patient_id?c=$timestamp") : "#therapy_details" ?>'>Therapy Details</a>
                  </li>
                  <li class="breadcrumb-item <?php if($tab == 'purchase_details') { ?> active_li <?php } ?>">
                      <a href='<?php echo ($tab != 'purchase_details') ? base_url("purchase/lists/patient/$patient_id?c=$timestamp") : "#purchase_details" ?>'>Purchase Details</a>
                  </li>
                   <li class="breadcrumb-item <?php if($tab == 'revisit_call_details') { ?> active_li <?php } ?>">
                      <a href='<?php echo ($tab != 'revisit_call_details') ? base_url("revisitcall/lists/patient/$patient_id?c=$timestamp") : "#revisit_details" ?>'> Revisit Call Details</a>
                  </li> 

                  <li class="breadcrumb-item <?php if($tab == 'repurchasecall_call_details') { ?> active_li <?php } ?>">
                      <a href='<?php echo ($tab != 'repurchasecall_call_details') ? base_url("repurchasecall/lists/patient/$patient_id?c=$timestamp") : "#repurchasecall_details" ?>'> Repurchasecall Call Details</a>
                  </li> 
                  <?php endif; ?>
                 <!-- <li class="breadcrumb-item">
                      <a href='<?php echo ($tab != 'cycle_details') ? base_url("cycle/lists/patient/$patient_id?c=$timestamp") : "#cycle_details" ?>'>Cycle Details</a>
                  </li> -->
                  <li class="breadcrumb-item active" aria-current="page">&nbsp;
                  </li>
              </ol>
          </nav>
        </div>
    </div>
  </div>
</div>

<?php if($tab !== "basic_details"): ?>
<?php $patient_info = Modules::run('patient/_get_info', (int) $patient_id); ?>
    <?php if(count($patient_info)): ?>
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-star">
                                    <li>Patient Name: <?php echo ($patient_info[0]->type != 'caretaker' ) ? $patient_info[0]->name_1 : ( empty($patient_info[0]->name_2) ? 'NA' : $patient_info[0]->name_2 )   ?></li>
                                    <li>Patient Mobile: <?php echo ($patient_info[0]->type != 'caretaker' ) ? $patient_info[0]->mobile_1 : ( empty($patient_info[0]->mobile_2) ? 'NA' : $patient_info[0]->mobile_2 )   ?></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-star">
                                    <li>Caretaker Name: <?php echo ($patient_info[0]->type == 'caretaker' ) ? $patient_info[0]->name_1 : ( empty($patient_info[0]->name_2) ? 'NA' : $patient_info[0]->name_2 )   ?></li>
                                    <li>Caretaker Mobile: <?php echo ($patient_info[0]->type == 'caretaker' ) ? $patient_info[0]->mobile_1 : ( empty($patient_info[0]->mobile_2) ? 'NA' : $patient_info[0]->mobile_2 ) ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php endif; ?>
<?php endif; ?>