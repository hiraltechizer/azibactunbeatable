<nav class="sidebar sidebar-offcanvas admin-sidebar" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    <img src="<?php echo base_url() ?>web/images/profile.png" alt="image"/>
                </div>
                <div class="profile-name">
                    <p class="name">
                        Welcome <?php echo ucwords($this->session->get_field_from_session('user_name')); ?>
                    </p>
                    <p class="designation">
                       <?php echo $this->session->get_field_from_session('role_label') ?>
                    </p>
                </div>
            </div>
        </li>
        <li class="nav-item <?php echo ($mainmenu == 'dashboard') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url('dashboard/admin') ?>">
                <i class="fa fa-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
         
        <li class="nav-item <?php echo (in_array($menu, ['brand', 'division','language','state','cities','strength','document_type','knowledge_world','speciality','quarter'])) ? $menu : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#master" aria-expanded="false" aria-controls="master">
              <i class="fa fa-server menu-icon"></i>
              <span class="menu-title">Master</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php echo (in_array($menu, ['brand', 'division','language','state','cities','strength','document_type','speciality','quarter','knowledge_world'])) ? $menu : ''; ?>" id="master">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/division/lists?t=$timestamp") ?>">Division</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/language/lists?t=$timestamp") ?>">Language</a></li> -->
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/state/lists?t=$timestamp") ?>">State</a></li> -->
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/cities/lists?t=$timestamp") ?>">City</a></li> -->
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/strength/lists?t=$timestamp") ?>">Strength</a></li> -->
                 <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/brand/lists?t=$timestamp") ?>">Brand</a></li> -->
               <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/document_type/lists?t=$timestamp") ?>">Document Type</a></li> -->
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/speciality/lists?t=$timestamp") ?>">Speciality</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/quarter/lists?t=$timestamp") ?>">Quarter</a></li> -->
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("master/knowledge_world/lists?t=$timestamp") ?>">Knowledge Word</a></li> -->
              </ul>
            </div>
        </li>
        <li class="nav-item <?php echo (in_array($menu, ['zone', 'region', 'area', 'city','main_city'])) ? $menu : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#Geography" aria-expanded="false" aria-controls="Geography">
              <i class="fa fa-globe menu-icon"></i>
              <span class="menu-title">Geography</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php echo (in_array($menu, ['zone', 'region', 'area', 'city','main_city'])) ? $menu: ''; ?>" id="Geography">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("geography/zone/lists?t=$timestamp") ?>">Zone</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("geography/region/lists?t=$timestamp") ?>">Region</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("geography/area/lists?t=$timestamp") ?>">Area</a></li>
                <!-- <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("geography/main_city/lists?t=$timestamp") ?>">Main City</a></li> -->
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("geography/city/lists?t=$timestamp") ?>"> City</a></li>
               
                
               
              </ul>
              </div>
          </li>
        <li class="nav-item <?php echo (in_array($menu, ['ho', 'zsm', 'rsm', 'asm', 'mr'])) ? $menu: ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#manpower" aria-expanded="false" aria-controls="manpower">
              <i class="fa fa-users menu-icon"></i>
              <span class="menu-title">Manpower</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php echo (in_array($menu, ['ho', 'zsm', 'rsm', 'asm', 'mr'])) ? $menu: ''; ?>" id="manpower">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("manpower/ho/lists?t=$timestamp") ?>">HO</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("manpower/zsm/lists?t=$timestamp") ?>">ZSM</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("manpower/rsm/lists?t=$timestamp") ?>">RSM</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("manpower/asm/lists?t=$timestamp") ?>">ASM</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("manpower/mr/lists?t=$timestamp") ?>">MR</a></li>
              </ul>
              </div>
        </li>
         <li class="nav-item <?php echo ($mainmenu == 'doctor') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("doctor/lists?t=$timestamp") ?>">
                <i class="fas fa-user-md menu-icon"></i>
                <span class="menu-title">Doctor</span>
            </a>
        </li>
        <li class="nav-item <?php echo ($mainmenu == 'quarter_target') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("quarter_target/lists?t=$timestamp") ?>">
                <i class="fas fa-wheelchair menu-icon"></i>
                <span class="menu-title">Quarter Wise Target</span>
            </a>
        </li>
        
        <li class="nav-item <?php echo ($mainmenu == 'quarter_wise_mr_wise_target') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("quarter_wise_mr_wise_target/lists?t=$timestamp") ?>">
                <i class="fas fa-wheelchair menu-icon"></i>
                <span class="menu-title">Quarter Wise Mr Wise Target</span>
            </a>
        </li> 
        
          <li class="nav-item <?php echo ($mainmenu == 'sale_archive') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("sale_archive/lists?t=$timestamp") ?>">
                <i class="fa fa-comment menu-icon"></i>
                <span class="menu-title">Sale Archive</span>
            </a>
        </li> 
        

        <li class="nav-item <?php echo ($mainmenu == 'ho_connect') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("ho_connect/lists?t=$timestamp") ?>">
                <i class="fa fa-user-md menu-icon"></i>
                <span class="menu-title">HO Connect</span>
            </a>
        </li>
<li class="nav-item <?php echo (in_array($menu, ['live_report','rxn_update','login_report','ranking_report'])) ? $menu: ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="false" aria-controls="reports">
              <i class="fa fa-users menu-icon"></i>
              <span class="menu-title">Reports</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php echo (in_array($menu, ['live_report','rxn_update','login_report','ranking_report'])) ? $menu: ''; ?>" id="reports">
              <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/live_report") ?>">Live Report</a></li>

              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/rxn_update") ?>">RXN Update Report</a></li>

              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/login_report") ?>">Login Report</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url("reports/ranking_report") ?>">Ranking Report</a></li>
              </ul>
              </div>
        </li>
        <li class="nav-item <?php echo ($mainmenu == 'blockuser') ? 'active': ''; ?>">
            <a class="nav-link" href="<?php echo base_url("blockuser/lists?t=$timestamp") ?>">
                <i class="fa fa-lock menu-icon"></i>
                <span class="menu-title">Block Users</span>
            </a>
        </li>
    </ul>
</nav>
