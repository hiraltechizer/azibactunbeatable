<div class="btn-group btn-pos" role="group" aria-label="Basic example">
    <?php if(in_array('add', $permissions)) : ?>
        <?php $url_report =  $this->uri->segment(1);
             if($url_report == 'ho_connect'){ 
                if($role == 'HO'){ ?>
                    <a class="btn btn-warning" style="color: #FFFFFF;"  href="<?php echo base_url("$controller/add") ?>" title="Add">
                    <i class="fa fa-plus"></i>
                </a>
            <?php }  }else{ ?>
                <a class="btn btn-warning" style="color: #FFFFFF;"  href="<?php echo base_url("$controller/add") ?>" title="Add">
                <i class="fa fa-plus"></i>
            </a>
           <?php  } ?>

    <?php endif; ?>
    <?php if(in_array('upload', $permissions)) : ?>
        <a class="btn btn-warning reset-upload"  style="color: #FFFFFF;"  href="#" id="import" data-toggle="modal" data-target="#uploadbox" title="Upload CSV">
            <i class="fa fa-upload"></i>
        </a>
    <?php endif; ?>
    <?php if(in_array('download', $permissions)) : ?>
         <a class="btn btn-warning"  style="color: #FFFFFF;"  href="<?php echo base_url("$controller/download") ?>" title="Download" id="export">
            <i class="fa fa-download"></i>
        </a>
    <?php endif; ?>
</div>