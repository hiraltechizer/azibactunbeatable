<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_ho_connect extends MY_Model {

    private $p_key = 'ho_conn_id';
    private $table = 'ho_connect';
    private $tb_alias = 'conn';
    private $fillable = ['title', 'description','upload_path'];
    private $column_list = []; //'File Type','Title', 'Description','Display Image','Status','DateTime'
    private $csv_columns =[];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
            // [
            //     'field_name'=>'dt|document_type',
            //     'field_label'=> 'Document TYpe',
            // ],
            // [
            //     'field_name'=>'conn|title',
            //     'field_label'=> 'Title',
            // ],
            // [
            //     'field_name'=>'conn|description',
            //     'field_label'=> 'Description',
            // ]
    

        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $user_id =(int) $this->security->xss_clean($this->session->get_field_from_session('user_id', 'user'));

        $role = $this->security->xss_clean($this->session->get_field_from_session('role', 'user'));

        $q = $this->db->select('conn.ho_conn_id,conn.title,conn.description,conn.display_image,conn.upload_path,conn.status,conn.insert_dt,dt.document_type_id,dt.document_type,
        (
            SELECT 
            GROUP_CONCAT(DISTINCT CONCAT(cfield.field_permission))
            FROM ho_connect_fields cfield
            WHERE cfield.ho_conn_id = conn.ho_conn_id
             ) as field_permission')
        ->from('ho_connect conn')
        ->join('document_type dt','dt.document_type_id = conn.document_type_id')
        ->join('ho_connect_fields cfield', 'cfield.ho_conn_id = conn.ho_conn_id');
        // ->join('city c', 'm.users_city_id = c.city_id')
        // ->join('area a', 'a.area_id = c.area_id')
        // ->join('region r', 'r.region_id = a.region_id')
        // ->join('zone z', 'z.zone_id = r.zone_id')
        // ->join('speciality s', 's.speciality_id = d.speciality_id')
        ;

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }
        if($role == 'MR' || $role == 'ASM' || $role == 'RSM' || $role == 'ZSM' ){
            $q->where('conn.status','send');
            $q->where('cfield.field_permission',$role);
        }
        
        $q->group_by('cfield.ho_conn_id');
        if(! $count) {
            $q->order_by('conn.ho_conn_id DESC');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
    //   echo '<pre>';
    //     print_r($this->db->last_query());die;
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
                [
					'field' => 'document_type_id',
					'label' => 'Document Type',
					'rules' => 'trim|required|xss_clean'
                ],
                [
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|max_length[150]|xss_clean'
                ]
                ,
                [
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|xss_clean'
                ],
                [
					'field' => 'status',
					'label' => 'status',
					'rules' => 'trim|required|xss_clean'
                ],

            ];
        }

        if($type == 'modify') {
            return [
                
                [
					'field' => 'document_type_id',
					'label' => 'Document Type',
					'rules' => 'trim|required|xss_clean'
                ]
                ,
                [
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|max_length[150]|xss_clean'
                ],
                [
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|xss_clean'
                ],
                [
					'field' => 'status',
					'label' => 'status',
					'rules' => 'trim|required|xss_clean'
                ],
            ];
        }
    }

    function save(){
        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->load->helper('upload_media');
        $this->form_validation->set_rules($this->validate('save'));

        if(!$this->form_validation->run()) {
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');
            
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }

        $document_type_id = $this->input->post('document_type_id');
        $fields_perm = $this->input->post('permission');

        if(empty($_FILES['upload_path']['size'])) {
			$upload_errors['status'] = FALSE;
			$upload_errors['errors'] = ['upload_path' => '<label class="error">Files Required.</label>'];
			return $upload_errors;
		}

        if(!isset($_POST['permission']) || empty($_POST['permission'])) {
            $fild_errors['status'] = FALSE;
            $fild_errors['errors'] = ['field_permission' => '<label class="error">Please select Send Notification Fields .</label>'];
            return $fild_errors;
        }
        #get document type
		$documentTypeInfo = $this->get_records(['document_type_id' => $document_type_id], 'document_type', ['document_type']);

		$document_type = $documentTypeInfo[0]->document_type;

		$path = $_FILES['upload_path']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

        $file_icon = '';
		$duration = '0';
		if(strtolower($document_type) == 'audio') {
			if(!in_array($ext,['wav','aif','mp3','mid','ogg','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload audio files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/audio.png";
		} else if(strtolower($document_type) == 'video') {
			if(!in_array($ext,['mp4','mov','wmv','avi','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload video files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/video.png";
		} else if(strtolower($document_type) == 'ppt') {
			if(!in_array($ext,['ppt','pptx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload ppt files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'pdf') {
			if(!in_array($ext,['pdf'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/pdf_file.png";
		}else if(strtolower($document_type) == 'spredsheet' || strtolower($document_type) == 'excel') {
			if(!in_array($ext,['xls','xlsx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'document' || strtolower($document_type) == 'doc') {
			if(!in_array($ext,['doc','docx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload doc files Only.</label>'];
				return $upload_errors;
			}
            $file_icon	=	"assets/images/doc_file.png";
		} else {
			if(!in_array($ext,['png', 'jpeg', 'jpg','PNG','JPEG','JPG'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload image files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/images_file.png";
		}


        $upload_filename = $this->UploadFile($document_type_id);

        $inserted_ids = [];
		if(!empty($upload_filename)) {
			$upload_result = [];
			$upload_result['document_type_id']	=	$document_type_id;
			$upload_result['title']	=	$this->input->post('title');
			$upload_result['description']	=	$this->input->post('description');
			$upload_result['upload_path'] = $upload_filename;
			$upload_result['display_image'] = $file_icon;
            $upload_result['status'] = $this->input->post('status');

			$id = $this->_insert($upload_result);

            #insert with persmission
            for($ic=0;$ic<count($_POST['permission']);$ic++){ 
                
                $fpermission = $_POST['permission'][$ic];

                $ho_connect_fields['ho_conn_id'] =  $id;
                $ho_connect_fields['field_permission'] =  $fpermission;
                $this->_insert($ho_connect_fields, 'ho_connect_fields');

            }
		}

        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
    }
    
    function modify(){
        // print_r($_FILES);exit;
        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->load->helper('upload_media');

        $is_Available = $this->check_for_posted_record($this->p_key, $this->table);
        if(! $is_Available['status']){ return $is_Available; }
        
        $this->form_validation->set_rules($this->validate('modify'));

        if(! $this->form_validation->run()){
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');

            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }   

        $document_type_id = $this->input->post('document_type_id');
        $fields_perm = $this->input->post('permission');

        if(!isset($_POST['permission']) || empty($_POST['permission'])) {
            $fild_errors['status'] = FALSE;
            $fild_errors['errors'] = ['field_permission' => '<label class="error">Please select Send Notification Fields .</label>'];
            return $fild_errors;
        }
        $p_key = $this->p_key;
        $id  = (int) $this->input->post($p_key);
        if(isset($_FILES['upload_path']['size']) && !empty($_FILES['upload_path']['size'])) {
        #get document type
		$documentTypeInfo = $this->get_records(['document_type_id' => $document_type_id], 'document_type', ['document_type']);

		$document_type = $documentTypeInfo[0]->document_type;

        $path = $_FILES['upload_path']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

        $file_icon = '';
		$duration = '0';
		if(strtolower($document_type) == 'audio') {
			if(!in_array($ext,['wav','aif','mp3','mid','ogg','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload audio files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/audio.png";
		} else if(strtolower($document_type) == 'video') {
			if(!in_array($ext,['mp4','mov','wmv','avi','vlc'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload video files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/video.png";
		} else if(strtolower($document_type) == 'ppt') {
			if(!in_array($ext,['ppt','pptx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload ppt files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'pdf') {
			if(!in_array($ext,['pdf'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/pdf_file.png";
		}else if(strtolower($document_type) == 'spredsheet' || strtolower($document_type) == 'excel') {
			if(!in_array($ext,['xls','xlsx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload pdf files Only.</label>'];
				return $upload_errors;
			}
		} else if(strtolower($document_type) == 'document' || strtolower($document_type) == 'doc') {
			if(!in_array($ext,['doc','docx'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload doc files Only.</label>'];
				return $upload_errors;
			}
		} else {
			if(!in_array($ext,['png', 'jpeg', 'jpg','PNG','JPEG','JPG'])) {
				$upload_errors['status'] = FALSE;
				$upload_errors['errors'] = ['upload_path' => '<label class="error">Please Upload image files Only.</label>'];
				return $upload_errors;
			}
			$file_icon	=	"assets/images/images_file.png";
		}

        $upload_filename = $this->UploadFile($document_type_id);
       
        $inserted_ids = [];
		if(!empty($upload_filename)) {
			
			$upload_result['document_type_id']	=	$document_type_id;
			$upload_result['upload_path'] = $upload_filename;
			$upload_result['display_image'] = $file_icon;

			$id = $this->_update([$p_key => $id],$upload_result,'ho_connect');

          }
        }
            
        $update_result['title']	=	$this->input->post('title');
		$update_result['description']	=	$this->input->post('description');
        $update_result['status'] = $this->input->post('status');

         
         $status = (int) $this->_update([$p_key => $id], $update_result);

         

         $this->_delete('ho_conn_id',['ho_conn_id' => $id], 'ho_connect_fields');

         for($ic=0;$ic<count($_POST['permission']);$ic++){ 
              
           

            $fpermission = $_POST['permission'][$ic];

            $ho_connect_fields['ho_conn_id'] =  $id;
            $ho_connect_fields['field_permission'] =  $fpermission;
            $this->_insert($ho_connect_fields, 'ho_connect_fields');

        }
        if(! $status){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
    }

    function remove(){
        
        if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
            $ids = $this->input->post('ids');
            $response = $this->_delete($this->p_key, $ids, $this->table);

            $msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
            return ['msg'=> $msg];
        }

        return ['msg'=> 'No Records Selected'];
    }

    
    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
             $records['MR Name'] = $rows['users_name'];
             $records['Doctor Name'] = $rows['doctor_name'];
             $records['Doctor Mobile'] = $rows['doctor_mobile'];
             $records['Speciality'] = $rows['speciality'];
             $records['HQ'] = $rows['city_name'];
             $records['Area'] = $rows['area_name'];
             $records['Region'] = $rows['region_name'];
             $records['Zone'] = $rows['zone_name'];
            $records['DateTime'] = $rows['insert_dt'];
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }

    function UploadFile($document_type_id) {
		$upload_data = $errors = array();
        $uploadPath = "uploads/ho_connect/$document_type_id/files";
        $s3uploadPath = '';

        if(!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, TRUE);
        }

       $file_status = $this->upload_file_response("upload_path", $uploadPath, '', ['png', 'jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi','PNG','JPEG','JPG']);
        if(isset($file_status['status'])){
            $file_status = is_array($file_status['data']) ? $file_status['data'] : (array)$file_status['data'];
            for($i=0;$i < count($file_status); ++$i){
                array_push($upload_data, $file_status[$i]) ;
            }
        } else {
            array_push($errors, $file_status['error']);
        }

        if(!empty($errors)){
            $assoc_arr = array_reduce($errors, function ($result, $item) {
                $result[$item['id']] = $item['message'];
                return $result;
            }, array());
    
            if(count($assoc_arr)){
                $result_errors['status'] = FALSE;
                $result_errors['errors'] = $assoc_arr;
                return $result_errors;
            }
        }

        $filename = '';
        if(count($upload_data)) {
            foreach ($upload_data as $key => $value) {
               $filename = $value;
            }
            
        } 
        return $filename;
	}

    function upload_file_response($file_name, $uploadPath, $s3uploadPath, $allowed_types = ['png','jpeg', 'jpg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx','wav','aif','mp3','mid','ogg','vlc','mp4','mov','wmv','avi','PNG','JPG','JPEG']){

		$file_response = upload_media($file_name, $uploadPath, $allowed_types, 100000000000, 0, $s3uploadPath);
//   print_r($file_response);exit;
		$upload_file_count = !empty($_FILES[$file_name]['name']) ? 1:0;

		if($upload_file_count > 1){
			$temp = array();
			$temp['status'] = TRUE;
			for($i=0; $i < $upload_file_count; ++$i){
				if(!empty($file_response[$i]['errors'])){
					$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
					$temp['status'] = FALSE;
				} else {
					#echo 'asfsaf';die;
					$temp['data'][] = $file_response[$i]['file_name'];
				}
			}
			return $temp;
		}else{

			if(!empty($file_response['errors'])){
				$temp['error'] = array('id' => $file_name ,'message'=> '<label class="error">'.$file_response['errors'].'</label>');
				$temp['status'] = FALSE;

				return $temp;
			}

			if($file_response) {

				$file_name = [];
				foreach ($file_response as $key => $value) {
					if(!empty($value['file_name'])){
						$temp['status'] = TRUE;
						array_push($file_name, $value['file_name']);
						
					}
				}
				$temp['data'] = $file_name;
				return $temp;
			}

			
		}
	}
}