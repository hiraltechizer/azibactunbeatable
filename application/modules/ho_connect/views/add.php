<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>

<div class="form-group row">
    <label for="title" class="col-12  ">Title <span class="text-danger">*</span></label>
    <div class="col-12">
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" maxlength="150">
    </div>
</div>
<div class="form-group row">
    <label for="description" class="col-12">Description </label>
    <div class="col-12">
        <textarea name="description" id="description" class="form-control" rows="8"></textarea>
    </div>
</div>

<div class="form-group row">
    <label for="document_type_id" class="col-12">File Type <span class="text-danger">*</span></label>
    <div class="col-12">
        <select class="form-control form-control-sm js-example-basic-single" id="document_type_id" name="document_type_id" data-placeholder="Select File Type">
            <option value="">Select File Type</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="upload_path" class="col-12">Content File <span class="text-danger">*</span></label>
    <div class="col-12" id="input_filedv">
        <input type="file" name="upload_path" class="dropify" id="upload_path" data-max-file-size="120M" data-allowed-file-extensions="png jpeg jpg pdf"/>
        <p>Support format: png, jpeg, jpg, pdf</p>
    </div>
</div>

<div class="form-group row">
<label for="upload_path" class="col-12">Send Notification Fields <span class="text-danger">*</span></label>
    <div class="col-12" id="input_filedv" style="padding-left: 30px;">

    <!-- <input type="checkbox" id="checkItem" name="permission[]" value="BO">BO<br>
	<input type="checkbox" id="checkItem" name="permission[]" value="ABM">ABM<br>
	<input type="checkbox" id="checkItem" name="permission[]" value="RBM">RBM<br> -->
 
    <input class="form-check-input" type="checkbox"  id="fields1" name="permission[]" value="MR">
    <label class="form-check-label" for="">
        BO
    </label>
  <div class="form-group">
  <input class="form-check-input" type="checkbox"  id="fields1" name="permission[]" value="ASM">
  <label class="form-check-label" for=""  >
    ABM
  </label>

<div class="form-group">
  <input class="form-check-input" type="checkbox" id="fields1" name="permission[]"  value="RSM">
  <label class="form-check-label" for="">
    RBM
  </label>
  <div class="form-group">
  <input class="form-check-input" type="checkbox" id="fields1" name="permission[]"  value="ZSM">
  <label class="form-check-label" for="">
  ZBM
  </label>
</div>

</div>
<input type="hidden" name="field_permission" value=""/>
</div>


</div>
</div>
<div class="form-group row">
    <label for="document_type_id" class="col-12">Send Notification Status <span class="text-danger">*</span></label>
    <div class="col-12">
        <select class="form-control form-control-sm js-example-basic-single" id="status" name="status" data-placeholder="Select Status">
            <option value="">Select Status</option>
            <option value="send">Send</option>
            <option value="drafts">Drafts</option>
        </select>
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>