<?php echo form_open("$controller/modify", array('class' => 'save-form')); ?>
<input type="hidden" name="ho_conn_id" value="<?php echo $info[0]['ho_conn_id']; ?>" />


<div class="form-group row">
    <label for="title" class="col-12 col-form-label">Title <span class="text-danger">*</span></label>
    <div class="col-12 ">
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" maxlength="150" value="<?php echo $info[0]['title']; ?>">
    </div>
</div>
<div class="form-group row">
    <label for="description" class="col-12 col-form-label">Description </label>
    <div class="col-12">
        <textarea name="description" id="description" class="form-control" rows="8"><?php echo $info[0]['description']; ?></textarea>
    </div>
</div>

<div class="form-group row">
    <label for="document_type_id" class="col-12 col-form-label">File Type <span class="text-danger">*</span></label>
    <div class="col-12">
        <select class="form-control form-control-sm js-example-basic-single" id="document_type_id" name="document_type_id" data-placeholder="Select File Type">
            <option value="<?php echo $info[0]['document_type_id']; ?>"><?php echo $info[0]['document_type']; ?>
            </option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="upload_path" class="col-12 col-form-label">Content File <span class="text-danger">*</span></label>
    <div class="col-12" id="input_filedv">
        <input type="file" name="upload_path" class="dropify" id="upload_path" data-max-file-size="20M" data-allowed-file-extensions="png jpeg jpg pdf wav aif mp3 mid ogg vlc mp4 mov wmv avi" />
        <div class="support-format-txt">
        (Support format: png, jpeg, jpg, pdf,wav,aif,mp3,mid,ogg,vlc,mp4,mov,wmv,avi)
        </div>
    </div>
</div>
<div class="form-group row">
    <label for="" class="col-12 col-form-label">Uploaded Content File <span class="text-danger">*</span></label>
    <?php
    $fileUrl = '';
    if (file_exists('./' . $info[0]['display_image'])) {
        $fileUrl = base_url() . $info[0]['display_image'];
    }
    $OfileUrl = '';
    if (file_exists('./' . $info[0]['upload_path'])) {
        $OfileUrl = base_url() . $info[0]['upload_path'];
    }
    if (!empty($fileUrl)) { ?>
        <a target="_blank" href="<?php echo $OfileUrl; ?>" style="" data-fancybox="images" data-fancybox>
            <img src="<?php echo $fileUrl; ?>" alt="" style="width:50px;height: 50px;border-radius: 0%;">
        </a>
    <?php } ?>
</div>

<div class="form-group row">
    <label for="" class="col-12 col-form-label">Send Notification Fields <span class="text-danger">*</span></label>
    <?php $field_permission = explode(",", $info[0]['field_permission']); ?>
    <div class="col-12 pl-4" id="input_filedv">
        <div class="">
            <div class="form-group">
                <input class="form-check-input" type="checkbox" id="fields1" name="permission[]" value="MR" <?php if (in_array("MR", $field_permission)) {
                                                                                                                echo "checked";
                                                                                                            } ?>>
                <label class="form-check-label" for="">
                    BO
                </label>
            </div>
            <div class="form-group">
                <input class="form-check-input" type="checkbox" id="fields1" name="permission[]" value="ASM" <?php if (in_array("ASM", $field_permission)) {
                                                                                                                    echo "checked";
                                                                                                                } ?>>
                <label class="form-check-label" for="">
                    ABM
                </label>
            </div>
            <div class="form-group">
                <input class="form-check-input" type="checkbox" id="fields1" name="permission[]" value="RSM" <?php if (in_array("RSM", $field_permission)) {
                                                                                                                    echo "checked";
                                                                                                                } ?>>
                <label class="form-check-label" for="">
                    RBM
                </label>
            </div>
            <div class="form-group">
                <input class="form-check-input" type="checkbox" id="fields1" name="permission[]" value="ZSM" <?php if (in_array("ZSM", $field_permission)) {
                                                                                                                    echo "checked";
                                                                                                                } ?>>
                <label class="form-check-label" for="">
                    ZBM
                </label>
            </div>

            <input type="hidden" name="field_permission" value="" />

        </div>

    </div>
</div>
<div class="form-group row">
    <label for="document_type_id" class="col-12 col-form-label">Send Notification Status <span class="text-danger">*</span></label>
    <div class="col-12">
        <select class="form-control form-control-sm js-example-basic-single" id="status" name="status" data-placeholder="Select Status">
            <option value="">Select Status</option>
            <option value="send" <?php if ($info[0]['status'] == 'send') {
                                        echo "selected";
                                    } else {
                                        echo "";
                                    } ?>>Send</option>
            <option value="drafts" <?php if ($info[0]['status'] == 'drafts') {
                                        echo "selected";
                                    } else {
                                        echo "";
                                    } ?>>Drafts</option>
        </select>
    </div>
</div>



<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
    Cancel
</a>
<?php echo form_close(); ?>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<link href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" rel="stylesheet">

<script src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
    $('[data-fancybox="images"]').fancybox({
        buttons: [
            'download',
            'thumbs',
            'close'
        ]
    });
</script>