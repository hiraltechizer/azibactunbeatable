
<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="ho_conn_id" value="<?php echo $info[0]['ho_conn_id']; ?>" />


<div class="form-group row">
    <label for="title" class="col-12">Title </label>
    <div class="col-12">
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" maxlength="150" value="<?php echo $info[0]['title']; ?>" readonly>
    </div>
</div>
<div class="form-group row">
    <label for="description" class="col-12">Description </label>
    <div class="col-12">
        <textarea name="description" id="description" class="form-control" rows="8" readonly ><?php echo $info[0]['description']; ?></textarea>
    </div>
</div>




    <div class="form-group row">
    <label for="" class="col-12">Uploaded Content File</label>
    <?php 
        $fileUrl = '';
        if(file_exists('./'.$info[0]['display_image'])) {
            $fileUrl = base_url().$info[0]['display_image'];
        } 
        $OfileUrl = '';
        if(file_exists('./'.$info[0]['upload_path'])) {
            $OfileUrl = base_url().$info[0]['upload_path'];
        }         
       if(!empty($fileUrl)) { ?>
        <a target="_blank" href="<?php echo $OfileUrl; ?>" style="" data-fancybox="images" data-fancybox >
        <img src="<?php echo $fileUrl; ?>" alt="" style="width:50px;height: 50px;border-radius: 0%;">
        </a>
   <?php } ?>
</div>
     





<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<link href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" rel="stylesheet">

<script src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
      
        $('[data-fancybox="images"]').fancybox({
  buttons : [
    'download',
    'thumbs',
    'close'
  ]
});
    </script>