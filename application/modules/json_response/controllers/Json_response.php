<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Json_response extends Admin_Controller
{
	private $module = 'json_response';
    private $model_name = 'mdl_json_response';
    private $controller = 'json_response';
    private $settings = [
        'permissions'=> ['download'],
        'pagination_index' => 3
    ];
    private $scripts = [];

   
	function __construct() {
         parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }
}
