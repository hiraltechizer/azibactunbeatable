<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['id']; ?>
<tr>
    <td>
        <label for="check_<?= $id ?>"></label>
    </td>
    <td><?php echo $record['request_uri'] ?></td>   
    <td><?php echo $record['raw_parameters'] ?></td>
    <td><?php echo $record['insert_datetime'] ?></td>
</tr>
<?php $i++;  } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>">
        <div class="row">
            <?php echo $this->ajax_pagination->create_links(); ?>    
        </div>
    </td>
</tr>