<?php echo form_open("$controller/save",array('class'=>'save-form')); ?>
<div class="form-group row">
    <label for="users_id" class="col-sm-3 col-form-label">MR Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="users_id" name="users_id" data-placeholder="Select MR">
            <option value="">Select MR</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="doctor_name" class="col-sm-3 col-form-label">Doctor Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="doctor_name" name="doctor_name" placeholder="Doctor Name" maxlength="50">
    </div>
</div>
<div class="form-group row">
    <label for="doctor_mobile" class="col-sm-3 col-form-label">Doctor mobile <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="doctor_mobile" name="doctor_mobile" placeholder="Doctor Name" maxlength="50">
    </div>
</div>
<div class="form-group row">
    <label for="doctor_sc_code" class="col-sm-3 col-form-label">Doctor SC Code <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="doctor_sc_code" name="doctor_sc_code" placeholder="Doctor SC Code" maxlength="50">
    </div>
</div>
<div class="form-group row">
    <label for="doctor_qualification" class="col-sm-3 col-form-label">Doctor Qualification <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="doctor_qualification" name="doctor_qualification" placeholder="Doctor Qualification" maxlength="150">
    </div>
</div>
<div class="form-group row">
    <label for="language_id" class="col-sm-3 col-form-label">Doctor Language <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-multiple" id="language_id" name="language_id" data-placeholder="Select Language" multiple="multiple">
            <option value="">Select Language</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="speciality_id" class="col-sm-3 col-form-label">Doctor Speciality <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="speciality_id" name="speciality_id" data-placeholder="Select Speciality">
            <option value="">Select Speciality</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="clinic_address" class="col-sm-3 col-form-label">Clinic Address <span class="text-danger">*</span></label>
    <div class="col-sm-9">
    	<textarea id="clinic_address" name="clinic_address" rows="9" cols="89"></textarea>
    </div>
</div>
<div class="form-group row">
	<label for="doctor_image" class="col-sm-3 col-form-label">Doctor Image <span class="text-danger">*</span></label>
	<div class="col-sm-9">
		<input type="file" name="doctor_image[]" class="dropify" data-min-width="1000" data-min-height="1000"/>
	</div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>