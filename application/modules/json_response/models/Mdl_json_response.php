<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_json_response extends MY_Model {

    private $p_key = 'id';
    private $table = 'usertracking';
    private $tb_alias = 'u';
    private $fillable = [];
    private $column_list = ['URL', 'Data','DateTime'];
    private $csv_columns =[];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $q = $this->db->select('u.request_uri, u.raw_parameters, u.insert_datetime,u.id')
        ->from('usertracking u')
        ->where("u.raw_parameters != ","")
        ;

        $where_condition = "u.request_uri LIKE '%api%'";
        
        $q->where($where_condition);

        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        

        if(! $count) {
            $q->order_by('u.id DESC');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
     // echo '<pre>';
    //print_r($this->db->last_query());die;
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
               
            ];
        }

        if($type == 'modify') {
            return [
                
            ];
        }
    } 

    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
            $records['URL']         = $rows['request_uri'];
            $records['Data']    = $rows['raw_parameters'];
            $records['DateTIme']    = $rows['insert_datetime'];
            

             
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }
}