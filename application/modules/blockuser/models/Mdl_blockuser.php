<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_blockuser extends MY_Model {

	private $p_key = 'users_id';
	private $table = 'manpower';
	private $alias = 'm';
	private $fillable = [];
    private $column_list = ['Users Name', 'Users Mobile', 'Users Type', 'Users Attempt'];
    private $csv_columns = ['Users Name', 'Users Mobile', 'Users Type', 'Users Attempt'];

	function __construct() {
        parent::__construct($this->table, $this->p_key,$this->alias);
    }
    
    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_filters() {
        return [
            
            [
                'field_name'=>'users_name',
                'field_label'=> 'Name',
            ],
            [
                'field_name'=>'users_mobile',
                'field_label'=> 'Mobile',
            ]
        ];
    }

    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

     function get_collection( $count = FALSE, $sfilters = [], $rfilters = [], $limit = 0, $offset = 0, ...$params )  {
		$field_filters = $this->get_filters_from($rfilters);
    	$q = $this->db->select('*')
		->from('manpower');
	
		if(sizeof($sfilters)) { 
			
			foreach ($sfilters as $key=>$value) { 
				$q->where("$key", $value); 
			}
		}

		if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }
				
				$key = str_replace('|', '.', $key);
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE(insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE(insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        $q->where('is_deleted','0');
        // $q->where('users_type','HO');
        $q->where('lock_flag','1');
		if(! $count) {
			$q->order_by('update_dt desc');
		}
		if(!empty($limit)) { $q->limit($limit, $offset); }        
		$collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
		// echo $this->db->last_query();die;
		return $collection;
				
	}
	
}