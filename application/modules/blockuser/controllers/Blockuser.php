<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Blockuser extends Admin_Controller
{
	private $module = 'blockuser';
    private $model_name = 'mdl_blockuser';
    private $controller = 'blockuser';
    private $settings = [
        'permissions'=> ['unblock'],
        'paginate_index' => 3
    ];
   
	function __construct() {
         $this->settings['filters'] = ['show_filters'=> TRUE, 'date_filters'=> FALSE, 'camp_filters' => FALSE];
        
        parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings
           # $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }


    function unblock(){
        if( ! $this->session->is_admin_logged_in() )
            redirect('admin/login','refresh');
       
        if(!in_array('unblock', $this->settings['permissions'])) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

        $uri_string = $this->uri->uri_string();

        if(! strpos($uri_string, 'record')) {
            show_404();
        }

        $revised_url = strstr($uri_string, 'record');

        list($k, $v) = explode('/', $revised_url);
        $array[$k] = $v;

        if(!empty($array['record'])) {

            #assign users id
            $id = $array['record'];

            #unblock users
            $update_data['lock_flag'] = '0';
            $update_data['attempt'] = '0';

            $retInfo = $this->model->_update(['users_id' => $id], $update_data, 'manpower');

            if($retInfo) {
                redirect($this->controller. '/' .'lists');
            } else {
                redirect($this->controller. '/' .'lists');
            }
        } else {
            redirect($this->controller. '/' .'lists');
        }

    }
}
