<?php $i = 1; if(sizeof($collection)) : foreach ($collection as $record) { $id = $record['users_id']; if($id){ ?>
<tr>
    <?php if(! isset($all_action) || $all_action): ?>
        <td>
            <input type="checkbox" name="ids[]" value="<?php echo $id ?>" id="check_<?= $id ?>" class="chk-col-<?= $settings['theme'] ?> filled-in" />
            <label for="check_<?= $id ?>"></label>
        </td>
    <?php endif; ?>
    <td><?php echo $record['users_name'] ?></td>
    <td><?php echo $record['users_mobile'] ?></td>
    <td><?php echo $record['users_type'] ?></td>
    <td><?php echo $record['attempt'] ?></td>
    
    <?php if(in_array('unblock', $permissions)): ?>
        <td><p><a href="<?php echo base_url("$controller/unblock/record/$id?c=$timestamp") ?>" class="tooltips" title="Unblock Users" ><i class="fa fa-unlock menu-icon"></i></a></p></td>
    <?php endif; ?>
</tr>
<?php $i++;  } } ?>

<?php else: ?>
    <tr><td colspan="<?= (count($columns) + 2) ?>"><center><i>No Record Found</i></center></td><tr>
<?php endif; ?>
<tr>
    <td colspan="<?= (count($columns) + 2) ?>"><?php echo $this->ajax_pagination->create_links(); ?></td>
</tr>