<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Serve extends Generic_Controller
{
	private $file;
    private $mime;

    function __construct($file){
        parent::__construct();
        $this->file = $file;
        $finfo = new finfo();
        $this->mime = $finfo->file($this->file, FILEINFO_MIME_TYPE);
    }


    
    function serveImage() {
        
        header("Content-Type: $this->mime");
        header('Content-Length: ' . filesize($this->file));
        readfile($this->file);
        
        /* 
         *  IF NOT Working in IE then Uncomment 
         *  1. getresource function
         *  2. serveResource function
         *  3. Also the below 2 lines
        */

        // $resource = $this->getResource();
        // $this->serveResource($resource); 

    }

    function servePdf() {
        header("Content-type:application/pdf");
        readfile($this->file);
    }

    function serveFile() {
        switch ($this->mime):
            case 'image/png':
            case 'image/jpeg': 
                $this->serveImage();
                break;
            case 'application/pdf':
                $this->servePdf();
            
        endswitch;
    }
	
}


$ci = &get_instance();

if($ci->session->is_logged_in()){

    $file = 'media/image/' . $_GET['file']; 
    $serve = new Serve($file);
    $serve->serveFile(); 

}else if(isset($_COOKIE['id'])){
    if($_COOKIE['id'] != ""){
    $file = 'media/image/' . $_GET['file']; 
    $serve = new Serve($file);
    $serve->serveFile(); 
    }
}
else{
    http_response_code(403);
    die('Forbidden');
}

