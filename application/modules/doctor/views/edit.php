<?php echo form_open("$controller/modify",array('class'=>'save-form')); ?>
<input type="hidden" name="doctor_id" value="<?php echo $info[0]['doctor_id']; ?>" />




<div class="form-group row">
    <label for="users_id" class="col-sm-3 col-form-label">MR Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="users_id" name="users_id" data-placeholder="Select MR">
            
            <option value="<?php echo $info[0]['users_id']; ?>" selected="selected"><?php echo $info[0]['users_name']; ?></option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="doctor_name" class="col-sm-3 col-form-label">Doctor Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="doctor_name" name="doctor_name" placeholder="Doctor Name" maxlength="50" value="<?php echo $info[0]['doctor_name']; ?>" >
    </div>
</div>
<div class="form-group row">
    <label for="sbu_code" class="col-sm-3 col-form-label">Doctor sbu code <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="sbu_code" name="sbu_code" placeholder="" maxlength="10" value="<?php echo $info[0]['sbu_code']; ?>" >
    </div>
</div>

<div class="form-group row">
    <label for="speciality_id" class="col-sm-3 col-form-label">Doctor Speciality <span class="text-danger">*</span></label>
    <div class="col-sm-9">
        <select class="form-control form-control-sm js-example-basic-single" id="speciality_id" name="speciality_id" data-placeholder="Select Speciality">
        <option value="<?php echo $info[0]['speciality_id']; ?>" selected="selected"><?php echo $info[0]['speciality']; ?></option>
        </select>
    </div>
</div>
<button type="submit" class="btn btn-primary mr-2">Save</button>
<a href="<?php echo base_url("$controller/lists?c=$timestamp") ?>" class="btn btn-light">
   Cancel
</a>
<?php echo form_close(); ?>