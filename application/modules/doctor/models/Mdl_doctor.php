<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_doctor extends MY_Model {

    private $p_key = 'doctor_id';
    private $table = 'doctor';
    private $tb_alias = 'd';
    private $fillable = ['users_id', 'doctor_name', 'doctor_mobile', 'speciality_id'];
    private $column_list = ['MR Name', 'HQ','Area','Region','Zone','Doctor Name', 'Doctor SBU Code', 'Speciality','DateTime'];
    private $csv_columns =['Doctor Name','Doctor Mobile','Speciality','MR Mobile No'];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
             [
                'field_name'=>'m|users_name',
                'field_label'=> 'MR Name',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'City Name',
            ],
            [
                'field_name'=>'a|area_name',
                'field_label'=> 'Area Name',
            ],
            [
                'field_name'=>'r|region_name',
                'field_label'=> 'Region Name',
            ],
            [
                'field_name'=>'z|zone_name',
                'field_label'=> 'Zone Name',
            ],
            [
                'field_name'=>'d|doctor_name',
                'field_label'=> 'Doctor Name',
            ],
            [
                'field_name'=>'d|sbu_code',
                'field_label'=> 'Doctor Ccode',
            ]
            ,
           
            [
                'field_name'=>'s|speciality',
                'field_label'=> 'Speciality',
            ]

        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $user_id =(int) $this->security->xss_clean($this->session->get_field_from_session('user_id', 'user'));

        $role = $this->security->xss_clean($this->session->get_field_from_session('role', 'user'));

        $q = $this->db->select('d.doctor_id,d.sbu_code,d.doctor_name, d.doctor_mobile,m.users_id, m.users_name, m.users_mobile, m.users_type, s.speciality_id, s.speciality,d.insert_dt,d.clinic_address,c.city_name,a.area_name,r.region_name,z.zone_name')
        ->from('doctor d')
        ->join('manpower m', 'm.users_id = d.users_id')
        ->join('city c', 'm.users_city_id = c.city_id')
        ->join('area a', 'a.area_id = c.area_id')
        ->join('region r', 'r.region_id = a.region_id')
        ->join('zone z', 'z.zone_id = r.zone_id')
        ->join('speciality s', 's.speciality_id = d.speciality_id')
        ;

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE(d.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        

        if(! $count) {
            $q->order_by('d.doctor_id DESC');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
      // echo '<pre>';
        //print_r($this->db->last_query());die;
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
                [
                    'field' => 'users_id',
                    'label' => 'MR Name',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'doctor_name',
                    'label' => 'Doctor Name',
                    'rules' => 'trim|required|valid_name|max_length[150]|xss_clean'
                ],
                [
                    'field' => 'doctor_mobile',
                    'label' => 'Doctor Mobile',
                    'rules' => 'trim|numeric|valid_mobile|required|unique_key[doctor.doctor_mobile]|xss_clean'
                ],
                [
                    'field' => 'speciality_id',
                    'label' => 'Doctor speciality',
                    'rules' => 'trim|required|xss_clean'
                ]
            ];
        }

        if($type == 'modify') {
            return [
                
                [
                    'field' => 'users_id',
                    'label' => 'MR Name',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'doctor_name',
                    'label' => 'Doctor Name',
                    'rules' => 'trim|required|valid_name|max_length[150]|xss_clean'
                ],
                [
                    'field' => 'sbu_code',
                    'label' => 'Doctor sbu code',
                    'rules' => 'trim|numeric|required|unique_key[doctor.sbu_code.doctor_id.'. (int) $this->input->post('doctor_id') .']|xss_clean'
                ],
                [
                    'field' => 'speciality_id',
                    'label' => 'Doctor speciality',
                    'rules' => 'trim|required|xss_clean'
                ]
            ];
        }
    }

    function save(){

        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validate('save'));
        
        if(!$this->form_validation->run()) {
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');
            
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }
        
        $data = $this->process_data($this->fillable, $_POST);
        $id = $this->_insert($data);
        
        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
    }
    
    function modify(){
        /*Load the form validation Library*/
        $this->load->library('form_validation');

        $is_Available = $this->check_for_posted_record($this->p_key, $this->table);
        if(! $is_Available['status']){ return $is_Available; }
        
        $this->form_validation->set_rules($this->validate('modify'));

        if(! $this->form_validation->run()){
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');

            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }       
        
        $data = $this->process_data($this->fillable, $_POST);

        $p_key = $this->p_key;
        $id  = (int) $this->input->post($p_key);

        $status = (int) $this->_update([$p_key => $id], $data);

        if(! $status){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
    }

    function remove(){
        
        if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
            $ids = $this->input->post('ids');
            $response = $this->_delete($this->p_key, $ids, $this->table);

            $msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
            return ['msg'=> $msg];
        }

        return ['msg'=> 'No Records Selected'];
    }

    
    function _format_data_to_export($data){
        
        $resultant_array = [];
        
        foreach ($data as $rows) {
             $records['MR Name'] = $rows['users_name'];
             $records['Doctor Name'] = $rows['doctor_name'];
             $records['Doctor SBU Code'] = $rows['sbu_code'];
             $records['Speciality'] = $rows['speciality'];
             $records['HQ'] = $rows['city_name'];
             $records['Area'] = $rows['area_name'];
             $records['Region'] = $rows['region_name'];
             $records['Zone'] = $rows['zone_name'];
            $records['DateTime'] = $rows['insert_dt'];
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }
}