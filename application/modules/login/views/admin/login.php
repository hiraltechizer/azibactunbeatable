<?php echo form_open($module . '/login/submit',array('class'=>'form-login')); ?>

<div class="form-group mb-5">
    <?php if(isset($errr_msg) && !empty($errr_msg)) { ?>
    <div class="alert alert-fill-danger" role="alert">
        <i class="fa fa-exclamation-triangle"></i>
        <?php echo (!empty($errr_msg)) ? $errr_msg : '' ?>
    </div>
    <?php } else { ?>
    <?php if( !empty($error_msg) || !empty(validation_errors()) ) : ?>
    <div class="alert alert-fill-danger" role="alert">

        <?php echo validation_errors(); ?>
        <?php echo (!empty($error_msg)) ? $error_msg : '' ?>
    </div>
    <?php endif; ?>
    <?php } ?>


    <div class=" position-relative mb-3">
        <input type="text" class="form-control form-control-lg  login-custom-input" id="username" name="username"
            placeholder="Username" autofocus value="<?php echo set_value('username') ?>" autocomplete="off">
        <span class="input-icon">
            <img src="<?php echo base_url(); ?>web/images/user-icon.png" alt="">
        </span>
    </div>
</div>

<div class="form-group ">

    <div class="position-relative ">

        <input type="password" class="form-control form-control-lg login-custom-input" id="password" name="password"
            placeholder="Password" autocomplete="off">
        <span class="input-icon">
            <img src="<?php echo base_url(); ?>web/images/pass-icon.png" alt="">
        </span>
    </div>
</div>

<div class="col-xs-12 col-sm-6  col-md-4  offset-sm-3 offset-md-4 mt-5">
    <button class="btn btn-login btn-blue " type="submit">Login</button>
</div>

<div class="col-xs-12 col-sm-12 text-center pad-t-100">
<img src="<?php echo base_url(); ?>web/images/logo.png" alt="">
</div>
<?php form_close(); ?>