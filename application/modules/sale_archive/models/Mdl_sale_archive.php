<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_sale_archive extends MY_Model {

    private $p_key = 'sale_archive_id';
    private $table = 'sale_archive';
    private $tb_alias = 'sale';
    private $fillable = ['quarter_id','city_id','mr_id', 'sale','date'];
    private $column_list = ['Quarter','Territory Code', 'HQ', 'Mr Name','Sales Archive','Sale Date','DateTime'];
    private $csv_columns =['Quarter [Quarter 1,..]','Territory Code', 'HQ', 'Mr Name','Sales Archive','Sale Date (YYYY-MM-DD)'];
    private $testcsv_columns = [];
    

    function __construct() {
        parent::__construct($this->table, $this->p_key, $this->tb_alias);
    }

    function get_csv_columns() {
        return $this->csv_columns;
    }

    function get_column_list() {
        return $this->column_list;
    }

    function get_testcsv_columns() {
        return $this->testcsv_columns;
    }
    
    function get_filters() {
        return [
            [],
             [
                'field_name'=>'c|territory_code',
                'field_label'=> 'Territory Code',
            ],
            [
                'field_name'=>'c|city_name',
                'field_label'=> 'City Name',
            ],
            [
                'field_name'=>'sale|sale',
                'field_label'=> 'Sales Archive',
            ],
           

        ];
    }
    
    function get_filters_from($filters) {
        $new_filters = array_column($this->get_filters(), 'field_name');
        
        if(array_key_exists('from_date', $filters))  {
            array_push($new_filters, 'from_date');
        }

        if(array_key_exists('to_date', $filters))  {
            array_push($new_filters, 'to_date');
        }

        return $new_filters;
    }

    function get_collection($count = FALSE, $f_filters = [], $rfilters = [], $limit = 0, $offset = 0) {

        $field_filters = $this->get_filters_from($rfilters);

        $user_id =(int) $this->security->xss_clean($this->session->get_field_from_session('user_id', 'user'));

        $role = $this->security->xss_clean($this->session->get_field_from_session('role', 'user'));

        $q = $this->db->select('sale.*,c.city_name,c.territory_code,q.quarter_name,m.users_name')
        ->from('sale_archive sale')
        ->join('city c', 'c.city_id = sale.city_id')
        ->join('quarter q', 'q.quarter_id = sale.quarter_id')
        ->join('manpower m', 'm.users_id = sale.mr_id')
        // ->join('area a', 'a.area_id = c.area_id')
        // ->join('region r', 'r.region_id = a.region_id')
        // ->join('zone z', 'z.zone_id = r.zone_id')
        // ->join('speciality s', 's.speciality_id = d.speciality_id')
        ;

                
        if(sizeof($f_filters)) { 
            foreach ($f_filters as $key=>$value) { 
                
                if($key == 'from_date' || $key == 'to_date') {
                    if($key == 'from_date' && $value) {
                        $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    } 

                    if($key == 'to_date' && $value) {
                        $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    }    
                } else {
                    if(strpos($value, ",") !== FALSE) {
                       $q->where("$key IN (".$value.")"); 
                    
                    } else {
                        //echo 'ppp';die;
                        $q->where("$key", $value); 
                    }
                   // $q->where("$key", $value); 
                }
                //$q->where("$key", $value); 
            }
        }

        if(is_array($rfilters) && count($rfilters) ) {

            foreach($rfilters as $key=> $value) {
                if(!in_array($key, $field_filters)) {
                    continue;
                }

                $key = str_replace('|', '.', $key);
                
                if($key == 'from_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) >=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if($key == 'to_date' && $value) {
                    $this->db->where('DATE('.$this->tb_alias.'.insert_dt) <=', date('Y-m-d', strtotime($value)));
                    continue;
                }

                if(!empty($value))
                    $this->db->like($key, $value);
            }
        }

        

        if(! $count) {
            $q->order_by('sale.update_dt DESC');
        }


        if(!empty($limit)) { $q->limit($limit, $offset); }        
        $collection = (! $count) ? $q->get()->result_array() : $q->count_all_results();
        
    //   echo '<pre>';
    //     print_r($this->db->last_query());die;
        return $collection;
    }   
    
    function validate($type)
    {
        if($type == 'save') {
            return [
                [
                    'field' => 'quarter_id',
                    'label' => 'Quarter',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'city_id',
                    'label' => 'city',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'territory_code',
                    'label' => 'Territory Code',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'sale',
                    'label' => 'Sale Archive',
                    'rules' => 'trim|numeric|required|xss_clean'
                ],
                [
                    'field' => 'date',
                    'label' => 'Sales Date',
                    'rules' => 'trim|date|required|xss_clean'
                ],
            ];
        }

        if($type == 'modify') {
            return [
                [
                    'field' => 'quarter_id',
                    'label' => 'Quarter',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'city_id',
                    'label' => 'city',
                    'rules' => 'trim|required|xss_clean'
                ],
                [
                    'field' => 'sale',
                    'label' => 'Sale Archive',
                    'rules' => 'trim|numeric|required|xss_clean'
                ],
                [
                    'field' => 'date',
                    'label' => 'Sales Date',
                    'rules' => 'trim|date|required|xss_clean'
                ],
            ];
        }
    }

    function save(){
        $quarter1 = 3;
        $quarter2 = 3;
        $quarter3 = 3;
        /*Load the form validation Library*/
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validate('save'));
        
        if(!$this->form_validation->run()) {
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');
            
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }
        
          $data['quarter_id'] = $quarter_id = $this->input->post('quarter_id');
          $data['city_id'] = $city_id = $this->input->post('city_id');
          $data['mr_id'] = $mr_id = $this->input->post('mr_id');
          $data['sale'] = $sale = $this->input->post('sale');
          $data['date'] = $date = $this->input->post('date');
          $get_date_month = strtotime($date);
          $month = str_replace('0','',date("m",$get_date_month));
          $sale_count = $this->model->get_records(['Month(date)' => $month,'mr_id' => $mr_id],'sale_archive',['COUNT(sale) AS salecount']);
          
          
        //   echo $this->db->last_query();exit;
        //   echo "<pre>";print_r($sale_count);exit;

        //   $quarterWiseSale = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'sale_archive');
        
        //   if(count($quarterWiseSale)){

        //     $sale_archive_id = $quarterWiseSale[0]->sale_archive_id;
        //     $allsale = $quarterWiseSale[0]->sale;

        //     $saleUpdate['sale'] =  $sale;

        //     $id = $this->_update(['sale_archive_id' => $sale_archive_id], $saleUpdate);
        //    }else{
              $id = $this->_insert($data);
        //    }
// exit;

$month_count = $this->model->get_records(['quarter_id' => $quarter_id,'mr_id' => $mr_id],'sale_archive',['Month(date) AS salesMonth'],'',0,0,'salesMonth');
        //   echo $this->db->last_query();exit;

        //   echo "<pre>";print_r($month_count);exit;   
        
        if($id){
            $quarter_target_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id],'quarter_wise_target');

            $exp_pmt_data = $this->model->get_records(['mr_id' => $mr_id],'exp_pmpt',['exp_slab1','exp_slab2'],'insert_dt DESC',1);
        //   echo "<pre>";print_r($exp_pmt_data);exit;   
            
            if($exp_pmt_data){
                
                if(count($month_count) >1 ){
                    $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab',[], 'mr_wise_slab_id DESC');
        //   echo $this->db->last_query();exit;

                    //   echo "<pre>";print_r($sale_count);exit;
                    
                    $slab_1 = number_format($updated_slab_data[0]->up_slab1 - $sale, 2, '.', '');
                    $slab_2 = number_format($updated_slab_data[0]->up_slab2 - $sale, 2, '.', '');
                    if($sale_count[0]->salecount == 2){
                    // exit;
                    
        //             $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');
        //   echo
        //                 $this->db->last_query();
        //                 exit;

                    if(count($month_count) == 3){
                        // echo "tesst";exit;
                        // echo "<pre>";print_r($slab_1 .'____'. $updated_slab_data[0]->up_slab1);exit;
                        $slab_1 = number_format($slab_1 + $updated_slab_data[0]->up_slab1, 2, '.', '');
                        $slab_2 = number_format($slab_2 + $updated_slab_data[0]->up_slab2, 2, '.', '');

                    }else{
                            // echo "tesst222";
                            // exit;

                        $slab_1 = number_format(($slab_1 / ($quarter1 - count($month_count))) + $updated_slab_data[0]->up_slab1, 2, '.', '');
                        $slab_2 = number_format(($slab_2 / ($quarter1 - count($month_count))) + $updated_slab_data[0]->up_slab2, 2, '.', '');
                    }
                    
                    $exp_data['month_status'] = '1';
                    // echo "<pre>"; print_r($slab_1.'_____'. $slab_2);exit;
                    
                    $up_slab_data['quarter_id'] = $quarter_id;
                    $up_slab_data['city_id'] = $city_id;
                    $up_slab_data['mr_id'] = $mr_id;
                    $up_slab_data['sale_archive_id'] = $id;
                    $up_slab_data['up_slab1'] = $slab_1;
                    $up_slab_data['up_slab2'] = $slab_2;
                    // if($updated_slab_data){

                    //     $this->_update(['mr_wise_slab_id' => $updated_slab_data[0]->mr_wise_slab_id], $up_slab_data,'updated_mr_wise_slab');
                    // }else{

                         $this->_insert($up_slab_data,'updated_mr_wise_slab');
                    // }

              }
                    // echo "test22";
                    // exit;

            }else{
                
                $slab_1 = number_format($quarter_target_data[0]->slab1 - $sale, 2, '.', '');
                $slab_2 = number_format($quarter_target_data[0]->slab2 - $sale, 2, '.', '');
                if($sale_count[0]->salecount == 2){
                    
                    $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab1, 2,'.','');
                    $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab2, 2,'.','');
    
                    $exp_data['month_status'] = '1';
    
                    $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');
                        // $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab1, 2,'.','');
                        // $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab2, 2,'.','');
                        $exp_data['month_status'] = '1';
    
                        $up_slab_data['quarter_id'] = $quarter_id;
                        $up_slab_data['city_id'] = $city_id;
                        $up_slab_data['mr_id'] = $mr_id;
                        $up_slab_data['sale_archive_id'] = $id;
                        $up_slab_data['up_slab1'] = $slab_1;
                        $up_slab_data['up_slab2'] = $slab_2;
                        // if($updated_slab_data){
    
                        //     $this->_update(['mr_wise_slab_id' => $updated_slab_data[0]->mr_wise_slab_id], $up_slab_data,'updated_mr_wise_slab');
                        // }else{
    
                             $this->_insert($up_slab_data,'updated_mr_wise_slab');
                        // }
                }
            }
            
          }else{
            //   echo "test";exit;
            $slab_1 = number_format($quarter_target_data[0]->slab1 - $sale , 2, '.', '');
            $slab_2 = number_format($quarter_target_data[0]->slab2 - $sale , 2, '.', '');
            
          }
          $exp_data['quarter_id'] = $quarter_id ;
          $exp_data['city_id'] = $city_id ;
          $exp_data['mr_id'] = $mr_id ;
          $exp_data['sale_arch_id'] = $id;
          $exp_data['exp_slab1'] = $slab_1;
          $exp_data['exp_slab2'] = $slab_2;
          
          
          $exp_data['sale'] = $sale;
        //   echo $this->db->get_compiled_select();die;
          $this->_insert($exp_data,'exp_pmpt');
            
        }

        if(! $id){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! new record created.';

        return $response;
    }
    
    function modify(){
        $quarter1 = 3;
        $quarter2 = 3;
        $quarter3 = 3;
        /*Load the form validation Library*/
        $this->load->library('form_validation');

        $is_Available = $this->check_for_posted_record($this->p_key, $this->table);
        if(! $is_Available['status']){ return $is_Available; }
        
        $this->form_validation->set_rules($this->validate('modify'));
        $data['quarter_id'] = $quarter_id = $this->input->post('quarter_id');
        $data['city_id'] = $city_id = $this->input->post('city_id');
      //   $data['territory_code'] = $territory_code = $this->input->post('territory_code');
        $data['mr_id'] = $mr_id = $this->input->post('mr_id');
        $data['sale'] = $sale = $this->input->post('sale');
        $data['date'] = $date = $this->input->post('date');
        if(! $this->form_validation->run()){
            $errors = array();          
            foreach ($this->input->post() as $key => $value)
                $errors[$key] = form_error($key, '<label class="error">', '</label>');

            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            
            return $response;
        }       
        
        $data = $this->process_data($this->fillable, $_POST);
        $get_date_month = strtotime($date);
        $month = str_replace('0','',date("m",$get_date_month));
        $sale_count = $this->model->get_records(['Month(date)' => $month,'mr_id' => $mr_id],'sale_archive',['COUNT(sale) AS salecount']);
        // echo $this->db->last_query();exit;
        $p_key = $this->p_key;
        $id  = (int) $this->input->post($p_key);

        $status = (int) $this->_update([$p_key => $id], $data);

        $month_count = $this->model->get_records(['quarter_id' => $quarter_id,'mr_id' => $mr_id],'sale_archive',['Month(date) AS salesMonth'],'',0,0,'salesMonth');

        $quarter_target_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id],'quarter_wise_target');

          $exp_pmt_data = $this->model->get_records(['mr_id' => $mr_id ],'exp_pmpt',['exp_pmpt_id','exp_slab1','exp_slab2'],'insert_dt desc',2);

          $exp_id = $exp_pmt_data[0]->exp_pmpt_id;
        //   echo "<pre>";print_r($exp_pmt_data);exit;
            // echo "<pre>";print_r($exp_pmt_data[1]->exp_slab2 -($sale - $exp_pmt_data[1]->exp_slab2));exit;


        //   $slab_1 = number_format($exp_pmt_data[1]->exp_slab1 - ($sale - $exp_pmt_data[1]->exp_slab1), 2, '.', '');
        //   $slab_2 = number_format($exp_pmt_data[1]->exp_slab2 - ($sale - $exp_pmt_data[1]->exp_slab2), 2, '.', '');


        if($exp_pmt_data){
            
                
                //   echo "<pre>";print_r(count($exp_pmt_data));exit;
                if(count($month_count) >1 ){
                 
                    $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');
                    
                    $slab_1 = number_format($updated_slab_data[0]->up_slab1 - $sale, 2, '.', '');
                    $slab_2 = number_format($updated_slab_data[0]->up_slab2 - $sale, 2, '.', '');
                    if($sale_count[0]->salecount == 3){
                        $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');
                        // echo "<pre>";print_r(count($updated_slab_data)-2);exit;
                        $i = count($updated_slab_data)-2;
                        $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $updated_slab_data[$i]->up_slab1, 2,'.','');
                        $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $updated_slab_data[$i]->up_slab2, 2,'.','');
                        $exp_data['month_status'] = '1';
    
                        $up_slab_data['quarter_id'] = $quarter_id;
                        $up_slab_data['city_id'] = $city_id;
                        $up_slab_data['mr_id'] = $mr_id;
                        $up_slab_data['sale_archive_id'] = $id;
                        $up_slab_data['up_slab1'] = $slab_1;
                        $up_slab_data['up_slab2'] = $slab_2;
                        if($updated_slab_data){
    
                            $this->_update(['mr_wise_slab_id' => $updated_slab_data[0]->mr_wise_slab_id], $up_slab_data,'updated_mr_wise_slab');
                        }else{
    
                             $this->_insert($up_slab_data);
                        }
    
                  }
                }else{
                    // echo "<pre>";print_r($sale_count);exit;
                    $slab_1 = number_format($quarter_target_data[0]->slab1 - $sale, 2, '.', '');
                    $slab_2 = number_format($quarter_target_data[0]->slab2 - $sale, 2, '.', '');
                    if($sale_count[0]->salecount == 3){
                        
                        $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab1, 2,'.','');
                        $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab2, 2,'.','');
        
                        $exp_data['month_status'] = '1';
        
                        $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');
                            // $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab1, 2,'.','');
                            // $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab2, 2,'.','');
                            $exp_data['month_status'] = '1';
        
                            $up_slab_data['quarter_id'] = $quarter_id;
                            $up_slab_data['city_id'] = $city_id;
                            $up_slab_data['mr_id'] = $mr_id;
                            $up_slab_data['sale_archive_id'] = $id;
                            $up_slab_data['up_slab1'] = $slab_1;
                            $up_slab_data['up_slab2'] = $slab_2;
                            if($updated_slab_data){
        
                                $this->_update(['mr_wise_slab_id' => $updated_slab_data[0]->mr_wise_slab_id], $up_slab_data,'updated_mr_wise_slab');
                            }else{
        
                                 $this->_insert($up_slab_data,'updated_mr_wise_slab');
                            }
                    }
                }
                
              }else{
              
                $slab_1 = number_format($quarter_target_data[0]->slab1 - $sale , 2, '.', '');
                $slab_2 = number_format($quarter_target_data[0]->slab2 - $sale , 2, '.', '');
                
              }


          $exp_data['quarter_id'] = $quarter_id ;
          $exp_data['city_id'] = $city_id ;
          $exp_data['mr_id'] = $mr_id ;
          $exp_data['sale_arch_id'] = $id;
          $exp_data['exp_slab1'] = $slab_1;
          $exp_data['exp_slab2'] = $slab_2;
          $exp_data['sale'] = $sale;
          $this->_update(['exp_pmpt_id' => $exp_id], $exp_data,'exp_pmpt');
        
        
          if(! $status){
            $response['message'] = 'Internal Server Error';
            $response['status'] = FALSE;
            return $response;
        }

        $response['status'] = TRUE;
        $response['message'] = 'Congratulations! record was updated.';
        
        return $response;
    }

    function remove(){
        
        if(isset($_POST['ids']) && sizeof($_POST['ids']) > 0){
            $ids = $this->input->post('ids');
            $response = $this->_delete($this->p_key, $ids, $this->table);

            $msg = ($response) ? "Record(s) Successfully deleted" : 'Error while deleting record(s)';
            return ['msg'=> $msg];
        }

        return ['msg'=> 'No Records Selected'];
    }

    function check_update_slabdata($quarter_id, $city_id, $mr_id){
        $q = $this->db->select('updated_mr_wise_slab.*')
            ->from('updated_mr_wise_slab, (
SELECT mr_id, MAX(sale_archive_id) AS salearch_id
FROM updated_mr_wise_slab
GROUP BY mr_id) max_updated_mr_wise_slab')
            ->where('updated_mr_wise_slab.mr_id=max_updated_mr_wise_slab.mr_id AND updated_mr_wise_slab.sale_archive_id=max_updated_mr_wise_slab.salearch_id AND quarter_id ='. $quarter_id.' AND city_id ='. $city_id)
            ;
        
            // $q->order_by('qua.quw_target_id DESC');
            
        
        $collection =  $q->get()->result_array() ;
 return $collection;
    }

    
    function _format_data_to_export($data){
        // echo "<pre>";print_r($data);exit;
        $resultant_array = [];
        
        foreach ($data as $rows) {
             $records['Quarter'] = $rows['quarter_name'];
             $records['Territory Code'] = $rows['territory_code'];
             $records['HQ'] = $rows['city_name'];
             $records['Mr Name'] = $rows['users_name'];
             $records['Sales Archive'] = $rows['sale'];
             $records['Sales Date'] = $rows['date'];
        
            $records['DateTime'] = $rows['insert_dt'];
            array_push($resultant_array, $records);
        }
        return $resultant_array;
    }
}