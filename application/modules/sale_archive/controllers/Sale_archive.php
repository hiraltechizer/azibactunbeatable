<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sale_archive extends Admin_Controller
{
	private $module = 'sale_archive';
    private $model_name = 'mdl_sale_archive';
    private $controller = 'sale_archive';
    private $settings = [
        'permissions'=> ['add','upload','download','edit'],
        'pagination_index' => 3
    ];
    private $scripts = ['division.js'];

    
	function __construct() {
        parent::__construct( 
            $this->module,
            $this->controller,
            $this->model_name,
            $this->settings,
            $this->scripts
        );
        #$this->load->helper('send_sms');
        $this->set_defaults();
    }
    
    function uploadcsv(){
        $quarter1 = 3;
        $quarter2 = 3;
        $quarter3 = 3;
        $this->session->is_Ajax_and_admin_logged_in();
        /*upload csv file */
        
        if(! is_uploaded_file($_FILES['csvfile']['tmp_name'])){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Please Upload CSV file</label>']]); exit;
        }
        
        if(!in_array($_FILES['csvfile']['type'], array('application/vnd.ms-excel', 'application/csv', 'text/csv')) ){
            echo json_encode(['errors'=> ['csvfile'=> '<label class="error">Only .CSV files allowed</label>']]); exit;
        }
        
        $file = $_FILES['csvfile']['tmp_name'];
        $handle = fopen($file, "r");
        $cnt = 0; $newrows = 0;
        
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){
            
            if(count($data) !== 6) { continue; }
            
            if(! $cnt){
                $cnt++; continue;
            }
            $quarter = trim($data[0]);
            $city_code = trim($data[1]);
            $city = trim($data[2]);
            $mr_name = trim($data[3]);
            $sale = trim($data[4]);
            $date = trim($data[5]);
            
            // if(empty((int)$sale)){
                //     echo "<pre>";print_r($sale);exit;
                
                // }else{
                    //     echo "pre";exit;
                    
                    // }
            
                    if(empty($quarter) || empty($city_code) || empty($city) || empty($mr_name)  || empty($date) ){
                        continue;
                    }
                    
                    // echo "pre";exit;
                    
            
        if(  
            !preg_match('/^\d+$/', $city_code)
             || !preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $quarter)
             || !preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $city)
             || !preg_match('/^[a-zA-Z][a-zA-Z0-9 \.]+$/', $mr_name)
             || !preg_match('/^-?[0-9]\d*(\.\d+)?$/', $sale)  
        ){
            continue;
        }
        // echo "1";exit;

        $quarterInfo = $this->model->get_records(['quarter_name'=> $quarter], 'quarter', ['quarter_id'], '', 1);
        
        if(!count($quarterInfo)) {
                continue;
         }

         $quarter_id = $quarterInfo[0]->quarter_id;

        $cityInfo = $this->model->get_records(['city_name' => $city,'territory_code'=> $city_code], 'city', ['city_id'], '', 1);
        
        if(!count($cityInfo)) {
                continue;
        }
        $city_id = $cityInfo[0]->city_id;

        $mrInfo = $this->model->get_records(['users_name' => $mr_name,'users_type' => "MR",'users_city_id' =>$city_id],'manpower',['users_id'],'',1);
        if(!count($mrInfo)) {
            continue;
    }
    $mr_id = $mrInfo[0]->users_id;
    
            #check for 
            $quarterWiseSale = $this->model->get_records(['city_id'=> $city_id,'quarter_id' => $quarter_id,'mr_id' => $mr_id,'date'=>$date], 'sale_archive');

            if(count($quarterWiseSale)){
                continue;
            }

        // echo "<pre>";print_r($quarterWiseSale);die;
            // if(count($quarterWiseSale)) {

            //     $sale_archive_id = $quarterWiseSale[0]->sale_archive_id;
            //     $allsale = $quarterWiseSale[0]->sale;
            //     $saleUpdate['sale'] = $sale;

            //     $id = $this->model->_update(['sale_archive_id' => $sale_archive_id], $saleUpdate);

            // }else{
                $insert['quarter_id'] = $quarter_id;
                $insert['city_id'] = $city_id;
                $insert['mr_id'] = $mr_id;
                $insert['sale'] = $sale;    
                $insert['date'] = $date;
                
                $get_date_month = strtotime($date);
                $month = str_replace('0','',date("m",$get_date_month));
                $sale_count = $this->model->get_records(['Month(date)' => $month,'mr_id' => $mr_id],'sale_archive',['COUNT(sale) AS salecount']);
                
                $id = $this->model->_insert($insert);

                // $month_count = $this->model->get_records(['quarter_id' => $quarter_id,'mr_id' => $mr_id],'sale_archive',['Month(date) AS salesMonth'],'',0,0,'');

                $month_count = $this->model->get_records(['quarter_id' => $quarter_id,'mr_id' => $mr_id],'sale_archive',['Month(date) AS salesMonth'],'',0,0,'salesMonth');
                
// echo $this->db->last_query();exit;
// echo "<pre>";print_r($month_count);exit;
                


                if($id){

          $quarter_target_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id],'quarter_wise_target');

          $exp_pmt_data = $this->model->get_records(['mr_id' => $mr_id],'exp_pmpt',['exp_slab1','exp_slab2'],'insert_dt DESC',1);
          
            // echo "<pre>";print_r($exp_pmt_data);exit;
          if($exp_pmt_data){
              
            if(count($month_count) >1 ){
               
            //   $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');

            $updated_slab_data = $this->model->check_update_slabdata($quarter_id,$city_id,$mr_id);
            //   echo "<pre>";print_r(count($updated_slab_data));exit;
            // echo $this->db->last_query();exit;
              
            $slab_1 = number_format($updated_slab_data[0]['up_slab1'] - $sale, 2, '.', '');
            $slab_2 = number_format($updated_slab_data[0]['up_slab2'] - $sale, 2, '.', '');
            if($sale_count[0]->salecount == 2){
                $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab', [], 'mr_wise_slab_id DESC');
                
                if (count($month_count) == 3) {
                                // echo "<pre>";print_r($updated_slab_data[0]->up_slab1);exit;
                                // echo "test";exit;
                                $slab_1 = number_format($slab_1 + $updated_slab_data[0]->up_slab1, 2, '.', '');
                                $slab_2 = number_format($slab_2 + $updated_slab_data[0]->up_slab2, 2, '.', '');
                            } else {
                                
                  
                  $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $updated_slab_data[0]->up_slab1, 2,'.','');
                  $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $updated_slab_data[0]->up_slab2, 2,'.','');
                            }
                  $exp_data['month_status'] = '1';

                  $up_slab_data['quarter_id'] = $quarter_id;
                  $up_slab_data['city_id'] = $city_id;
                  $up_slab_data['mr_id'] = $mr_id;
                  $up_slab_data['sale_archive_id'] = $id;
                  $up_slab_data['up_slab1'] = $slab_1;
                  $up_slab_data['up_slab2'] = $slab_2;
                  // if($updated_slab_data){

                  //     $this->_update(['mr_wise_slab_id' => $updated_slab_data[0]->mr_wise_slab_id], $up_slab_data,'updated_mr_wise_slab');
                  // }else{

                       $this->model->_insert($up_slab_data,'updated_mr_wise_slab');
                  // }

            }
          }else{
              
              $slab_1 = number_format($quarter_target_data[0]->slab1 - $sale, 2, '.', '');
              $slab_2 = number_format($quarter_target_data[0]->slab2 - $sale, 2, '.', '');
              if($sale_count[0]->salecount == 2){
                  
                  $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab1, 2,'.','');
                  $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab2, 2,'.','');
  
                  $exp_data['month_status'] = '1';
  
                  $updated_slab_data = $this->model->get_records(['quarter_id' => $quarter_id,'city_id' => $city_id,'mr_id' => $mr_id],'updated_mr_wise_slab');
                      // $slab_1 = number_format(($slab_1 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab1, 2,'.','');
                      // $slab_2 = number_format(($slab_2 / ($quarter1-count($month_count))) + $quarter_target_data[0]->slab2, 2,'.','');
                      $exp_data['month_status'] = '1';
  
                      $up_slab_data['quarter_id'] = $quarter_id;
                      $up_slab_data['city_id'] = $city_id;
                      $up_slab_data['mr_id'] = $mr_id;
                      $up_slab_data['sale_archive_id'] = $id;
                      $up_slab_data['up_slab1'] = $slab_1;
                      $up_slab_data['up_slab2'] = $slab_2;
                      // if($updated_slab_data){
  
                      //     $this->_update(['mr_wise_slab_id' => $updated_slab_data[0]->mr_wise_slab_id], $up_slab_data,'updated_mr_wise_slab');
                      // }else{
  
                           $this->model->_insert($up_slab_data,'updated_mr_wise_slab');
                      // }
              }
          }
          
        }else{
            
          $slab_1 = number_format($quarter_target_data[0]->slab1 - $sale , 2, '.', '');
          $slab_2 = number_format($quarter_target_data[0]->slab2 - $sale , 2, '.', '');
          
        }
        $exp_data['quarter_id'] = $quarter_id ;
        $exp_data['city_id'] = $city_id ;
        $exp_data['mr_id'] = $mr_id ;
        $exp_data['sale_arch_id'] = $id;
        $exp_data['exp_slab1'] = $slab_1;
        $exp_data['exp_slab2'] = $slab_2;
        
        
        $exp_data['sale'] = $sale;
      //   echo $this->db->get_compiled_select();die;
        $this->model->_insert($exp_data,'exp_pmpt');
          
      }






        //   if($exp_pmt_data){
        //     $slab_1 = number_format($exp_pmt_data[0]->exp_slab1 - ($sale - $exp_pmt_data[0]->exp_slab1), 2, '.', '');
        //     $slab_2 = number_format($exp_pmt_data[0]->exp_slab2 - ($sale - $exp_pmt_data[0]->exp_slab2), 2, '.', '');
        //   }else{
        //     $slab_1 = number_format($quarter_target_data[0]->slab1 - ($sale - $quarter_target_data[0]->slab1), 2, '.', '');
        //     $slab_2 = number_format($quarter_target_data[0]->slab2 - ($sale - $quarter_target_data[0]->slab2), 2, '.', '');
        //   }
        //   $exp_data['quarter_id'] = $quarter_id ;
        //   $exp_data['city_id'] = $city_id ;
        //   $exp_data['mr_id'] = $mr_id ;
        //   $exp_data['sale_arch_id'] = $id;
        //   $exp_data['exp_slab1'] = $slab_1;
        //   $exp_data['exp_slab2'] = $slab_2;
        //   $exp_data['sale'] = $sale;
        // //   echo $this->db->get_compiled_select();die;
        //   $this->model->_insert($exp_data,'exp_pmpt');
            // }        

            $newrows++;
        }

        fclose($handle);

        echo json_encode(['newrows'=> "$newrows record(s) added successfully"]);
    }

    function getcityBycode(){
        $city_id = $this->input->post('city_id');
        $cityCode = $this->model->get_records(['city_id' => $city_id],'city',['territory_code']);
        echo json_encode($cityCode);
    }
}
