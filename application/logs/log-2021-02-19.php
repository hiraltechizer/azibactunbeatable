<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

INFO - 2021-02-19 18:35:37 --> Config Class Initialized
INFO - 2021-02-19 18:35:37 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:35:37 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:35:37 --> Utf8 Class Initialized
INFO - 2021-02-19 18:35:37 --> URI Class Initialized
INFO - 2021-02-19 18:35:37 --> Router Class Initialized
INFO - 2021-02-19 18:35:37 --> Output Class Initialized
INFO - 2021-02-19 18:35:37 --> Security Class Initialized
DEBUG - 2021-02-19 18:35:37 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:35:37 --> CSRF cookie sent
INFO - 2021-02-19 18:35:37 --> Input Class Initialized
INFO - 2021-02-19 18:35:37 --> Language Class Initialized
INFO - 2021-02-19 18:35:38 --> Language Class Initialized
INFO - 2021-02-19 18:35:38 --> Config Class Initialized
INFO - 2021-02-19 18:35:38 --> Loader Class Initialized
INFO - 2021-02-19 18:35:38 --> Helper loaded: url_helper
INFO - 2021-02-19 18:35:38 --> Helper loaded: form_helper
INFO - 2021-02-19 18:35:38 --> Helper loaded: html_helper
INFO - 2021-02-19 18:35:38 --> Helper loaded: security_helper
INFO - 2021-02-19 18:35:38 --> Database Driver Class Initialized
INFO - 2021-02-19 18:35:38 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:35:38 --> Controller Class Initialized
INFO - 2021-02-19 18:35:39 --> MY_Model class loaded
INFO - 2021-02-19 18:35:39 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:35:39 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:35:39 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:35:39 --> Query error: Not unique table/alias: 'cs' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `ch`.`call_status_id`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:35:39 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:36:16 --> Config Class Initialized
INFO - 2021-02-19 18:36:16 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:36:16 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:36:16 --> Utf8 Class Initialized
INFO - 2021-02-19 18:36:16 --> URI Class Initialized
INFO - 2021-02-19 18:36:16 --> Router Class Initialized
INFO - 2021-02-19 18:36:16 --> Output Class Initialized
INFO - 2021-02-19 18:36:16 --> Security Class Initialized
DEBUG - 2021-02-19 18:36:16 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:36:16 --> CSRF cookie sent
INFO - 2021-02-19 18:36:16 --> Input Class Initialized
INFO - 2021-02-19 18:36:16 --> Language Class Initialized
INFO - 2021-02-19 18:36:16 --> Language Class Initialized
INFO - 2021-02-19 18:36:16 --> Config Class Initialized
INFO - 2021-02-19 18:36:16 --> Loader Class Initialized
INFO - 2021-02-19 18:36:16 --> Helper loaded: url_helper
INFO - 2021-02-19 18:36:16 --> Helper loaded: form_helper
INFO - 2021-02-19 18:36:16 --> Helper loaded: html_helper
INFO - 2021-02-19 18:36:16 --> Helper loaded: security_helper
INFO - 2021-02-19 18:36:16 --> Database Driver Class Initialized
INFO - 2021-02-19 18:36:16 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:36:16 --> Controller Class Initialized
INFO - 2021-02-19 18:36:16 --> MY_Model class loaded
INFO - 2021-02-19 18:36:16 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:36:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:36:16 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:36:16 --> Query error: Unknown column 'sc.sub_comment_id' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:36:16 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:36:55 --> Config Class Initialized
INFO - 2021-02-19 18:36:55 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:36:55 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:36:55 --> Utf8 Class Initialized
INFO - 2021-02-19 18:36:55 --> URI Class Initialized
INFO - 2021-02-19 18:36:55 --> Router Class Initialized
INFO - 2021-02-19 18:36:55 --> Output Class Initialized
INFO - 2021-02-19 18:36:55 --> Security Class Initialized
DEBUG - 2021-02-19 18:36:55 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:36:55 --> CSRF cookie sent
INFO - 2021-02-19 18:36:55 --> Input Class Initialized
INFO - 2021-02-19 18:36:55 --> Language Class Initialized
INFO - 2021-02-19 18:36:55 --> Language Class Initialized
INFO - 2021-02-19 18:36:55 --> Config Class Initialized
INFO - 2021-02-19 18:36:55 --> Loader Class Initialized
INFO - 2021-02-19 18:36:55 --> Helper loaded: url_helper
INFO - 2021-02-19 18:36:55 --> Helper loaded: form_helper
INFO - 2021-02-19 18:36:55 --> Helper loaded: html_helper
INFO - 2021-02-19 18:36:55 --> Helper loaded: security_helper
INFO - 2021-02-19 18:36:55 --> Database Driver Class Initialized
INFO - 2021-02-19 18:36:55 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:36:55 --> Controller Class Initialized
INFO - 2021-02-19 18:36:56 --> MY_Model class loaded
INFO - 2021-02-19 18:36:56 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:36:56 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:36:56 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:36:56 --> Query error: Unknown column 'm.users_name' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:36:56 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:36:57 --> Config Class Initialized
INFO - 2021-02-19 18:36:57 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:36:57 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:36:57 --> Utf8 Class Initialized
INFO - 2021-02-19 18:36:57 --> URI Class Initialized
INFO - 2021-02-19 18:36:57 --> Router Class Initialized
INFO - 2021-02-19 18:36:57 --> Output Class Initialized
INFO - 2021-02-19 18:36:57 --> Security Class Initialized
DEBUG - 2021-02-19 18:36:57 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:36:57 --> CSRF cookie sent
INFO - 2021-02-19 18:36:57 --> Input Class Initialized
INFO - 2021-02-19 18:36:57 --> Language Class Initialized
INFO - 2021-02-19 18:36:57 --> Language Class Initialized
INFO - 2021-02-19 18:36:57 --> Config Class Initialized
INFO - 2021-02-19 18:36:57 --> Loader Class Initialized
INFO - 2021-02-19 18:36:57 --> Helper loaded: url_helper
INFO - 2021-02-19 18:36:57 --> Helper loaded: form_helper
INFO - 2021-02-19 18:36:57 --> Helper loaded: html_helper
INFO - 2021-02-19 18:36:57 --> Helper loaded: security_helper
INFO - 2021-02-19 18:36:57 --> Database Driver Class Initialized
INFO - 2021-02-19 18:36:57 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:36:57 --> Controller Class Initialized
INFO - 2021-02-19 18:36:57 --> MY_Model class loaded
INFO - 2021-02-19 18:36:57 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:36:57 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:36:57 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:36:57 --> Query error: Unknown column 'm.users_name' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:36:57 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:38:01 --> Config Class Initialized
INFO - 2021-02-19 18:38:01 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:38:01 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:38:01 --> Utf8 Class Initialized
INFO - 2021-02-19 18:38:01 --> URI Class Initialized
INFO - 2021-02-19 18:38:02 --> Router Class Initialized
INFO - 2021-02-19 18:38:02 --> Output Class Initialized
INFO - 2021-02-19 18:38:02 --> Security Class Initialized
DEBUG - 2021-02-19 18:38:02 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:38:02 --> CSRF cookie sent
INFO - 2021-02-19 18:38:02 --> Input Class Initialized
INFO - 2021-02-19 18:38:02 --> Language Class Initialized
INFO - 2021-02-19 18:38:02 --> Language Class Initialized
INFO - 2021-02-19 18:38:02 --> Config Class Initialized
INFO - 2021-02-19 18:38:02 --> Loader Class Initialized
INFO - 2021-02-19 18:38:02 --> Helper loaded: url_helper
INFO - 2021-02-19 18:38:02 --> Helper loaded: form_helper
INFO - 2021-02-19 18:38:02 --> Helper loaded: html_helper
INFO - 2021-02-19 18:38:02 --> Helper loaded: security_helper
INFO - 2021-02-19 18:38:02 --> Database Driver Class Initialized
INFO - 2021-02-19 18:38:02 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:38:02 --> Controller Class Initialized
INFO - 2021-02-19 18:38:02 --> MY_Model class loaded
INFO - 2021-02-19 18:38:02 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:38:02 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:38:02 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:38:02 --> Query error: Unknown column 'cc.coupon_code' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:38:02 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:38:03 --> Config Class Initialized
INFO - 2021-02-19 18:38:03 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:38:03 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:38:03 --> Utf8 Class Initialized
INFO - 2021-02-19 18:38:03 --> URI Class Initialized
INFO - 2021-02-19 18:38:03 --> Router Class Initialized
INFO - 2021-02-19 18:38:03 --> Output Class Initialized
INFO - 2021-02-19 18:38:03 --> Security Class Initialized
DEBUG - 2021-02-19 18:38:03 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:38:03 --> CSRF cookie sent
INFO - 2021-02-19 18:38:03 --> Input Class Initialized
INFO - 2021-02-19 18:38:03 --> Language Class Initialized
INFO - 2021-02-19 18:38:03 --> Language Class Initialized
INFO - 2021-02-19 18:38:03 --> Config Class Initialized
INFO - 2021-02-19 18:38:03 --> Loader Class Initialized
INFO - 2021-02-19 18:38:03 --> Helper loaded: url_helper
INFO - 2021-02-19 18:38:03 --> Helper loaded: form_helper
INFO - 2021-02-19 18:38:03 --> Helper loaded: html_helper
INFO - 2021-02-19 18:38:03 --> Helper loaded: security_helper
INFO - 2021-02-19 18:38:04 --> Database Driver Class Initialized
INFO - 2021-02-19 18:38:04 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:38:04 --> Controller Class Initialized
INFO - 2021-02-19 18:38:04 --> MY_Model class loaded
INFO - 2021-02-19 18:38:04 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:38:04 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:38:04 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:38:04 --> Query error: Unknown column 'cc.coupon_code' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:38:04 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:38:38 --> Config Class Initialized
INFO - 2021-02-19 18:38:38 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:38:38 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:38:38 --> Utf8 Class Initialized
INFO - 2021-02-19 18:38:38 --> URI Class Initialized
INFO - 2021-02-19 18:38:38 --> Router Class Initialized
INFO - 2021-02-19 18:38:38 --> Output Class Initialized
INFO - 2021-02-19 18:38:38 --> Security Class Initialized
DEBUG - 2021-02-19 18:38:38 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:38:38 --> CSRF cookie sent
INFO - 2021-02-19 18:38:38 --> Input Class Initialized
INFO - 2021-02-19 18:38:38 --> Language Class Initialized
INFO - 2021-02-19 18:38:38 --> Language Class Initialized
INFO - 2021-02-19 18:38:38 --> Config Class Initialized
INFO - 2021-02-19 18:38:38 --> Loader Class Initialized
INFO - 2021-02-19 18:38:38 --> Helper loaded: url_helper
INFO - 2021-02-19 18:38:38 --> Helper loaded: form_helper
INFO - 2021-02-19 18:38:38 --> Helper loaded: html_helper
INFO - 2021-02-19 18:38:38 --> Helper loaded: security_helper
INFO - 2021-02-19 18:38:38 --> Database Driver Class Initialized
INFO - 2021-02-19 18:38:38 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:38:38 --> Controller Class Initialized
INFO - 2021-02-19 18:38:38 --> MY_Model class loaded
INFO - 2021-02-19 18:38:38 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:38:39 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:38:39 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:38:39 --> Query error: Unknown column 'cc.coupon_code' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:38:39 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:40:11 --> Config Class Initialized
INFO - 2021-02-19 18:40:11 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:40:11 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:40:11 --> Utf8 Class Initialized
INFO - 2021-02-19 18:40:11 --> URI Class Initialized
INFO - 2021-02-19 18:40:11 --> Router Class Initialized
INFO - 2021-02-19 18:40:11 --> Output Class Initialized
INFO - 2021-02-19 18:40:11 --> Security Class Initialized
DEBUG - 2021-02-19 18:40:11 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:40:11 --> CSRF cookie sent
INFO - 2021-02-19 18:40:11 --> Input Class Initialized
INFO - 2021-02-19 18:40:11 --> Language Class Initialized
INFO - 2021-02-19 18:40:11 --> Language Class Initialized
INFO - 2021-02-19 18:40:11 --> Config Class Initialized
INFO - 2021-02-19 18:40:11 --> Loader Class Initialized
INFO - 2021-02-19 18:40:11 --> Helper loaded: url_helper
INFO - 2021-02-19 18:40:11 --> Helper loaded: form_helper
INFO - 2021-02-19 18:40:11 --> Helper loaded: html_helper
INFO - 2021-02-19 18:40:11 --> Helper loaded: security_helper
INFO - 2021-02-19 18:40:11 --> Database Driver Class Initialized
INFO - 2021-02-19 18:40:11 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:40:11 --> Controller Class Initialized
INFO - 2021-02-19 18:40:11 --> MY_Model class loaded
INFO - 2021-02-19 18:40:11 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:40:11 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:40:11 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:40:11 --> Query error: Unknown column 'ach.comment' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ach.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:40:11 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:40:35 --> Config Class Initialized
INFO - 2021-02-19 18:40:35 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:40:35 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:40:35 --> Utf8 Class Initialized
INFO - 2021-02-19 18:40:35 --> URI Class Initialized
INFO - 2021-02-19 18:40:35 --> Router Class Initialized
INFO - 2021-02-19 18:40:36 --> Output Class Initialized
INFO - 2021-02-19 18:40:36 --> Security Class Initialized
DEBUG - 2021-02-19 18:40:36 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:40:36 --> CSRF cookie sent
INFO - 2021-02-19 18:40:36 --> Input Class Initialized
INFO - 2021-02-19 18:40:36 --> Language Class Initialized
INFO - 2021-02-19 18:40:36 --> Language Class Initialized
INFO - 2021-02-19 18:40:36 --> Config Class Initialized
INFO - 2021-02-19 18:40:36 --> Loader Class Initialized
INFO - 2021-02-19 18:40:36 --> Helper loaded: url_helper
INFO - 2021-02-19 18:40:36 --> Helper loaded: form_helper
INFO - 2021-02-19 18:40:36 --> Helper loaded: html_helper
INFO - 2021-02-19 18:40:36 --> Helper loaded: security_helper
INFO - 2021-02-19 18:40:36 --> Database Driver Class Initialized
INFO - 2021-02-19 18:40:36 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:40:36 --> Controller Class Initialized
INFO - 2021-02-19 18:40:36 --> MY_Model class loaded
INFO - 2021-02-19 18:40:36 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:40:36 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:40:36 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:40:36 --> Query error: Unknown column 'adh2.call_status_id' in 'on clause' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, ch.comment, ch.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `patient` `p`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `ct`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `p`.`mobile_1`
LEFT JOIN  ( SELECT * FROM call_history chl WHERE 1=1 AND chl.call_history_id IN ( SELECT MAX(call_history_id) As call_history_id FROM call_history ch WHERE 1=1 GROUP BY ch.mobile ) ) ch ON `p`.`mobile_1` = `ch`.`mobile`
GROUP BY `p`.`patient_id`
ORDER BY `missCallDate` DESC
 LIMIT 10
INFO - 2021-02-19 18:40:36 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:48:36 --> Config Class Initialized
INFO - 2021-02-19 18:48:36 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:48:36 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:36 --> Utf8 Class Initialized
INFO - 2021-02-19 18:48:36 --> URI Class Initialized
INFO - 2021-02-19 18:48:36 --> Router Class Initialized
INFO - 2021-02-19 18:48:36 --> Output Class Initialized
INFO - 2021-02-19 18:48:36 --> Security Class Initialized
DEBUG - 2021-02-19 18:48:36 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:48:36 --> CSRF cookie sent
INFO - 2021-02-19 18:48:36 --> Input Class Initialized
INFO - 2021-02-19 18:48:36 --> Language Class Initialized
INFO - 2021-02-19 18:48:36 --> Language Class Initialized
INFO - 2021-02-19 18:48:36 --> Config Class Initialized
INFO - 2021-02-19 18:48:36 --> Loader Class Initialized
INFO - 2021-02-19 18:48:36 --> Helper loaded: url_helper
INFO - 2021-02-19 18:48:36 --> Helper loaded: form_helper
INFO - 2021-02-19 18:48:36 --> Helper loaded: html_helper
INFO - 2021-02-19 18:48:36 --> Helper loaded: security_helper
INFO - 2021-02-19 18:48:36 --> Database Driver Class Initialized
INFO - 2021-02-19 18:48:36 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:48:36 --> Controller Class Initialized
INFO - 2021-02-19 18:48:36 --> MY_Model class loaded
INFO - 2021-02-19 18:48:36 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:48:36 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:48:36 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:48:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:48:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:48:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:48:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:48:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:48:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:48:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:48:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:48:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:48:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:48:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:48:38 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:48:38 --> Database Forge Class Initialized
INFO - 2021-02-19 18:48:38 --> User Agent Class Initialized
INFO - 2021-02-19 18:48:38 --> Final output sent to browser
DEBUG - 2021-02-19 18:48:38 --> Total execution time: 2.3420
INFO - 2021-02-19 18:48:50 --> Config Class Initialized
INFO - 2021-02-19 18:48:50 --> Config Class Initialized
INFO - 2021-02-19 18:48:50 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:48:50 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:50 --> Utf8 Class Initialized
INFO - 2021-02-19 18:48:50 --> URI Class Initialized
INFO - 2021-02-19 18:48:50 --> Hooks Class Initialized
INFO - 2021-02-19 18:48:50 --> Router Class Initialized
INFO - 2021-02-19 18:48:50 --> Output Class Initialized
INFO - 2021-02-19 18:48:50 --> Security Class Initialized
DEBUG - 2021-02-19 18:48:50 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:48:50 --> CSRF cookie sent
INFO - 2021-02-19 18:48:50 --> Input Class Initialized
INFO - 2021-02-19 18:48:50 --> Language Class Initialized
ERROR - 2021-02-19 18:48:50 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:48:50 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:50 --> Utf8 Class Initialized
INFO - 2021-02-19 18:48:50 --> URI Class Initialized
INFO - 2021-02-19 18:48:50 --> Router Class Initialized
INFO - 2021-02-19 18:48:50 --> Output Class Initialized
INFO - 2021-02-19 18:48:50 --> Security Class Initialized
DEBUG - 2021-02-19 18:48:50 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:48:50 --> CSRF cookie sent
INFO - 2021-02-19 18:48:50 --> Config Class Initialized
INFO - 2021-02-19 18:48:50 --> Hooks Class Initialized
INFO - 2021-02-19 18:48:50 --> Config Class Initialized
INFO - 2021-02-19 18:48:50 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:48:50 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:50 --> Utf8 Class Initialized
DEBUG - 2021-02-19 18:48:50 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:50 --> Utf8 Class Initialized
INFO - 2021-02-19 18:48:50 --> URI Class Initialized
INFO - 2021-02-19 18:48:50 --> Input Class Initialized
INFO - 2021-02-19 18:48:50 --> URI Class Initialized
INFO - 2021-02-19 18:48:50 --> Router Class Initialized
INFO - 2021-02-19 18:48:50 --> Router Class Initialized
INFO - 2021-02-19 18:48:50 --> Output Class Initialized
INFO - 2021-02-19 18:48:50 --> Output Class Initialized
INFO - 2021-02-19 18:48:50 --> Security Class Initialized
INFO - 2021-02-19 18:48:50 --> Security Class Initialized
DEBUG - 2021-02-19 18:48:50 --> Global POST, GET and COOKIE data sanitized
DEBUG - 2021-02-19 18:48:50 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:48:50 --> CSRF cookie sent
INFO - 2021-02-19 18:48:50 --> CSRF cookie sent
INFO - 2021-02-19 18:48:50 --> Input Class Initialized
INFO - 2021-02-19 18:48:50 --> Input Class Initialized
INFO - 2021-02-19 18:48:50 --> Language Class Initialized
INFO - 2021-02-19 18:48:50 --> Language Class Initialized
INFO - 2021-02-19 18:48:50 --> Language Class Initialized
ERROR - 2021-02-19 18:48:50 --> 404 Page Not Found: /index
ERROR - 2021-02-19 18:48:50 --> 404 Page Not Found: /index
ERROR - 2021-02-19 18:48:50 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:48:56 --> Config Class Initialized
INFO - 2021-02-19 18:48:56 --> Config Class Initialized
INFO - 2021-02-19 18:48:56 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:48:56 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:56 --> Utf8 Class Initialized
INFO - 2021-02-19 18:48:56 --> URI Class Initialized
INFO - 2021-02-19 18:48:56 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:48:56 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:48:56 --> Utf8 Class Initialized
INFO - 2021-02-19 18:48:56 --> URI Class Initialized
INFO - 2021-02-19 18:48:56 --> Router Class Initialized
INFO - 2021-02-19 18:48:56 --> Router Class Initialized
INFO - 2021-02-19 18:48:56 --> Output Class Initialized
INFO - 2021-02-19 18:48:56 --> Security Class Initialized
DEBUG - 2021-02-19 18:48:56 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:48:56 --> Output Class Initialized
INFO - 2021-02-19 18:48:56 --> Security Class Initialized
DEBUG - 2021-02-19 18:48:56 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:48:56 --> CSRF cookie sent
INFO - 2021-02-19 18:48:56 --> CSRF token verified
INFO - 2021-02-19 18:48:56 --> Input Class Initialized
INFO - 2021-02-19 18:48:56 --> Language Class Initialized
INFO - 2021-02-19 18:48:56 --> CSRF cookie sent
INFO - 2021-02-19 18:48:56 --> Language Class Initialized
INFO - 2021-02-19 18:48:56 --> Config Class Initialized
INFO - 2021-02-19 18:48:56 --> Loader Class Initialized
INFO - 2021-02-19 18:48:56 --> Helper loaded: url_helper
INFO - 2021-02-19 18:48:56 --> CSRF token verified
INFO - 2021-02-19 18:48:56 --> Helper loaded: form_helper
INFO - 2021-02-19 18:48:56 --> Helper loaded: html_helper
INFO - 2021-02-19 18:48:56 --> Helper loaded: security_helper
INFO - 2021-02-19 18:48:56 --> Input Class Initialized
INFO - 2021-02-19 18:48:56 --> Database Driver Class Initialized
INFO - 2021-02-19 18:48:56 --> Language Class Initialized
INFO - 2021-02-19 18:48:56 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:48:56 --> Language Class Initialized
INFO - 2021-02-19 18:48:56 --> Config Class Initialized
INFO - 2021-02-19 18:48:56 --> Loader Class Initialized
INFO - 2021-02-19 18:48:56 --> Controller Class Initialized
INFO - 2021-02-19 18:48:56 --> MY_Model class loaded
INFO - 2021-02-19 18:48:56 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:48:56 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:48:56 --> Helper loaded: send_sms_helper
INFO - 2021-02-19 18:48:56 --> Helper loaded: url_helper
INFO - 2021-02-19 18:48:56 --> Helper loaded: form_helper
INFO - 2021-02-19 18:48:56 --> Helper loaded: html_helper
INFO - 2021-02-19 18:48:56 --> Helper loaded: security_helper
ERROR - 2021-02-19 18:48:56 --> Severity: error --> Exception: Call to undefined method Mdl_patient::get_filters_from_revisite() D:\xampp\htdocs\vilcose_psp\application\modules\patient\models\Mdl_patient.php 99
INFO - 2021-02-19 18:48:56 --> Database Driver Class Initialized
INFO - 2021-02-19 18:48:56 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:48:56 --> Controller Class Initialized
INFO - 2021-02-19 18:48:56 --> MY_Model class loaded
INFO - 2021-02-19 18:48:56 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:48:56 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:48:56 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:48:56 --> Severity: error --> Exception: Call to undefined method Mdl_patient::get_filters_from_revisite() D:\xampp\htdocs\vilcose_psp\application\modules\patient\models\Mdl_patient.php 99
INFO - 2021-02-19 18:49:31 --> Config Class Initialized
INFO - 2021-02-19 18:49:31 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:49:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:31 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:31 --> URI Class Initialized
INFO - 2021-02-19 18:49:31 --> Router Class Initialized
INFO - 2021-02-19 18:49:31 --> Config Class Initialized
INFO - 2021-02-19 18:49:31 --> Output Class Initialized
INFO - 2021-02-19 18:49:31 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:31 --> CSRF cookie sent
INFO - 2021-02-19 18:49:31 --> Input Class Initialized
INFO - 2021-02-19 18:49:31 --> Language Class Initialized
INFO - 2021-02-19 18:49:31 --> Hooks Class Initialized
INFO - 2021-02-19 18:49:31 --> Language Class Initialized
INFO - 2021-02-19 18:49:31 --> Config Class Initialized
INFO - 2021-02-19 18:49:31 --> Loader Class Initialized
INFO - 2021-02-19 18:49:31 --> Helper loaded: url_helper
INFO - 2021-02-19 18:49:31 --> Helper loaded: form_helper
DEBUG - 2021-02-19 18:49:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:31 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:31 --> URI Class Initialized
INFO - 2021-02-19 18:49:31 --> Router Class Initialized
INFO - 2021-02-19 18:49:31 --> Output Class Initialized
INFO - 2021-02-19 18:49:31 --> Helper loaded: html_helper
INFO - 2021-02-19 18:49:31 --> Helper loaded: security_helper
INFO - 2021-02-19 18:49:31 --> Database Driver Class Initialized
INFO - 2021-02-19 18:49:31 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:31 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:49:31 --> Controller Class Initialized
INFO - 2021-02-19 18:49:31 --> MY_Model class loaded
INFO - 2021-02-19 18:49:31 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:49:31 --> Helper loaded: send_sms_helper
INFO - 2021-02-19 18:49:31 --> CSRF cookie sent
INFO - 2021-02-19 18:49:31 --> Input Class Initialized
INFO - 2021-02-19 18:49:31 --> Language Class Initialized
INFO - 2021-02-19 18:49:31 --> Language Class Initialized
INFO - 2021-02-19 18:49:31 --> Config Class Initialized
INFO - 2021-02-19 18:49:31 --> Loader Class Initialized
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
INFO - 2021-02-19 18:49:31 --> Helper loaded: url_helper
INFO - 2021-02-19 18:49:31 --> Helper loaded: form_helper
INFO - 2021-02-19 18:49:31 --> Helper loaded: html_helper
INFO - 2021-02-19 18:49:31 --> Helper loaded: security_helper
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
INFO - 2021-02-19 18:49:31 --> Database Driver Class Initialized
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:49:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:49:31 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:49:31 --> Database Forge Class Initialized
INFO - 2021-02-19 18:49:31 --> User Agent Class Initialized
INFO - 2021-02-19 18:49:32 --> Final output sent to browser
DEBUG - 2021-02-19 18:49:32 --> Total execution time: 0.7946
INFO - 2021-02-19 18:49:32 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:49:32 --> Controller Class Initialized
INFO - 2021-02-19 18:49:32 --> MY_Model class loaded
INFO - 2021-02-19 18:49:32 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:49:32 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:49:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:49:32 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:49:32 --> Database Forge Class Initialized
INFO - 2021-02-19 18:49:32 --> User Agent Class Initialized
INFO - 2021-02-19 18:49:32 --> Final output sent to browser
DEBUG - 2021-02-19 18:49:32 --> Total execution time: 1.5128
INFO - 2021-02-19 18:49:33 --> Config Class Initialized
INFO - 2021-02-19 18:49:33 --> Config Class Initialized
INFO - 2021-02-19 18:49:33 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:49:33 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:33 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:33 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:49:33 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:33 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:33 --> URI Class Initialized
INFO - 2021-02-19 18:49:33 --> URI Class Initialized
INFO - 2021-02-19 18:49:33 --> Router Class Initialized
INFO - 2021-02-19 18:49:33 --> Output Class Initialized
INFO - 2021-02-19 18:49:33 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:33 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:33 --> CSRF cookie sent
INFO - 2021-02-19 18:49:33 --> Input Class Initialized
INFO - 2021-02-19 18:49:33 --> Language Class Initialized
ERROR - 2021-02-19 18:49:33 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:49:33 --> Router Class Initialized
INFO - 2021-02-19 18:49:33 --> Config Class Initialized
INFO - 2021-02-19 18:49:33 --> Output Class Initialized
INFO - 2021-02-19 18:49:33 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:33 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:33 --> CSRF cookie sent
INFO - 2021-02-19 18:49:33 --> Input Class Initialized
INFO - 2021-02-19 18:49:33 --> Language Class Initialized
ERROR - 2021-02-19 18:49:33 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:49:33 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:49:33 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:33 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:33 --> URI Class Initialized
INFO - 2021-02-19 18:49:33 --> Router Class Initialized
INFO - 2021-02-19 18:49:33 --> Output Class Initialized
INFO - 2021-02-19 18:49:33 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:33 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:33 --> CSRF cookie sent
INFO - 2021-02-19 18:49:33 --> Input Class Initialized
INFO - 2021-02-19 18:49:33 --> Language Class Initialized
ERROR - 2021-02-19 18:49:33 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:49:34 --> Config Class Initialized
INFO - 2021-02-19 18:49:34 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:49:34 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:34 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:34 --> URI Class Initialized
INFO - 2021-02-19 18:49:34 --> Router Class Initialized
INFO - 2021-02-19 18:49:34 --> Output Class Initialized
INFO - 2021-02-19 18:49:34 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:34 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:34 --> CSRF cookie sent
INFO - 2021-02-19 18:49:34 --> Input Class Initialized
INFO - 2021-02-19 18:49:34 --> Language Class Initialized
ERROR - 2021-02-19 18:49:34 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:49:37 --> Config Class Initialized
INFO - 2021-02-19 18:49:37 --> Config Class Initialized
INFO - 2021-02-19 18:49:37 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:49:37 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:37 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:37 --> Hooks Class Initialized
INFO - 2021-02-19 18:49:37 --> URI Class Initialized
INFO - 2021-02-19 18:49:37 --> Router Class Initialized
INFO - 2021-02-19 18:49:37 --> Output Class Initialized
INFO - 2021-02-19 18:49:37 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:37 --> Global POST, GET and COOKIE data sanitized
DEBUG - 2021-02-19 18:49:37 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:49:37 --> Utf8 Class Initialized
INFO - 2021-02-19 18:49:37 --> URI Class Initialized
INFO - 2021-02-19 18:49:37 --> Router Class Initialized
INFO - 2021-02-19 18:49:37 --> CSRF cookie sent
INFO - 2021-02-19 18:49:37 --> Output Class Initialized
INFO - 2021-02-19 18:49:37 --> Security Class Initialized
DEBUG - 2021-02-19 18:49:37 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:49:37 --> CSRF cookie sent
INFO - 2021-02-19 18:49:37 --> CSRF token verified
INFO - 2021-02-19 18:49:37 --> Input Class Initialized
INFO - 2021-02-19 18:49:37 --> Language Class Initialized
INFO - 2021-02-19 18:49:37 --> CSRF token verified
INFO - 2021-02-19 18:49:37 --> Language Class Initialized
INFO - 2021-02-19 18:49:37 --> Config Class Initialized
INFO - 2021-02-19 18:49:37 --> Loader Class Initialized
INFO - 2021-02-19 18:49:37 --> Helper loaded: url_helper
INFO - 2021-02-19 18:49:37 --> Input Class Initialized
INFO - 2021-02-19 18:49:37 --> Helper loaded: form_helper
INFO - 2021-02-19 18:49:37 --> Helper loaded: html_helper
INFO - 2021-02-19 18:49:37 --> Helper loaded: security_helper
INFO - 2021-02-19 18:49:37 --> Language Class Initialized
INFO - 2021-02-19 18:49:37 --> Database Driver Class Initialized
INFO - 2021-02-19 18:49:37 --> Language Class Initialized
INFO - 2021-02-19 18:49:37 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:49:37 --> Controller Class Initialized
INFO - 2021-02-19 18:49:37 --> MY_Model class loaded
INFO - 2021-02-19 18:49:37 --> Model "MY_Model" initialized
INFO - 2021-02-19 18:49:37 --> Config Class Initialized
DEBUG - 2021-02-19 18:49:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:49:37 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:49:37 --> Severity: error --> Exception: Call to undefined method Mdl_patient::get_filters_revisite() D:\xampp\htdocs\vilcose_psp\application\modules\patient\models\Mdl_patient.php 97
INFO - 2021-02-19 18:49:37 --> Loader Class Initialized
INFO - 2021-02-19 18:49:37 --> Helper loaded: url_helper
INFO - 2021-02-19 18:49:37 --> Helper loaded: form_helper
INFO - 2021-02-19 18:49:37 --> Helper loaded: html_helper
INFO - 2021-02-19 18:49:37 --> Helper loaded: security_helper
INFO - 2021-02-19 18:49:37 --> Database Driver Class Initialized
INFO - 2021-02-19 18:49:37 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:49:37 --> Controller Class Initialized
INFO - 2021-02-19 18:49:37 --> MY_Model class loaded
INFO - 2021-02-19 18:49:37 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:49:37 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:49:37 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:49:37 --> Severity: error --> Exception: Call to undefined method Mdl_patient::get_filters_revisite() D:\xampp\htdocs\vilcose_psp\application\modules\patient\models\Mdl_patient.php 97
INFO - 2021-02-19 18:50:16 --> Config Class Initialized
INFO - 2021-02-19 18:50:16 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:50:16 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:16 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:16 --> URI Class Initialized
INFO - 2021-02-19 18:50:16 --> Router Class Initialized
INFO - 2021-02-19 18:50:16 --> Output Class Initialized
INFO - 2021-02-19 18:50:16 --> Security Class Initialized
DEBUG - 2021-02-19 18:50:16 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:50:16 --> CSRF cookie sent
INFO - 2021-02-19 18:50:16 --> Input Class Initialized
INFO - 2021-02-19 18:50:16 --> Language Class Initialized
INFO - 2021-02-19 18:50:16 --> Language Class Initialized
INFO - 2021-02-19 18:50:16 --> Config Class Initialized
INFO - 2021-02-19 18:50:16 --> Loader Class Initialized
INFO - 2021-02-19 18:50:16 --> Helper loaded: url_helper
INFO - 2021-02-19 18:50:16 --> Helper loaded: form_helper
INFO - 2021-02-19 18:50:16 --> Helper loaded: html_helper
INFO - 2021-02-19 18:50:16 --> Helper loaded: security_helper
INFO - 2021-02-19 18:50:16 --> Database Driver Class Initialized
INFO - 2021-02-19 18:50:16 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:50:16 --> Controller Class Initialized
INFO - 2021-02-19 18:50:16 --> MY_Model class loaded
INFO - 2021-02-19 18:50:16 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:50:16 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:50:16 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:50:17 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:50:17 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:50:17 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:50:17 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:50:17 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:50:17 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:50:17 --> Database Forge Class Initialized
INFO - 2021-02-19 18:50:17 --> User Agent Class Initialized
INFO - 2021-02-19 18:50:17 --> Config Class Initialized
INFO - 2021-02-19 18:50:17 --> Config Class Initialized
INFO - 2021-02-19 18:50:17 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:50:17 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:17 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:17 --> URI Class Initialized
INFO - 2021-02-19 18:50:17 --> Final output sent to browser
DEBUG - 2021-02-19 18:50:17 --> Total execution time: 1.2634
INFO - 2021-02-19 18:50:17 --> Hooks Class Initialized
INFO - 2021-02-19 18:50:17 --> Router Class Initialized
INFO - 2021-02-19 18:50:17 --> Output Class Initialized
INFO - 2021-02-19 18:50:17 --> Security Class Initialized
DEBUG - 2021-02-19 18:50:17 --> Global POST, GET and COOKIE data sanitized
DEBUG - 2021-02-19 18:50:17 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:17 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:17 --> URI Class Initialized
INFO - 2021-02-19 18:50:17 --> CSRF cookie sent
INFO - 2021-02-19 18:50:17 --> Router Class Initialized
INFO - 2021-02-19 18:50:17 --> Output Class Initialized
INFO - 2021-02-19 18:50:17 --> Security Class Initialized
DEBUG - 2021-02-19 18:50:17 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:50:17 --> Input Class Initialized
INFO - 2021-02-19 18:50:17 --> Language Class Initialized
ERROR - 2021-02-19 18:50:17 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:50:17 --> CSRF cookie sent
INFO - 2021-02-19 18:50:17 --> Input Class Initialized
INFO - 2021-02-19 18:50:17 --> Language Class Initialized
ERROR - 2021-02-19 18:50:17 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:50:17 --> Config Class Initialized
INFO - 2021-02-19 18:50:17 --> Config Class Initialized
INFO - 2021-02-19 18:50:17 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:50:17 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:17 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:17 --> URI Class Initialized
INFO - 2021-02-19 18:50:17 --> Hooks Class Initialized
INFO - 2021-02-19 18:50:17 --> Router Class Initialized
INFO - 2021-02-19 18:50:17 --> Output Class Initialized
INFO - 2021-02-19 18:50:17 --> Security Class Initialized
DEBUG - 2021-02-19 18:50:17 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:50:17 --> CSRF cookie sent
INFO - 2021-02-19 18:50:17 --> Input Class Initialized
INFO - 2021-02-19 18:50:17 --> Language Class Initialized
ERROR - 2021-02-19 18:50:17 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:50:17 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:17 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:17 --> URI Class Initialized
INFO - 2021-02-19 18:50:17 --> Router Class Initialized
INFO - 2021-02-19 18:50:17 --> Output Class Initialized
INFO - 2021-02-19 18:50:17 --> Security Class Initialized
DEBUG - 2021-02-19 18:50:17 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:50:17 --> CSRF cookie sent
INFO - 2021-02-19 18:50:17 --> Input Class Initialized
INFO - 2021-02-19 18:50:17 --> Language Class Initialized
ERROR - 2021-02-19 18:50:17 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:50:21 --> Config Class Initialized
INFO - 2021-02-19 18:50:21 --> Config Class Initialized
INFO - 2021-02-19 18:50:21 --> Hooks Class Initialized
INFO - 2021-02-19 18:50:21 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:50:21 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:21 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:21 --> URI Class Initialized
INFO - 2021-02-19 18:50:21 --> Router Class Initialized
INFO - 2021-02-19 18:50:21 --> Output Class Initialized
DEBUG - 2021-02-19 18:50:21 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:50:21 --> Utf8 Class Initialized
INFO - 2021-02-19 18:50:21 --> URI Class Initialized
INFO - 2021-02-19 18:50:21 --> Router Class Initialized
INFO - 2021-02-19 18:50:21 --> Output Class Initialized
INFO - 2021-02-19 18:50:22 --> Security Class Initialized
DEBUG - 2021-02-19 18:50:22 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:50:22 --> CSRF cookie sent
INFO - 2021-02-19 18:50:22 --> CSRF token verified
INFO - 2021-02-19 18:50:22 --> Input Class Initialized
INFO - 2021-02-19 18:50:22 --> Language Class Initialized
INFO - 2021-02-19 18:50:22 --> Security Class Initialized
INFO - 2021-02-19 18:50:22 --> Language Class Initialized
INFO - 2021-02-19 18:50:22 --> Config Class Initialized
INFO - 2021-02-19 18:50:22 --> Loader Class Initialized
INFO - 2021-02-19 18:50:22 --> Helper loaded: url_helper
DEBUG - 2021-02-19 18:50:22 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:50:22 --> CSRF cookie sent
INFO - 2021-02-19 18:50:22 --> CSRF token verified
INFO - 2021-02-19 18:50:22 --> Input Class Initialized
INFO - 2021-02-19 18:50:22 --> Language Class Initialized
INFO - 2021-02-19 18:50:22 --> Language Class Initialized
INFO - 2021-02-19 18:50:22 --> Config Class Initialized
INFO - 2021-02-19 18:50:22 --> Helper loaded: form_helper
INFO - 2021-02-19 18:50:22 --> Helper loaded: html_helper
INFO - 2021-02-19 18:50:22 --> Helper loaded: security_helper
INFO - 2021-02-19 18:50:22 --> Database Driver Class Initialized
INFO - 2021-02-19 18:50:22 --> Loader Class Initialized
INFO - 2021-02-19 18:50:22 --> Helper loaded: url_helper
INFO - 2021-02-19 18:50:22 --> Helper loaded: form_helper
INFO - 2021-02-19 18:50:22 --> Helper loaded: html_helper
INFO - 2021-02-19 18:50:22 --> Helper loaded: security_helper
INFO - 2021-02-19 18:50:22 --> Database Driver Class Initialized
INFO - 2021-02-19 18:50:22 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:50:22 --> Controller Class Initialized
INFO - 2021-02-19 18:50:22 --> MY_Model class loaded
INFO - 2021-02-19 18:50:22 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:50:22 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:50:22 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:50:22 --> Query error: Not unique table/alias: 'l' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `c` ON `c`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:50:22 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:50:22 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:50:22 --> Controller Class Initialized
INFO - 2021-02-19 18:50:22 --> MY_Model class loaded
INFO - 2021-02-19 18:50:22 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:50:22 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:50:22 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:50:22 --> Query error: Not unique table/alias: 'l' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `c` ON `c`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:50:22 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:53:28 --> Config Class Initialized
INFO - 2021-02-19 18:53:28 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:53:28 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:28 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:28 --> URI Class Initialized
INFO - 2021-02-19 18:53:28 --> Config Class Initialized
INFO - 2021-02-19 18:53:28 --> Router Class Initialized
INFO - 2021-02-19 18:53:28 --> Output Class Initialized
INFO - 2021-02-19 18:53:28 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:28 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:28 --> CSRF cookie sent
INFO - 2021-02-19 18:53:28 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:53:28 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:28 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:28 --> URI Class Initialized
INFO - 2021-02-19 18:53:28 --> Input Class Initialized
INFO - 2021-02-19 18:53:28 --> Router Class Initialized
INFO - 2021-02-19 18:53:28 --> Output Class Initialized
INFO - 2021-02-19 18:53:28 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:28 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:28 --> CSRF cookie sent
INFO - 2021-02-19 18:53:28 --> Input Class Initialized
INFO - 2021-02-19 18:53:28 --> Language Class Initialized
INFO - 2021-02-19 18:53:28 --> Language Class Initialized
INFO - 2021-02-19 18:53:28 --> Language Class Initialized
INFO - 2021-02-19 18:53:28 --> Config Class Initialized
INFO - 2021-02-19 18:53:28 --> Loader Class Initialized
INFO - 2021-02-19 18:53:28 --> Helper loaded: url_helper
INFO - 2021-02-19 18:53:28 --> Helper loaded: form_helper
INFO - 2021-02-19 18:53:28 --> Language Class Initialized
INFO - 2021-02-19 18:53:28 --> Config Class Initialized
INFO - 2021-02-19 18:53:28 --> Loader Class Initialized
INFO - 2021-02-19 18:53:28 --> Helper loaded: url_helper
INFO - 2021-02-19 18:53:28 --> Helper loaded: html_helper
INFO - 2021-02-19 18:53:28 --> Helper loaded: form_helper
INFO - 2021-02-19 18:53:28 --> Helper loaded: html_helper
INFO - 2021-02-19 18:53:28 --> Helper loaded: security_helper
INFO - 2021-02-19 18:53:28 --> Database Driver Class Initialized
INFO - 2021-02-19 18:53:28 --> Config Class Initialized
INFO - 2021-02-19 18:53:28 --> Hooks Class Initialized
INFO - 2021-02-19 18:53:28 --> Helper loaded: security_helper
DEBUG - 2021-02-19 18:53:28 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:28 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:28 --> URI Class Initialized
INFO - 2021-02-19 18:53:28 --> Router Class Initialized
INFO - 2021-02-19 18:53:28 --> Output Class Initialized
INFO - 2021-02-19 18:53:28 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:53:28 --> Controller Class Initialized
INFO - 2021-02-19 18:53:28 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:28 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:28 --> CSRF cookie sent
INFO - 2021-02-19 18:53:28 --> Input Class Initialized
INFO - 2021-02-19 18:53:28 --> Language Class Initialized
INFO - 2021-02-19 18:53:28 --> Database Driver Class Initialized
INFO - 2021-02-19 18:53:28 --> Language Class Initialized
INFO - 2021-02-19 18:53:28 --> MY_Model class loaded
INFO - 2021-02-19 18:53:28 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:53:28 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:53:28 --> Helper loaded: send_sms_helper
INFO - 2021-02-19 18:53:28 --> Config Class Initialized
INFO - 2021-02-19 18:53:29 --> Loader Class Initialized
INFO - 2021-02-19 18:53:29 --> Helper loaded: url_helper
INFO - 2021-02-19 18:53:29 --> Helper loaded: form_helper
INFO - 2021-02-19 18:53:29 --> Helper loaded: html_helper
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
INFO - 2021-02-19 18:53:29 --> Helper loaded: security_helper
INFO - 2021-02-19 18:53:29 --> Database Driver Class Initialized
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:53:29 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:53:29 --> Database Forge Class Initialized
INFO - 2021-02-19 18:53:29 --> User Agent Class Initialized
INFO - 2021-02-19 18:53:29 --> Final output sent to browser
DEBUG - 2021-02-19 18:53:29 --> Total execution time: 0.8618
INFO - 2021-02-19 18:53:29 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:53:29 --> Controller Class Initialized
INFO - 2021-02-19 18:53:29 --> MY_Model class loaded
INFO - 2021-02-19 18:53:29 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:53:29 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:53:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:53:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:53:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:53:30 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:53:30 --> Database Forge Class Initialized
INFO - 2021-02-19 18:53:30 --> User Agent Class Initialized
INFO - 2021-02-19 18:53:30 --> Final output sent to browser
DEBUG - 2021-02-19 18:53:30 --> Total execution time: 1.9635
INFO - 2021-02-19 18:53:30 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:53:30 --> Controller Class Initialized
INFO - 2021-02-19 18:53:30 --> MY_Model class loaded
INFO - 2021-02-19 18:53:30 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:53:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:53:30 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:53:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:53:31 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:53:31 --> Database Forge Class Initialized
INFO - 2021-02-19 18:53:31 --> User Agent Class Initialized
INFO - 2021-02-19 18:53:31 --> Final output sent to browser
DEBUG - 2021-02-19 18:53:31 --> Total execution time: 2.7172
INFO - 2021-02-19 18:53:31 --> Config Class Initialized
INFO - 2021-02-19 18:53:31 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:53:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:31 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:31 --> URI Class Initialized
INFO - 2021-02-19 18:53:31 --> Router Class Initialized
INFO - 2021-02-19 18:53:31 --> Output Class Initialized
INFO - 2021-02-19 18:53:31 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:32 --> CSRF cookie sent
INFO - 2021-02-19 18:53:32 --> Input Class Initialized
INFO - 2021-02-19 18:53:32 --> Language Class Initialized
ERROR - 2021-02-19 18:53:32 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:53:32 --> Config Class Initialized
INFO - 2021-02-19 18:53:32 --> Config Class Initialized
INFO - 2021-02-19 18:53:32 --> Hooks Class Initialized
INFO - 2021-02-19 18:53:32 --> Config Class Initialized
INFO - 2021-02-19 18:53:32 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:53:32 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:32 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:32 --> URI Class Initialized
DEBUG - 2021-02-19 18:53:32 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:32 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:32 --> URI Class Initialized
INFO - 2021-02-19 18:53:32 --> Router Class Initialized
INFO - 2021-02-19 18:53:32 --> Hooks Class Initialized
INFO - 2021-02-19 18:53:32 --> Output Class Initialized
INFO - 2021-02-19 18:53:32 --> Router Class Initialized
INFO - 2021-02-19 18:53:32 --> Security Class Initialized
INFO - 2021-02-19 18:53:32 --> Output Class Initialized
DEBUG - 2021-02-19 18:53:32 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:32 --> CSRF cookie sent
INFO - 2021-02-19 18:53:32 --> Input Class Initialized
INFO - 2021-02-19 18:53:32 --> Security Class Initialized
INFO - 2021-02-19 18:53:32 --> Language Class Initialized
DEBUG - 2021-02-19 18:53:32 --> Global POST, GET and COOKIE data sanitized
ERROR - 2021-02-19 18:53:32 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:53:32 --> CSRF cookie sent
INFO - 2021-02-19 18:53:32 --> Input Class Initialized
INFO - 2021-02-19 18:53:32 --> Language Class Initialized
ERROR - 2021-02-19 18:53:32 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:53:32 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:32 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:33 --> URI Class Initialized
INFO - 2021-02-19 18:53:33 --> Router Class Initialized
INFO - 2021-02-19 18:53:33 --> Output Class Initialized
INFO - 2021-02-19 18:53:33 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:33 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:33 --> CSRF cookie sent
INFO - 2021-02-19 18:53:33 --> Input Class Initialized
INFO - 2021-02-19 18:53:33 --> Language Class Initialized
ERROR - 2021-02-19 18:53:33 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:53:46 --> Config Class Initialized
INFO - 2021-02-19 18:53:46 --> Config Class Initialized
INFO - 2021-02-19 18:53:46 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:53:46 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:46 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:46 --> URI Class Initialized
INFO - 2021-02-19 18:53:46 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:53:46 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:53:46 --> Utf8 Class Initialized
INFO - 2021-02-19 18:53:46 --> URI Class Initialized
INFO - 2021-02-19 18:53:46 --> Router Class Initialized
INFO - 2021-02-19 18:53:46 --> Router Class Initialized
INFO - 2021-02-19 18:53:46 --> Output Class Initialized
INFO - 2021-02-19 18:53:46 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:46 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:46 --> Output Class Initialized
INFO - 2021-02-19 18:53:46 --> Security Class Initialized
DEBUG - 2021-02-19 18:53:46 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:53:46 --> CSRF cookie sent
INFO - 2021-02-19 18:53:46 --> CSRF token verified
INFO - 2021-02-19 18:53:46 --> Input Class Initialized
INFO - 2021-02-19 18:53:46 --> Language Class Initialized
INFO - 2021-02-19 18:53:46 --> CSRF cookie sent
INFO - 2021-02-19 18:53:46 --> Language Class Initialized
INFO - 2021-02-19 18:53:46 --> Config Class Initialized
INFO - 2021-02-19 18:53:46 --> Loader Class Initialized
INFO - 2021-02-19 18:53:46 --> Helper loaded: url_helper
INFO - 2021-02-19 18:53:46 --> CSRF token verified
INFO - 2021-02-19 18:53:46 --> Input Class Initialized
INFO - 2021-02-19 18:53:46 --> Language Class Initialized
INFO - 2021-02-19 18:53:46 --> Language Class Initialized
INFO - 2021-02-19 18:53:46 --> Config Class Initialized
INFO - 2021-02-19 18:53:46 --> Helper loaded: form_helper
INFO - 2021-02-19 18:53:46 --> Loader Class Initialized
INFO - 2021-02-19 18:53:46 --> Helper loaded: url_helper
INFO - 2021-02-19 18:53:46 --> Helper loaded: form_helper
INFO - 2021-02-19 18:53:46 --> Helper loaded: html_helper
INFO - 2021-02-19 18:53:46 --> Helper loaded: security_helper
INFO - 2021-02-19 18:53:46 --> Helper loaded: html_helper
INFO - 2021-02-19 18:53:46 --> Database Driver Class Initialized
INFO - 2021-02-19 18:53:46 --> Helper loaded: security_helper
INFO - 2021-02-19 18:53:46 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:53:46 --> Controller Class Initialized
INFO - 2021-02-19 18:53:46 --> MY_Model class loaded
INFO - 2021-02-19 18:53:46 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:53:46 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:53:46 --> Database Driver Class Initialized
INFO - 2021-02-19 18:53:46 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:53:46 --> Query error: Not unique table/alias: 'c' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `c` ON `c`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:53:46 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:53:46 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:53:46 --> Controller Class Initialized
INFO - 2021-02-19 18:53:47 --> MY_Model class loaded
INFO - 2021-02-19 18:53:47 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:53:47 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:53:47 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:53:47 --> Query error: Not unique table/alias: 'c' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `c` ON `c`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:53:47 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:55:28 --> Config Class Initialized
INFO - 2021-02-19 18:55:28 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:55:28 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:28 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:28 --> URI Class Initialized
INFO - 2021-02-19 18:55:28 --> Router Class Initialized
INFO - 2021-02-19 18:55:28 --> Output Class Initialized
INFO - 2021-02-19 18:55:28 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:28 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:55:28 --> CSRF cookie sent
INFO - 2021-02-19 18:55:28 --> Input Class Initialized
INFO - 2021-02-19 18:55:28 --> Language Class Initialized
INFO - 2021-02-19 18:55:28 --> Language Class Initialized
INFO - 2021-02-19 18:55:28 --> Config Class Initialized
INFO - 2021-02-19 18:55:28 --> Loader Class Initialized
INFO - 2021-02-19 18:55:28 --> Helper loaded: url_helper
INFO - 2021-02-19 18:55:28 --> Helper loaded: form_helper
INFO - 2021-02-19 18:55:28 --> Helper loaded: html_helper
INFO - 2021-02-19 18:55:28 --> Helper loaded: security_helper
INFO - 2021-02-19 18:55:28 --> Database Driver Class Initialized
INFO - 2021-02-19 18:55:28 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:55:28 --> Controller Class Initialized
INFO - 2021-02-19 18:55:28 --> MY_Model class loaded
INFO - 2021-02-19 18:55:28 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:55:28 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:55:28 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:55:29 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:55:29 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:55:29 --> Database Forge Class Initialized
INFO - 2021-02-19 18:55:29 --> User Agent Class Initialized
INFO - 2021-02-19 18:55:29 --> Config Class Initialized
INFO - 2021-02-19 18:55:29 --> Config Class Initialized
INFO - 2021-02-19 18:55:29 --> Hooks Class Initialized
INFO - 2021-02-19 18:55:29 --> Final output sent to browser
DEBUG - 2021-02-19 18:55:29 --> Total execution time: 1.2460
DEBUG - 2021-02-19 18:55:29 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:29 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:29 --> URI Class Initialized
INFO - 2021-02-19 18:55:29 --> Hooks Class Initialized
INFO - 2021-02-19 18:55:29 --> Router Class Initialized
INFO - 2021-02-19 18:55:29 --> Output Class Initialized
INFO - 2021-02-19 18:55:29 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:29 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:55:29 --> CSRF cookie sent
INFO - 2021-02-19 18:55:29 --> Input Class Initialized
INFO - 2021-02-19 18:55:29 --> Language Class Initialized
ERROR - 2021-02-19 18:55:29 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:55:29 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:29 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:29 --> URI Class Initialized
INFO - 2021-02-19 18:55:29 --> Router Class Initialized
INFO - 2021-02-19 18:55:29 --> Output Class Initialized
INFO - 2021-02-19 18:55:29 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:29 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:55:29 --> CSRF cookie sent
INFO - 2021-02-19 18:55:29 --> Input Class Initialized
INFO - 2021-02-19 18:55:29 --> Language Class Initialized
ERROR - 2021-02-19 18:55:29 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:55:29 --> Config Class Initialized
INFO - 2021-02-19 18:55:29 --> Config Class Initialized
INFO - 2021-02-19 18:55:29 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:55:29 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:29 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:29 --> URI Class Initialized
INFO - 2021-02-19 18:55:29 --> Hooks Class Initialized
INFO - 2021-02-19 18:55:29 --> Router Class Initialized
INFO - 2021-02-19 18:55:29 --> Output Class Initialized
INFO - 2021-02-19 18:55:29 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:29 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:55:29 --> CSRF cookie sent
INFO - 2021-02-19 18:55:29 --> Input Class Initialized
INFO - 2021-02-19 18:55:29 --> Language Class Initialized
ERROR - 2021-02-19 18:55:29 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:55:29 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:29 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:29 --> URI Class Initialized
INFO - 2021-02-19 18:55:29 --> Router Class Initialized
INFO - 2021-02-19 18:55:29 --> Output Class Initialized
INFO - 2021-02-19 18:55:29 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:29 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:55:29 --> CSRF cookie sent
INFO - 2021-02-19 18:55:29 --> Input Class Initialized
INFO - 2021-02-19 18:55:29 --> Language Class Initialized
ERROR - 2021-02-19 18:55:30 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:55:38 --> Config Class Initialized
INFO - 2021-02-19 18:55:38 --> Config Class Initialized
INFO - 2021-02-19 18:55:38 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:55:38 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:38 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:38 --> URI Class Initialized
INFO - 2021-02-19 18:55:38 --> Hooks Class Initialized
INFO - 2021-02-19 18:55:38 --> Router Class Initialized
INFO - 2021-02-19 18:55:38 --> Output Class Initialized
INFO - 2021-02-19 18:55:38 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:38 --> Global POST, GET and COOKIE data sanitized
DEBUG - 2021-02-19 18:55:38 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:55:38 --> Utf8 Class Initialized
INFO - 2021-02-19 18:55:38 --> URI Class Initialized
INFO - 2021-02-19 18:55:38 --> Router Class Initialized
INFO - 2021-02-19 18:55:38 --> CSRF cookie sent
INFO - 2021-02-19 18:55:38 --> Output Class Initialized
INFO - 2021-02-19 18:55:38 --> Security Class Initialized
DEBUG - 2021-02-19 18:55:38 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:55:38 --> CSRF cookie sent
INFO - 2021-02-19 18:55:38 --> CSRF token verified
INFO - 2021-02-19 18:55:38 --> Input Class Initialized
INFO - 2021-02-19 18:55:38 --> Language Class Initialized
INFO - 2021-02-19 18:55:38 --> CSRF token verified
INFO - 2021-02-19 18:55:38 --> Language Class Initialized
INFO - 2021-02-19 18:55:38 --> Config Class Initialized
INFO - 2021-02-19 18:55:38 --> Loader Class Initialized
INFO - 2021-02-19 18:55:38 --> Helper loaded: url_helper
INFO - 2021-02-19 18:55:38 --> Input Class Initialized
INFO - 2021-02-19 18:55:38 --> Helper loaded: form_helper
INFO - 2021-02-19 18:55:38 --> Helper loaded: html_helper
INFO - 2021-02-19 18:55:38 --> Helper loaded: security_helper
INFO - 2021-02-19 18:55:38 --> Database Driver Class Initialized
INFO - 2021-02-19 18:55:38 --> Language Class Initialized
INFO - 2021-02-19 18:55:38 --> Language Class Initialized
INFO - 2021-02-19 18:55:38 --> Config Class Initialized
INFO - 2021-02-19 18:55:38 --> Loader Class Initialized
INFO - 2021-02-19 18:55:38 --> Helper loaded: url_helper
INFO - 2021-02-19 18:55:38 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:55:38 --> Controller Class Initialized
INFO - 2021-02-19 18:55:38 --> MY_Model class loaded
INFO - 2021-02-19 18:55:38 --> Helper loaded: form_helper
INFO - 2021-02-19 18:55:38 --> Helper loaded: html_helper
INFO - 2021-02-19 18:55:38 --> Helper loaded: security_helper
INFO - 2021-02-19 18:55:38 --> Model "MY_Model" initialized
INFO - 2021-02-19 18:55:38 --> Database Driver Class Initialized
DEBUG - 2021-02-19 18:55:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:55:38 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:55:38 --> Query error: Unknown column 'ch.call_status_id' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:55:38 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:55:38 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:55:38 --> Controller Class Initialized
INFO - 2021-02-19 18:55:38 --> MY_Model class loaded
INFO - 2021-02-19 18:55:38 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:55:38 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:55:38 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:55:38 --> Query error: Unknown column 'ch.call_status_id' in 'field list' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, ch.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:55:38 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:56:24 --> Config Class Initialized
INFO - 2021-02-19 18:56:24 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:56:24 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:24 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:24 --> URI Class Initialized
INFO - 2021-02-19 18:56:24 --> Router Class Initialized
INFO - 2021-02-19 18:56:24 --> Output Class Initialized
INFO - 2021-02-19 18:56:24 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:24 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:24 --> CSRF cookie sent
INFO - 2021-02-19 18:56:24 --> Input Class Initialized
INFO - 2021-02-19 18:56:24 --> Language Class Initialized
INFO - 2021-02-19 18:56:24 --> Language Class Initialized
INFO - 2021-02-19 18:56:24 --> Config Class Initialized
INFO - 2021-02-19 18:56:24 --> Loader Class Initialized
INFO - 2021-02-19 18:56:24 --> Helper loaded: url_helper
INFO - 2021-02-19 18:56:24 --> Helper loaded: form_helper
INFO - 2021-02-19 18:56:24 --> Helper loaded: html_helper
INFO - 2021-02-19 18:56:24 --> Helper loaded: security_helper
INFO - 2021-02-19 18:56:24 --> Database Driver Class Initialized
INFO - 2021-02-19 18:56:24 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:56:24 --> Controller Class Initialized
INFO - 2021-02-19 18:56:24 --> MY_Model class loaded
INFO - 2021-02-19 18:56:24 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:56:24 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:56:24 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:56:24 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:56:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:56:25 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:56:25 --> Database Forge Class Initialized
INFO - 2021-02-19 18:56:25 --> User Agent Class Initialized
INFO - 2021-02-19 18:56:25 --> Final output sent to browser
DEBUG - 2021-02-19 18:56:25 --> Total execution time: 1.3013
INFO - 2021-02-19 18:56:25 --> Config Class Initialized
INFO - 2021-02-19 18:56:25 --> Config Class Initialized
INFO - 2021-02-19 18:56:25 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:56:25 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:25 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:25 --> URI Class Initialized
INFO - 2021-02-19 18:56:25 --> Hooks Class Initialized
INFO - 2021-02-19 18:56:25 --> Router Class Initialized
INFO - 2021-02-19 18:56:25 --> Output Class Initialized
INFO - 2021-02-19 18:56:25 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:25 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:25 --> CSRF cookie sent
INFO - 2021-02-19 18:56:25 --> Input Class Initialized
INFO - 2021-02-19 18:56:25 --> Language Class Initialized
ERROR - 2021-02-19 18:56:25 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:56:25 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:25 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:25 --> URI Class Initialized
INFO - 2021-02-19 18:56:25 --> Router Class Initialized
INFO - 2021-02-19 18:56:25 --> Output Class Initialized
INFO - 2021-02-19 18:56:25 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:25 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:25 --> CSRF cookie sent
INFO - 2021-02-19 18:56:25 --> Input Class Initialized
INFO - 2021-02-19 18:56:25 --> Language Class Initialized
ERROR - 2021-02-19 18:56:25 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:56:25 --> Config Class Initialized
INFO - 2021-02-19 18:56:25 --> Config Class Initialized
INFO - 2021-02-19 18:56:25 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:56:25 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:25 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:25 --> URI Class Initialized
INFO - 2021-02-19 18:56:25 --> Hooks Class Initialized
INFO - 2021-02-19 18:56:25 --> Router Class Initialized
INFO - 2021-02-19 18:56:25 --> Output Class Initialized
INFO - 2021-02-19 18:56:25 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:25 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:25 --> CSRF cookie sent
INFO - 2021-02-19 18:56:25 --> Input Class Initialized
INFO - 2021-02-19 18:56:25 --> Language Class Initialized
ERROR - 2021-02-19 18:56:25 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:56:25 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:25 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:25 --> URI Class Initialized
INFO - 2021-02-19 18:56:25 --> Router Class Initialized
INFO - 2021-02-19 18:56:25 --> Output Class Initialized
INFO - 2021-02-19 18:56:25 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:25 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:25 --> CSRF cookie sent
INFO - 2021-02-19 18:56:25 --> Input Class Initialized
INFO - 2021-02-19 18:56:25 --> Language Class Initialized
ERROR - 2021-02-19 18:56:25 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:56:32 --> Config Class Initialized
INFO - 2021-02-19 18:56:32 --> Config Class Initialized
INFO - 2021-02-19 18:56:32 --> Hooks Class Initialized
INFO - 2021-02-19 18:56:32 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:56:32 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:32 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:32 --> URI Class Initialized
INFO - 2021-02-19 18:56:32 --> Router Class Initialized
INFO - 2021-02-19 18:56:32 --> Output Class Initialized
INFO - 2021-02-19 18:56:32 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:32 --> Global POST, GET and COOKIE data sanitized
DEBUG - 2021-02-19 18:56:32 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:32 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:32 --> URI Class Initialized
INFO - 2021-02-19 18:56:32 --> Router Class Initialized
INFO - 2021-02-19 18:56:32 --> CSRF cookie sent
INFO - 2021-02-19 18:56:32 --> Output Class Initialized
INFO - 2021-02-19 18:56:32 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:32 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:32 --> CSRF cookie sent
INFO - 2021-02-19 18:56:32 --> CSRF token verified
INFO - 2021-02-19 18:56:32 --> Input Class Initialized
INFO - 2021-02-19 18:56:32 --> Language Class Initialized
INFO - 2021-02-19 18:56:32 --> CSRF token verified
INFO - 2021-02-19 18:56:32 --> Language Class Initialized
INFO - 2021-02-19 18:56:32 --> Config Class Initialized
INFO - 2021-02-19 18:56:32 --> Loader Class Initialized
INFO - 2021-02-19 18:56:32 --> Input Class Initialized
INFO - 2021-02-19 18:56:32 --> Language Class Initialized
INFO - 2021-02-19 18:56:32 --> Language Class Initialized
INFO - 2021-02-19 18:56:32 --> Config Class Initialized
INFO - 2021-02-19 18:56:32 --> Loader Class Initialized
INFO - 2021-02-19 18:56:32 --> Helper loaded: url_helper
INFO - 2021-02-19 18:56:32 --> Helper loaded: form_helper
INFO - 2021-02-19 18:56:32 --> Helper loaded: html_helper
INFO - 2021-02-19 18:56:32 --> Helper loaded: security_helper
INFO - 2021-02-19 18:56:32 --> Helper loaded: url_helper
INFO - 2021-02-19 18:56:32 --> Database Driver Class Initialized
INFO - 2021-02-19 18:56:32 --> Helper loaded: form_helper
INFO - 2021-02-19 18:56:32 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:56:32 --> Controller Class Initialized
INFO - 2021-02-19 18:56:32 --> MY_Model class loaded
INFO - 2021-02-19 18:56:32 --> Model "MY_Model" initialized
INFO - 2021-02-19 18:56:32 --> Helper loaded: html_helper
DEBUG - 2021-02-19 18:56:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:56:32 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:56:32 --> Query error: Unknown column 'c.area_id' in 'on clause' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, adh2.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:56:32 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:56:32 --> Helper loaded: security_helper
INFO - 2021-02-19 18:56:32 --> Database Driver Class Initialized
INFO - 2021-02-19 18:56:32 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:56:32 --> Controller Class Initialized
INFO - 2021-02-19 18:56:32 --> MY_Model class loaded
INFO - 2021-02-19 18:56:32 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:56:32 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:56:32 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:56:32 --> Query error: Unknown column 'c.area_id' in 'on clause' - Invalid query: SELECT p.patient_id, p.name_1, p.name_2, p.mobile_1, sc.sub_comment_id, sc.reason, p.test_done_by_coupon_code, p.mobile_2, p.`type`, p.patient_age, p.patient_gender, p.patient_code, p.lab_test, d.doctor_name, d.doctor_mobile, c.city_name AS doctor_city_name, c.city_id as doctor_city, l.language_name, m.users_name, citi.city_id as patient_city_id, l.language_id, m.users_mobile, ad.address_line, ad.pincode, pt.hba1c_result, pt.tc_result, p.test_sms_send_to_mr, bc.booklet_code, bc.booklet_code_id, pt.ldl_result, pt.hdl_result, pt.tg_result, tt.test_type, pt.test_type_id, bc.doctor_id, pcc.diet_coupon_code_id, pt.coupon_code_id, dcc.diet_coupon_code, cc.coupon_code, asm.users_name AS asm, rsm.users_name AS rsm, zsm.users_name AS zsm, ct.city_id, ct.city_name AS city_name, a.area_name, r.region_name, z.zone_name, MAX(miss_his.insert_dt) as missCallDate, cs.status_name, adh2.call_status_id, p.insert_dt as miss_call_date, adh2.comment, adh2.insert_dt as commentUpdateDate, ( SELECT COUNT(ch1.call_history_id) FROM call_history ch1 WHERE ch1.mobile = p.mobile_1 AND ch1.call_status_id IS NOT NULL GROUP BY ch1.mobile )AS total_calls
FROM `revisite_call_history` `adh2`
LEFT JOIN `patient` `p` ON `p`.`patient_id` = `adh2`.`patient_id`
LEFT JOIN `patient_doctor` `pd` ON `pd`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `c` ON `c`.`city_id` = `pd`.`city_id`
LEFT JOIN `address` `ad` ON `ad`.`patient_id` = `p`.`patient_id`
LEFT JOIN `cities` `citi` ON `citi`.`city_id` = ad.city
LEFT JOIN `patient_test` `pt` ON `pt`.`patient_id` = `p`.`patient_id`
LEFT JOIN `test_type` `tt` ON `tt`.`test_type_id` =`pt`.`test_type_id`
LEFT JOIN `patient_coupon_code` `pcc` ON `pcc`.`patient_id` = `p`.`patient_id`
LEFT JOIN `diet_coupon_code` `dcc` ON `dcc`.`diet_coupon_code_id` = `pcc`.`diet_coupon_code_id`
LEFT JOIN `booklet_code` `bc` ON `bc`.`booklet_code_id` = `dcc`.`booklet_code_id`
LEFT JOIN `doctor` `d` ON `d`.`doctor_id` = `bc`.`doctor_id`
LEFT JOIN `call_status` `cs` ON `cs`.`status_id` = `adh2`.`call_status_id`
LEFT JOIN `language` `l` ON `l`.`language_id` = `p`.`language_id`
LEFT JOIN `therapy` ON `therapy`.`patient_id` = `p`.`patient_id`
LEFT JOIN `brand` ON `brand`.`brand_id` = `bc`.`brand_id`
LEFT JOIN `strength` ON `strength`.`strength_id` = `therapy`.`strength`
LEFT JOIN `coupon_code` `cc` ON `cc`.`coupon_code_id` = `pt`.`coupon_code_id`
LEFT JOIN `manpower` `m` ON `d`.`users_id` = `m`.`users_id`
LEFT JOIN `manpower` `asm` ON `asm`.`users_id` = `m`.`users_parent_id`
LEFT JOIN `manpower` `rsm` ON `rsm`.`users_id` = `asm`.`users_parent_id`
LEFT JOIN `manpower` `zsm` ON `zsm`.`users_id` = `rsm`.`users_parent_id`
LEFT JOIN `city` `ct` ON `ct`.`city_id` = `m`.`users_city_id`
LEFT JOIN `area` `a` ON `a`.`area_id` = `c`.`area_id`
LEFT JOIN `region` `r` ON `r`.`region_id` = `a`.`region_id`
LEFT JOIN `zone` `z` ON `z`.`zone_id` = `r`.`zone_id`
LEFT JOIN `sub_comment` `sc` ON `sc`.`sub_comment_id` = `p`.`sub_comment_id`
LEFT JOIN `miscall_history` `miss_his` ON `miss_his`.`miscall_mobile` = `adh2`.`patient_contact_mobile`
WHERE 
                    adh2.revisite_history_id IN (
                    SELECT MAX(adh.revisite_history_id)
                    FROM revisite_call_history adh
                    GROUP BY adh.patient_id) AND 
                    (`adh2`.`call_status_id` NOT IN (11,5) OR adh2.call_status_id IS NULL)
                
AND `call_days` = 'after'
GROUP BY `adh2`.`patient_id`
ORDER BY `missCallDate` desc
 LIMIT 10
INFO - 2021-02-19 18:56:32 --> Language file loaded: language/english/db_lang.php
INFO - 2021-02-19 18:56:59 --> Config Class Initialized
INFO - 2021-02-19 18:56:59 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:56:59 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:56:59 --> Utf8 Class Initialized
INFO - 2021-02-19 18:56:59 --> URI Class Initialized
INFO - 2021-02-19 18:56:59 --> Router Class Initialized
INFO - 2021-02-19 18:56:59 --> Output Class Initialized
INFO - 2021-02-19 18:56:59 --> Security Class Initialized
DEBUG - 2021-02-19 18:56:59 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:56:59 --> CSRF cookie sent
INFO - 2021-02-19 18:56:59 --> Input Class Initialized
INFO - 2021-02-19 18:56:59 --> Language Class Initialized
INFO - 2021-02-19 18:56:59 --> Language Class Initialized
INFO - 2021-02-19 18:56:59 --> Config Class Initialized
INFO - 2021-02-19 18:56:59 --> Loader Class Initialized
INFO - 2021-02-19 18:56:59 --> Helper loaded: url_helper
INFO - 2021-02-19 18:56:59 --> Helper loaded: form_helper
INFO - 2021-02-19 18:56:59 --> Helper loaded: html_helper
INFO - 2021-02-19 18:57:00 --> Helper loaded: security_helper
INFO - 2021-02-19 18:57:00 --> Database Driver Class Initialized
INFO - 2021-02-19 18:57:00 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:57:00 --> Controller Class Initialized
INFO - 2021-02-19 18:57:00 --> MY_Model class loaded
INFO - 2021-02-19 18:57:00 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:57:00 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:57:00 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:57:00 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:57:00 --> Database Forge Class Initialized
INFO - 2021-02-19 18:57:00 --> User Agent Class Initialized
INFO - 2021-02-19 18:57:00 --> Final output sent to browser
INFO - 2021-02-19 18:57:00 --> Config Class Initialized
INFO - 2021-02-19 18:57:00 --> Hooks Class Initialized
INFO - 2021-02-19 18:57:00 --> Config Class Initialized
INFO - 2021-02-19 18:57:00 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:00 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:00 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:00 --> URI Class Initialized
DEBUG - 2021-02-19 18:57:00 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:00 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:00 --> URI Class Initialized
INFO - 2021-02-19 18:57:00 --> Router Class Initialized
DEBUG - 2021-02-19 18:57:00 --> Total execution time: 1.2754
INFO - 2021-02-19 18:57:00 --> Output Class Initialized
INFO - 2021-02-19 18:57:00 --> Router Class Initialized
INFO - 2021-02-19 18:57:00 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:00 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:00 --> CSRF cookie sent
INFO - 2021-02-19 18:57:00 --> Output Class Initialized
INFO - 2021-02-19 18:57:00 --> Input Class Initialized
INFO - 2021-02-19 18:57:00 --> Language Class Initialized
INFO - 2021-02-19 18:57:00 --> Security Class Initialized
ERROR - 2021-02-19 18:57:00 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:57:00 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:00 --> CSRF cookie sent
INFO - 2021-02-19 18:57:00 --> Input Class Initialized
INFO - 2021-02-19 18:57:00 --> Language Class Initialized
ERROR - 2021-02-19 18:57:00 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:57:01 --> Config Class Initialized
INFO - 2021-02-19 18:57:01 --> Config Class Initialized
INFO - 2021-02-19 18:57:01 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:01 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:01 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:01 --> URI Class Initialized
INFO - 2021-02-19 18:57:01 --> Router Class Initialized
INFO - 2021-02-19 18:57:01 --> Output Class Initialized
INFO - 2021-02-19 18:57:01 --> Security Class Initialized
INFO - 2021-02-19 18:57:01 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:01 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:01 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:01 --> URI Class Initialized
INFO - 2021-02-19 18:57:01 --> Router Class Initialized
INFO - 2021-02-19 18:57:01 --> Output Class Initialized
DEBUG - 2021-02-19 18:57:01 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:01 --> CSRF cookie sent
INFO - 2021-02-19 18:57:01 --> Input Class Initialized
INFO - 2021-02-19 18:57:01 --> Language Class Initialized
ERROR - 2021-02-19 18:57:01 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:57:01 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:01 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:01 --> CSRF cookie sent
INFO - 2021-02-19 18:57:01 --> Input Class Initialized
INFO - 2021-02-19 18:57:01 --> Language Class Initialized
ERROR - 2021-02-19 18:57:01 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:57:05 --> Config Class Initialized
INFO - 2021-02-19 18:57:05 --> Config Class Initialized
INFO - 2021-02-19 18:57:05 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:05 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:05 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:05 --> Hooks Class Initialized
INFO - 2021-02-19 18:57:05 --> URI Class Initialized
INFO - 2021-02-19 18:57:05 --> Router Class Initialized
INFO - 2021-02-19 18:57:05 --> Output Class Initialized
INFO - 2021-02-19 18:57:05 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:05 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:05 --> CSRF cookie sent
INFO - 2021-02-19 18:57:05 --> CSRF token verified
INFO - 2021-02-19 18:57:05 --> Input Class Initialized
INFO - 2021-02-19 18:57:05 --> Language Class Initialized
DEBUG - 2021-02-19 18:57:05 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:05 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:05 --> URI Class Initialized
INFO - 2021-02-19 18:57:05 --> Router Class Initialized
INFO - 2021-02-19 18:57:05 --> Language Class Initialized
INFO - 2021-02-19 18:57:05 --> Config Class Initialized
INFO - 2021-02-19 18:57:05 --> Loader Class Initialized
INFO - 2021-02-19 18:57:05 --> Helper loaded: url_helper
INFO - 2021-02-19 18:57:05 --> Output Class Initialized
INFO - 2021-02-19 18:57:05 --> Helper loaded: form_helper
INFO - 2021-02-19 18:57:05 --> Helper loaded: html_helper
INFO - 2021-02-19 18:57:05 --> Helper loaded: security_helper
INFO - 2021-02-19 18:57:05 --> Database Driver Class Initialized
INFO - 2021-02-19 18:57:05 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:05 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:05 --> CSRF cookie sent
INFO - 2021-02-19 18:57:05 --> CSRF token verified
INFO - 2021-02-19 18:57:05 --> Input Class Initialized
INFO - 2021-02-19 18:57:05 --> Language Class Initialized
INFO - 2021-02-19 18:57:05 --> Language Class Initialized
INFO - 2021-02-19 18:57:05 --> Config Class Initialized
INFO - 2021-02-19 18:57:05 --> Loader Class Initialized
INFO - 2021-02-19 18:57:05 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:57:05 --> Controller Class Initialized
INFO - 2021-02-19 18:57:05 --> MY_Model class loaded
INFO - 2021-02-19 18:57:05 --> Model "MY_Model" initialized
INFO - 2021-02-19 18:57:05 --> Helper loaded: url_helper
DEBUG - 2021-02-19 18:57:05 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:57:05 --> Helper loaded: send_sms_helper
INFO - 2021-02-19 18:57:05 --> Helper loaded: form_helper
INFO - 2021-02-19 18:57:05 --> Helper loaded: html_helper
INFO - 2021-02-19 18:57:05 --> Helper loaded: security_helper
INFO - 2021-02-19 18:57:05 --> Database Driver Class Initialized
DEBUG - 2021-02-19 18:57:05 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:57:05 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:57:05 --> Database Forge Class Initialized
INFO - 2021-02-19 18:57:05 --> User Agent Class Initialized
INFO - 2021-02-19 18:57:06 --> Final output sent to browser
DEBUG - 2021-02-19 18:57:06 --> Total execution time: 0.6087
INFO - 2021-02-19 18:57:06 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:57:06 --> Controller Class Initialized
INFO - 2021-02-19 18:57:06 --> MY_Model class loaded
INFO - 2021-02-19 18:57:06 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:57:06 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:57:06 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:57:06 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:57:06 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:57:06 --> Database Forge Class Initialized
INFO - 2021-02-19 18:57:06 --> User Agent Class Initialized
INFO - 2021-02-19 18:57:06 --> Final output sent to browser
DEBUG - 2021-02-19 18:57:06 --> Total execution time: 1.1184
INFO - 2021-02-19 18:57:24 --> Config Class Initialized
INFO - 2021-02-19 18:57:24 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:25 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:25 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:25 --> URI Class Initialized
INFO - 2021-02-19 18:57:25 --> Router Class Initialized
INFO - 2021-02-19 18:57:25 --> Output Class Initialized
INFO - 2021-02-19 18:57:25 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:25 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:25 --> CSRF cookie sent
INFO - 2021-02-19 18:57:25 --> Input Class Initialized
INFO - 2021-02-19 18:57:25 --> Language Class Initialized
INFO - 2021-02-19 18:57:25 --> Language Class Initialized
INFO - 2021-02-19 18:57:25 --> Config Class Initialized
INFO - 2021-02-19 18:57:25 --> Loader Class Initialized
INFO - 2021-02-19 18:57:25 --> Helper loaded: url_helper
INFO - 2021-02-19 18:57:25 --> Helper loaded: form_helper
INFO - 2021-02-19 18:57:25 --> Helper loaded: html_helper
INFO - 2021-02-19 18:57:25 --> Helper loaded: security_helper
INFO - 2021-02-19 18:57:25 --> Database Driver Class Initialized
INFO - 2021-02-19 18:57:25 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:57:25 --> Controller Class Initialized
INFO - 2021-02-19 18:57:25 --> MY_Model class loaded
INFO - 2021-02-19 18:57:25 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:57:25 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:57:25 --> Helper loaded: send_sms_helper
ERROR - 2021-02-19 18:57:25 --> Severity: Notice --> Undefined offset: 1 D:\xampp\htdocs\vilcose_psp\application\modules\patient\controllers\Patient.php 93
ERROR - 2021-02-19 18:57:25 --> 404 Page Not Found: 
INFO - 2021-02-19 18:57:29 --> Config Class Initialized
INFO - 2021-02-19 18:57:29 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:29 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:29 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:30 --> URI Class Initialized
INFO - 2021-02-19 18:57:30 --> Router Class Initialized
INFO - 2021-02-19 18:57:30 --> Output Class Initialized
INFO - 2021-02-19 18:57:30 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:30 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:30 --> CSRF cookie sent
INFO - 2021-02-19 18:57:30 --> Input Class Initialized
INFO - 2021-02-19 18:57:30 --> Language Class Initialized
INFO - 2021-02-19 18:57:30 --> Language Class Initialized
INFO - 2021-02-19 18:57:30 --> Config Class Initialized
INFO - 2021-02-19 18:57:30 --> Loader Class Initialized
INFO - 2021-02-19 18:57:30 --> Helper loaded: url_helper
INFO - 2021-02-19 18:57:30 --> Helper loaded: form_helper
INFO - 2021-02-19 18:57:30 --> Helper loaded: html_helper
INFO - 2021-02-19 18:57:30 --> Helper loaded: security_helper
INFO - 2021-02-19 18:57:30 --> Database Driver Class Initialized
INFO - 2021-02-19 18:57:30 --> Session: Class initialized using 'files' driver.
INFO - 2021-02-19 18:57:30 --> Controller Class Initialized
INFO - 2021-02-19 18:57:30 --> MY_Model class loaded
INFO - 2021-02-19 18:57:30 --> Model "MY_Model" initialized
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/models/Mdl_patient.php
INFO - 2021-02-19 18:57:30 --> Helper loaded: send_sms_helper
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\controllers/../modules/template/controllers/Template.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/loader.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/top-bar.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/admin/sidebar.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/action-btns.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/patient/views/records.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/table-listing.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/popup/csv-popup-box.php
DEBUG - 2021-02-19 18:57:30 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/get_post_modal.php
DEBUG - 2021-02-19 18:57:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/components/container/lists.php
DEBUG - 2021-02-19 18:57:31 --> File loaded: D:\xampp\htdocs\vilcose_psp\application\modules/template/views/admin.php
DEBUG - 2021-02-19 18:57:31 --> Config file loaded: D:\xampp\htdocs\vilcose_psp\application\config/usertracking.php
INFO - 2021-02-19 18:57:31 --> Database Forge Class Initialized
INFO - 2021-02-19 18:57:31 --> User Agent Class Initialized
INFO - 2021-02-19 18:57:31 --> Final output sent to browser
INFO - 2021-02-19 18:57:31 --> Config Class Initialized
INFO - 2021-02-19 18:57:31 --> Hooks Class Initialized
INFO - 2021-02-19 18:57:31 --> Config Class Initialized
INFO - 2021-02-19 18:57:31 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:31 --> Total execution time: 1.3470
DEBUG - 2021-02-19 18:57:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:31 --> Utf8 Class Initialized
DEBUG - 2021-02-19 18:57:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:31 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:31 --> URI Class Initialized
INFO - 2021-02-19 18:57:31 --> URI Class Initialized
INFO - 2021-02-19 18:57:31 --> Router Class Initialized
INFO - 2021-02-19 18:57:31 --> Router Class Initialized
INFO - 2021-02-19 18:57:31 --> Output Class Initialized
INFO - 2021-02-19 18:57:31 --> Output Class Initialized
INFO - 2021-02-19 18:57:31 --> Security Class Initialized
INFO - 2021-02-19 18:57:31 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:31 --> CSRF cookie sent
INFO - 2021-02-19 18:57:31 --> Input Class Initialized
INFO - 2021-02-19 18:57:31 --> Language Class Initialized
ERROR - 2021-02-19 18:57:31 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:57:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:31 --> CSRF cookie sent
INFO - 2021-02-19 18:57:31 --> Input Class Initialized
INFO - 2021-02-19 18:57:31 --> Language Class Initialized
ERROR - 2021-02-19 18:57:31 --> 404 Page Not Found: /index
INFO - 2021-02-19 18:57:31 --> Config Class Initialized
INFO - 2021-02-19 18:57:31 --> Config Class Initialized
INFO - 2021-02-19 18:57:31 --> Hooks Class Initialized
DEBUG - 2021-02-19 18:57:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:31 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:31 --> URI Class Initialized
INFO - 2021-02-19 18:57:31 --> Router Class Initialized
INFO - 2021-02-19 18:57:31 --> Hooks Class Initialized
INFO - 2021-02-19 18:57:31 --> Output Class Initialized
INFO - 2021-02-19 18:57:31 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:31 --> CSRF cookie sent
INFO - 2021-02-19 18:57:31 --> Input Class Initialized
INFO - 2021-02-19 18:57:31 --> Language Class Initialized
ERROR - 2021-02-19 18:57:31 --> 404 Page Not Found: /index
DEBUG - 2021-02-19 18:57:31 --> UTF-8 Support Enabled
INFO - 2021-02-19 18:57:31 --> Utf8 Class Initialized
INFO - 2021-02-19 18:57:31 --> URI Class Initialized
INFO - 2021-02-19 18:57:31 --> Router Class Initialized
INFO - 2021-02-19 18:57:31 --> Output Class Initialized
INFO - 2021-02-19 18:57:31 --> Security Class Initialized
DEBUG - 2021-02-19 18:57:31 --> Global POST, GET and COOKIE data sanitized
INFO - 2021-02-19 18:57:31 --> CSRF cookie sent
INFO - 2021-02-19 18:57:31 --> Input Class Initialized
INFO - 2021-02-19 18:57:31 --> Language Class Initialized
ERROR - 2021-02-19 18:57:31 --> 404 Page Not Found: /index
