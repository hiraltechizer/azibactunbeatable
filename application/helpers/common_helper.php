<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('roundup_to_any')) {

	function roundup_to_any($n, $x = 10) {
        return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
    }
}

?>