<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('tiny_url')) {

	function tiny_url($url, ...$params){
		if(empty($url)) { return false; }
		// $ci = &get_instance();
        // $ci->load->database();
        
        $ch = curl_init(); 		
		$timeout = 5; 		
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url); 		
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 		
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout); 		
		$data = curl_exec($ch); 		
		curl_close($ch); 
    
		// $is_success = 1;
		// $output = '';
      
		// $data = [
		// 		'msg_for' => $msg_for,
		// 		'voice_id' => $voice_id,
		// 		'mobile' => $to,
		// 		'is_success' => $is_success,
		// 		'output' => $output,
		// 		'insertdatetime' => date("Y-m-d H:i:s")
		// 	];

		// $ci->db->insert('voice_log', $data);

		return $data;
    }
}