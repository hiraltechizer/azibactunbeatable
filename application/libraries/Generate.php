<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generate
{
    protected $_ci;
    // CodeIgniter instance  	
    function __construct($url = '') {
        $this->_ci =& get_instance();
    }

    function posters($template_path, $path_to_resize_img, $final_save_path) {
        /*Poster width & height calculated to use for placehoder image creation*/
        $poster = imagecreatefrompng($template_path);
       
        //-------------template path------------- 	
       
        $poster_width      = imageSX($poster);
        $poster_height     = imageSY($poster);

        /*Placeholder image created of the same size of the poster to contain the doctor image*/
        
        $placeholder_img   = imagecreatetruecolor($poster_width , $poster_height);
        $white = imagecolorallocate($placeholder_img, 255, 255, 255);
        imagefill($placeholder_img, 0, 0, $white);
       
        $revised_doc_image = imagecreatefrompng($path_to_resize_img);
       
       
        $doctor_img_width  = imageSX($revised_doc_image);
        $doctor_img_height = imageSY($revised_doc_image);

        /*Positions for the appropriate location after merge*/
        $pos_x             = 390;
        $pos_y             = 152;
        //echo $placeholder_img;exit;
        imageCopyMerge($poster,$revised_doc_image, $pos_x, $pos_y, 0,0, $doctor_img_width, $doctor_img_height ,100);
        
        /*Saving the image with doctor image merged on placeholder*/

        imagepng($placeholder_img, 'uploads/doc_placeholder.png');

        imagedestroy($placeholder_img);
        /*Getting doctor merged image with placehoder to merge with poster*/
      //  exit;

        $canvas_img = imagecreatefrompng("uploads/doc_placeholder.png");
       

        imagecopy($canvas_img,$poster, 0 , 0 , 0 , 0 ,$poster_width ,$poster_height);
       
        // unlink("uploads/doc_placeholder.png");

        /*setting the header*/
        // header("Content-Type: image/png");
        imagejpeg($canvas_img, $final_save_path);
        imagedestroy($canvas_img);
    }
    
    
    function resize($doctor_img_path, $path_to_resize_img){
        
        $last_part_file = explode("/", $doctor_img_path);
        $image =  end($last_part_file);
        $temp = explode(".", $image);
        $file_extension = end($temp);
        
        $width          = 646;
        $height         = 646;
        
        /*Doctor image resized to 256  */
        if($file_extension == 'png' || $file_extension == 'PNG' ){
            $doctor_image  = imagecreatefrompng($doctor_img_path);
        }else{
            $doctor_image  = imagecreatefromjpeg($doctor_img_path);
        }
        // header('Content-Type: image/png');
        // imagepng($doctor_image);exit;
        //---------doctor-image------------ 	
        $og_doc_height = imagesy($doctor_image);
        $og_doc_width = imagesx($doctor_image);


        // Calculate the scaling we need to do to fit the image inside our frame
			$scale      = min($width/$og_doc_width, $height/$og_doc_height);

			// Get the new dimensions
			$new_width  = ceil($scale*$og_doc_width);
            $new_height = ceil($scale*$og_doc_height);
            

        // $height = (($og_doc_height * $width) / $og_doc_width);

        $resized_doc_image = imagecreatetruecolor($width, $height);

        $white = imagecolorallocate($resized_doc_image, 255, 255, 255);
        imagefill($resized_doc_image, 0, 0, $white);
        

       
       imagecopyresized($resized_doc_image, $doctor_image, 0, 0, 0, 0, $width, $height, $og_doc_width, $og_doc_height);
       // imagecopyresampled($resized_doc_image, $doctor_image, 0, 0, 0, 0, $width, $height, $og_doc_width, $og_doc_height);

        imagejpeg($resized_doc_image, $path_to_resize_img);
        // header('Content-Type: image/jpeg');
        // imagejpeg($resized_doc_image);exit;
        imagedestroy($resized_doc_image);
        if (file_exists($doctor_img_path) && file_exists($path_to_resize_img)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>