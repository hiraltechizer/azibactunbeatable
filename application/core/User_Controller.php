<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); 
abstract class User_Controller extends Generic_Controller{
	private $scripts;
    private $controller; 
    private $module; 
    private $settings;
    private $permissions;
    private $template;
    private $role;
    private $plugins;

    function __construct($module, $controller, $model_name, $settings=[], $scripts = [], $plugins = []){

        parent::__construct();
        $this->load->model($model_name, 'model');

		$admin_config = $this->model->get_records([], 'config', ['config_type', 'config_value']);
		
		$config = [];
		foreach($admin_config as $record){
			$config[$record->config_type] = $record->config_value;
		}

		$this->scripts = $scripts;
		$this->controller = $controller;
        $this->module = $module;
        $this->plugins = $plugins;

        $this->settings['permissions'] = array_key_exists('permissions', $settings) ? $settings['permissions'] : [];
        $this->settings['paginate_index'] = array_key_exists('paginate_index', $settings) ? $settings['paginate_index'] : 3;

        //print_r( $this->settings);die;
        $this->settings['filters'] = array_key_exists('filters', $settings) ? $settings['filters'] : ['column_filters'=> TRUE, 'date_filters'=> TRUE];

        $this->permissions = $this->settings['permissions'];

        $this->data['settings'] = $config; 
        $this->set_defaults();
        
        $this->role = $role =  $this->session->get_field_from_session('role','user');

        if(empty($this->role)) {
            $this->role = $role =  $this->session->get_field_from_session('role');
        }

        $this->data['users_id'] = $users_id =  $this->session->get_field_from_session('user_id','user');

        if(empty($this->data['users_id'])) {
            $this->data['users_id'] = $users_id =  $this->session->get_field_from_session('user_id');
        }
        
        $this->data['imagename'] = base_url().'/web/images/profile.png';
        $this->data['image_flag'] = FALSE;
        
        $this->data['role'] = $this->role;
        $this->template = ( in_array($this->role, ['SA', 'A'])) ? '_admin' : '_user';
    }

    protected function set_defaults($defaults = []) {
        $this->set_default_data($defaults, $this->module, $this->controller);            
    }

    function lists(){
		if( ! $this->session->is_logged_in() ){
			show_error("Forbidden", 403);
		}
        $sfilters = array();
        if($this->data['controller'] == 'prescription') {
            $users_id = $this->session->get_field_from_session('user_id', 'user');
            $sfilters = ['d.users_id' => $users_id];
        }
        
		if($this->data['controller'] == 'sales_performance') {
            $users_id = $this->session->get_field_from_session('user_id', 'user');
            $role =  $this->session->get_field_from_session('role','user');
            if($role == 'MR'){
                $userInfo = $this->model->get_records(['users_id' => $users_id], 'manpower', ['users_city_id','users_id']);
                $sfilters = ['qwt.city_id' => $userInfo[0]->users_city_id ,'qwmwt.mr_id' => $userInfo[0]->users_id];
          }elseif($role == 'ASM' || $role == 'RSM'){
            //   echo $users_id;exit;
            //  $userInfo = $this->model->get_role_wise_city($users_id,strtolower($role));
             $asmInfo = $this->model->get_role_wise_region($users_id,strtolower($role));
             $region_id = $asmInfo[0]['users_region_id'];
             $mrInfo = $this->model->get_role_wise_region_wise_mr($region_id,strtolower($role));
             $mr_ids = $mrInfo[0]['UsersId']; 
            
                if($mr_ids){
                    // $sfilters =  ['qwt.city_id' => $userInfo[0]['city_id']];
                    $sfilters =  ['qwmwt.mr_id' => $mr_ids];
                }else{
                    $sfilters =  array();
                }
            
          }elseif($role == 'ZSM'){
            // $userInfo = $this->model->get_zsm_wise_city($users_id,strtolower($role));
            $rsmInfo = $this->model->get_role_wise_zone($users_id,strtolower($role));
             $zone_id = $rsmInfo[0]['users_zone_id'];
             $mrInfo = $this->model->get_role_wise_zone_wise_mr($zone_id,strtolower($role));
             $mr_ids = $mrInfo[0]['UsersId']; 
            if($rsmInfo){
                $sfilters =  ['qwmwt.mr_id' => $mr_ids];
                // $sfilters =  ['qwt.city_id' => $userInfo[0]['city_id']];
            }else{
                $sfilters =  array();
            }
          }

        }

        $offset = (int) $this->input->post('page');
        // print_r(print_r($_POST)); die();
        // $keywords = !empty($this->input->post('keywords'))?$this->input->post('keywords'):'';

        $post_array = $this->input->post();
        unset($post_array['page']);
        unset($post_array['search']);
        // echo "<pre>";print_r($post_array);exit;
        
		$this->data['collection'] =  $this->model->get_collection($count = FALSE, $sfilters, $post_array, $this->perPage, $offset);
        
        
		$totalRec = count($this->model->get_collection( $count = FALSE, $sfilters, $post_array));
        // echo $this->db->last_query();die;
		// $data_count = $this->model->get_collection($count = FALSE, $sfilters, $post_array , $offset);
		// $totalRec = count($data_count);
        // echo $this->db->last_query();die;
        // echo count($data_count);exit;
        // echo "<pre>";print_r($totalRec);exit;
        
        $this->paginate($this->data['controller'], $totalRec, $this->settings['paginate_index']);
        array_unshift($this->plugins, 'paginate');
        $this->data['plugins'] = $this->plugins;

        if($this->data['controller'] == 'update_patient_diet_chart') {
            $this->data['plugins'] = ['paginate','fancybox'];
        }

        
        /* columns for list & CSV */
        $table_columns = $this->model->get_column_list();
        $csv_columns = $this->model->get_csv_columns();
      
       
       /*   $filter_columns = $this->model->get_filters();
        if(count($filter_columns)) {
            $this->data['show_filters'] = TRUE;
        }
        $this->data['date_filters'] = TRUE; */

        $filter_columns = $this->model->get_filters();

        $this->role = $this->session->get_field_from_session('role');
    
        if(empty($this->role)) {
            $this->role = $this->session->get_field_from_session('role', 'user');
            $this->UId = $this->session->get_field_from_session('user_id', 'user');
        }

       

       // echo $this->role;die;
      //  echo $this->data['controller'];die;

        $this->data['show_filters'] = array_key_exists('show_filters', $this->settings['filters']) ? $this->settings['filters']['show_filters'] : TRUE;
        $this->data['date_filters'] = array_key_exists('date_filters', $this->settings['filters']) ? $this->settings['filters']['date_filters'] : TRUE;
        
        $this->set_view_columns($table_columns, $csv_columns, $filter_columns);
        /* END columns */
        if($this->data['controller'] == 'prescription') {
            $records_view = $this->data['controller'].'/records';
        }else{
            $records_view = $this->data['controller'].'/cards';
        }
        
       // $records_view = $this->data['controller'].'/records';
        if ( in_array($this->role, ['SA', 'A'])) {
            $records_view = $this->data['controller'].'/records';
        }
        $this->data['permissions'] = $this->settings['permissions'];
      //  echo $this->data['controller'];die;
        if ($this->input->post('search') == TRUE) {
                $this->load->view($records_view, $this->data);
            }else
            {
                $this->data['records_view'] = $records_view;
                $this->set_view($this->data, 'template/components/container/lists', $this->template);
            }
        
	}

    function add(){
    	if( ! $this->session->is_logged_in() )
			show_error("Forbidden", 403);

        if(!in_array('add', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        }   

        if($this->data['controller'] == 'patient_diet_chart_create' || $this->data['controller'] == 'update_patient_diet_chart') {
            array_unshift($this->scripts, 'form-submit1.js');            
        } else {
            array_unshift($this->scripts, 'form-submit.js');
        }

		//array_unshift($this->scripts, 'form-submit.js');
		$this->data['js'] = $this->scripts;

        array_unshift($this->plugins, 'select2');
		$this->data['plugins'] = $this->plugins;
		
         $this->data['section_title'] = 'Add '. ucfirst($this->data['module']);

        
        $this->set_view($this->data, 'template/components/container/add', $this->template);
	}

	function edit(){
		if( ! $this->session->is_logged_in() )
			show_error("Forbidden", 403);

        if(!in_array('edit', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

        $uri_string = $this->uri->uri_string();

        if(! strpos($uri_string, 'record')) {
            show_404();
        }

        $revised_url = strstr($uri_string, 'record');

        list($k, $v) = explode('/', $revised_url);
        $array[$k] = $v;

        // $array = $this->uri->uri_to_assoc(); 
        /* print_r($array); 
        die();
	 */
        $tb_alias = $this->model->get_alias();

        $alias = (!empty($tb_alias)) ? $tb_alias : '';
        $table = $this->model->get_table();
        $p_key = $this->model->get_pkey();

        if(!isset($array['record'])) {
			show_404();
		}

		$temp = [];
		foreach($array as $i=>$value) {
			if($i == 'record') {
				$i = $p_key;
				$this->data[$i] = $id = (int) $array['record'];
			}
			$tempKey = (empty($alias)) ? "$table.$i" : "$alias.$i";
			$temp[$tempKey] = $value;
		}

		$this->data['info'] = $this->model->get_collection(FALSE, $temp);

		if(! count($this->data['info']) ){ show_404(); }

		array_unshift($this->plugins, 'select2');
        $this->data['plugins'] = $this->plugins;
       
        if($this->data['controller'] == 'patient_diet_chart_create' || $this->data['controller'] == 'update_patient_diet_chart') {
            array_unshift($this->scripts, 'form-submit1.js');            
        } else {
            array_unshift($this->scripts, 'form-submit.js');
        }
		//array_unshift($this->scripts, 'form-submit.js');
        $this->data['js'] = $this->scripts;

         $this->data['section_title'] = 'Edit '. ucfirst($this->data['module']);
        $this->set_view($this->data, 'template/components/container/edit', $this->template);
	}

	function save(){
		$this->session->is_Ajax_and_logged_in();

        if(!in_array('add', $this->permissions)) {
            echo ['status'=> FALSE, 'message'=> 'This action is not allowed'];
            exit;
        } 

		$result = $this->model->save();
		echo json_encode($result);
	}

	function modify(){
		$this->session->is_Ajax_and_logged_in();

        if(!in_array('edit', $this->permissions)) {
            echo ['status'=> FALSE, 'message'=> 'This action is not allowed']; 
            exit;
        }

        $result = $this->model->modify();
        
        if($result['status']) {
            $result['message'] = 'Record Updated Successfully!';
        }
		echo json_encode($result);
	}

	function remove(){
		$this->session->is_Ajax_and_logged_in();

        if(!in_array('remove', $this->permissions)) {
            echo ['status'=> FALSE, 'message'=> 'This action is not allowed'];
        }

		$response = $this->model->remove();
		echo json_encode($response);
	}
	
	function remove_single_row() {
		if( ! $this->session->is_logged_in() )
            show_error("Forbidden", 403);
            
        if(!in_array('remove', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

		$response = $this->model->remove($this->input->get());
		echo json_encode($response);
	}

	function download(){

		if( ! $this->session->is_logged_in() )
            show_error("Forbidden", 403);
            
        if(!in_array('download', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

        $get_request = $this->input->get();
        $new_request = array_filter($get_request, function($v) { return $v !== ''; } ); 
        $sfilters = [];
        if($this->data['controller'] == 'prescription') {
            $users_id = $this->session->get_field_from_session('user_id', 'user');
            $sfilters = ['d.users_id' => $users_id];
        }

		$data = $this->model->get_collection($count = FALSE, $sfilters, $new_request);
		$fields = $this->model->_format_data_to_export($data);

		$this->download_file($this->data['controller'], $fields);
	}

    function notifcation_display(){
    	if( ! $this->session->is_logged_in() )
			show_error("Forbidden", 403);

    
        $uri_string = $this->uri->uri_string();

        if(! strpos($uri_string, 'view')) {
            show_404();
        }

        $revised_url = strstr($uri_string, 'view');
        // print_r($revised_url); exit;
        list($k, $v) = explode('/', $revised_url);
        $array[$k] = $v;

        // $array = $this->uri->uri_to_assoc(); 
        /* print_r($array); 
        die();
	 */
        $tb_alias = $this->model->get_alias();
        // print_r($tb_alias); exit;
        $alias = (!empty($tb_alias)) ? $tb_alias : '';
        $table = $this->model->get_table();
        $p_key = $this->model->get_pkey();

        if(!isset($array['view'])) {
			show_404();
		}
//   print_r($tb_alias); exit;
		$temp = [];
		foreach($array as $i=>$value) {
            // print_r($i); exit;
			if($i == 'view') {
				$i = $p_key;
				$this->data[$i] = $id = (int) $array['view'];
			}
			$tempKey = (empty($alias)) ? "$table.$i" : "$alias.$i";
			$temp[$tempKey] = $value;
		}
//  print_r($temp); exit;
		$this->data['info'] = $this->model->get_collection(FALSE, $temp);
//  print_r($this->data['info']); exit;
		if(! count($this->data['info']) ){ show_404(); }

		array_unshift($this->plugins, 'select2');
        $this->data['plugins'] = $this->plugins;
       
        if($this->data['controller'] == 'patient_diet_chart_create' || $this->data['controller'] == 'update_patient_diet_chart') {
            array_unshift($this->scripts, 'form-submit1.js');            
        } else {
            array_unshift($this->scripts, 'form-submit.js');
        }
		//array_unshift($this->scripts, 'form-submit.js');
        $this->data['js'] = $this->scripts;

         $this->data['section_title'] = 'View '. ucfirst($this->data['module']);

        $this->set_view($this->data, 'template/components/container/display', $this->template);
	}
}