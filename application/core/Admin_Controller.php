<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); 
abstract class Admin_Controller extends Generic_Controller{
	private $scripts;
    private $controller; 
    private $module; 
    private $settings;
    private $permissions;

    function __construct($module, $controller, $model_name, $settings=[], $scripts = []){
        parent::__construct();

       
        $this->load->model($model_name, 'model');

		$admin_config = $this->model->get_records([], 'config', ['config_type', 'config_value']);
		
		$config = [];
		foreach($admin_config as $record){
			$config[$record->config_type] = $record->config_value;
		}

		$this->scripts = $scripts;
		$this->controller = $controller;
        $this->module = $module;

        $this->settings['permissions'] = array_key_exists('permissions', $settings) ? $settings['permissions'] : [];
        $this->settings['paginate_index'] = array_key_exists('paginate_index', $settings) ? $settings['paginate_index'] : 3;
        $this->settings['filters'] = array_key_exists('filters', $settings) ? $settings['filters'] : ['column_filters'=> TRUE, 'date_filters'=> TRUE];

        /*if($this->controller == 'manpower/ho'){
             $HOInfo = $this->model->get_records(['users_type'=>'HO'],'manpower');
             
            if(count($HOInfo) > 0) {
                unset($this->settings['permissions'][0]);
            }
        } */

        $this->permissions = $this->settings['permissions'];

        $this->data['settings'] = $config; 
		$this->set_defaults();
    }

    protected function set_defaults($defaults = []) {
        $this->set_default_data($defaults, $this->module, $this->controller);            
    }

    function lists(){
        // echo "testing";exit;
		
		if( ! $this->session->is_admin_logged_in() ){
			redirect('admin/login','refresh');
		}

		$sfilters = array();

        $offset = (int) $this->input->post('page');
        //print_r(print_r($_POST)); die();
        // $keywords = !empty($this->input->post('keywords'))?$this->input->post('keywords'):'';

        $post_array = $this->input->post();
        unset($post_array['page']);
        unset($post_array['search']);

        if($this->data['controller'] == 'json_response') {
            $this->data['date_filters'] = FALSE;
            $this->perPage = 100;
           
        }
        
		$this->data['collection'] = $this->model->get_collection($count = FALSE, $sfilters, $post_array, $this->perPage, $offset);
		$totalRec = $this->model->get_collection( $count = TRUE, $sfilters, $post_array);
        
        $this->paginate($this->data['controller'], $totalRec, $this->settings['paginate_index']);
		$this->data['plugins'] = ['paginate'];
        
        /* columns for list & CSV */
        $table_columns = $this->model->get_column_list();
        $csv_columns = $this->model->get_csv_columns();

       
        /* $filter_columns = $this->model->get_filters();
        if(count($filter_columns)) {
            $this->data['show_filters'] = TRUE;
        }
        $this->data['date_filters'] = TRUE; */

        $filter_columns = $this->model->get_filters();
        
        $this->data['show_filters'] = array_key_exists('show_filters', $this->settings['filters']) ? $this->settings['filters']['show_filters'] : TRUE;
        $this->data['date_filters'] = array_key_exists('date_filters', $this->settings['filters']) ? $this->settings['filters']['date_filters'] : TRUE;
        
        $this->set_view_columns($table_columns, $csv_columns, $filter_columns);
        /* END columns */

		
        $records_view = $this->data['controller'].'/records';
        
        $this->data['permissions'] = $this->settings['permissions'];
     	if ($this->input->post('search') == TRUE) {
           
			$this->load->view($records_view, $this->data);
        } 
        else
        {
           	$this->data['records_view'] = $records_view;
			$this->set_view($this->data, 'template/components/container/lists', '_admin');
		}
	}

    function add(){
    	if( ! $this->session->is_admin_logged_in() )
			redirect('admin/login','refresh');

            if(!in_array('add', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        }   

		array_unshift($this->scripts, 'form-submit.js');
		$this->data['js'] = $this->scripts;

		$this->data['plugins'] = ['select2'];
		$this->data['section_title'] = 'Add '. ucfirst($this->data['module']);

        if(($this->data['controller'] == 'quarter_target') || ($this->data['controller'] == 'sale_archive') || ($this->data['controller'] == 'quarter_wise_mr_wise_target')) {
            $this->data['quarter_list']    =   $this->model->get_records([], 'quarter');
        }

        // if(($this->data['controller'] == 'quarter_target') || ($this->data['controller'] == 'sale_archive') || ($this->data['controller'] == 'quarter_wise_mr_wise_target')) {
        //     $this->data['quarter_list']    =   $this->model->get_records([], 'quarter');
        // }


        $this->set_view($this->data, 'template/components/container/add', '_admin');
	}

	function edit(){
		if( ! $this->session->is_admin_logged_in() )
			redirect('admin/login','refresh');

        if(!in_array('edit', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

        $uri_string = $this->uri->uri_string();

        if(! strpos($uri_string, 'record')) {
            show_404();
        }

        $revised_url = strstr($uri_string, 'record');

        list($k, $v) = explode('/', $revised_url);
        $array[$k] = $v;

        // $array = $this->uri->uri_to_assoc(); 
        /* print_r($array); 
        die();
	 */
        $tb_alias = $this->model->get_alias();

        $alias = (!empty($tb_alias)) ? $tb_alias : '';
        $table = $this->model->get_table();
        $p_key = $this->model->get_pkey();

        if(!isset($array['record'])) {
			show_404();
		}

		$temp = [];
		foreach($array as $i=>$value) {
			if($i == 'record') {
				$i = $p_key;
				$this->data[$i] = $id = (int) $array['record'];
			}
			$tempKey = (empty($alias)) ? "$table.$i" : "$alias.$i";
			$temp[$tempKey] = $value;
		}

		$this->data['info'] = $this->model->get_collection(FALSE, $temp);

		if(! count($this->data['info']) ){ show_404(); }

		$this->data['plugins'] = ['select2'];
		array_unshift($this->scripts, 'form-submit.js');
        $this->data['js'] = $this->scripts;

        if($this->data['controller'] == 'master/brand') {
            $this->data['strength_array']    =   $strength_array = $this->model->get_records_in_array(['is_deleted' => '0'], 'strength',['strength_id','strength']);
            
           
             $strengthInfo  = $this->model->get_records(['brand_id' => $id], 'brand_strength', ['strength_id']);

             $temp_strengtharray   =   [];
             if(count($strengthInfo)) {
                    foreach ($strengthInfo as $key => $value) {
                        array_push($temp_strengtharray, $value->strength_id);
                    }
             }

            $this->data['selected_strength'] = $temp_strengtharray;
        }

        if(($this->data['controller'] == 'quarter_target') || ($this->data['controller'] == 'sale_archive') || ($this->data['controller'] == 'quarter_wise_mr_wise_target') ) {
            $this->data['quarter_list']    =   $this->model->get_records([], 'quarter');
        }
        
        $this->data['section_title'] = 'Edit '. ucfirst($this->data['module']);
        $this->set_view($this->data, 'template/components/container/edit', '_admin');
	}

	function save(){
		$this->session->is_Ajax_and_admin_logged_in();

        #print_r($this->permissions);die;
        if(!in_array('add', $this->permissions)) {
            $result =  ['status'=> FALSE, 'message'=> 'This action is not allowed']; 
            //echo ['status'=> FALSE, 'message'=> 'This action is not allowed'];
            echo json_encode($result);
            exit;
        } 

		$result = $this->model->save();
		echo json_encode($result);
	}

	function modify(){
      //  echo '<pre>';
        //print($_POST);die;
		$this->session->is_Ajax_and_admin_logged_in();

        if(!in_array('edit', $this->permissions)) {
            echo ['status'=> FALSE, 'message'=> 'This action is not allowed']; 
            exit;
        }

        $result = $this->model->modify();
        
        if($result['status']) {
            $result['message'] = 'Record Updated Successfully!';
        }
		echo json_encode($result);
	}

	function remove(){
		$this->session->is_Ajax_and_admin_logged_in();

        if(!in_array('remove', $this->permissions)) {
            echo ['status'=> FALSE, 'message'=> 'This action is not allowed'];
        }

		$response = $this->model->remove();
		echo json_encode($response);
	}
	
	function remove_single_row() {
		if( ! $this->session->is_admin_logged_in() )
            redirect('admin/login','refresh');
            
        if(!in_array('remove', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

		$response = $this->model->remove($this->input->get());
		echo json_encode($response);
	}

	function download(){

		if( ! $this->session->is_admin_logged_in() )
            redirect('admin/login','refresh');
            
        if(!in_array('download', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

        $get_request = $this->input->get();
        // print_r($get_request); die();
        $new_request = array_filter($get_request, function($v) { return $v !== ''; } ); 
        
		$data = $this->model->get_collection($count = FALSE, [], $new_request);
		
        $fields = $this->model->_format_data_to_export($data);

		$this->download_file($this->data['controller'], $fields);
	}


    function mr_edit(){
		if( ! $this->session->is_admin_logged_in() )
			redirect('admin/login','refresh');

        if(!in_array('edit', $this->permissions)) {
            show_error('You Don\'t have access to view this page', 403, 'Access Denied');
        } 

        $uri_string = $this->uri->uri_string();

        if(! strpos($uri_string, 'view')) {
            show_404();
        }

        $revised_url = strstr($uri_string, 'view');

        list($k, $v) = explode('/', $revised_url);
        $array[$k] = $v;

        $tb_alias = $this->model->get_alias();

        $alias = (!empty($tb_alias)) ? $tb_alias : '';
        $table = $this->model->get_table();
        $p_key = $this->model->get_pkey();

        if(!isset($array['view'])) {
			show_404();
		}

		$temp = [];
		foreach($array as $i=>$value) {
			if($i == 'view') {
				$i = $p_key;
				$this->data[$i] = $id = (int) $array['view'];
			}
			$tempKey = (empty($alias)) ? "$table.$i" : "$alias.$i";
			$temp[$tempKey] = $value;
		}

		$this->data['info'] = $this->model->get_collection(FALSE, $temp);

		if(! count($this->data['info']) ){ show_404(); }

		$this->data['plugins'] = ['select2'];
		array_unshift($this->scripts, 'form-submit.js','asm_info.js');
        $this->data['js'] = $this->scripts;
        
        $this->data['section_title'] = 'Edit '. ucfirst($this->data['module']);
        $this->set_view($this->data, 'template/components/container/display', '_admin');
	}

    
}