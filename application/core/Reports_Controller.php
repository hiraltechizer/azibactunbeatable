<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); 
abstract class Reports_Controller extends Generic_Controller{
	private $scripts;
    private $controller; 
    private $module; 
    private $columns;
    private $role;


    function __construct($module, $controller, $model_name, $columns = [], $scripts = []){
//echo 'ss11';die;
        //$curnt_url = current_url();

        $role   =    $this->session->get_field_from_session('role');
//echo $role;
        if(empty($role)) {
            $role =    $this->session->get_field_from_session('role', '_user');
        }
 //echo $role;die;
        if($role == 'SA' || $role == 'A') {
            if( ! $this->session->is_admin_logged_in() ){
                redirect('admin/login','refresh');
            }
        } else {
            if($role == 'HO' || $role == 'MR' || $role == 'RSM' || $role == 'ZSM' || $role == 'ASM'){

                if( ! $this->session->user_logged_in() ){
                        redirect('user','refresh');
                 }
            
            } else {
                redirect('user','refresh');
            }
        }

        parent::__construct();
        $this->load->model($model_name, 'model');

		$admin_config = $this->model->get_records([], 'config', ['config_type', 'config_value']);
		
		$config = [];
		foreach($admin_config as $record){
			$config[$record->config_type] = $record->config_value;
		}

		$this->scripts = $scripts;
		$this->controller = $controller;
		$this->module = $module;
		$this->columns = $columns;

		$this->data['settings'] = $config; 
        $this->role = $this->session->get_field_from_session('role','user');

        if(empty($this->role)) {
            $this->role = $this->session->get_field_from_session('role');
        }
        
        $this->data['role'] = $this->role;
		$this->set_defaults();

        $userId = $this->session->get_field_from_session('user_id');
        
         #check account is blocked or not
        $adminInfo = $this->model->get_records(['user_id' => $userId], 'admin',[], '', 1);
        if(!empty($adminInfo)) {
            if($adminInfo[0]->lock_flag == 1) {
                #redirect to login page
                $session_key = config_item('session_data_key');
                $sessionData = array('user_id'=>'', 'user_name'=>'', 'role'=>'');
                
                unset($_SESSION['attempt']);
                $this->session->unset_userdata($session_key, $sessionData);
                redirect('admin/login','refresh');
                exit();
            } 
        }
         $this->data['imagename'] = base_url().'/assets/images/white-user.png';
    }

    protected function set_defaults($defaults = []) {
        $this->set_default_data($defaults, $this->module, $this->controller);            
    }

    function index(){
		if( ! $this->session->is_logged_in() ){
			show_error("Forbidden", 403);
		}

		$sfilters = array();

        $offset = (int) $this->input->post('page');
        // print_r(print_r($_POST)); die();
        // $keywords = !empty($this->input->post('keywords'))?$this->input->post('keywords'):'';

        $post_array = $this->input->post();
        unset($post_array['page']);
        unset($post_array['search']);       
		
        if($this->data['controller'] == 'reports/live_report' || $this->data['controller'] == 'reports/rxn_update') {
            $users_id = $this->session->get_field_from_session('user_id', 'user');
            $role =  $this->session->get_field_from_session('role','user');
            
            if($role == 'MR'){
               $sfilters = ['pre.users_id' => $users_id];
            }elseif($role == 'ASM' || $role == 'RSM'){
           
            $userInfo = $this->model->get_role_wise_city($users_id,strtolower($role));
           
            if($userInfo){
                $sfilters =  ['c.city_id' => $userInfo[0]['city_id']];
            }else{
                $sfilters =  array();
            }
        }elseif($role == 'ZSM'){
            $userInfo = $this->model->get_zsm_wise_city($users_id,strtolower($role));
            
            if($userInfo){
                $sfilters =  ['c.city_id' => $userInfo[0]['city_id']];
            }else{
                $sfilters =  array();
            }
          }
    }

		$this->data['collection'] = $this->model->get_collection($count = FALSE, $sfilters, $post_array, $this->perPage, $offset);
        $totalRec = $this->model->get_collection($count = TRUE, $sfilters, $post_array);
         $this->data['totalRec'] =   $totalRec;

        //print_r($totalRec);
       //echo $this->db->last_query();die; 
        $this->paginate($this->data['controller'], $totalRec, 4);
		$this->data['plugins'] = ['paginate','fancybox'];
        
        /* columns for list */
        $table_columns = $this->columns;
        $this->data['all_action'] = FALSE;

        $this->set_defaults([
            'listing_url'=> $this->controller . '/index', 
            'download_url'=> $this->controller . '/download' ,
            'module_title'=> $this->module
        ]);
        $this->set_view_columns($table_columns);
        /* END columns */
        $records_view = $this->data['controller'].'/records';
        
        $role = $this->session->get_field_from_session('role');

        $this->data['permissions'] = ['download'];
        
        $template = ( in_array($role, ['SA', 'A'])) ? '_admin' : '_user';

        $filter_columns = $this->model->get_filters();
        
        $this->data['show_filters'] = TRUE;  
        $this->data['date_filters'] = TRUE;

        $this->set_view_columns($table_columns, [], $filter_columns);

        $this->data['js'] = $this->scripts;
      
        if($this->input->post('search') == TRUE) {
			$this->load->view($records_view, $this->data);
        } else {
            $this->data['records_view'] = $records_view;
			$this->set_view($this->data, 'template/components/container/lists', $template);
		}
	}

	function download(){

		if( ! $this->session->is_logged_in() )
            show_error("Forbidden", 403);
            

        $get_request = $this->input->get();
        // print_r($get_request); die();
        $new_request = array_filter($get_request, function($v) { return $v !== ''; } ); 
        $sfilters = [];
        if($this->data['controller'] == 'reports/live_report' || $this->data['controller'] == 'reports/rxn_update') {
            $users_id = $this->session->get_field_from_session('user_id', 'user');
            $role =  $this->session->get_field_from_session('role','user');
            if($role == 'MR'){
               $sfilters = ['pre.users_id' => $users_id];
            }elseif($role == 'ASM' || $role == 'RSM'){
                $userInfo = $this->model->get_role_wise_city($users_id,strtolower($role));
                if($userInfo){
                    $sfilters =  ['c.city_id' => $userInfo[0]['city_id']];
                }else{
                    $sfilters =  array();
                }
            }elseif($role == 'ZSM'){
                $userInfo = $this->model->get_zsm_wise_city($users_id,strtolower($role));
                // print_r($userInfo);exit;
                if($userInfo){
                    $sfilters =  ['c.city_id' => $userInfo[0]['city_id']];
                }else{
                    $sfilters =  array();
                }
              }
        }
		$data = $this->model->get_collection($count = FALSE, $sfilters, $new_request);
		$fields = $this->model->_format_data_to_export($data);

		$this->download_file($this->data['controller'], $fields);
	}
}