<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); 
abstract class Login_Controller extends Generic_Controller{
	private $scripts;
    private $controller; 
    private $module; 
    private $roles;

    function __construct($module, $controller, $model_name, $roles = [], $scripts = []){
        parent::__construct();
        $this->load->model($model_name, 'model');

		$admin_config = $this->model->get_records([], 'config', ['config_type', 'config_value']);
		
		$config = [];
		foreach($admin_config as $record){
			$config[$record->config_type] = $record->config_value;
		}

		$this->scripts = $scripts;
		$this->controller = $controller;
        $this->module = $module;
        $this->roles = $roles;

        $this->data['settings'] = $config; 
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;

    }

    function index() {
        $status = (in_array('SA', $this->roles)) ? $this->session->is_admin_logged_in() : $this->session->user_logged_in();

        if( $status ){

			redirect('dashboard/'. $this->module,'refresh');	
		}

        
		$this->set_view($this->data, 'login',  '_login');
    }

    function submit(){
		if(! $this->input->post()){ show_404();	}

		/* if(isset($_POST['g-recaptcha-response'])) { 			
			$captcha = $this->input->post('g-recaptcha-response');
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . RE_CAPTCHA_SECRET_KEY . "&response=" . $captcha); 			
			$responseKeys = json_decode($response, true);
			
			if (intval($responseKeys["success"]) == 1) {	 */	
				$status = $this->model->authenticate();
				$role = $this->session->get_field_from_session('role');

				if(empty($role)) {
					$role = $this->session->get_field_from_session('role', 'user');
				}
				$this->data["errr_msg"] = "";
				$curnt_url = current_url();
				
				if($status){
					if(($role == 'MR') || ($role == 'ASM') || ($role == 'RSM') || ($role == 'ZSM')) {
						// echo "test";exit;
					//    redirect('dashboard/user/popupmodel','refresh');
					redirect('dashboard/'. $this->module,'refresh');	
					}else{
						
                       redirect('dashboard/'. $this->module,'refresh');	
					}
					
				}

				//echo $curnt_url;die;
				$login_attamp = 0;
				$username = $this->input->post('username');
				$password = $this->input->post('password');
 
				//echo $username;die;
				if(strpos($curnt_url, 'admin') !== false) {
					#get no of times attamp occure
					$rsRecords = $this->model->get_records(['username' => $username, 'is_deleted' => '0'], 'admin', ['attempt']);
					
					if(!empty($rsRecords)) {
						$attempt = $rsRecords[0]->attempt;
					} else {
						$attempt = 0;
					}
					//$attempt = $this->session->userdata('attempt');
					$attempt++;
			   		
			   		//echo $attempt;
			   		#update attampt into db
			   		$ret = $this->model->_update(['username' => $username],["attempt"=>$attempt], "admin");
			   		
			   		#get login attampt
			   		$rsattemptRecords = $this->model->get_records(['username' => $username, 'is_deleted' => '0'], 'admin', ['attempt']);

			   		if(!empty($rsattemptRecords)) {
			   			$login_attamp =	$rsattemptRecords[0]->attempt;
			   		}
				} else {
					#get no of times attamp occure
					$rsuserRecords = $this->model->get_records(['users_username' => $username,'is_deleted' => '0', 'users_type' => array('HO','MR','ASM','RSM','ZSM')], 'manpower', ['attempt']);
					
					if(!empty($rsuserRecords)) {
						$attempt = $rsuserRecords[0]->attempt;
					} else {
						$attempt = 0;
					}
					$attempt++;

					$ret = $this->model->_update(['users_username' => $username],["attempt"=>$attempt], "manpower");
					
					
					#get login attampt
					$rsattemptInfo = $this->model->get_records(['users_username' => $username,'is_deleted' => '0'], 'manpower', ['attempt']);
  		

			   		if(!empty($rsattemptInfo)) {
			   			$login_attamp =	$rsattemptInfo[0]->attempt;
			   		}

				}   
			   //echo "Attempt : ".$attempt;die; 
			    $errors = validation_errors();
			    //$curnt_url = current_url();
			    if ($login_attamp >= 3) {
			        $this->data["errr_msg"] = "Your account is locked";

			        if(strpos($curnt_url, 'admin') !== false) {
			        	$this->model->_update(["lock_flag"=> '0','username' => $username],["lock_flag"=>1], "admin");
			        } else {
			        	
			        	//if($hoflag) {
			        		$this->model->_update(["lock_flag"=> '0','users_username' => $username],["lock_flag"=>1], "manpower");
			        //	}
						// else {
			        	// 	$this->model->_update(["lock_flag"=> '0','users_username' => $username, 'users_password' => $password],["lock_flag"=>1], "dietitian_users");
			        	// }
			        }
			    }

		    	if(empty($errors)) {
		    		
		    		if(strpos($curnt_url, 'admin') !== false) {
		    			$adminrecord = $this->model->get_collection(FALSE, ['username' => $username, 'admin.is_deleted' => 0, 'admin.is_active' => 1]);

			    		if(!empty($adminrecord)) {

			    			if($adminrecord[0]['lock_flag'] == '1') {
			    				$this->data['error_msg'] = 'Your account is locked.';
			    			}
			    		} 
		    		} else {
		    			$usersrecord = $this->model->get_records(['users_username' => $username, 'users_password' => $password,'is_deleted' => 0]);

			    		if(!empty($usersrecord)) {

			    			if($usersrecord[0]->lock_flag == '1') {
			    				$this->data['error_msg'] = 'Your account is locked.';
			    			}
			    		}
			    		#	$this->data['error_msg'] = 'Username and Password don\'t match';
			    		
		    		}

		    		$this->data['error_msg'] = 'Username and Password don\'t match';
   				} 
				
			/* } else {
				$this->data['error_msg'] = 'Incorrect Captcha';
			}
		}	 */
		
		$this->set_view($this->data, 'login',  '_login');
	}
}