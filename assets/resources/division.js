var city_id = $('#city_id').val();

(function ($) {

    var load = function (elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {

        $('#' + elem).select2({
            placeholder: "Select " + placeholder_txt,
            allowClear: true,
            ajax: {
                url: baseUrl + controller + '/options',
                dataType: 'json',
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                        token: $('.save-form').find('input[name=token]').val()
                    }

                    if (data.id) {
                        query['id'] = data.id;
                    }
                   // alert(query);
                    console.log(query);
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                },
                cache: true
            }
        });

        if (change_trigger) {
            if (attempt == 'reset') {
                $('#' + elem).val(null).trigger('change');
            } else {
                $('#' + elem).trigger('change');
            }
        }
    }
    load('division_id', 'Division', 'master/division', true);
    load('city_id', 'City Name', 'geography/city', true);
    load('speciality_id', 'speciality', 'master/speciality', true);

    load('mr_id', 'Mr Name', 'manpower/mr', true,city_id);

    $("#city_id").on("change", function(){
        $('#territory_code').html("");
        var city_id = $(this).val();
        var div_data = '';
        //alert(zone_id);
        $.ajax({
            type: "POST",
            url: baseUrl + "quarter_target/getcityBycode",
            data: {'city_id': city_id,'token': $('meta[name=csrf-token]').attr("content")},
            dataType: "json",
            success: function (data) {
                $('#territory_code').empty();
                $.each(data, function (i, obj){
                    div_data += "<option value=" + obj.territory_code + ">" + obj.territory_code + "</option>";
                });
                $('#territory_code').append(div_data);

            }
        });
    });

    $("mr_id").on("change", function(){
        alert('test');
        $('#mr_id').html("");
        var city_id = $(this).val();
        var div_data = '';
        //alert(zone_id);
        $.ajax({
            type: "POST",
            url: baseUrl + "manpower/mr/options",
            // url: baseUrl + "quarter_wise_mr_wise_target/getmrBycity",
            data: {'city_id': city_id,'token': $('meta[name=csrf-token]').attr("content"),
            page: params.page || 1,},
            dataType: "json",
            success: function (data) {
                $('#mr_id').empty();
                $.each(data, function (i, obj){
                    
                    div_data += "<option value=" + obj.users_name + ">" + obj.users_name + "</option>";
                });
                $('#mr_id').append(div_data);

            }
        });
    });

    
})(jQuery);