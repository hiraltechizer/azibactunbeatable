(function($) {

    var b_load_cnt = 0; //sku: 0 counter stands for when no change event is triggered
    var s_load_cnt = 0;

	var load = function(elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {

		//alert(controller);
		var refurl = baseUrl + controller;
		/*if(controller == 'manpower/mr') {
			refurl =  baseUrl + controller + '/Mroptions';
		} else {
			refurl =  baseUrl + controller + '/options';
		}*/

		$('#' + elem).select2({
			placeholder: 'Select ' + placeholder_txt,
			allowClear: true,
			ajax: {
				url: refurl,
				dataType: 'json',
				type: 'POST',
				data: function(params) {
					var query = {
						search: params.term,
						page: params.page || 1,
						token: $('.save-form')
							.find('input[name=token]')
							.val()
					};

					if (data.id) {
						query['id'] = data.id;
					}
					console.log(query);
					// Query parameters will be ?search=[term]&page=[page]
					return query;
					// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
				},
				cache: true
			}
		});

		if (change_trigger) {
			if (attempt == 'reset') {
				$('#' + elem)
					.val(null)
					.trigger('change');
			} else {
				$('#' + elem).trigger('change');
			}
		}
	};

	load('brand_id', 'Brand', 'master/brand/options', true);
})(jQuery);