(function ($) {
    var division = $('#division_id');
    var strength = $('#strength_id');

    /*counter to reset select2 values on a change event of the element*/
    //var b_load_cnt = 0; //Region: 0 counter stands for when no change event is triggered
    var s_load_cnt = 0; //sku: 0 counter stands for when no change event is triggered
    var m_load_cnt = 0;

    var load = function (elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {

        var rurl;
        rurl = baseUrl + controller + '/options';

        $('#' + elem).select2({
            placeholder: "Select " + placeholder_txt,
            allowClear: true,
            ajax: {
                url: rurl,
                dataType: 'json',
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                        token: $('.save-form').find('input[name=token]').val()
                    }

                    if (data.id) {
                        query['id'] = data.id;
                    }
                   // alert(query);
                    console.log(query);
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                },
                cache: true
            }
        });

        if (change_trigger) {
            if (attempt == 'reset') {
                $('#' + elem).val(null).trigger('change');
            } else {
                $('#' + elem).trigger('change');
            }
        }
    }

    load('division_id', 'Division', 'master/division', true);
    load('strength_id', 'Strength', 'master/strength', true);

})(jQuery);