(function($) {
	var reporting_mgr = $('#reporting-mgr');

	/*counter to reset select2 values on a change event of the element*/
	var z_count = 0; //counter for zone change event
	var r_count = 0; // counter for region change event
	var c_load_cnt = 0;
	var z_load_cnt = 0;
	var r_load_cnt = 0;

	var load = function(elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {
		$('#' + elem).select2({
			placeholder: 'Select ' + placeholder_txt,
			allowClear: true,
			ajax: {
				url: baseUrl + controller + '/options',
				dataType: 'json',
				type: 'POST',
				data: function(params) {
					var query = {
						search: params.term,
						page: params.page || 1,
						token: $('.save-form')
							.find('input[name=token]')
							.val()
					};

					if (data.id) {
						query['id'] = data.id;
					}
					//console.log(query);
					// Query parameters will be ?search=[term]&page=[page]
					return query;
					// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
				},
				cache: true
			}
		});

		if (change_trigger) {
			if (attempt == 'reset') {
				$('#' + elem)
					.val(null)
					.trigger('change');
			} else {
				$('#' + elem).trigger('change');
			}
		}
	};

	var load_fforce_head = function(role) {
		reporting_mgr.select2({
			placeholder: 'Select ' + role,
			allowClear: true,
			ajax: {
				url: baseUrl + 'manpower/mr/options',
				dataType: 'JSON',
				type: 'POST',
				data: function(params) {
					var query = {
						search: params.term,
						page: params.page || 1,
						role: role,
						token: $('.save-form')
							.find('input[name=token]')
							.val()
					};
					// Query parameters will be ?search=[term]&page=[page]
					return query;
					// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
				},
				cache: true
			}
		});

		reporting_mgr.trigger('change');
	};

	if ($('#reporting-mgr').length) {
		load_fforce_head('MR');
	}
})(jQuery);
