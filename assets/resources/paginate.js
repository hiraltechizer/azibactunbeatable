(function($) {
	//var controller = $('.custom-table').attr('id');
//alert(585);
	var filters = $('.filters');
	var colspan = $('#tbody')
		.find('tr')
		.last()
		.find('td')
		.attr('colspan');
	var zone_id = $("#zone_id");
	var region_id = $("#region_id");
	var area_id = $("#area_id");
	var city_id = $("#city_id");
	var brand_id = $("#brand_id");
	var year = $("#year");
	var month_id = $("#month_id");
	var test_type_id = $("#test_type_id");
	var sub_comment_id = $('#sub_comment_id');


	$('#collectinfo').on('submit', function(e) {
		e.preventDefault();
	});

	var data = {};

	/*search data as per keyword input & other filters*/
	function search(count, pagecount, controller) {

		data.search = 1;
		data.token = $('input[name="token"]').val();
		
		filters.each(function(i, value) {
			$this = $(this);
			if ($this.val()) {
				data[$this.attr('name')] = $this.val();
			}
		});

		data.page = pagecount;

		$('#checkall').prop('checked', false);

		$.post(baseUrl + listing_url + '/' + count, data, function(data) {

			$('#tbody').html(data);
			window.scrollTo(0, 0);
		});
	}

	/*pagination bullets clicked call the search function*/
	$(document).on('click', '.page-bullets', function(e) {
		var count = $(this).attr('data-count');
		var pagecount = $(this).attr('data-page');

		search(count, pagecount, controller);
	});

	/*search on keyword updatation while typing into the omni search box*/
	filters.on('keyup', function(e) {
		data.page = 0;
		data.search = 1;
		data.token = $('input[name="token"]').val();

		if(zone_id.length) {
			//alert(zone_id.val());
			data['z|zone_id'] = zone_id.val();
		}
		 if(region_id.length) {
			data['r|region_id'] = region_id.val();
		}
		if(area_id.length) {
			data['a|area_id'] = area_id.val();
		}
		if(city_id.length) {
			data['c|city_id'] = city_id.val();
		} 
		if(brand_id.length) {
			data['b|brand_id'] = brand_id.val();
		} 
		if(sub_comment_id.length) {
			data['ct|sub_comment_id'] = sub_comment_id.val();	
		}

		if(month_id.length) {
			data['mth|month_id'] = month_id.val();	
		}

		if(test_type_id.length) {
			data['pt|test_type_id'] = test_type_id.val();	
		}

		if(year.length) {
			data['yr|year'] = year.val();	
		}

		var query_str = '?xcel=1';

		filters.each(function(i, value) {
			var query_download = '';
			var valu = '';
			$this = $(this);
			data[$this.attr('name')] = $this.val();
			//query_str += '&' + $this.attr('name') + '=' + $this.val();

			if($this.attr('name') == 'zone_id') {
				 query_download = 'z|zone_id';
				 valu = $this.val();
			}else if($this.attr('name') == 'region_id') {
				query_download = 'r|region_id';
				 if($this.val() == null){
					valu = '';
				 }else{
					valu = $this.val();
				 }
			}else if($this.attr('name') == 'area_id') {
				query_download = 'a|area_id';
				valu = $this.val();
			}else if($this.attr('name') == 'city_id') {
				query_download = 'c|city_id';
				valu = $this.val();
			}else if($this.attr('name') == 'brand_id') {
				query_download = 'b|brand_id';
				valu = $this.val();
			}else if($this.attr('name') == 'sub_comment_id') {
				query_download = 'ct|sub_comment_id';
				valu = $this.val();
			}else if($this.attr('name') == 'month_id') {
				query_download = 'mth|month_id';
				valu = $this.val();
			}else if($this.attr('name') == 'year') {
				query_download = 'yr|year';
				valu = $this.val();
			}else if($this.attr('name') == 'test_type_id') {
				query_download = 'pt|test_type_id';
				valu = $this.val();
			}else{
				query_download = $this.attr('name');
				valu = $this.val();
			}

			//query_download = $this.attr('name');
			query_str += '&' + query_download + '=' + valu;
		});

		var download_url_with_params = baseUrl + download_url + query_str;
		$('a#export').attr('href', download_url_with_params);
		var postdata = data
		//var url = baseUrl + controller + '/lists/';
		var url = baseUrl + listing_url;

		$.ajax({
			url: url,
			data: postdata,
			type: 'POST',
			beforeSend: function(xhr, opts) {
				$('#tbody').html('<tr><td colspan="' + colspan + '"><center><b>Searching....</b></center></td><tr>');
			},
			success: function(data) {
				if ($('#checkall').length) {
					$('#checkall').prop('checked', false);
				}  
				
				 if(controller == 'reports/patient_journey_report' || controller == 'reports/target_vs_acheived_report' || controller == 'reports/geography_wise_doctor_wise_patient_report' || controller == 'reports/geography_wise_patient_count_report' || controller == 'reports/dropout_patients_report' || controller == 'reports/doctor_wise_diet_chart_share_report') {

						getTotalCount(postdata); 	
				}
				$('#tbody').html(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//alert('Oops ! there might be some problem, please try after some time');
			}
		});
	});

	/*Initialize from_date & to_date with condition(i.e to_date must be greater than from_date)*/
	var from_date = $('#from_date');
	var to_date = $('#to_date');
	if (from_date.length) {
		// condition block will work only if from & to date filters are available

		from_date.datepicker({
			dateFormat: 'yy-mm-dd',
			autoclose: true,
			onSelect: function(date) {
				var pattern = /(\d{2})\-(\d{2})\-(\d{4})/;
				var dt = new Date(date.replace(pattern, '$3-$2-$1'));

				var selectedDate = new Date(dt);
				//var msecsInADay = 86400000;
				var msecsInADay = 0;
				var endDate = new Date(selectedDate.getTime() + msecsInADay);
				to_date.datepicker('option', 'minDate', endDate);
				$(this).change();
			}
		});

		to_date.datepicker({
			dateFormat: 'yy-mm-dd',
			autoclose: true,
			onSelect: function() {
				$(this).change();
			}
		});

		$('#from_date, #to_date, #call_day').on('change', function() {
			console.log('inn');
			filters.eq(0).trigger('keyup');
		});
	}

	$('#call_day').on('change', function() {
		console.log('inn');
		//alert("hiii");
		filters.eq(0).trigger('keyup');
	});
	
	$('#zone_id').on('change', function() {
		// var zone_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#region_id').on('change', function() {
		//var region_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#area_id').on('change', function() {
		//var area_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#city_id').on('change', function() {
		//var city_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#brand_id').on('change', function() {
		//var city_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#sub_comment_id').on('change', function() {
		//var city_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#month_id').on('change', function() {
		//var city_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#year').on('change', function() {
		//var city_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});

	$('#test_type_id').on('change', function() {
		//var city_id = $(this).val();
		filters.eq(0).trigger('keyup');
	});


	$('#quarterid').on('change', function() {
		//alert(44);
			filters.eq(0).trigger('keyup');
			assignquartersubinfo($('#quarterid').val());
	});

	if($('#quarterid').val() != '' && $('#quarterid').val() != null && typeof $('#quarterid').val() != 'undefined') {
		filters.eq(0).trigger('keyup');
	}

	$('#quartersubid').on('change', function() {
			filters.eq(0).trigger('keyup');
	});

	if($('#quartersubid').val() != '' && $('#quartersubid').val() != null && typeof $('#quartersubid').val() != 'undefined') {
		filters.eq(0).trigger('keyup');
	}

	$('#brandid').on('change', function() {
			filters.eq(0).trigger('keyup');
	});

	if($('#brandid').val() != '' && $('#brandid').val() != null && typeof $('#brandid').val() != 'undefined') {
		filters.eq(0).trigger('keyup');
	}

	/*delete records - select for delete*/
	$(document).on('click', '#checkall', function() {
		if ($(this).prop('checked')) $("input[type='checkbox']").prop('checked', true);
		else $("input[type='checkbox']").prop('checked', false);
	});

	$('.filters').keypress(function(e) {
		var code = e.keyCode || e.which;
		/* if (code != 8) {
			return false;
		} */
		if (code == 13) {
			e.preventDefault();
		}
	});

	/*delete selected records*/
	$('.deleteAction').on('click', function(e) {
		e.preventDefault();
		$this = $(this).closest('form');

		if (!$('input[name="ids[]"]:checked').length) {
			swal('No records selected!');
			return;
		}

		swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			showCancelButton: true,
			confirmButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			showLoaderOnConfirm: true
		}).then(function(result) {
			if (result.value) {
				var data = $this.serialize();

				$.ajax({
					url: $this.attr('action'),
					data: data,
					type: 'POST',
					dataType: 'JSON',
					beforeSend: function(xhr, opts) {},
					success: function(data) {
						filters.trigger('keyup');
						if (data.msg) {
							swal('Deleted!', data.msg);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {}
				});
			}
		});
	});

	/*Upload CSV file for importing data*/
	$('#uploadForm').on('submit', function(e) {
		e.preventDefault();

		var $this = jQuery(this);
		var formUrl = $this.attr('action');

		$('[name=csvfile]')
			.parent()
			.removeClass('error')
			.siblings('label.error')
			.remove();

		if (window.FormData != 'undefined') {
			var formData = new FormData($this[0]);

			jQuery.ajax({
				url: formUrl,
				type: 'POST',
				data: formData,
				dataType: 'JSON',
				mimeType: 'multipart/form-data',
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function(xhr, opts) {
					$('#preloader').show();
				},
				success: function(data, textStatus, jqXHR) {
					if (data.newrows) {
						str = data.newrows;
						if (data.csv_error) {
							str += '\n';
							str += data.csv_error;
						}
						$('#show_msg')
							.html(str)
							.show()
							.siblings()
							.hide();
						$('#upload-btn').hide();

						//$("input[name=keywords]").trigger('keyup');
						filters.trigger('keyup');

						$this[0].reset();
					} else {
						if (data.csv_error) {
							alert(data.csv_error);
						}

						if (data.errors) {
							$('[name=csvfile]')
								.parent()
								.addClass('error')
								.siblings('label.error')
								.remove()
								.end()
								.after(data.errors.csvfile);
						}
					}
					$('#preloader').hide();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert('Error: There might be issues while uploading, please check the fields!');
					$('#preloader').hide();
				}
			});
		}
	});

	$('.reset-upload, #import').on('click', function() {
		
		var uploadForm = $(this)
			.parent()
			.parent('form');
		uploadForm[0].reset();

		uploadForm.find('label.error').remove();
		uploadForm.find('div.form-line').removeClass('error');

		$('#show_msg')
			.html('')
			.hide()
			.siblings()
			.show();
		$('#upload-btn').show();
		$('#show_msg_1')
			.html('')
			.hide()
			.siblings()
			.show();
		$('#upload-btn_1').show();
	});

	$(document).ready(function() {
		// keywords.trigger('keyup');
		if ($('.double-scroll').length) {
			$('.double-scroll').doubleScroll();
		}
	});

	$('a.asc_desc').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);

		data.order_by = $this.attr('data-column');
		data.asc_or_desc = $this.attr('data-order');

		$('a.asc_desc')
			.find('i')
			.remove();

		if ($this.attr('data-order') == 'asc') {
			$this.append('<i class="fa fa-caret-up"></i>');
			$this.attr('data-order', 'desc');
		} else {
			$this.append('<i class="fa fa-caret-down"></i>');
			$this.attr('data-order', 'asc');
		}

		filters.trigger('keyup');
	});


	$('.pshare_link').on('click', function(e) {
		e.preventDefault();
		$this = $(this).closest('form');

		if (!$('input[name="ids[]"]:checked').length) {
			swal('No records selected!');
			return;
		}

		if ($('input[name="ids[]"]:checked').length > 0) {
			var pdata = $('input[name="ids[]"]:checked').serialize();
			$.ajax({
				url: baseUrl + 'patient/sharelink',
				data: {data:pdata, token: $('#frm_delete')
                            .find('input[name=token]')
                            .val()},
				type: 'POST',
				dataType: 'JSON',
				beforeSend: function(xhr, opts) {
					 $('#preloader').show();
				},
				success: function(data) {
					filters.trigger('keyup');
					 $('#preloader').hide();
					if (data.status) {
						swal('success!', data.message);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {}
			});
		}
	});

	$('.pdownload_link').on('click', function(e) {
		e.preventDefault();
		$this = $(this).closest('form');

		if (!$('input[name="ids[]"]:checked').length) {
			swal('No records selected!');
			return;
		}

		$("#frm_delete").attr('action', 'downloadlink');
		$('#frm_delete').submit();
		/*if ($('input[name="ids[]"]:checked').length > 0) {
			var pdata = $('input[name="ids[]"]:checked').serialize();
			$.ajax({
				url: baseUrl + 'patient/downloadlink',
				data: {data:pdata, token: $('#frm_delete')
                            .find('input[name=token]')
                            .val()},
				type: 'POST',
				dataType: 'JSON',
				beforeSend: function(xhr, opts) {},
				success: function(data) {
					filters.trigger('keyup');
					if (data.status) {
						swal('success!', data.message);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {}
			});
		} */
	});

	$('.cshare_link').on('click', function(e) {
		e.preventDefault();
		$this = $(this).closest('form');

		if (!$('input[name="ids[]"]:checked').length) {
			swal('No records selected!');
			return;
		}

		if ($('input[name="ids[]"]:checked').length > 0) {
			var pdata = $('input[name="ids[]"]:checked').serialize();
			$.ajax({
				url: baseUrl + 'doctor/sharelink',
				data: {data:pdata, token: $('#frm_delete')
                            .find('input[name=token]')
                            .val()},
				type: 'POST',
				dataType: 'JSON',
				beforeSend: function(xhr, opts) {
					 $('#preloader').show();
				},
				success: function(data) {
					filters.trigger('keyup');
					 $('#preloader').hide();
					if (data.status) {
						swal('success!', data.message);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {}
			});
		}
	});

	$('.cdownload_link').on('click', function(e) {
		e.preventDefault();
		$this = $(this).closest('form');

		if (!$('input[name="ids[]"]:checked').length) {
			swal('No records selected!');
			return;
		}

		$("#frm_delete").attr('action', 'downloadlink');
		$('#frm_delete').submit();
	});


})(jQuery);

function assignquartersubinfo(QID) {
	//alert(QID);
	$.ajax({
            url:  baseUrl + controller + '/quartersuboptions/',
            data: {
                quarter_id: QID,
                token:$('input[name="token"]').val()
            },
            type: "POST",
            dataType: "html",
            beforeSend: function(xhr, opts) {
                //$('#preloader').show();
            },
            success: function (data) {
                $('#quartersubid').html(data);
                //$('#preloader').hide();
            },
            error: function (xhr, status) {
                alert("Sorry, there was a problem!");
            },
            complete: function (xhr, status) {
                $('#preloader').hide();
            }
        });
}

function getTotalCount(postdata) {
	postdata.token = $('input[name="token"]').val();

	$.ajax({
            url:  baseUrl + controller + '/getTotalcount',
            data: postdata,
            type: "POST",
            dataType: "JSON",                    
            beforeSend: function(xhr, opts) {
                //$('#preloader').show();
            },
            success: function (data) {
               	$("#totalreportcnt").html('Total Records: '+data.total_record);
            },
            error: function (xhr, status) {
                alert("Sorry, there was a problem!");
            },
            complete: function (xhr, status) {
                $('#preloader').hide();
            }
        });
}