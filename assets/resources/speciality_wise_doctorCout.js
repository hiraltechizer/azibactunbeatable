

$(document).ready(function(){
  $(".speciality_data_formatter").on("change", function(){
    var status = '';
    var result_arr;
   
    var id = $(this).attr('data-id');
    month = $('[data-id="'+id+'"][data-type="month"]').val();
    year = $('[data-id="'+id+'"][data-type="year"]').val();
    // alert(month); 
    // alert(year); 
    var data_arr = {};
    data_arr.id = id;

   if(month!='' && month!='undefined'){ 
     data_arr.month = month;    
  }

  if(year!='' && year!='undefined'){
     data_arr.year = year;    
   }

    data_arr.token = $('meta[name=csrf-token]').attr("content");
    // alert(data_arr);  
    $.ajax({
      url:  baseUrl + controller + '/filters_info',
      data: data_arr,
      type: 'POST',
      dataType:'JSON',
      beforeSend: function (xhr, opts) {
        $('#preloader').show();
    },
      success: function(data, textStatus, jqXHR) {
        if(data.success == "0") {

           result_arr = data.result[id];

            if(id =="speciality_wise_patient_count") {
              specialitywisedoctorcount.series[0].update({data : data.result.doctor_count}, false);
              specialitywisedoctorcount.xAxis[0].categories = data.result.speciality_name;
              specialitywisedoctorcount.redraw();
            }
        }
        $('#preloader').hide();
      }
      
    });
  });

  var specialitywisedoctorcount = Highcharts.chart('chart_specialitywisedoctorcount', {
    chart: {
        type: 'column',
        backgroundColor: '#fff'
    },
    title: {
        text: 'Speciality Wise Doctor Registered',
        style: {  
          color: '#fff'
        }
    },
    xAxis: {
      title: {
        text: "Speciality Name",
        style: {
          fontWeight: 'bold'
        }
      },
      tickWidth: 0,
      labels: {
        style: {
            color: '#333',
          },
          rotation: 45
        },
      categories: []
    },
    yAxis: {
      gridLineWidth: .5,
      gridLineDashStyle: 'dash',
      gridLineColor: 'black',
      title: {
            text: 'Doctor Count',
            style: {
              color: '#333',
              fontWeight: 'bold'
            }
        },
        labels: {
          formatter: function() {
            return Highcharts.numberFormat(this.value, 0, '', ',');
          },
          style: {
              color: '#333',
            }
          }
        },
    legend: {
        enabled: false,
    },
    credits: {
        enabled: false
    },
    plotOptions: {
      column: {
        colorByPoint: true,
        dataLabels: {
            enabled: true
        },
        
        borderRadius: 0,
        pointPadding: -0.1,
        groupPadding: 0.1
        } 
    },
    series: [{
        name: 'Speciality Wise Doctor Registered',
        data: []
    }]
  });
  
  $(".speciality_data_formatter").trigger('change');
});