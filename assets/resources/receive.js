(function($){
    $('#addModal').on('show.bs.modal', function(e) {
        var mobile = $(e.relatedTarget).data('mobile');
        var history = $(e.relatedTarget).data('history');
        var call_parent_id = $(e.relatedTarget).data('parent');
        $(e.currentTarget).find('input[name="mobile"]').val(mobile);
        $(e.currentTarget).find('input[name="call_parent_id"]').val(call_parent_id);
    });

    $('#editModal').on('show.bs.modal', function(e) {
        var mobile = $(e.relatedTarget).data('mobile'); 
        var history = $(e.relatedTarget).data('history');       
        $(e.currentTarget).find('input[name="mobile"]').val(mobile);
        $(e.currentTarget).find('input[name="call_history_id"]').val(history);
    });

    function ajaxCall(url, data, callback) {
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            success: function(data) {
                return callback(data);            
            },
            error: function(jqXHR, textStatus, errorThrown){
                return callback(errorThrown);
            }
        })
    }

})(jQuery);