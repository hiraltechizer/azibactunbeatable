(function ($) {

    //var brand = $('#brand_id');
    var quarter_id = $('#quarter_id');
    var quarter_sub_id = $('#quarter_sub_id');

    var s_load_cnt = 0; //sku: 0 counter stands for when no change event is triggered
    var m_load_cnt = 0;

    var load = function (elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {

        var rurl;
        if(elem == 'quarter_sub_id') {
            rurl = baseUrl  + 'speak_altan_speak/suboptions1';
        } else {
            rurl = baseUrl + controller + '/options';
        }

        $('#' + elem).select2({
            placeholder: "Select " + placeholder_txt,
            allowClear: true,
            ajax: {
                url: rurl,
                dataType: 'json',
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                        token: $('.save-form').find('input[name=token]').val()
                    }

                    if (data.id) {
                        query['id'] = data.id;
                    }
                    //console.log(query);
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                },
                cache: true
            }
        });

        if (change_trigger) {
            if (attempt == 'reset') {
                $('#' + elem).val(null).trigger('change');
            } else {
                $('#' + elem).trigger('change');
            }
        }
    }

    if (quarter_sub_id.length) {

        quarter_id.on('change', function () {
            m_load_cnt++;
            var m_attempt_to = (m_load_cnt > 1) ? 'reset' : 'load';

            data = {
                id: $(this).val()
            }
            load('quarter_sub_id', 'Month', 'master/quarter', true, m_attempt_to, data);
        });
    }
    load('quarter_id', 'Quarter', 'master/quarter', true);
   // load('brand_id', 'Brand', 'master/brand', true);
})(jQuery);