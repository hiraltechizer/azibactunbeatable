(function ($) {
    
    $("#zone_id").on("change", function(){
        $('#region_id').html("");
        var zone_id = $(this).val();
        var div_data = '<option value="">Select Region</option>';
        $.ajax({
            type: "POST",
            url: baseUrl + "reports/target_vs_acheived_report/getzoneByregion",
            data: {'zone_id': zone_id,'token': $('meta[name=csrf-token]').attr("content")},
            dataType: "json",
            success: function (data) {
                $('#region_id').empty();
                $.each(data, function (i, obj){
                    div_data += "<option value=" + obj.region_id + ">" + obj.region_name + "</option>";
                });
                $('#region_id').append(div_data);
            }
        });
    });


    $("#szone_id").on("change", function(){
        $('#sregion_id').html("");
        var zone_id = $(this).val();
        var div_data = '<option value="">Select Region</option>';
        $.ajax({
            type: "POST",
            url: baseUrl + "reports/target_vs_acheived_report/getzoneByregion",
            data: {'zone_id': zone_id,'token': $('meta[name=csrf-token]').attr("content")},
            dataType: "json",
            success: function (data) {
                $('#sregion_id').empty();
                $.each(data, function (i, obj){
                    div_data += "<option value=" + obj.region_id + ">" + obj.region_name + "</option>";
                });
                $('#sregion_id').append(div_data);
            }
        });
    });
    
    })(jQuery);