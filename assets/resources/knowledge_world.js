(function ($) {

    var load = function (elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {

        var action =    '';
        if(elem == 'quarter_id') {
            action = '/activeoptions';
        } else {
            action = '/options';
        }

        $('#' + elem).select2({
            placeholder: "Select " + placeholder_txt,
            allowClear: true,
            ajax: {
                url: baseUrl + controller + action,
                dataType: 'json',
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1,
                        token: $('.save-form').find('input[name=token]').val()
                    }

                    if (data.id) {
                        query['id'] = data.id;
                    }
                   // alert(query);
                    console.log(query);
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                },
                cache: true
            }
        });

        if (change_trigger) {
            if (attempt == 'reset') {
                $('#' + elem).val(null).trigger('change');
            } else {
                $('#' + elem).trigger('change');
            }
        }
    }
    // load('quarter_id', 'Quarter', 'master/quarter', true);
    load('document_type_id', 'Document Type', 'master/document_type', true);
})(jQuery);