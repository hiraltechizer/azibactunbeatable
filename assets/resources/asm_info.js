
(function ($) {

$("#users_area_id").on("change", function(){
    var users_area_id = $(this).val();
    var div_data = '<option value="">Select Asm</option>';
    $.ajax({
        type: "POST",
        url:  baseUrl + 'manpower/mr/getareaByasm',
        data: {'users_area_id': users_area_id,'token': $('meta[name=csrf-token]').attr("content")},
        dataType: "json",
        success: function (data) {
            $('#users_parent_id').empty(); 
            $.each(data, function (i, obj){
                div_data += "<option value=" + obj.users_id + ">" + obj.users_name + "</option>";
            });
            $('#users_parent_id').append(div_data);
        }
    });
});

})(jQuery);
