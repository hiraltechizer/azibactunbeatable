

$(document).ready(function(){
  $(".brand_data_formatter").on("change", function(){
    var status = '';
    var result_arr;
   
    var id = $(this).attr('data-id');
    month = $('[data-id="'+id+'"][data-type="month"]').val();
    year = $('[data-id="'+id+'"][data-type="year"]').val();
    zone_id = $('[data-id="'+id+'"][data-type="zone_id"]').val();
    region_id = $('[data-id="'+id+'"][data-type="region_id"]').val();
    //alert(month); 
    //alert(year); 
    var data_arr = {};
    data_arr.id = id;

   if(month!='' && month!='undefined'){ 
     data_arr.month = month;    
  }
  if(year!='' && year!='undefined'){
     data_arr.year = year;    
   }
   if(zone_id!='' && zone_id!='undefined'){
    data_arr.zone_id = zone_id;    
  }
  if(region_id!='' && region_id!='undefined'){
    data_arr.region_id = region_id;    
  }


    data_arr.token = $('meta[name=csrf-token]').attr("content");
    // alert(data_arr);  
    $.ajax({
      url:  baseUrl + controller + '/filters_info',
      data: data_arr,
      type: 'POST',
      dataType:'JSON',
      beforeSend: function (xhr, opts) {
        $('#preloader').show();
    },
      success: function(data, textStatus, jqXHR) {
        if(data.success == "0") {

           result_arr = data.result[id];

            if(id =="brand_wise_patient_count") {
              brandwisepatientcount.series[0].update({data : data.result.patient_count}, false);
              brandwisepatientcount.xAxis[0].categories = data.result.brand_name;
              brandwisepatientcount.redraw();
            }
        }
        $('#preloader').hide();
      }
    });
  });

  var brandwisepatientcount = Highcharts.chart('chart_brandwisepatientcount', {
    chart: {
        type: 'column',
        backgroundColor: '#fff'
    },
    title: {
        text: 'Brand wise Patient Recruited count',
        style: {  
          color: '#fff'
        }
    },
    xAxis: {
      title: {
        text: "Brand Name",
        style: {
          fontWeight: 'bold'
        }
      },
      tickWidth: 0,
      labels: {
        style: {
            color: '#333',
          },
          rotation: 45
        },
      categories: []
    },
    yAxis: {
      gridLineWidth: .5,
      gridLineDashStyle: 'dash',
      gridLineColor: 'black',
      title: {
            text: 'Patient Count',
            style: {
              color: '#333',
              fontWeight: 'bold'
            }
        },
        labels: {
          formatter: function() {
            return Highcharts.numberFormat(this.value, 0, '', ',');
          },
          style: {
              color: '#333',
            }
          }
        },
    legend: {
        enabled: false,
    },
    credits: {
        enabled: false
    },
    plotOptions: {
      column: {
        colorByPoint: true,
        dataLabels: {
            enabled: true
        },
        
        borderRadius: 0,
        pointPadding: -0.1,
        groupPadding: 0.1
        } 
    },
    series: [{
        name: 'Brand wise Patient Recruited count',
        data: []
    }]
  });
  
  $(".brand_data_formatter").trigger('change');
});