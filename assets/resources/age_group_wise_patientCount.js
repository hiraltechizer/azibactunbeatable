$(document).ready(function(){
    $(".age_data_formatter").on("change", function(){
        var status          = '';
        var result_arr;
         var id = $(this).attr('data-id');


    month = $('[data-id="'+id+'"][data-type="month"]').val();
    year = $('[data-id="'+id+'"][data-type="year"]').val();
    // alert(month); 
    // alert(year); 
    var data_arr = {};
    data_arr.id = id;

   if(month!='' && month!='undefined'){ 
     data_arr.month = month;    
  }

  if(year!='' && year!='undefined'){
     data_arr.year = year;    
   }

    data_arr.token = $('meta[name=csrf-token]').attr("content");
    // alert(data_arr);  
    $.ajax({
      url:  baseUrl + controller + '/filters_info',
      data: data_arr,
      type: 'POST',
      dataType:'JSON',
      beforeSend: function (xhr, opts) {
        $('#preloader').show();
    },
      success: function(data, textStatus, jqXHR) {
        if(data.success == "0") {

           result_arr = data.result[id];
          //  console.log(data.result);
            if(id =="age_wise_patient_count") {
              agewisedoctorcount.series[0].update({data : data.result}, true);
             // agewisedoctorcount.xAxis[0].categories = data.result.age_group;
              agewisedoctorcount.redraw();
            }
        }
        $('#preloader').hide();
      }
      
    });
  });

  var agewisedoctorcount = Highcharts.chart('chart_agewisedoctorcount', {
    chart: {
        type: 'pie',
        backgroundColor: '#fff'
    },
    title: {
        text: 'Age Group Wise Patient Recruited',
        style: {  
          color: '#fff'
        }
    },
    tooltip: {
      pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>Patient Count: {point.y}'
    },
    accessibility: {
      point: {
          valueSuffix: ''
      }
  },
    legend: {
        enabled: false,
    },
    credits: {
        enabled: false
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: true,
          format: '<b>Age Group {point.name}</b>:<br>{point.percentage:.1f} %<br>Patient Count: {point.y}',
        }
      }
    },
    series: [{
        name: 'Age Group Wise Patient Recruited',
        data: []
       
    }]
  });
  
  $(".age_data_formatter").trigger('change');
});