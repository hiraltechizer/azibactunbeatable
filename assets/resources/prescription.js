function autocomplete(inp, arr) {
    // var users_id = $('#users_id');
    // alert(users_id);
    var currentFocus;

    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
    
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
    
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
       
        this.parentNode.appendChild(a);
       
        for (i = 0; i < arr.length; i++) {
        
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
           
            b = document.createElement("DIV");
           
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
           
            b.innerHTML += "<input id='doctor_id' name ='doctor_id' type='hidden' value='" + arr[i] + "'>";
           
            b.addEventListener("click", function(e) {
             
                inp.value = this.getElementsByTagName("input")[0].value;
              
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
   
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
  }
  
  /*An array containing all the country names in the world:*/
//  

 
$('#doctor_name').keyup(function(){
  var doctor_name = $(this).val();
 
    $('#doctor_id').empty(); 
    $('#sbu_code').val("");
    $('#speciality_id').empty(); 
    
    var div_data = '';

  $.ajax({
    type: "POST",
    url: baseUrl + "prescription/get_doctor",
    data: {'search' : doctor_name,'token': $('meta[name=csrf-token]').attr("content")},
    dataType: "json",
    success: function (data) {
        console.log(data.results); 
       if(data.error){
         $('#doctor_id').hide();
        }else{
        $('#doctor_id').show();
    
        $('#doctor_id').empty(); 
        $('#sbu_code').val("");
        $('#speciality_id').empty(); 

        var docData = data.results;

        $.each(docData, function (i, obj){

           div_data += "<li class='list-gpfrm-list' data-doc_id='"+obj.id +"' data-doctorname='"+ obj.text+"' data-sbucode='"+ obj.sub_code+"' data-specialityid='"+ obj.speciality_id+"' >" +obj.text +"</li>";
        });

        $('#doctor_id').append(div_data);

      }  

    }
});

//fill the input

$(document).on('click', '.list-gpfrm-list', function(e){
  e.preventDefault();
  $('#doctor_id').hide();
  var doc_id = $(this).data('doc_id');
  var doctorname = $(this).data('doctorname');
  var sbucode = $(this).data('sbucode');
  var specialityid = $(this).data('specialityid');

  $('#doctor_name').val(doctorname);
  $('#sbu_code').val(sbucode);

  var div_spc = '';
  $.ajax({
      type: "POST",
      url:  baseUrl + 'prescription/getspecialiy',
      data: {'speciality_id': specialityid,'token': $('meta[name=csrf-token]').attr("content")},
      dataType: "json",
      success: function (data) {
          $('#speciality_id').empty(); 
          $.each(data, function (i, obj){
           
              div_spc += "<option value=" + obj.speciality_id + ">" + obj.speciality + "</option>";
          });
          $('#speciality_id').append(div_spc);
      }
  });

});
 });


