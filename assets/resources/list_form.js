(function ($) {

    $('.save-form').on('submit', function (e) {
        e.preventDefault();

        var formObj = $(this);
        var formUrl = formObj.attr('action');
        var users_type = formObj.data('usertype');

        $.each(formObj.find('input, select, textarea'), function (i, field) {
            var elem = $('[name="' + field.name + '"]').parent();

            if (elem.hasClass('select2-hidden-accessible')) {
                elem.next().removeClass('error').siblings('label').remove()
            } else {
                elem.removeClass('error')
                    .next('label.error').remove();
            }
        });

        if (window.FormData != 'undefined') {
            var formData = new FormData(formObj[0]);
           
            $.ajax({
                url: formUrl,
                type: 'POST',
                data: formData,
                dataType: 'JSON',
                mimeType: 'multipart/form-data',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function (xhr, opts) {
                    $('#preloader').show();
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.status == true) {
                        getmarketingInfo(data.brand_id,data.quarter_id,data.quarter_sub_id);
                    } else {
                        if (data.errors) {
                            //alert(44444);
                            var i= 1;
                            $.each(data.errors, function (key, val) {
                                var elem = $('[name="' + key + '"]', formObj).parent();

                                if (elem.hasClass('select2-hidden-accessible')) {
                                    elem.next().addClass('error').siblings('label').remove().end().after(val);
                                } else {
                                    elem.removeClass('error')
                                        .next('label.error').remove()
                                        .end()
                                        .addClass('error').after(val);
                                }

                                if(i == 1) {
                                    $('[name="' + key + '"]', formObj).focus();    
                                }
                                
                                i++;
                            });
                            $('#preloader').hide();
                            //$('.form-group.error').eq(0).addClass('focused');
                             //$('.form-group.error').eq(0).addClass('focused');
                        }

                        if (data.message) {
                           // lert(455555554);
                            swal({
                                title: 'Error!',
                                text: data.message
                            });
                        }
                        $('#preloader').hide();
                    }
                    
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Problems while saving data!')
                    $('#preloader').hide();
                }
            });
        }
    });
})(jQuery)