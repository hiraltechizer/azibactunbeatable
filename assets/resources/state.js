(function($) {
	var city = $('#city_id');
	var state = $('#state_id');
	
	/*counter to reset select2 values on a change event of the element*/
	var c_load_cnt = 0; //Region: 0 counter stands for when no change event is triggered
	var s_load_cnt = 0; //Area: 0 counter stands for when no change event is triggered
	
	var load = function(elem, placeholder_txt, controller, change_trigger = false, attempt, data = '') {
		$('#' + elem).select2({
			placeholder: 'Select ' + placeholder_txt,
			allowClear: true,
			ajax: {
				url: baseUrl + controller + '/options',
				dataType: 'json',
				type: 'POST',
				data: function(params) {
					var query = {
						search: params.term,
						page: params.page || 1,
						token: $('.save-form')
							.find('input[name=token]')
							.val()
					};

					if (data.id) {
						query['id'] = data.id;
					}
					console.log(query);
					// Query parameters will be ?search=[term]&page=[page]
					return query;
					// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
				},
				cache: true
			}
		});

		if (change_trigger) {
			if (attempt == 'reset') {
				$('#' + elem)
					.val(null)
					.trigger('change');
			} else {
				$('#' + elem).trigger('change');
			}
		}
	};

	if (city.length) {
		state.on('change', function() {
			s_load_cnt++;
			var s_attempt_to = s_load_cnt > 1 ? 'reset' : 'load';
			data = {
				id: $(this).val()
			};
			console.log(s_attempt_to, data);
			load('city_id', 'City', 'master/city', true,s_attempt_to, data);
		});
	}

	
	load('state_id', 'State', 'master/state', true);
})(jQuery);