(function ($) {
    
    $("#zone_id").on("change", function(){
        $('#region_id').html("");
        var zone_id = $(this).val();
        var div_data = '<option value="">Select Region</option>';
        var div_area = '<option value="">Area</option>';
        //alert(zone_id);
        $.ajax({
            type: "POST",
            url: baseUrl + "reports/target_vs_acheived_report/getzoneByregion",
            data: {'zone_id': zone_id,'token': $('meta[name=csrf-token]').attr("content")},
            dataType: "json",
            success: function (data) {
                $('#area_id').empty(); 
                $('#region_id').empty();
                
                $.each(data, function (i, obj){
                    div_data += "<option value=" + obj.region_id + ">" + obj.region_name + "</option>";
                });
                $('#region_id').append(div_data);
                $('#area_id').append(div_area);

            }
        });
    });
    
    $("#region_id").on("change", function(){
        var region_id = $(this).val();
        var div_data = '<option value="">Select Area</option>';
        $.ajax({
            type: "POST",
            url:  baseUrl + 'reports/target_vs_acheived_report/getreagionByarea',
            data: {'region_id': region_id,'token': $('meta[name=csrf-token]').attr("content")},
            dataType: "json",
            success: function (data) {
                $('#area_id').empty(); 
                $.each(data, function (i, obj){
                    div_data += "<option value=" + obj.area_id + ">" + obj.area_name + "</option>";
                });
                $('#area_id').append(div_data);
            }
        });
    });
    
    
    $("#area_id").on("change", function(){
        var area_id = $(this).val();
        var div_data = '<option value="">Select City</option>';
        $.ajax({
            type: "POST",
            url:  baseUrl + 'reports/target_vs_acheived_report/getareaBycity',
            data: {'area_id': area_id,'token': $('meta[name=csrf-token]').attr("content")},
            dataType: "json",
            success: function (data) {
                $('#city_id').empty(); 
                $.each(data, function (i, obj){
                    div_data += "<option value=" + obj.city_id + ">" + obj.city_name + "</option>";
                });
                $('#city_id').append(div_data);
            }
        });
    });
    


   
    
    })(jQuery);