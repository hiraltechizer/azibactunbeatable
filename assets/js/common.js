function EvaluationClickCount()
{
    $.ajax({
            url:  baseUrl + controller + '/evaluationclickcount/',
            data: {
                token:$('meta[name=csrf-token]').attr('content')
            },
            type: "POST",
            dataType: 'JSON',
            beforeSend: function(xhr, opts) {
                //$('#preloader').show();
            },
            success: function (data) {
               //alert(data.redirect_url);
                window.location.href = data.redirect_url;
            },
            error: function (xhr, status) {
                alert("Sorry, there was a problem!");
            },
            complete: function (xhr, status) {
             //   $('#preloader').hide();
            }
        });

}